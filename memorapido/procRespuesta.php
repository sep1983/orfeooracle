<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$ruta_raiz = "..";

if ($_SESSION["krd"]) {
    $krd = $_SESSION["krd"];
}

if (!$_SESSION['dependencia']){
    include "$ruta_raiz/rec_session.php";
}

if($_SESSION["draftMemo"]){
 $idDraft = $_SESSION["draftMemo"];
}else{
  die;
}

/* * *********************************************
  // INICIO DE VARIABLES Y CONSTANTES
 * ********************************************* */
// envio de respuesta via email
// Obtiene los datos de la respuesta rapida.
$ruta_libs = $ruta_raiz . "/memorapido/";
define('ADODB_ASSOC_CASE', 0);
define('SMARTY_DIR', $ruta_libs . 'libs/');
include $ruta_raiz . "/config.php";

//formato para fecha en documentos
function fechaFormateada($FechaStamp) {
    $ano = date('Y', $FechaStamp); //<-- Ano
    $mes = date('m', $FechaStamp); //<-- n�mero de mes (01-31)
    $dia = date('d', $FechaStamp); //<-- D�a del mes (1-31)
    $dialetra = date('w', $FechaStamp); //D�a de la semana(0-7)

    switch ($dialetra) {
        case 0 :
            $dialetra = "domingo";
            break;
        case 1 :
            $dialetra = "lunes";
            break;
        case 2 :
            $dialetra = "martes";
            break;
        case 3 :
            $dialetra = "mi&eacute;rcoles";
            break;
        case 4 :
            $dialetra = "jueves";
            break;
        case 5 :
            $dialetra = "viernes";
            break;
        case 6 :
            $dialetra = "S&aacute;bado";
            break;
    }

    switch ($mes) {
        case '01' :
            $mesletra = "Enero";
            break;
        case '02' :
            $mesletra = "Febrero";
            break;
        case '03' :
            $mesletra = "Marzo";
            break;
        case '04' :
            $mesletra = "Abril";
            break;
        case '05' :
            $mesletra = "Mayo";
            break;
        case '06' :
            $mesletra = "Junio";
            break;
        case '07' :
            $mesletra = "Julio";
            break;
        case '08' :
            $mesletra = "Agosto";
            break;
        case '09' :
            $mesletra = "Septiembre";
            break;
        case '10' :
            $mesletra = "Octubre";
            break;
        case '11' :
            $mesletra = "Noviembre";
            break;
        case '12' :
            $mesletra = "Diciembre";
            break;
    }

    return htmlentities("$dialetra, $dia de $mesletra de $ano");
}

$encabe = session_name() . "=" . session_id() . "&krd=$krd";
$pos = strpos('salidaRespuesta', $_SERVER['HTTP_REFERER']);

if ($pos !== false) {
    header("Location: index.php?$encabe");
}

require_once($ruta_raiz . "/config.php");
require_once($ruta_raiz . "/include/db/ConnectionHandler.php");
include_once($ruta_raiz . "/class_control/AplIntegrada.php");
include_once($ruta_raiz . "/class_control/anexo.php");
include_once($ruta_raiz . "/class_control/anex_tipo.php");
include_once($ruta_raiz . "/include/tx/Tx.php");
include_once($ruta_raiz . "/include/tx/Radicacion.php");
include_once($ruta_raiz . "/class_control/Municipio.php");
include_once($ruta_raiz . "/include/PHPMailer_v5.1/class.phpmailer.php");
require_once($ruta_raiz . "/tcpdf/config/lang/eng.php");
require_once($ruta_raiz . "/tcpdf/tcpdf.php");

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//$db->conn->debug = true;

/* * *********************************************
  // BORRAR PLANTILLA
 * ********************************************* */
// envio de respuesta via email
if ($_POST['planaborrar'] && $_POST['delPlanVal'] ) {
    $planti = implode(",", $_POST['planaborrar']);
    if (!empty($planti)) {
        $sql21 = "DELETE FROM SGD_PLAN_PLANTILLAS WHERE ID IN ($planti)";
        $reg = $db->conn->Execute($sql21);
        header("Location: index.php?$encabe");
        die();
    }
}

/***********************************************
// INSERTAR PLANTILLA
***********************************************/

    if ($_POST['nuevaplan'] == 1) {
        $nombre    = $_POST["nombre"];
        $nivel     = $_POST["nivel"];
        $contenido = $_POST["contplant"];
        $fecha     = $db->conn->OffsetDate(0,$db->conn->sysTimeStamp);
        $dep_code    = $_SESSION["depecodi"];
                $usu_code    = $_SESSION["codusuario"];
                $nextval     = $db->nextId("sec_plantillas");

        $sql21 = "
                            INSERT INTO
                            SGD_PLAN_PLANTILLAS(
                              id,
                              plan_plantilla,
                              plan_nombre,
                              plan_fecha,
                              depe_codi,
                              usua_codi,
                              plan_tipo)
                            VALUES (
                              $nextval,
                              '$contenido',
                              '$nombre',
                               $fecha,
                              '$dep_code',
                              '$usu_code',
                              '$nivel')";
        $rsg   = $db->conn->Execute($sql21);
        header("Location: index.php?$encabe");
        die();
    }

$hist    = new Historico($db);
$Tx      = new Tx($db);
$anex    = new Anexo($db);
$anexTip = new Anex_tipo($db);
$mail    = new PHPMailer(true);

/* * *********************************************
  // CREACION DEL RADICADO RESPUESTA
 * ********************************************* */
//Para crear el numero de radicado se realiza el siguiente procedimiento

//Traer el contenido aprobado por el borrador
$sqlDraft = "SELECT
               DRAFT_CONTENT
             FROM
               SGD_DRAFT
             WHERE
               ID = $idDraft";

$exteDraft = $db->conn->Execute($sqlDraft);

if(!$exteDraft->EOF){
  $archtml = utf8_encode(html_entity_decode($exteDraft->fields["DRAFT_CONTENT"]));
}else{
  die;
}


$sql = "SELECT DEPE_RAD_TP3 as SECUENCIA FROM DEPENDENCIA WHERE DEPE_CODI = " . $_SESSION['dependencia'];
$rsRAD = $db->conn->Execute($sql);
$tpDepeRad = $rsRAD->fields["SECUENCIA"];
$rad = new Radicacion($db);
$rad->radiTipoDeri = 0; // ok ????
$rad->radiCuentai = 'null';  // ok, Cuenta Interna, Oficio, Referencia
$rad->mrecCodi = 12; // medio de correspondencia, 3 Mail
$rad->radiFechOfic = date('d/m/Y'); // igual fecha radicado;
$rad->radiPais = $pais; //OK, codigo pais
$rad->descAnex = '.'; //OK anexos
$rad->raAsun = $_POST['asunto']; // ok asunto
$rad->radiDepeActu = $_SESSION['dependencia']; // ok dependencia actual quien radica
$rad->radiUsuaActu = $_SESSION['codusuario'];  // ok usuario actual responsable
$rad->radiDepeRadi = $_SESSION['dependencia']; //ok dependencia que radica
$rad->usuaCodi = $_SESSION['codusuario'];  // ok usuario actual responsable
$rad->dependencia = $_SESSION['dependencia']; //ok dependencia que radica
$rad->trteCodi = 0;                        //ok, tipo de codigo de remitente
$rad->tdocCodi = 0;                        //ok, tipo documental
$rad->tdidCodi = 0;                        //ok, ????
$rad->carpCodi = 1;                        //ok, carpeta entradas
$rad->carPer = 0;                        //ok, carpeta personal
$rad->radiPath = 'null';
$rad->sgd_apli_codi = '0';
$rad->usuaDoc = $_SESSION['usua_doc'];
$nurad = $rad->newRadicado(3, $tpDepeRad);
if ($nurad == "-1") {
    header("Location: salidaRespuesta.php?$encabe&error=1");
    die;
}

//Inserta el evento del radicado nuevo.
$hist->insertarHistorico(array($nurad), $_SESSION['dependencia'], $_SESSION['codusuario'], $_SESSION['dependencia'], $_SESSION['codusuario'], "Memorando Electronico.", 2);

//CREADO RADICADO... PROCEDEMOS CON SGD_DIR_DRECCIONES
//Obtenemos data del remitente.. nombre, direccion, etc
$sql = "SELECT M.ID_CONT, M.ID_PAIS, M.DPTO_CODI, M.MUNI_CODI, M.MUNI_NOMB, T.DPTO_NOMB, P.NOMBRE_PAIS, 
            D.DEP_DIRECCION, U.USUA_NOMB, U.USUA_DOC, U.USUA_EXT, U.USUA_EMAIL, U.DEPE_CODI, U.USUA_CODI 
        FROM USUARIO U 
	    INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI
	    INNER JOIN MUNICIPIO M ON M.ID_CONT=D.ID_CONT AND M.ID_PAIS=D.ID_PAIS AND M.DPTO_CODI=D.DPTO_CODI AND M.MUNI_CODI=D.MUNI_CODI 
	    INNER JOIN DEPARTAMENTO T ON  M.ID_CONT=T.ID_CONT AND M.ID_PAIS=T.ID_PAIS AND M.DPTO_CODI=T.DPTO_CODI 
	    INNER JOIN SGD_DEF_PAISES P ON  M.ID_CONT=P.ID_CONT AND M.ID_PAIS=P.ID_PAIS 
	WHERE 
            U.USUA_CODI= 1 and U.DEPE_CODI='" . $_POST['codDepeDest'] . "'";

$dataRemitente = $db->conn->GetRow($sql);

$nextval = $db->nextId("sec_dir_direcciones");
$isql = "insert into SGD_DIR_DRECCIONES(
            SGD_TRD_CODIGO, SGD_DIR_NOMREMDES, SGD_DIR_DOC,
            DPTO_CODI, MUNI_CODI, ID_PAIS, 
            ID_CONT, SGD_DOC_FUN, SGD_OEM_CODIGO,
            SGD_CIU_CODIGO, SGD_ESP_CODI, RADI_NUME_RADI,
            SGD_SEC_CODIGO, SGD_DIR_DIRECCION, SGD_DIR_TELEFONO,
            SGD_DIR_MAIL, SGD_DIR_TIPO, SGD_DIR_CODIGO, SGD_DIR_NOMBRE)
        values( 1, '" . $dataRemitente['USUA_NOMB'] . "', NULL," .
        $dataRemitente['DPTO_CODI'] . ", " . $dataRemitente['MUNI_CODI'] . ", " . $dataRemitente['ID_PAIS'] . ", " .
        $dataRemitente['ID_CONT'] . ", '" . $dataRemitente['USUA_DOC'] . "', NULL,
            NULL, NULL, $nurad,
            0, '" . $dataRemitente['DEP_DIRECCION'] . "' , '" . $dataRemitente['USUA_EXT'] . "', '" .
        $dataRemitente['USUA_EMAIL'] . "', 1, $nextval, '" . $dataRemitente['USUA_NOMB'] . "')";

$rsTercero = $db->conn->Execute($isql);

//CREADO RADICADO... PROCEDEMOS CON SGD_DIR_DRECCIONES
//Obtenemos data del destinatario.. nombre, direccion, etc
$sql = "SELECT M.ID_CONT, M.ID_PAIS, M.DPTO_CODI, M.MUNI_CODI, M.MUNI_NOMB, T.DPTO_NOMB, P.NOMBRE_PAIS, 
            D.DEP_DIRECCION, U.USUA_NOMB, U.USUA_DOC, U.USUA_EXT, U.USUA_EMAIL, U.DEPE_CODI, U.USUA_CODI 
        FROM USUARIO U 
	    INNER JOIN DEPENDENCIA D ON U.DEPE_CODI=D.DEPE_CODI
	    INNER JOIN MUNICIPIO M ON M.ID_CONT=D.ID_CONT AND M.ID_PAIS=D.ID_PAIS AND M.DPTO_CODI=D.DPTO_CODI AND M.MUNI_CODI=D.MUNI_CODI 
	    INNER JOIN DEPARTAMENTO T ON  M.ID_CONT=T.ID_CONT AND M.ID_PAIS=T.ID_PAIS AND M.DPTO_CODI=T.DPTO_CODI 
	    INNER JOIN SGD_DEF_PAISES P ON  M.ID_CONT=P.ID_CONT AND M.ID_PAIS=P.ID_PAIS 
	WHERE 
            U.USUA_CODI= 1 and U.DEPE_CODI='" . $_POST['codDepeDest'] . "'";

$dataTercero = $db->conn->GetRow($sql);


/* * *********************************************
  // VALIDAR DATOS ADJUNTOS
 * ********************************************* */
if (!empty($_FILES["archs"]["name"][0])) {
    //Arreglo para Validar la extension
    $sql = " select anex_tipo_codi as CODIGO, anex_tipo_ext as EXT, anex_tipo_mime as MIME from anexos_tipo";
    $rsExt = $db->conn->Execute($sql);
    while (!$rsExt->EOF) {
        $codigo = $rsExt->fields["CODIGO"];
        $ext = $rsExt->fields["EXT"];
        $mime1 = $rsExt->fields["MIME"];
        $mime2 = explode(",", $mime1);
        //arreglo para validar la extension
        $exts["." . $ext] = array('codigo' => $codigo,
            'mime' => $mime2);
        $rsExt->MoveNext();
    }
    $adjuntos = $carpetaBodega . '/' . date('Y') . "/" . $_SESSION['dependencia'] . "/docs/";
    //Si no existe la carpeta se crea.
    if (!is_dir($ruta_raiz . "/" . $adjuntos)) {
        $rs = mkdir($adjuntos, 0700);
        if (empty($rs)) {
            $errores .= empty($errores) ? "&error=2" : '-2';
        }
    }
    $i = 0;
    $anexo = new Anexo($db);
    $tamano = 0;
    $tamanoMax = 7 * 1024 * 1024; // 7 megabytes
    //Validaciones y envio para grabar archivos
    foreach ($_FILES["archs"]["name"] as $key => $name) {
        $nombre = strtolower(trim($_FILES["archs"]["name"][$key]));
        $type = trim($_FILES["archs"]["type"][$key]);
        $tamano = trim($_FILES["archs"]["size"][$key]);
        $tmporal = trim($_FILES["archs"]["tmp_name"][$key]);
        $error = trim($_FILES["archs"]["error"][$key]);
        $ext = strrchr($nombre, '.');
        if (is_array($exts[$ext])) {
            foreach ($exts[$ext]['mime'] as $value) {
                if (eregi($type, $value)) {
                    $bandera = true;
                    if ($tamano < $tamanoMax) {
                        //grabar el registro en la base de datos
                        if (strlen($str) > 90) {
                            $nombre = substr($nombre, '-90:');
                        }
                        $anexo->anex_radi_nume = $nurad;
                        $anexo->usuaCodi = $_SESSION['codusuario'];
                        $anexo->depe_codi = $_SESSION['dependencia'];
                        $anexo->anex_solo_lect = "'S'";
                        $anexo->anex_tamano = $tamano;
                        $anexo->anex_creador = "'$usualog'";
                        $anexo->anex_desc = "Adjunto: " . $nombre;
                        $anexo->anex_nomb_archivo = $nombre;
                        $auxnumero = (int) $anexo->obtenerMaximoNumeroAnexo($nurad);
                        $anexoCodigo = $anexo->anexarFilaRadicado($auxnumero);
                        $nomFinal = $anexo->get_anex_nomb_archivo();
                        //Guardar el archivo en la carpteta ya creada
                        $Grabar_path = $adjuntos . $nomFinal;
                        if (move_uploaded_file($tmporal, $ruta_raiz . "/" . $Grabar_path)) {
                            //si existen adjuntos los agregamos para enviarlos por correo
                            $mail->AddAttachment($ruta_raiz . "/" . $Grabar_path, $nombre);
                        } else {
                            $errores .= empty($errores) ? "&error=6" : '-6';
                        }
                    } else {
                        $errores .= empty($errores) ? "&error=5" : '-5';
                    }
                }
            }
            if (empty($bandera)) {
                $errores .= empty($errores) ? "&error=4" : '-4';
            }
        } else {
            $errores .= empty($errores) ? "&error=3" : '-3';
        }
        $contador ++;
    }
}

$fecha = fechaFormateada($fecha1);

/***********************************************
// AGREGAR LOS ADJUNTOS AL RADICADO
***********************************************/

    $tipo           = 7;  #pdf
    $tamano         = 400;
    $auxsololect    = 'S';
    $radicado_rem   = 1;
    $descr          = 'Documento del memorando electronico';
    $fechrd         = $ddate.$mdate.$fechproc4;
    $ruta           = $nurad . ".pdf";
    $sqlFechaHoy      = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
    $coddepe        = $_SESSION["dependencia"] * 1 ;
    $usua           = $_SESSION["krd"];


    $auxnumero    = $anex->obtenerMaximoNumeroAnexo($nurad);

    do{
        $auxnumero += 1;
        $codigo     = trim($nurad) . trim(str_pad($auxnumero, 5, "0", STR_PAD_LEFT));
    }while ($anex->existeAnexo($codigo));

    $isql = "INSERT INTO ANEXOS (SGD_REM_DESTINO,
                                ANEX_RADI_NUME,
                                ANEX_CODIGO,
                                ANEX_ESTADO,
                                ANEX_TIPO,
                                ANEX_TAMANO,
                                ANEX_SOLO_LECT,
                                ANEX_CREADOR,
                                ANEX_DESC,
                                ANEX_NUMERO,
                                ANEX_NOMB_ARCHIVO,
                                ANEX_BORRADO,
                                ANEX_SALIDA,
                                SGD_DIR_TIPO,
                                ANEX_DEPE_CREADOR,
                                SGD_TPR_CODIGO,
                                ANEX_FECH_ANEX,
                                SGD_APLI_CODI,
                                SGD_TRAD_CODIGO,
                                RADI_NUME_SALIDA,
                                SGD_EXP_NUMERO)
                        values ($radicado_rem,
                                $nurad,
                                '$codigo',
                                4,
                                '$tipo',
                                $tamano,
                                '$auxsololect',
                                '$usua',
                                '$descr',
                                $auxnumero,
                                '$ruta',
                                'N',
                                1,
                                $radicado_rem,
                                $coddepe,
                                NULL,
                                $sqlFechaHoy,
                                NULL,
                                1,
                                $nurad,
                                NULL)";

    $bien = $db->conn->Execute($isql);
    // Si actualizo BD correctamente
    if (!$bien) {
      $errores .= empty($errores)? "&error=7" : '-7';
    }
/* * *********************************************
  // REMPLAZAR DATOS EN EL ASUNTO
 * ********************************************* */
//REMPLAZO DE DATOS
$archtml = str_replace("F_RAD_S", date('d-m-Y'), $archtml);
$archtml = str_replace("RAD_S", $nurad, $archtml);
$archtml = str_replace("\xe2\x80\x8b", '', $archtml);

$ruta2 = "/$carpetaBodega/".date('Y') . "/" . $_SESSION['dependencia'] . "/docs/" . $nurad . ".pdf";
$ruta3 = "/".date('Y') . "/" . $_SESSION['dependencia'] . "/docs/" . $nurad . ".pdf";

//Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $this->Image('../img/banerPDF.JPG', 30, 10, 167, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        // Page number
        $txt = "<div align='center'>";
        $this->writeHTMLCell($w = 0, $h = 3, $x = '32', $y = '', $txt, $border = 0, $ln = 1, $fill = 0, $reseth = true);
    }

    function Code39($xpos, $ypos, $code = 0, $baseline = 0.6, $height = 8) {
        $wide = $baseline;
        $narrow = $baseline / 3;
        $gap = $narrow;

        $barChar['0'] = 'nnnwwnwnn';
        $barChar['1'] = 'wnnwnnnnw';
        $barChar['2'] = 'nnwwnnnnw';
        $barChar['3'] = 'wnwwnnnnn';
        $barChar['4'] = 'nnnwwnnnw';
        $barChar['5'] = 'wnnwwnnnn';
        $barChar['6'] = 'nnwwwnnnn';
        $barChar['7'] = 'nnnwnnwnw';
        $barChar['8'] = 'wnnwnnwnn';
        $barChar['9'] = 'nnwwnnwnn';
        $barChar['A'] = 'wnnnnwnnw';
        $barChar['B'] = 'nnwnnwnnw';
        $barChar['C'] = 'wnwnnwnnn';
        $barChar['D'] = 'nnnnwwnnw';
        $barChar['E'] = 'wnnnwwnnn';
        $barChar['F'] = 'nnwnwwnnn';
        $barChar['G'] = 'nnnnnwwnw';
        $barChar['H'] = 'wnnnnwwnn';
        $barChar['I'] = 'nnwnnwwnn';
        $barChar['J'] = 'nnnnwwwnn';
        $barChar['K'] = 'wnnnnnnww';
        $barChar['L'] = 'nnwnnnnww';
        $barChar['M'] = 'wnwnnnnwn';
        $barChar['N'] = 'nnnnwnnww';
        $barChar['O'] = 'wnnnwnnwn';
        $barChar['P'] = 'nnwnwnnwn';
        $barChar['Q'] = 'nnnnnnwww';
        $barChar['R'] = 'wnnnnnwwn';
        $barChar['S'] = 'nnwnnnwwn';
        $barChar['T'] = 'nnnnwnwwn';
        $barChar['U'] = 'wwnnnnnnw';
        $barChar['V'] = 'nwwnnnnnw';
        $barChar['W'] = 'wwwnnnnnn';
        $barChar['X'] = 'nwnnwnnnw';
        $barChar['Y'] = 'wwnnwnnnn';
        $barChar['Z'] = 'nwwnwnnnn';
        $barChar['-'] = 'nwnnnnwnw';
        $barChar['.'] = 'wwnnnnwnn';
        $barChar[' '] = 'nwwnnnwnn';
        $barChar['*'] = 'nwnnwnwnn';
        $barChar['$'] = 'nwnwnwnnn';
        $barChar['/'] = 'nwnwnnnwn';
        $barChar['+'] = 'nwnnnwnwn';
        $barChar['%'] = 'nnnwnwnwn';

        $this->SetFont('Arial', '', 12);
        //$this->Text($xpos, $ypos + $height + 4, "Radicado ".$code);
        $this->SetFillColor(0);

        $code = '*' . strtoupper($code) . '*';
        for ($i = 0; $i < strlen($code); $i++) {
            $char = $code{$i};
            if (!isset($barChar[$char])) {
                $this->Error('Invalid character in barcode: ' . $char);
            }
            $seq = $barChar[$char];
            for ($bar = 0; $bar < 9; $bar++) {
                if ($seq{$bar} == 'n') {
                    $lineWidth = $narrow;
                } else {
                    $lineWidth = $wide;
                }
                if ($bar % 2 == 0) {
                    $this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
                }
                $xpos += $lineWidth;
            }
            $xpos += $gap;
        }
    }

}

// create new PDF document
$pdf = new MYPDF('P', PDF_UNIT, 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($setAutor);
$pdf->SetTitle($SetTitle);
$pdf->SetSubject($SetSubject);
$pdf->SetKeywords($SetKeywords);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// output the HTML content
$pdf->Code39(85, 35, $nurad);
$pdf->writeHTML('MEMORANDO', true, false, true, false, 'C');
$pdf->writeHTML($nurad, true, false, true, false, 'C');
$pdf->writeHTML('', true, false, true, false, 'C');
$pdf->writeHTML($archtml, true, false, true, false, '');

// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($ruta_raiz . $ruta2, 'F');

if (!empty($mail->ErrorInfo)) {
    $errores .= empty($errores) ? "&error=9" : '-9';
}


//Agregar un nuevo evento en el historico para que
//muestre como contestado y no genere alarmas.
//A la respuesta se le agrega el siguiente evento
$hist->insertarHistorico(array($nurad), $_SESSION['dependencia'], $_SESSION['codusuario'], $_SESSION['dependencia'], $_SESSION['codusuario'], "Imagen asociada desde memorando electronico.", 42);


/* * ********************************************
 * REENVIO AL DESTINATARIO Y CONSECUENTE CORREO
 * ********************************************* */
/*
  $sql = "SELECT USUA_EMAIL, USUA_CODI, DEPE_CODI FROM USUARIO WHERE USUA_LOGIN = '" . $_POST['txtHiddenOR'] . "'";
  $arrEmailOr = $db->conn->GetOne($sql);
 */
$Tx->reasignar(array($nurad), $usualog, $dataTercero['DEPE_CODI'], $_SESSION['dependencia'], $dataTercero['USUA_CODI'], $_SESSION['codusuario'], 'no', "Reasignaci&oacute;n memorando electr&oacute;nico.", 9, 0);

if (!empty($dataTercero['USUA_EMAIL'])){
	$mail->AddAddress($dataTercero['USUA_EMAIL'], $dataTercero['USUA_NOMB']);

	//Si hay copias, se realiza transaccion de Informados y respectivo tr?te de envio de correo.
	if (!empty($codDepecc)) {
		$cadCC = implode(",", $codDepecc);

	  $sql = "SELECT USUA_EMAIL, USUA_CODI, DEPE_CODI FROM USUARIO WHERE USUA_CODI = 1 AND DEPE_CODI IN ($cadCC)";
		$rsEmailCC = $db->conn->Execute($sql);
		foreach ($rsEmailCC as $k => $row) {
			if (!empty($row['USUA_EMAIL'])) {
				$mail->AddCC($row['USUA_EMAIL']);
				$correocopia .= $row['USUA_EMAIL'] . ", ";
			}
			$Tx->informar(array($nurad), $usualog, $row['DEPE_CODI'], $_SESSION['dependencia'], $row['USUA_CODI'], $_SESSION['codusuario'], "Informado memorando electr�nico.");
		}
	}
	$correocopia = substr($correocopia, 0, strlen($correocopia) - 1);

	$cuerpo = " 	<br><p>El Ministerio de Transporte le notifica que se le ha asignado el Memorando Electr�nico No. $nurad, 
		por favor ingresar al SGD-ORFEO para los respectivos tramites</p>
		<br><br><center><br>
		http://gestiondocumental.mintransporte.gov.co
		<br><br><br><b>$entidad_largo</b></center><br>";
	$mail->IsSMTP();
	$mail->Host = $server_mail;
	$mail->SMTPAuth = $auth_mail;
	$mail->SMTPSecure = $tls_mail;
	$mail->Port = $port_mail;
	$mail->Username = $correo_mail;
	$mail->Password = $passwd_mail;
	$mail->SetFrom($correo_mail, $correo_mail);
	$mail->Subject = "Notificacion radicado " . $nurad . " del Ministerio de Transporte";
	$mail->AltBody = "Para ver el mensaje, por favor use un visor de E-mail compatible!";
	$mail->Body = $cuerpo;
	$mail->IsHTML(true);
	$mail->SMTPDebug = 1; // 1 = errors and messages // 2 = messages only 
	$mail->AddAttachment($ruta_raiz . $ruta2); //Aneamos pdf del radicado al correo
	if (!$mail->Send()) {
		$errores .= empty($errores) ? "&error=8" : '-8';
	}
}

$sql_sgd_renv_codigo = "SELECT SGD_RENV_CODIGO FROM SGD_RENV_REGENVIO ORDER BY SGD_RENV_CODIGO DESC ";
$rsRegenvio = $db->conn->SelectLimit($sql_sgd_renv_codigo, 2);
$nextval = $rsRegenvio->fields["SGD_RENV_CODIGO"];
$nextval++;
$fechaActual = $db->conn->OffsetDate(0, $db->conn->sysTimeStamp);
$destinatarios = "Destino:" . $dataTercero['USUA_EMAIL'] . " Copia:" . $correocopia;


$sql = "  INSERT INTO SGD_RENV_REGENVIO(
                SGD_RENV_CODIGO, SGD_FENV_CODIGO, SGD_RENV_FECH,
                RADI_NUME_SAL, SGD_RENV_DESTINO, SGD_RENV_MAIL,
                SGD_RENV_PESO, SGD_RENV_VALOR, SGD_RENV_ESTADO,
                USUA_DOC, SGD_RENV_NOMBRE, SGD_RENV_PLANILLA,
                SGD_RENV_FECH_SAL, DEPE_CODI, SGD_DIR_TIPO,
                RADI_NUME_GRUPO, SGD_RENV_DIR, SGD_RENV_CANTIDAD,
                SGD_RENV_TIPO, SGD_RENV_OBSERVA, SGD_RENV_GRUPO,
                SGD_RENV_VALORTOTAL, SGD_RENV_VALISTAMIENTO, SGD_RENV_VDESCUENTO,
                SGD_RENV_VADICIONAL, SGD_DEPE_GENERA, SGD_RENV_PAIS)
            VALUES (
                $nextval, 112, $fechaActual,
                $nurad, '$destinatarios', '$destinatarios',
                '0', '0', 1," .
        	$_SESSION["usua_doc"] . ", '" . $destinat . "', '0',
                $fechaActual, " . $_SESSION["dependencia"] . ", 1,
                $nurad, '$destinatarios', 1, 
                1, 'Envio Memorando electronico a Correo Electronico', $nurad,
                '0', '0', '0',
                '0', " . $_SESSION["dependencia"] . ", 'Colombia')";

$rsRegenvio = $db->conn->query($sql);
$db->conn->Execute("UPDATE ANEXOS SET ANEX_ESTADO=4 WHERE RADI_NUME_SALIDA=$nurad");

$sql = "UPDATE RADICADO SET RADI_PATH = '$ruta3' WHERE RADI_NUME_RADI = $nurad";
$db->conn->Execute($sql);

//Cambiamos el borrador que genero el memorando
//y lo enviamos a la carpeta de memorando para no
//usarlo mas y si conservar el historico de las
//accioens realizadas con el memorando.

$sqlU70 = "
  UPDATE
    SGD_DRAFT
  SET
    DRAFT_READ = 1,
    DRAFT_FOLDER = 1007
  WHERE
   ID = $idDraft";

$oks = $db->conn->query($sqlU70);

$sql36 = "
  UPDATE
    SGD_DRAFT_ASSIGNMENT
  SET
    DRAFT_ASSIG_READ = 1,
    DRAFT_ASSIG_STATE = 2,
    DRAFT_ASSIG_FOLDER = 1007
  WHERE
    DRAFT_ID = $idDraft";

$resU36 = $db->conn->query($sql36);

header("Location: salidaRespuesta.php?$encabe&nurad=$nurad" . $errores);
?>
