<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

$ruta_raiz = "..";
if (!$_SESSION['dependencia']){
    include "$ruta_raiz/rec_session.php";
}

define('SMARTY_DIR', './libs/');
require_once $ruta_raiz . '/config.php';
require (SMARTY_DIR . 'Smarty.class.php');
require_once 'libs/htmlpurifier/HTMLPurifier.auto.php';

//formato para fecha en documentos
function fechaFormateada($FechaStamp) {
    $ano = date('Y', $FechaStamp); //<-- Ano
    $mes = date('m', $FechaStamp); //<-- número de mes (01-31)
    $dia = date('d', $FechaStamp); //<-- Día del mes (1-31)
    $dialetra = date('w', $FechaStamp); //Día de la semana(0-7)

    switch ($dialetra) {
        case 0 :
            $dialetra = "domingo";
            break;
        case 1 :
            $dialetra = "lunes";
            break;
        case 2 :
            $dialetra = "martes";
            break;
        case 3 :
            $dialetra = "miércoles";
            break;
        case 4 :
            $dialetra = "jueves";
            break;
        case 5 :
            $dialetra = "viernes";
            break;
        case 6 :
            $dialetra = "Sábado";
            break;
    }

    switch ($mes) {
        case '01' :
            $mesletra = "Enero";
            break;
        case '02' :
            $mesletra = "Febrero";
            break;
        case '03' :
            $mesletra = "Marzo";
            break;
        case '04' :
            $mesletra = "Abril";
            break;
        case '05' :
            $mesletra = "Mayo";
            break;
        case '06' :
            $mesletra = "Junio";
            break;
        case '07' :
            $mesletra = "Julio";
            break;
        case '08' :
            $mesletra = "Agosto";
            break;
        case '09' :
            $mesletra = "Septiembre";
            break;
        case '10' :
            $mesletra = "Octubre";
            break;
        case '11' :
            $mesletra = "Noviembre";
            break;
        case '12' :
            $mesletra = "Diciembre";
            break;
    }

    return htmlentities("$dialetra, $dia de $mesletra de $ano");
}

$smarty = new Smarty;
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->config_dir = './configs/';
$smarty->cache_dir = './cache/';

$smarty->left_delimiter = '<!--{';
$smarty->right_delimiter = '}-->';

function byteSize($bytes) {
    $size = $bytes / 1024;
    if ($size < 1024) {
        $size = number_format($size, 2);
        $size .= ' KB';
    } else {
        if ($size / 1024 < 1024) {
            $size = number_format($size / 1024, 2);
            $size .= ' MB';
        } else if ($size / 1024 / 1024 < 1024) {
            $size = number_format($size / 1024 / 1024, 2);
            $size .= ' GB';
        }
    }
    return $size;
}

if (isset($_SESSION["krd"]))
    $krd = $_SESSION["krd"];
else
    $krd = "";

define('ADODB_ASSOC_CASE', 2);
include_once $ruta_raiz . "/include/db/ConnectionHandler.php";

$db = new ConnectionHandler("$ruta_raiz");

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//define('ADODB_ASSOC_CASE', 2);
$iddraft     = $_SESSION["draftMemo"];
$usuario     = $_SESSION["usua_nomb"];
$dependencia = $_SESSION["depecodi"];
$dep_code    = $_SESSION["depecodi"];
$usua_code   = $_SESSION["codusuario"];
$encabezado  = session_name() . "=" . session_id();
$encabezado .= "&krd= $krd";

$sqlDraft = "SELECT
               DRAFT_CONTENT
             FROM
               SGD_DRAFT
             WHERE
               ID = $iddraft";

$exteDraft = $db->conn->Execute($sqlDraft);

if(!$exteDraft->EOF){
  $archtml = html_entity_decode($exteDraft->fields["DRAFT_CONTENT"]);
}else{
  die;
}


$sql = "select
	USUA_LOGIN, USUA_NOMB
	from usuario
	where usua_esta=1 and " . $db->conn->length . "(USUA_EMAIL)>1 order by usua_nomb";

$rsUsrsOrfeo = $db->conn->Execute($sql);
foreach ($rsUsrsOrfeo as $row) {
    $arrUsrsOrfeo[$row['USUA_LOGIN']] = $row['USUA_NOMB'];
}

$sql1 = "select anex_tipo_ext as EXT from anexos_tipo";
$exte = $db->conn->Execute($sql1);
while (!$exte->EOF) {
    $val = $exte->fields["EXT"];
    $extn .= empty($extn) ? $val : "|" . $val;
    //arreglo para validar la extension
    $exte->MoveNext();
};

$sqlSubstDesc = $db->conn->substr . "(anex_desc, 0, 50)";

$post = strpos(strtolower($pathPadre), 'bodega');
$pathPadre = substr($pathPadre, $post + 6);
$rutaPadre = trim($ruta_raiz . '/' . $carpetaBodega . '/' . $pathPadre);

//Plantillas guardadas
$perPlanilla = 3; //$_SESSION["usua_perm_resplantilla"];

if ($perPlanilla > 2) {
    $permPlnatill[] = array("nombre" => "Generales", "codigo" => 3);
}

if ($perPlanilla > 1) {
    $permPlnatill[] = array("nombre" => "Dependencia", "codigo" => 2);
}

$permPlnatill[] = array("nombre" => "Personales", "codigo" => 1);
$sql21 = "SELECT
                      ID,
                      PLAN_PLANTILLA,
                      PLAN_NOMBRE,
                      PLAN_FECHA,
                      DEPE_CODI,
                      USUA_CODI,
                      PLAN_TIPO
                    FROM
                      SGD_PLAN_PLANTILLAS
                    WHERE
		      PLAN_TIPO = 3 OR
                      (PLAN_TIPO = 2 AND DEPE_CODI = $dependencia) OR
                      (PLAN_TIPO = 1 AND DEPE_CODI = $dependencia AND USUA_CODI = $usua_code )";
$plant = $db->conn->Execute($sql21);
while (!$plant->EOF) {
    $grupDepende = array();
    $grupGeneral = array();
    $grupPersonal = array();

    $plan_id = $plant->fields["ID"];

    $plan_nombre = $plant->fields["PLAN_NOMBRE"];
    $plan_fecha = $plant->fields["PLAN_FECHA"];
    $plan_tipo = $plant->fields["PLAN_TIPO"];
    $plan_depend = $plant->fields["DEPE_CODI"];
    $plan_usurio = $plant->fields["USUA_CODI"];
    $plan_plantilla = $plant->fields["PLAN_PLANTILLA"];

    $plan_plantilla = str_replace('"', "'", $plan_plantilla);
    $plan_plantilla = str_replace("\r", '', $plan_plantilla);
    $plan_plantilla = str_replace("\n", '', $plan_plantilla);
    $plan_plantilla = str_replace("\t", '', $plan_plantilla);
    //include "combinaCampos.php";
    if ($plan_tipo == 3) {
        $showInput = ($perPlanilla >= $plan_tipo) ? true : false;
        $carpetas['Generales'][] = array("id" => $plan_id, "nombre" => $plan_nombre, "ruta" => $plan_plantilla, "show" => $showInput);
    } elseif ($plan_tipo == 2) {
        $showInput = ($perPlanilla >= $plan_tipo) ? true : false;
        $carpetas['Dependencia'][] = array("id" => $plan_id, "nombre" => $plan_nombre, "ruta" => $plan_plantilla, "show" => $showInput);
    } elseif ($plan_tipo == 1) {
        $carpetas['Personales'][] = array("id" => $plan_id, "nombre" => $plan_nombre, "ruta" => $plan_plantilla, "show" => true);
    }
    $plant->MoveNext();
};

$sqlConcat = $db->conn->Concat("dep_sigla", "'-'",$db->conn->substr."(depe_nomb,1,30) ");

$sql = "SELECT $sqlConcat ,DEPE_CODI
	FROM DEPENDENCIA
	WHERE DEPE_ESTADO = 1 order by 1";

$rsDep2 = $db->conn->Execute($sql);
$rsDep = $db->conn->Execute($sql);
$selectDepen = $rsDep2->GetMenu2("codDepeDest","$dep_sel","0: -- Seleccione una dependencia--", False, 0," class='select' id='txtHiddenOR'");
$selectDepencc = $rsDep->GetMenu2("codDepecc[]","$dep_selcc","0: -- Seleccione una dependencia--", true, 6," class='select'");


$smarty->assign("session", session_name() . "=" . session_id());//session por get
$smarty->assign("usuacodi", $usuacodi);
$smarty->assign("extn", $extn);
$smarty->assign("depecodi", $depecodi);
$smarty->assign("codigoCiu", $codigoCiu);
$smarty->assign("radPadre", $radicado);
$smarty->assign("rutaPadre", $rutaPadre);
$smarty->assign("usuanomb", $usuanomb);
$smarty->assign("usualog", $krd);
$smarty->assign("destinatario", $destinatario);
$smarty->assign("asunto", $archtml);
$smarty->assign("carpetas", $carpetas);
$smarty->assign("perm_carps", $permPlnatill);
$smarty->assign("perm_respuesta", $_SESSION["usua_perm_respuesta"]);
$smarty->assign("tpNumRad", $_SESSION["tpNumRad"]);
$smarty->assign("tpDescRad", $_SESSION["tpDescRad"]);
$smarty->assign("modalidad", $_SESSION["perm_radmemelec"]);
$smarty->assign("usrsOrfeo", $arrUsrsOrfeo);
$smarty->assign("selectDepecc", $selectDepencc);
$smarty->assign("selectDepe", $selectDepen);
$smarty->display('index.tpl');

?>
