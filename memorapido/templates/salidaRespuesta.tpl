<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Respuesta Rapida</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../estilos/orfeo.css" type="text/css"  rel="stylesheet" />
        <style type="text/css">

            HTML, BODY{
                font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
                margin: 0px; 
                height: 100%;
            }
        </style>
    </head>
    <body>

        <table width="70%" border="0" align="center" margin="4" CELLPADDING="10" cellspacing="0" >

            <tr bordercolor="#FFFFFF">
                <td colspan="2" height="40" align="center" class="titulos4"
                    valign="middle">
                    <b><span class=etexto>Memorando Electronico</span></b>
                </td>
            </tr>
            <!--{if $noerror ge 1 or $salida eq 'ok'}-->
                <tr>
                    <td <!--{if !$sali}--> colspan="4" <!--{/if}--> valign="middle">
                        <b><span class=etexto>Memorando No. <!--{$nurad}--></span></b>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" >
                        <b><span class=etexto><!--{$error}--></span></b>
                    </td>
                </tr>
            <!--{else}-->
                <tr>
                    <td colspan="2" >
                        <b><span class=etexto><!--{$error}--></span></b>
                    </td>
                </tr>
            <!--{/if}-->
        </table>
        <!--{if $noerror ge 1 or $salida eq 'ok'}-->
        <iframe id="iframetrd"src="../radicacion/tipificar_documento.php?<!--{$sid}-->&nurad=<!--{$nurad}-->&dependencia=<!--{$dependencia}-->&krd=<!--{$krd}-->&tsub=0&codserie=0&ind_ProcAnex=N" 
        width='100%' height='440px' style='border: 0px'></iframe>
        <!--{/if}-->
    </body>
</html>
