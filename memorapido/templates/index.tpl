<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Memorando Rapida</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <link   href="../estilos/orfeo.css"           type="text/css"  rel="stylesheet" />
        <link   href="../estilos/jquery.treeview.css" type="text/css"  rel="stylesheet" />
        <link   href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet" />
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src='../js/jquery.form.js'            type="text/javascript" language="javascript"></script>
        <script src='../js/jquery.MultiFile.pack.js'  type="text/javascript" language="javascript"></script>

        <script language="javascript">

            $(document).ready(function() {


                $('#button').on('click', function(event) {
                    if (validateForm()) {
                        $(this).prop('disabled', true);
                        return;
                    }
                    event.preventDefault();
                });

                $('span[ref]').on('click', function() {
                    var idString = $(this).attr('ref');
                    var textnew = $("#" + idString).html();
                });

                $('#T7').MultiFile({
                    STRING: {
                        remove: '<img src="./js/bin.gif" height="16" width="16" alt="x"/>'
                    },
                    list: '#T7-list'
                });


                var tmrReady = setInterval(isPageFullyLoaded, 100);

                function isPageFullyLoaded() {
                  if (document.readyState == "loaded" || document.readyState == "complete") {
                    subclassForms();
                    clearInterval(tmrReady);

                  }

                }

                function submitDisabled(_form, currSubmit) {
                  return function () {
                    var mustSubmit = true;
                    if (currSubmit != null)
                      mustSubmit = currSubmit();

                    var els = _form.elements;
                    for (var i = 0; i < els.length; i++) {
                      if (els[i].type == "submit")
                        if (mustSubmit)
                          [i].disabled = true;

                    }
                    return mustSubmit;

                  }

                }

                function subclassForms() {
                  for (var f = 0; f < document.forms.length; f++) {
                    var frm = document.forms[f];
                    frm.onsubmit = submitDisabled(frm, frm.onsubmit);

                  }

                }


            });

            function validateForm() {
                var result = true;
                if ( ($('#txtHiddenOR').val()=="0") || ($('#asunto').val().trim().length==0) ) {
                    alert('Los campos Destinatario y Asunto son requeridos.');
                    $('#destinatario').focus();
                    result = false;
                }
                return result;
            }
        </script>

        <style type="text/css">

            HTML, BODY{
                font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
                margin: 0px;
                height: 100%;
            }

            #load{
                position:absolute;
                z-index:1;
                border:3px double #999;
                background:#f7f7f7;
                width:300px;
                height:300px;
                margin-top:-150px;
                margin-left:-150px;
                top:50%;
                left:50%;
                text-align:center;
                line-height:300px;
                font-family: verdana, arial,tahoma;
                font-size: 14pt;
            }

            img {
                border: 0 none;
            }

            .MultiFile-label{
                float: left;
                margin: 3px 15px 3px 3px;
            }

            .linkCargar{
                background: url(../estilos/images/flechaAzul.gif) no-repeat;
                cursor: pointer;
                padding-bottom: 17px;
                padding-left: 17px;
            }

        </style>
    </head>
    <body>

        <div id="load" style="display:none;">Enviando.....</div>
        <form id="form1" name="form1" method="post" enctype="multipart/form-data" action='../memorapido/procRespuesta.php?<!--{$session}-->&usualog=<!--{$usualog}-->'>
            <table width="100%" align="center" cellspacing="2" cellpadding="0">
                <tr align="center" class="titulos2">
                    <td height="15" colspan="4" class="titulos4">
                        MEMORANDO ELECTR&Oacute;NICO
                    </td>
                </tr>
    <tr>
        <td width="10%" class="titulos" colspan="1"><span>&nbsp;&nbsp;</span> Destinatario:</td>
        <td width="90%" colspan="3">
        <!--{$selectDepe}-->
        </td>
    </tr>
    <tr>
        <td width="20px" class="titulos"><span>&nbsp;&nbsp;</span> Asunto:</td>
        <td>
          <textarea rows="3" cols="90" name="asunto" id="asunto"></textarea>
        </td>
        <td colspan=2 class="titulos">Seleccione a los usuarios para enviar informados:<br/>
        <!--{$selectDepecc}-->
        </td>
    </tr>
    <tr>
        <input type="hidden" value="2" name="medioRadicar"></input>
        <td colspan="1"><span>&nbsp;&nbsp;</span> Adjuntar </td>
        <td colspan="3">
            <input class="select_resp" name="archs[]" type="file" id="T7" accept="<!--{$extn}-->"/>
            <div id="T7-list" class="select_resp" ></div>
        </td>
    </tr>
                <tr>
                    <td colspan="4">
                        <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
                        </table>
                    </td>
                </tr>

                <tr align="center">
                    <td width="100%" height="25" class="titulos5" align="center" colspan="4">
      <br/>
                        <input type="submit" id="button" name="Button" value="ENVIAR" class="botones">
      <br/>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <!--{$asunto}-->
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
