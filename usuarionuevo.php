<?php
session_start();

$ruta_raiz = ".";
if(!$_SESSION['dependencia']) include "$ruta_raiz/rec_session.php";

$krdOld      = $krd;
$carpetaOld  = $carpeta;
$tipoCarpOld = $tipo_carp;

if(!$krd) $krd=$krdOld;

include_once  "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler($ruta_raiz);
?>
<html>
<title>Adm - Contrase&ntilde;as - ORFEO</title>

<head>
  <link rel="stylesheet" href="./estilos/orfeo.css">
  <link rel="stylesheet" href="./estilos/login.css">
  <style>
    body {min-height: 545px; }
  </style>
</head>

<body>
  <section class="hero is-info">
    <div class="hero-body">
      <div class="container">
          <?php
          if(!$depsel){
            $depsel = $dependencia;
          }

          if($aceptar=="grabar") {
            $isql = "UPDATE
                      USUARIO
                     SET USUA_NUEVO='1',
                      USUA_PASW='".substr(md5($contraver),1,26)."',
                      depe_codi=$depsel,
                      USUA_SESION='CAMBIO PWD(".date("Ymd")."'
                     WHERE USUA_LOGIN='$usuarionew'";
            $rs = $db->conn->query($isql);
            if($rs==-1){
              echo "No se ha podido cambiar la contrase&ntilde;a, Verifique los datoe e intente de nuevo</center>";
            } else {
              echo "<p>Su contrase&ntilde;a ha sido cambiada correctamente.</p><a href='./login.php'class='button is-success is-inverted is-outlined'>Regresar al inicio</a>";
              session_destroy();
            }
          } else {
            if($contradrd==$contraver) { ?>
            <h1 class="title">
                Confirmar datos
            </h1>
            <h2 class="subtitle">
              Usuario <?=$usuarionew ?>
            </h2>
            <h2 class="subtitle">
              Dependencia <?=$depsel?>
            </h2>
            <form action="usuarionuevo.php?krd=<?=$krd?>&<?=session_name()?>=<?=session_id()?>" method="post">
              <input type=hidden name=usuarionew value='<?=$usuarionew?>'>
              <input type=hidden name=contraver value='<?=$contraver?>'>
              <input type=hidden name=depsel value='<?=$depsel?>'>
              <h2 class="subtitle">
                Est&aacute; Seguro de estos datos ?
                <input type=submit value='grabar' name=aceptar class='button is-white'>
              </h2>
            </form>
            <?php
            }
          }
        ?>
      </div>
    </div>
  </section>
</body>
</html>
