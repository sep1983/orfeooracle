<?php

$tmpscript = array_slice(explode("/",$_SERVER["SCRIPT_NAME"]), '-1', 1);
$nScript = $tmpscript[0];

if (!$ruta_raiz)
    $ruta_raiz = "..";
?>
<link rel="stylesheet" type="text/css" href="<?= $ruta_raiz ?>/js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="<?= $ruta_raiz ?>/js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript">
    <!-- Funcion que activa el sistema de marcar o desmarcar todos los check  -->
    setRutaRaiz ('<?= $ruta_raiz ?>');
    function markAll(){
      if(document.form1.elements['checkAll'].checked){
        for(i=1;i<document.form1.elements.length;i++){
          document.form1.elements[i].checked=1;
        }
      }else{
        for(i=1;i<document.form1.elements.length;i++){
          document.form1.elements[i].checked=0;
        }
      }
    }

    <!--
<?php
// print ("El control agenda en tx($controlAgenda");

$ano_ini = date("Y");
$mes_ini = substr("00" . (date("m") - 1), -2);
if ($mes_ini == 0) {
    $ano_ini == $ano_ini - 1;
    $mes_ini = "12";
}
$dia_ini = date("d");
if (!$fecha_ini)
    $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
$fecha_busq = date("Y/m/d");
if (!$fecha_fin)
    $fecha_fin = $fecha_busq;
?>

           //--></script>
<?php
require_once("$ruta_raiz/pestanas.js");
/**  TRANSACCIONES DE DOCUMENTOS
 *  @depsel number  contiene el codigo de la dependcia en caso de reasignacion de documentos
 *  @depsel8 number Contiene el Codigo de la dependencia en caso de Informar el documento
 *  @carpper number Indica codigo de la carpeta a la cual se va a mover el documento.
 *  @codTx   number Indica la transaccion a Trabajar. 8->Informat, 9->Reasignar, 21->Devlver
 */
?>

<link rel="stylesheet" type="text/css" href="./estilos/component.css" />
<script language="JavaScript" type="text/JavaScript">
    // Variable que guarda la ultima opcion de la barra de herramientas de funcionalidades seleccionada
    seleccionBarra = -1;
    <!--
        function valid_trd(){
          changedepesel(81);
          sw=0;
        <?php  if(!$verrad) {  ?>
            for(i=1;i<document.form1.elements.length;i++) {
              if (document.form1.elements[i].checked) {
                sw=1;
              }
            }
            if (sw==0) {
              alert ("Debe seleccionar uno o mas radicados");
              return;
            }
        <?php   }   ?>
            document.form1.submit();
        }

        //-->
</script>
<script>
        function archivarConUnaSalida(){
            var sw = 0;
            var tmprad = '';

            for(i=1;i<document.form1.elements.length;i++){
              if (document.form1.elements[i].checked) {
                const arrayd = /\d+/.exec(document.form1.elements[i].name)
                if(typeof(arrayd) !== "object" || arrayd === null){continue;}
                let nurad = /\d+/.exec(document.form1.elements[i].name)[0].slice(-1);
                if(nurad === '2' ){
                  sw += 1;
                }else{
                  document.form1.elements[i].checked = false;
                }
              }
            }

            if (sw < 1){
                alert("Debe seleccionar un radicado de entrada.");
                return false;
            }

            changedepesel(30);
            envioTx();
        }

        function respuestaTx(){
            var sw = 0;
            var tmprad = 0;
            for(i=1;i<document.form1.elements.length;i++)
                    if (document.form1.elements[i].checked) {
                        sw += 1;
                        tmprad = document.form1.elements[i].name;
                    }
            if (sw != 1) {
                alert("Debe seleccionar un radicado.");
                return false;
            }
            var params      = 'width='+screen.width;
                params      += ', height='+screen.height;
                params      += ', top=0, left=0'
                params      += ', scrollbars=yes'
                params      += ', fullscreen=yes';
            window.open("respuestaRapida/index.php?<?=session_name()?>=<?=session_id()?>&radicadopadre=" + tmprad.substr(11,14) +
                        "&krd=<?=$krd?>", "Respuesta Rapida", params);
        }

        function vistoBueno(){
            //datos para mostrar codigo de visto bueno.
            changedepesel(20);
            document.getElementById('EnviaraV').value = 'VoBo';
            envioTx();
        }

        function devolver(){
            changedepesel(12);
            envioTx();
        }

        function txAgendar() {
          fecha_hoy =  '<?= date('Y') . "-" . date('m') . "-" . date('d') ?>';
          fecha = document.form1.elements['fechaAgenda'].value;
          var msg='';
          var band=false;
          var sw=0;

          if (fecha==""){
            msg+="-Debe suministrar la fecha de agenda\n";
            band= true;
          }

          if (!fechas_comp_ymd(fecha_hoy,fecha) ) {
            msg+="-La fecha de agenda debe ser mayor que la fecha de hoy\n";
            band= true;
          }

          for(i=1;i<document.form1.elements.length;i++){
            if (document.form1.elements[i].checked)
              sw=1;
          }
                if (sw==0){
                  msg+="-Debe seleccionar uno o mas radicados";
                  band= true;
                }
                //document.form1.submit();

                if(msg!='' && band){
                  alert(msg)
                    return !band;
                }
                changedepesel(14);
                document.form1.submit();
        }

        function txNoAgendar(){
            changedepesel(15);
            envioTx();
        }

        function archivar(){
            changedepesel(13);
            envioTx();
        }

        function nrr(){
            changedepesel(16);
            envioTx();
        }

        function Expedient() {
           changedepesel(80);
           envioTx();
        }

        function envioTx() {
                sw=0;
    <?php if (!$verrad) { ?>
                       for(i=1;i<document.form1.elements.length;i++)
                           if (document.form1.elements[i].checked)
                               sw=1;
                       if (sw==0) {
                           alert("Debe seleccionar uno o mas radicados");
                           return false;
                       }
     <?php } ?>
                        document.form1.submit();
            }

            function expMasiva(objeto){
                        var sw=0;
                        var rads= '';
                        var formulario = document.forms[objeto];
      <?php if (!$verrad) { ?>
                       for(i=1;i<formulario.elements.length;i++)
                           if (formulario.elements[i].checked)
                       {
                           if(formulario.elements[i].name.substring(0, 10) == 'checkValue')
                               rads += formulario.elements[i].name.substring(11, 25) + ',';
                           sw=1;
                       }
                       if (sw==0)
                       {
                           alert("Debe seleccionar uno o mas radicados");
                           return;
                       }
            <?php } ?>
                        rads = rads.substring(0, rads.length - 1);
                        if(rads.length > 5){
                            window.open("tx/incluirExpMasiva.php?radicados="+rads);
                        }else{
                            alert('Esta accion se debe realizar en la pestaña Expedientes.');
                        }
                    }

            function window_onload(){
              if(document.getElementById('depsel')){document.getElementById('depsel').style.display = 'none';}
              if(document.getElementById('depsel8')){document.getElementById('depsel8').style.display = 'none';}
              if(document.getElementById('carpper')){document.getElementById('carpper').style.display = 'none';}
              if(document.getElementById('Enviar')){document.getElementById('Enviar').style.display = 'none';}

               <?php
               if (!$verrad) {

               } else {
               ?>
                 window_onload2();
               <?php
               }
               if ($carpeta == 11 and $_SESSION['usua_vobo_perm'] == 1) {
                 echo "document.getElementById('salida').style.display = ''; ";
                 echo "document.getElementById('enviara').style.display = ''; ";
                 echo "document.getElementById('Enviar').style.display = ''; ";
               } ELSE {
                 echo " ";
               }
               if ($carpeta == 11 and $_SESSION['usua_vobo_perm'] != 1) {
                 echo "document.getElementById('enviara').style.display = 'none'; ";
                 echo "document.getElementById('Enviar').style.display = 'none'; ";
               }
               ?>
              }
</script>

<?php
if (($mostrar_opc_envio == 0)
  || ($_SESSION['codusuario'] == $radi_usua_actu
  && $_SESSION['dependencia'] == $radi_depe_actu)) {
  ?>
  <div class='columns'>
    <div class='column is-narrow'>
      <?php
      $sql =
       "SELECT
        PERM_ARCHI AS \"PERM_ARCHI\",
        USUA_PERM_NORESPUESTA AS \"USUA_PERM_NORESPUESTA\",
        PERM_VOBO AS \"PERM_VOBO\"
        FROM USUARIO WHERE USUA_CODI = " . $_SESSION['codusuario'] . " AND
        DEPE_CODI = " . $_SESSION['dependencia'];
      $rs = $db->query($sql);

      if (!$rs->EOF) {
        $permArchi = $rs->fields["PERM_ARCHI"];
        $permNRR   = $rs->fields["USUA_PERM_NORESPUESTA"];
        $permVobo  = $rs->fields["PERM_VOBO"];
      }

      if ($controlAgenda == 1) { //Si el esta consultando la carpeta de documentos agendados entonces muestra el boton de sacar de la agenda
        if ($agendado) {
          echo("<img name='principal_r5_c1'  src='$ruta_raiz/imagenes/internas/noAgendar.gif' width='130' height='20' border='0' alt=''>");
          echo("<input name='Submit2' type='button' class='botones_2' value='&gt;&gt;' onClick='txNoAgendar();'>");
        } else {
        ?>
        <label class="label is-small">Agendar</label>
        <div class="field is-grouped">
          <div class="control">
            <script language="javascript">
              var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "form1","fechaAgenda","btnDate1","",scBTNMODE_CUSTOMBLUE);
              dateAvailable.date = "2003-08-05";
              dateAvailable.writeControl();
              dateAvailable.dateFormat="yyyy-MM-dd";
            </script>
          </div>
          <div class="control">
            <input class='button is-info is-small' type="button" class="botones_2" value="Agendar" onClick='txAgendar();'>
          </div>
        </div>
        <?php
        }
      } ?>
      </div>
      <div class="column">
        <?php if (!$agendado){?>
        <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
          <?php if ($nScript != 'verradicado.php'){ ?>

            <a class="hi-icon hi-icon-rocket" href="#" onClick="archivarConUnaSalida()">
              <div class="popover above">
                <div class='title is-6'>
                Archivo Con Una Salida
                </div>
                <p>Archiva varios documentos de entrada para los cuales
                    podemos hacer una sola salida.
                    La respuesta de salida ya debe estar creada y enviada.
                </p>
              </div>
            </a>

            <a class="hi-icon hi-icon-flag" href="#" onClick="valid_trd();">
              <div class="popover above">
                <div class='title is-6'>
                  Tabla Retenci&oacute;n Documental TRD
                </div>
                <p>Clasificaci&oacute;n requerida para organizar los documentos
                    generados en la Entidad. Si el radicado tiene anexos, &eacute;stos
                    quedar&aacute;n con la misma clasificaci&oacute;n.
                </p>
              </div>
            </a>

            <a class="hi-icon hi-icon-folder-add" href="#" onClick="expMasiva('form1')">
              <div class="popover above">
                <div class='title is-6'>
                  Expediente Masivo
                </div>
                <p>Guarda los documentos de manera virtual en carpetas como se hace de forma
                    f&iacute;sica. Con esta acci&oacute;n puede guardar m&aacute;s de un
                    radicado en un expediente.
                </p>
              </div>
            </a>

          <?php
          }
          if ($_SESSION["usua_perm_respuesta"] >= 1 and $nScript != 'verradicado.php') { ?>
              <a class="hi-icon hi-icon-mail" href="#" onClick="respuestaTx()">
                <div class="popover above">
                  <div class='title is-6'>
                  Respuesta R&aacute;pida
                  </div>
                  <p>Permite el env&iacute;o de respuestas por correo electr&oacute;nico de la
                  Entidad, Este tipo de respuesta genera certificaci&oacute;n.
                  </p>
                </div>
              </a>
          <?php
          }
          ?>
              <a class="hi-icon hi-icon-export" href="#" onClick="seleccionBarra = 10;changedepesel(10);">
                <div class="popover above">
                  <div class='title is-6'>
                    Carpetas Personales
                  </div>
                  <p>Organiza los documentos de forma que pueda distribuir el
                      trabajo diaro de manera efectiva. Esta acci&oacute;n permite
                      redireccionar los radicados a la carpeta seleccionada.
                  </p>
                </div>
              </a>
              <a class="hi-icon hi-icon-right" href="#" onClick="seleccionBarra = 9;changedepesel(9);">
                <div class="popover above">
                  <div class='title is-6'>
                    Reasignar
                  </div>
                  <p>Si tiene un documento y necesita que otro funcionario
                      lo tramite con esta opci&oacute;n se puede enviar a la
                      bandeja del responsable de terminar la acci&oacute;n.
                  </p>
                </div>
              </a>
              <a class="hi-icon hi-icon-podcast" href="#" onClick="seleccionBarra = 8;changedepesel(8);">
                <div class="popover above">
                  <div class='title is-6'>
                  Informar
                  </div>
                  <p>Proyecta una respueta y env&iacute;a el documento para que sea
                      revisado o visto por otros usuarios.
                  </p>
                </div>
              </a>
              <a class="hi-icon hi-icon-loop" href="#" onClick="seleccionBarra = 12;changedepesel(12);">
                <div class="popover above">
                    <div class='title is-6'>Devolver</div>
                  <p>Regresa el documento al usuario que lo remiti&iacute;.  </p>
                </div>
              </a>

          <?php
          if (($_SESSION['depe_codi_padre'] and $_SESSION['usua_vobo_perm'] == 1) or $_SESSION['usua_vobo_perm'] != 1) {
            if (!empty($permVobo) && $permVobo != 0) { ?>
              <a class="hi-icon hi-icon-ok" href="#" onclick="seleccionBarra = 14;vistoBueno();" >
                <div class="popover above">
                    <div class='title is-6'>
                      Visto Bueno
                    </div>
                  <p>Env&iacute;a el documento para ser revisado por el jefe de la
                      dependencia antes de terminar el tr&acute;mite.
                  </p>
                </div>
              </a>
            <?
            }
          }

          if (!empty($permArchi) && $permArchi != 0) { ?>
            <a class="hi-icon  hi-icon-box" href="#" onClick="seleccionBarra = 13;changedepesel(13);">
                <div class="popover above">
                    <div class='title is-6'>
                      Archivar
                    </div>
                  <p>S&iacute; el documento ha terminado el tr&aacute;mite y tiene la TRD,
                      el Expediente y cuenta  con una respuesta, podemos
                      enviar el documento para archivo.
                  </p>
                </div>
            </a>
          <?php
          }

          if (!empty($permNRR) && $permNRR!= 0){ ?>
          <a class="hi-icon  hi-icon-na" href="#" onclick="seleccionBarra = 14;changedepesel(16);">
            <div class="popover above">
                <div class='title is-6'>
                  No Requiere Respuesta
                </div>
              <p>
                  Si tiene un documento que fue informativo como una invitaci&oacute;n,
                  una carta de notificaci&oacute;n o alg&uacute;n documento que no requiera
                  un proceso o una respuesta, puede utilizar esta opci&oacute;n
                  para archivar el radicado.
              </p>
            </div>
          </a>
          <?php }
        } ?>
        </div>
      </div>
    </div>
    <?
    }

/* si esta en la Carpeta de Visto Bueno no muesta las opciones de reenviar */
if (($mostrar_opc_envio == 0) || ($_SESSION['codusuario'] == $radi_usua_actu
  && $_SESSION['dependencia'] == $radi_depe_actu)) {

  // Combo en el que se muestran las dependencias, en
  // el caso  de que el usuario escoja reasignar.
  $dependencianomb = substr($dependencianomb, 0, 35);
  $subDependencia  = $db->conn->substr . "(d.depe_nomb,0,50)";

  if ($_SESSION['usua_vobo_perm'] != 1 && $_SESSION["usuario_reasignacion"] != 1) {
      $whereReasignar = " and d.depe_codi = $dependencia";
  } else {
      $whereReasignar = "";
  }

  $sql1 = "select
            $subDependencia,
            d.depe_codi
          from
            DEPENDENCIA d
            inner join usuario u on u.depe_codi=d.depe_codi
          where
            depe_estado=1 and u.usua_codi=1 $whereReasignar ORDER BY DEPE_NOMB";
  $rs1 = $db->query($sql1);

  $showDepenReas = $rs1->GetMenu2('depsel', $dependencia, false, false);

  if($showDepenReas){
    $showDepenReas = "<div id='depsel' class='select'> $showDepenReas </div>";
  }


  // genera las dependencias para informar
  $dependencianomb = substr($dependencianomb, 0, 35);
  $subDependencia = $db->conn->substr . "(depe_nomb,0,50)";
  $sql2 = "select
            d.depe_codi||' '||$subDependencia,
            d.depe_codi
          from
            DEPENDENCIA d
            inner join usuario u on u.depe_codi=d.depe_codi
          where
            depe_estado=1 and u.usua_codi=1 ORDER BY d.DEPE_CODI";
  $rs2 = $db->conn->Execute($sql2);
  $showDepenInfor = $rs2->GetMenu2('depsel8[]', $dependencia, false, true, 4,"id='depsel8'");


  // Aqui se muestran las carpetas Personales
  $codigoCarpetaPer = $db->conn->Concat("'11000'", "codi_carp");


  $codus1  = $_SESSION['codusuario'];
  $depen1 = $_SESSION['depecodi'];

  $sql32 = "select nomb_carp,
            $codigoCarpetaPer as carp_codi
            from carpeta_per
            where
            usua_codi = $codus1
            and depe_codi = $depen1
            order by carp_codi";
  $rs32 = $db->conn->Execute($sql32);
  $showPersFold = $rs32->GetMenu2('carpSel', 1, false, false, 0);

  if($showPersFold){
    $showPersFold = "<div id='carpper' class='select'> $showPersFold </div>";
  }
}
?>

<div class="columns is-gapless">

  <div class="column">
  <? if ($controlAgenda == 1) { ?>
      <div class="content">
        <p>Ordenar por:
          <a href='<?=$ruta_raiz?>/cuerpo.php?<?= session_name() . "=" . trim(session_id()) . $encabezado . "7&orderTipo=DESC&orderNo=10"; ?>' alt='Ordenar Por Leidos'>
            <span class='leidos'>Le&iacute;dos</span>
          </a>
          <a href='<?= $ruta_raiz ?>/cuerpo.php?<?= session_name() . "=" . trim(session_id()) . $encabezado . "8&orderTipo=ASC&orderNo=10" ?>' alt='Ordenar Por Le&iacute;dos'
            class="tparr">
            <span class='no_leidos'>No le&iacute;dos</span>
          </a>
        </p>
      </div>
  <? } ?>
  </div>

  <div class="column">
      <?=$showDepenInfor?>
      <?=$showDepenReas?>
      <?=$showPersFold?>
    <input type=hidden name=enviara value=9>
    <input type=hidden name=EnviaraV id=EnviaraV value=''>
    <input type=hidden name=codTx value=9>
    <input type='button' class="button is-info is-small" value='Enviar' name=Enviar id=Enviar onClick="envioTx();">
  </div>

</div>
