<?php
session_start();

$ruta_raiz = ".";

if($_SESSION['dependencia'] == null ){
 include "$ruta_raiz/cerrar_session.php";
}

$krd = $_SESSION["krd"];
$ssid = session_name()."=".session_id()."&krd=$krd";

include_once($ruta_raiz.'/config.php'); 			// incluir configuracion.
include_once($ruta_raiz."/include/db/ConnectionHandler.php");
$fechah = date("dmy") . "_" . time("hms");

$db = new ConnectionHandler("$ruta_raiz");
if (!$db){
  die('Sin conexion a la base de datos');
}

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
include($ruta_raiz."/include/class/Contadores.php");
$contador = new Contadores($db);
$numRadicadosUsu = $contador->getContadorRad($_SESSION['dependencia'], $_SESSION['codusuario']);
$numRadicadosDepe = $contador->getContadorJefe($_SESSION['dependencia']);
if ($_SESSION["codusuario"] == 1){
	$vencidos = $contador->getContadorJefe($_SESSION['dependencia']);
}

$vencidos0= $contador->getContador($_SESSION['dependencia'], $_SESSION['codusuario'], 0);
$vencidos1= $contador->getContador($_SESSION['dependencia'], $_SESSION['codusuario'], 1);
$vencidos2= $contador->getContador($_SESSION['dependencia'], $_SESSION['codusuario'], 2);
$expVencido=$contador->getExpPresVencidos($krd,0);
$expVencidoBolqueado=$contador->getExpPresVencidos($krd,1);

//Realiza la consulta del usuarios y de una vez cruza con la tabla dependencia
$isql = " SELECT
            A.USUA_LOGIN,
            A.USUA_CODI,
            A.CODI_NIVEL,
            A.PERM_RADI,
            A.USUA_TIMERELOAD,
            B.DEPE_NOMB
          FROM
            USUARIO A,
            DEPENDENCIA B
          WHERE
              A.DEPE_CODI=B.DEPE_CODI AND
              USUA_LOGIN ='$krd' ";

$rs = $db->query($isql);

$phpsession = session_name()."=".session_id();
$log        = trim($rs->fields["USUA_LOGIN"]);

if($autentica_por_LDAP != 1){
  $itemPass = "<a class='navbar-item is-hidden-desktop-only'
                  href='./contraxx.php'
                  target='_top'>
                    <i class='el el-key el-fw'></i>
                    Contrase&ntilde;a
               </a>";
  $itemPass2 = "<a class='navbar-item is-hidden-desktop'
                  href='./contraxx.php'
                  target='_top'>
                    <i class='el el-key el-fw'></i>
               </a>";
}

?>
<html>
  <head>
    <title>.:: Sistema de Gesti&oacute;n Documental ::.</title>
    <link rel="stylesheet" href="./estilos/orfeo.css">
    <meta name="viewport" content="width=device-width">
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="icon" href="./icon.ico" type="image/x-icon" />
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript" src="./js/vue.js"></script>
    <script language="javascript" type="text/javascript">
      document.addEventListener("DOMContentLoaded", function(event) {
        // Get all "navbar-burger" elements
        var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any nav burgers
        if ($navbarBurgers.length > 0) {

          // Add a click event on each of them
          $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

              // Get the target from the "data-target" attribute
              var target = $el.dataset.target;
              var $target = document.getElementById(target);

              // Toggle the class on both the "navbar-burger" and the "navbar-menu"
              $el.classList.toggle('is-active');
              $target.classList.toggle('is-hidden-touch');
            });
          });
        }

        var app6 = new Vue({
          el: '#app-leftMenu',
          data: {
            show1: false,
            show2: false,
            show3: true,
          },
          methods: {
            showTab: function(noTab) {
              switch (noTab) {
                case 1:
                  this.show1 = !this.show1;
                  this.show2 = false;
                  this.show3 = false;
                  break;
                case 2:
                  this.show1 = false;
                  this.show2 = !this.show2;
                  this.show3 = false;
                  break;
                case 3:
                  this.show1 = false;
                  this.show2 = false;
                  this.show3 = !this.show3;
              }
            },
          }
        });
      })
    </script>
  </head>
  <body>
    <nav class="navbar navMenuEdit">
      <div class="navbar-brand">
        <span class="navbar-item">
          <img src="./img/logoEntidad.jpg" alt="SGD-Orfeo" height="28">
        </span>

        <a class="navbar-item" href="./index_frames.php?<?=$ssid?>nomcarpeta=Entrada&carpeta=0&tipo_carpt=0&adodb_next_page=1">
          <i class="el el-refresh"></i>
        </a>

        <a class="navbar-item is-hidden-desktop" href="./Manuales/index.php?<?=$ssid?>" target="mainFrame">
          <i class="el el-website el-fw"></i>
        </a>
        <!--Init Mostrar cambio de clave -->
        <?=$itemPass2?>
        <!--End -->
        <a class="navbar-item is-hidden-desktop" href="./estadisticas/vistaFormConsulta.php?<?=$ssid?>" target="mainFrame">
          <i class="el el-th-list el-fw"></i>
        </a>
        <a class="navbar-item  is-hidden-desktop" href="./mod_datos.php?<?=$ssid?>" target="mainFrame">
          <i class="el el-user el-fw"></i>
        </a>
        <a class="navbar-item is-hidden-desktop" href="./cerrar_session.php?<?=$ssid?>" target="_top">
          <i class="el el-off el-fw"></i>
        </a>

        <div class="navbar-burger burger has-text-light" data-target="menuBox">
          <span></span>
          <span></span>
          <span></span>
        </div>

      </div>
      <div class="navbar-menu">

        <div class="navbar-end">
          <a class="navbar-item is-hidden-desktop-only" href="./Manuales/index.php?<?=$ssid?>" target="mainFrame">
            <i class="el el-website el-fw"></i> Plantillas
          </a>
          <!--Init Mostrar cambio de clave -->
          <?=$itemPass?>
          <!--End -->
          <a class="navbar-item is-hidden-desktop-only" href="./estadisticas/vistaFormConsulta.php?<?=$ssid?>" target="mainFrame">
            <i class="el el-th-list el-fw"></i> Estadisticas
          </a>
          <a class="navbar-item is-hidden-desktop-only" href="./mod_datos.php?<?=$ssid?>" target="mainFrame">
            <i class="el el-user el-fw"></i> Usuario
          </a>
          <a class="navbar-item is-hidden-desktop-only" href="./cerrar_session.php?<?=$ssid?>" target="_top">
            <i class="el el-off el-fw"></i> Cerrar
          </a>
        </div>
      </div>
    </nav>
        <?php

        if ($log == $krd){
          $codusuario      = $rs->fields["USUA_CODI"];
          $dependencianomb = $rs->fields["DEPE_NOMB"];
          $nivel           = $rs->fields["CODI_NIVEL"];
          $iusuario        = " and us_usuario = '$krd'";
          $perrad          = $rs->fields["PERM_RADI"];
          $reload          = ($rs->fields["USUA_TIMERELOAD"]) ? $rs->fields["USUA_TIMERELOAD"] : 9;
          echo "
            <div class='columns is-gapless' style='margin-top: 55px;' >
              <div class='column is-hidden-touch' id='menuBox'>
                  <aside class='menu' id='app-leftMenu' >";
                        include_once("$ruta_raiz/menu/menuPrimero.php");
                        include_once("$ruta_raiz/menu/radicacion.php");
                        include_once("$ruta_raiz/menu/menuCarpetas.php");
                        echo "<img src='$logo'>".
                  "</aside>
              </div>
              <iframe name='mainFrame'
                min-height=\"100%\" width=\"100%\"
                class='iframe-container'
                src='$ruta_raiz/cuerpo.php'
                scrolling='auto'>
            </div>";
        }

        ?>
  <body>
</html>
