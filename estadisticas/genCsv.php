<?php
session_start();
$ruta_raiz = "..";

$timeCSV = $_GET['xtime'];

if($_SESSION['dependencia'] == null ){
  include "$ruta_raiz/cerrar_session.php";
}

$dataCsv  = $_SESSION['csv'][$timeCSV];

header("Content-disposition: attachment; filename=".time().".csv");
header("Content-Type: text/csv");

$out = fopen('php://output', 'w');
foreach ($dataCsv as $fields) {
  fputcsv($out, $fields, ';');
}

fclose($out);
?>
