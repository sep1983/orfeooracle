<?php
session_start();

$ruta_raiz = "..";
include_once($ruta_raiz.'/config.php');      // incluir configuracion.
include_once($ruta_raiz."/include/db/ConnectionHandler.php");

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$db->conn->Execute("ALTER SESSION SET NLS_DATE_FORMAT='RRRR/MM/DD HH24:SS'");
  $depen_sql = '';
  $tipopqr = '';

  $fecha = " r.radi_fech_radi between '".$_GET['fechai']."' and '".$_GET['fechaf']."'";
  if($_GET['depen'] !== '99999'){
    $depen_sql = " and h.DEPE_CODI = ".$_GET['depen']. " and h.SGD_TTR_CODIGO = 2";
  }
  $tipopqr = " and p.par_serv_nombre = '".$_GET['tipqr']. "'" ;

  $radicadospqr="select
                   r.radi_nume_radi,
                   r.radi_fech_radi,
                   r.ra_asun,
                   m.mrec_desc,
                   d2.depe_nomb as depe_asignada,
                   d.depe_nomb,
                   d.depe_codi,
                   u.usua_nomb,
                   p.par_serv_nombre,
                   r.sgd_spub_codigo,
                   dante.usua_nomb as USUA_ANTE,
                   a.ANEX_FECH_ENVIO,
                   a.ANEX_FECH_ANEX,
                   a.radi_nume_salida,
                   sumadiashabiles(r.radi_fech_radi,td.sgd_tpr_termino) as vencimiento,
                   td.sgd_tpr_descrip,
                   dante.depe_nomb as DEPE_ANTE
                 FROM  radicado r
                   INNER JOIN par_serv_servicios p ON r.par_serv_secue = p.par_serv_secue
                   INNER JOIN medio_recepcion m ON m.mrec_codi = r.mrec_codi
                   INNER JOIN dependencia d ON d.depe_codi = r.radi_depe_actu
                   INNER JOIN dependencia d2 ON d2.depe_codi = r.radi_depe_radi
                   INNER JOIN usuario u ON u.usua_codi = r.radi_usua_actu AND u.depe_codi = r.radi_depe_actu
                   INNER JOIN sgd_tpr_tpdcumento td ON r.tdoc_codi = td.sgd_tpr_codigo AND r.tdoc_codi is not null
                   INNER JOIN hist_eventos h ON r.radi_nume_radi = h.radi_nume_radi
                   LEFT JOIN anexos A  ON r.radi_nume_radi = a.anex_radi_nume and  a.radi_nume_salida is not null
                   LEFT OUTER JOIN Usuario u2 ON u2.usua_login = r.radi_usu_ante
                   LEFT OUTER JOIN (select de.depe_nomb, us.usua_nomb, us.usua_login from dependencia de, usuario us where us.depe_codi = de.depe_codi) dante
                     ON r.radi_usu_ante = dante.usua_login
                 where
                   $fecha
                   $tipopqr
                   $depen_sql
                 order by
                   r.radi_fech_radi, a.radi_nume_salida, r.radi_nume_radi";

  $repu2 = $db->conn->Execute($radicadospqr);

?>
<html>
    <head>
       <link rel="stylesheet" href="../estilos/orfeo.css">
    </head>
    <body>
        <table>
            <tr class="titulos3">
                <td>No.</td>
                <td>RADICADO</td>
                <td width="110px">FECHA DE RADICADO</td>
                <td width="210px">ASUNTO</td>
                <td>MEDIO DE RECEPCION</td>
                <td>DEPENDENCIA ASIGNADA</td>
                <td>DEPENDENCIA ACTUAL</td>
                <td>USUARIO ACTUAL</td>
                <td>TIPO PQR</td>
                <td>RADICADO SALIDA</td>
                <td>FECHA RADICADO SALIDA</td>
                <td>FECHA DE ENVIO</td>
                <td>USUARIO ANTERIOR</td>
                <td>DEPENDENCIA ANTERIOR</td>
                <td>VENCIMIENTO</td>
                <td>TIPO DOCUMENTO</td>
            </tr>
<?php
  $i=1;
  $estilo='listado1';
  while($repu2 && !$repu2->EOF){

    $fldRADI_NUME_RADI = $repu2->fields['RADI_NUME_RADI'];


    if($i%2==0){
      $estilo='listado1';
    }else{
      $estilo='listado2';
    }

    $fldRADI_FECH_RADI = $repu2->fields['RADI_FECH_RADI' ];
    $ra_asun           = $repu2->fields['RA_ASUN'];

    $mrec_desc         = $repu2->fields['MREC_DESC'];
    $pqr_nomb          = $repu2->fields['PAR_SERV_NOMBRE'];
    $dep_asig          = $repu2->fields['DEPE_ASIGNADA'];

    $aRADI_USUA_ACTU   = $repu2->fields['USUA_NOMB'];
    $depe_codi         = $repu2->fields['DEPE_CODI'];
    $aRADI_DEPE_ACTU   = $repu2->fields['DEPE_NOMB'];

    $numsal            = $repu2->fields['RADI_NUME_SALIDA'];
    $numsal_fech       = $repu2->fields['ANEX_FECH_ANEX'];
    $numsal_fech_envio = $repu2->fields['ANEX_FECH_ENVIO'];

    $usua_ante_nomb    = $repu2->fields['USUA_ANTE'];
    $depe_ante_nomb    = $repu2->fields['DEPE_ANTE'];
    $tpr_descr         = $repu2->fields['SGD_TPR_DESCRIP'];
    $date_venc         = substr($repu2->fields['VENCIMIENTO'], 0 ,10);
    $res_asun          = utf8_decode($ra_asun);

    $rows = "
      <tr class=$estilo>
      <td>$i</td>
      <td class='leidos'>$fldRADI_NUME_RADI</td>
      <td class='leidos'>$fldRADI_FECH_RADI</td>
      <td class='leidos'  style='word-wrap:break-word;'>$res_asun</td>
      <td class='leidos'>$mrec_desc</td>
      <td class='leidos'>$dep_asig</td>
      <td class='leidos'>$depe_codi - $aRADI_DEPE_ACTU</td>
      <td class='leidos'>$aRADI_USUA_ACTU</td>
      <td class='leidos'>$pqr_nomb </td>
      <td class='leidos'>$numsal</td>
      <td class='leidos'>$numsal_fech </td>
      <td class='leidos'>$numsal_fech_envio</td>
      <td class='leidos'>$usua_ante_nomb</td>
      <td class='leidos'>$depe_ante_nomb</td>
      <td class='leidos'>$date_venc </td>
      <td class='leidos'>$tpr_descr </td>
      </tr> ";

    if(!in_array($fldRADI_NUME_RADI, $ac) ){
      echo $rows;
      $i++;
      $ac[]=$fldRADI_NUME_RADI;
    }

    $repu2->MoveNext();
  }
?>
  </table>
 </body>
</html>
