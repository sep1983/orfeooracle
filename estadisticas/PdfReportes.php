
<?php 
session_start();

require_once('../dompdf/dompdf_config.inc.php');
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');






 $ruta_raiz = "..";
 include_once($ruta_raiz.'/config.php'); // incluir configuracion.
 include_once($ruta_raiz."/include/db/ConnectionHandler.php");
 $db = new ConnectionHandler("$ruta_raiz");
 $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$data = array();
$leyenda="";
$etique = array();
$arreglox = array();
$arregloy=array();
$titulo="";
$fila="";
$i=0;
$tabla="";

if($_GET['tipo']==1){
    $titulo="Reporte PQRs";
     $repu1 = $db->conn->Execute($_SESSION['cadena']);
        
        $tabla.= "<table width='100%' id='table27' bgcolor='#FFFFFF'>";
        $tabla.= "<tr class='titulos3'><td>Tipo PQR</td><td>Cantidad</td></tr>";
        while(!$repu1->EOF)
          {
            
             $tabla .="<tr class='listado1'>";
             $tabla.="<td>".utf8_decode($repu1->fields['PAR_SERV_NOMBRE'])."</td>";
             $tabla.="<td style='text-align:center'>".$repu1->fields['TOTAL']."</td>";
             $tabla.="</tr>";
             $data[$i]=$repu1->fields['TOTAL'];
             $etique[$i] = "".utf8_decode($repu1->fields['PAR_SERV_NOMBRE'])."";
             $i++;
             $repu1->MoveNext();
          }
         $tabla.="</table>";


$tama = strlen($leyenda);
$leyenda=substr($leyenda,0,$tama-1);

// Create the Pie Graph. 
$graph = new PieGraph(700,800);
//$theme_class= new VividTheme;
//$graph->SetTheme($theme_class);
// Set A title for the plot
$graph->title->Set("Reporte por PQR");
// Create
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->SetScale("textlin");

$p1 = new PiePlot3D($data);
$p1->SetLegends($etique);

$p1->ShowBorder();
$p1->SetColor('black');
$graph->Add($p1);
$graph->legend->SetAbsPos(40, 700, 'left', 'top');

//$p1->ExplodeSlice(1);
$graph->Stroke('imagen1.png'); 
 shell_exec('chmod -R 777 imagen1.png');
}

//Reporte2
elseif($_GET['tipo']==2){
  $titulo="Reporte PQRs por Aerolinea";
 require_once('../jpgraph/src/jpgraph_bar.php');
  $datay=array();
   $repu1 = $db->conn->Execute($_SESSION['cadena']);
       
        $tabla.= "<table width='100%' id='table27' bgcolor='#FFFFFF'>";
        $tabla.= "<tr class='titulos3'><td>AEROLINEA</td><td>Cantidad</td></tr>";
        while(!$repu1->EOF)
          {
             $tabla .="<tr class='listado1'>";
             $tabla.="<td>".utf8_decode($repu1->fields['SGD_DCAU_DESCRIP'])."</td>";
             $tabla.="<td style='text-align:center'>".$repu1->fields['TOTAL']."</td>";
             $tabla.="</tr>";
             $datay[$i]=$repu1->fields['TOTAL'];
             $etique[$i] = "".utf8_decode($repu1->fields['SGD_DCAU_DESCRIP'])."";
             $i++;
             $repu1->MoveNext();
          }
          

// Create the graph. These two calls are always required
$graph = new Graph(700,1000,'auto');
$graph->SetScale("textlin");

//$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());

        
$aux=min($datay);
if($aux==1)
    {$aux=0;}
for($j=0;$j<$i;$j++)
{
    $arreglox[$j]=$aux;
    $arregloy[$j]=$aux;
    $aux++;
}

$graph->yaxis->SetTickPositions($arreglox, $arregloy);
$graph->SetBox(false);

//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels($etique);
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($datay);

// ...and add it to the graPH
$graph->Add($b1plot);

$b1plot->value->Show();
$graph->xaxis->title->Set('Aerolinea');
$graph->yaxis->title->Set('Cantidad');
$b1plot->SetColor("white");
$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
$b1plot->SetWidth(45);
$graph->title->Set("Reporte de Aerolineas");
$graph->Stroke('imagen1.png'); 
}


/////////////////////////PDF TOP 10//////////////
elseif($_GET['tipo']==3){
   require_once('../jpgraph/src/jpgraph_bar.php');
     $repu1 = $db->conn->Execute($_SESSION['cadena']);
   $k=0;
   $titulo="Reporte PQRs Top 10";
       $datay=array();
       $i=1;
        $tabla.= "<table width='100%' id='table27' bgcolor='#FFFFFF'>";
        $tabla.= "<tr class='titulos3'><td>No.</td><td>Sigla</td><td>Causal</td><td>Cantidad</td></tr>";
        while(!$repu1->EOF)
          {
             $tabla .="<tr class='listado1'>";
             $tabla.="<td>".$i++."</td>";
             $tabla.="<td>".$repu1->fields['SGD_DCAU_SIGLA']."</td>";
             $tabla.="<td>".utf8_decode($repu1->fields['SGD_DCAU_DESCRIP'])."</td>";
             $tabla.="<td style='text-align:center'>".$repu1->fields['TOTAL']."</td>";
             $tabla.="</tr>";
             $datay[$k]=$repu1->fields['TOTAL'];
             $etique[$k] = "".utf8_decode($repu1->fields['SGD_DCAU_SIGLA'])."";
             $k++;
             $repu1->MoveNext();
          }
       // Create the graph. These two calls are always required
$graph = new Graph(700,800,'auto');
$graph->SetScale("intint");
$graph->yaxis->scale->SetGrace(5);
 
//$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());

        
$aux=min($datay);
$x=1;
if($aux==1)
    {$aux=0;}
for($j=0;$j<$i;$j++)
{
    $arreglox[$j]=$aux;
    $arregloy[$j]=$aux;
    $aux++;
}

$graph->yaxis->SetTickPositions($arreglox, $arregloy);
$graph->SetBox(true);
$graph->xaxis->scale->ticks->Set(1,1);
$graph->yaxis->scale->ticks->Set(1,1);
//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels($etique);
$graph->xaxis->SetLabelAngle(90);
$graph->xaxis->SetTextLabelInterval(1);

$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);
//$graph->title->SetFont(20); 
// Create the bar plots
$b1plot = new BarPlot($datay);
$graph->xaxis->setFont(FF_ARIAL,FS_BOLD,8);
// ...and add it to the graPH
$graph->Add($b1plot);
$graph->img->setMargin(40,20,60,75);

$graph->xaxis->title->Set('Detalle PQR');
$graph->yaxis->title->Set('Cantidad');
$graph->yaxis->SetTitleMargin(25);
$b1plot->SetColor("white");
$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION,50);
$b1plot->SetWidth(40);
$graph->title->Set("Reporte PQRs top10");
$graph->Stroke('imagen1.png'); 
    
}
elseif($_GET['tipo']==4){
     $tabla;
    $titulo="PQRs Consolidado Por Aerolineas";
     $t=0;
     $i=1;
      $tri1=0;
          $tri2=0;
          $tri3=0;
          $tri4=0;
      $tabla = "<table width='100%' id='table27' bgcolor='#FFFFFF'>";
      $tabla.= "<tr class='titulos3'><td>No</td><td width='150px'>Causal</td><td>Enero</td><td>Febrero</td><td>Marzo</td><td>Trimestre Uno</td><td>Abril</td><td>Mayo</td><td>Junio</td><td>Trimestre Dos</td><td>Julio</td><td>Agosto</td><td>Septiembre</td><td>trimestre Tres</td><td>Octubre</td><td>Noviembre</td><td>Diciembre</td><td>Trimestre Cuatro</td><td>Total</td></tr>";
      
  $repu = $db->conn->Execute($_SESSION['conA']);
       while(!$repu->EOF)
            {
           
              
                
             $fila.="<tr class='listado1'>";
             $fila.="<td>".$i."</td>";  
             $fila.="<td>".$repu->fields['CAUSAL']."</td>";
             $fila.="<td>".$repu->fields['ENERO']."</td>";  
             $fila.=" <td>".$repu->fields['FEBRERO']."</td>";   
             $fila.=" <td>".$repu->fields['MARZO']."</td>"  ; 
             $fila.="<td>".($repu->fields['ENERO']+$repu->fields['FEBRERO']+$repu->fields['MARZO'])."</td>";  
             $fila.="<td>".$repu->fields['ABRIL']."</td>" ;   
             $fila.=" <td> ".$repu->fields['MAYO']."</td>" ;  
             $fila.=" <td> ".$repu->fields['JUNIO']."</td>" ;  
             $fila.=" <td> ".($repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO'])."</td>" ;  
             $fila.="<td> ".$repu->fields['JULIO']."</td>";  
             $fila.="<td> ".$repu->fields['AGOSTO']."</td>";
             $fila.="<td> ".$repu->fields['SEPTIEMBRE']."</td>"  ; 
             $fila.="<td>".($repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE'])."</td>";  
             $fila.=" <td>".$repu->fields['OCTUBRE'] ."</td>"  ; 
             $fila.=" <td>".$repu->fields['NOVIEMBRE']."</td>";  
             $fila.=" <td>".$repu->fields['DICIEMBRE']."</td>";   
             $fila.=" <td>".($repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'])."</td>" ; 
             $fila.="<td>".($repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE']+$repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO']+$repu->fields['FEBRERO']+$repu->fields['MARZO']+$repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE']+$repu->fields['JULIO']+$repu->fields['ENERO'])."</td>";  
           
              
              $i++;
              $t += $repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE']+$repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO']+$repu->fields['FEBRERO']+$repu->fields['MARZO']+$repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'];
              $tri1 += $repu->fields['ENERO']+$repu->fields['FEBRERO']+$repu->fields['MARZO'];
              $tri2 += $repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO'];
              $tri3 += $repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE'];
              $tri4 += $repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'];
              $repu->MoveNext();
               $fila.=" </tr>";
                $tabla.=$fila; 
                $fila = "";
            }
            
            $repu2 = $db->conn->Execute($_SESSION['totalA']);
     
               while(!$repu2->EOF)
            {
                       $tabla.="<tr class='listado1'>";
           
             $fila.="<td> </td>";
             $fila.="<td class='titulos3'> TOTAL PQR's AEROLINEAS</td>";
             $fila.="<td>".(($repu2->fields['ENERO']=='')?0:$repu2->fields['ENERO'])."</td>";
             $fila.="<td>".(($repu2->fields['FEBRERO']=='')?0:$repu2->fields['FEBRERO'])."</td>";
             $fila.="<td>".(($repu2->fields['MARZO']=='')?0:$repu2->fields['MARZO'])."</td>";
             $fila.="<td>".($tri1)."</td>";
             $fila.="<td>".(($repu2->fields['MAYO']=='')?0:$repu2->fields['MAYO'])."</td>";
             $fila.="<td>".(($repu2->fields['ABRIL']=='')?0:$repu2->fields['ABRIL'])."</td>";
             $fila.="<td>".(($repu2->fields['JUNIO']=='')?0:$repu2->fields['JUNIO'])."</td>"; 
             $fila.="<td>". ($tri2)."</td>";
             $fila.="<td>".(($repu2->fields['JULIO']=='')?0:$repu2->fields['JULIO'])."</td>"; 
             $fila.=" <td>".(($repu2->fields['AGOSTO']=='')?0:$repu2->fields['AGOSTO'])."</td>";
             $fila.="<td>".(($repu2->fields['SEPTIEMBRE']=='')?0:$repu2->fields['SEPTIEMBRE'])."</td>"; 
             $fila.="<td>".($tri3)."</td>"; 
             $fila.="<td>".(($repu2->fields['OCTUBRE']=='')?0:$repu2->fields['OCTUBRE'])."</td>";
             $fila.="<td>".(($repu2->fields['NOVIEMBRE']=='')?0:$repu2->fields['NOVIEMBRE'])."</td>"; 
             $fila.="<td>".(($repu2->fields['DICIEMBRE']=='')?0:$repu2->fields['DICIEMBRE'])."</td>";
             $fila.="<td>".($tri4)."</td>";
             $fila.="<td>".($t)."</td>"; 
         
              
              $i++;
              $repu2->MoveNext();
               $fila.="</tr>"; 
               $tabla.=$fila;
               $fila="";
            }   
             
     $tabla.="</table>";

            
           }
           
           
      elseif($_GET['tipo']==5)
          {   $fila="";
              $titulo="PQRs Consolidado General ";
              $t1=0;
             $t=0;
             $trimopa1=0;
             $trimopa2=0;
             $trimopa3=0;
             $trimopa4=0;
             $trimnopa1=0;
             $trimnopa2=0;
             $trimnopa3=0;
             $trimnopa4=0;
             $genetocausal="SELECT * FROM SGD_CAU_CAUSAL WHERE  SGD_CAU_CODIGO != 1 and perentidad != 0 and perentidad != 0";
             $res = $db->conn->Execute($genetocausal);
             $tabla = "<table width='100%' id='table27' bgcolor='#FFFFFF'>";
             $tabla.= "<tr class='titulos3'><td>No</td><td width='150px'>Causal</td><td>Enero</td><td>Febrero</td><td>Marzo</td><td>Trimestre Uno</td><td>Abril</td><td>Mayo</td><td>Junio</td><td>Trimestre Dos</td><td>Julio</td><td>Agosto</td><td>Septiembre</td><td>trimestre Tres</td><td>Octubre</td><td>Noviembre</td><td>Diciembre</td><td>Trimestre Cuatro</td><td>Total</td></tr>";
               $i=1;
           while(!$res->EOF)
           {
                $general="SELECT * FROM crosstab ('SELECT ca.sgd_cau_descrip,extract(month from r.radi_fech_radi) as mes,count(r.radi_nume_radi) AS TOTAL
                FROM RADICADO r INNER JOIN PAR_SERV_SERVICIOS p ON r.PAR_SERV_SECUE = p.PAR_SERV_SECUE 
                INNER JOIN MEDIO_RECEPCION m ON m.mrec_codi = r.mrec_codi
                INNER JOIN DEPENDENCIA d ON d.depe_codi = r.radi_depe_actu
                INNER JOIN USUARIO u ON u.usua_codi = r.radi_usua_actu and u.depe_codi = r.radi_depe_actu 
                FULL JOIN SGD_CAUX_CAUSALES cu ON r.radi_nume_radi = cu.radi_nume_radi 
                FULL JOIN SGD_DCAU_CAUSAL dc ON dc.sgd_dcau_codigo = cu.sgd_dcau_codigo 
                FULL JOIN SGD_CAU_CAUSAL ca ON ca.sgd_cau_codigo = dc.sgd_cau_codigo 
                WHERE CAST(r.radi_nume_radi AS char) like ''%2'' and extract(year from r.radi_fech_radi) = ".$_GET['anio']." and ca.sgd_cau_codigo=".$res->fields['SGD_CAU_CODIGO']."
                GROUP BY 1,mes,dc.sgd_cau_codigo ORDER BY dc.sgd_cau_codigo','select * from generate_series(1,12)')
                as (Causal varchar, Enero int, Febrero int ,Marzo  int, Abril int ,Mayo int ,Junio int, Julio int,  Agosto int,septiembre int, Octubre int, Noviembre int, Diciembre int)
          ";
               $t=0;  
               $repu = $db->conn->Execute($general);  
                 while(!$repu->EOF)
            {
                 $t=($repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE']+$repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO']+$repu->fields['FEBRERO']+$repu->fields['MARZO']+$repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE']);
                 $fila .="<tr class='listado1'>";
                 $fila.="<td>".$i."</td>";
                 $fila.="<td>".$repu->fields['CAUSAL']."</td>";
                 $fila.="<td>".$repu->fields['ENERO']."</td>";
                 $fila.="<td>".$repu->fields['FEBRERO']."</td>";
                 $fila.="<td>".$repu->fields['MARZO']."</td>";
                 $fila.="<td>".($repu->fields['ENERO']+$repu->fields['FEBRERO']+$repu->fields['MARZO'])."</td>";
                 $fila.="<td>".$repu->fields['ABRIL']."</td>";
                 $fila.="<td>".$repu->fields['MAYO']."</td>";
                 $fila.="<td>".$repu->fields['JUNIO']."</td>";
                 $fila.="<td>".($repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO'])."</td>";
                  $fila.="<td>".$repu->fields['JULIO']."</td>";
                 $fila.="<td>".$repu->fields['AGOSTO']."</td>";
                 $fila.="<td>".$repu->fields['SEPTIEMBRE']."</td>";
                 $fila.="<td>".($repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".$repu->fields['OCTUBRE']."</td>";
                 $fila.="<td>".$repu->fields['NOVIEMBRE']."</td>";
                 $fila.="<td>".$repu->fields['DICIEMBRE']."</td>";
                 $fila.="<td>".($repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($t)."</td>";
                 $fila.="</tr>";
              
              $i++;
               $tabla.=$fila;
                $fila = "";
              $repu->MoveNext();
              if($res->fields['PERENTIDAD']==1)
                { 
                   $trimopa1+=$repu->fields['ENERO']+$repu->fields['FEBRERO']+$repu->fields['MARZO'];
                   $trimopa2+=$repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO'];
                   $trimopa3+=$repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE'] ;
                   $trimopa4+=$repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'];
                }
                else{
                    $trimnopa1+=$repu->fields['ENERO']+$repu->fields['FEBRERO']+$repu->fields['MARZO'];
                    $trimnopa2+=$repu->fields['ABRIL']+$repu->fields['MAYO']+$repu->fields['JUNIO'];
                    $trimnopa3+=$repu->fields['JULIO']+$repu->fields['AGOSTO']+$repu->fields['SEPTIEMBRE'] ;
                    $trimnopa4+=$repu->fields['OCTUBRE']+$repu->fields['NOVIEMBRE']+$repu->fields['DICIEMBRE'];
                } 
               
               }
               $fila = "";
                $res->MoveNext();
                  $fila = "";
            }

         $repu1 = $db->conn->Execute($_SESSION['total']);
             while(!$repu1->EOF)
            {
            
                 $fila ="<tr class='listado1'>";
                 $fila.="<td></td>";
                 $fila.="<td class='titulos3'> APLICAN A OPAIN</td>";
                 $fila.="<td>".(($repu1->fields['ENERO']=='')?0:$repu1->fields['ENERO'])."</td>";
                 $fila.="<td>".(($repu1->fields['FEBRERO']=='')?0:$repu1->fields['FEBRERO'])."</td>";
                 $fila.="<td>".(($repu1->fields['MARZO']=='')?0:$repu1->fields['MARZO'])."</td>";
                 $fila.="<td>".$trimopa1."</td>";
                 $fila.="<td>".(($repu1->fields['ABRIL']=='')?0:$repu1->fields['ABRIL'])."</td>";
                 $fila.="<td>".(($repu1->fields['MAYO']=='')?0:$repu1->fields['MAYO'])."</td>";
                 $fila.="<td>".(($repu1->fields['JUNIO']=='')?0:$repu1->fields['JUNIO'])."</td>";
                 $fila.="<td>".$trimopa2."</td>";
                 $fila.="<td>".(($repu1->fields['JULIO']=='')?0:$repu1->fields['JULIO'])."</td>";
                 $fila.="<td>".(($repu1->fields['AGOSTO']=='')?0:$repu1->fields['AGOSTO'])."</td>";
                 $fila.="<td>".(($repu1->fields['SEPTIEMBRE']=='')?0:$repu1->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".$trimopa3."</td>";
                 $fila.="<td>".(($repu1->fields['OCTUBRE']=='')?0:$repu1->fields['OCTUBRE'])."</td>";
                 $fila.="<td>".(($repu1->fields['NOVIEMBRE']=='')?0:$repu1->fields['NOVIEMBRE'])."</td>";
                 $fila.="<td>".(($repu1->fields['DICIEMBRE']=='')?0:$repu1->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".$trimopa4."</td>";
                 $fila.="<td>".($repu1->fields['JULIO']+$repu1->fields['AGOSTO']+$repu1->fields['SEPTIEMBRE']+$repu1->fields['ABRIL']+$repu1->fields['MAYO']+$repu1->fields['JUNIO']+$repu1->fields['FEBRERO']+$repu1->fields['MARZO']+$repu1->fields['OCTUBRE']+$repu1->fields['NOVIEMBRE']+$repu1->fields['DICIEMBRE'])."</td>";
                 $fila.="</tr>";
              
              $i++;
               $tabla.=$fila;
              $fila="";
              $repu1->MoveNext();
             
            }
            $repu3 = $db->conn->Execute($_SESSION['total1']);               
             while(!$repu3->EOF)
            {
            
                 $fila ="<tr class='listado1'>";
                 $fila.="<td> </td>";
                 $fila.="<td class='titulos3'> NO APLICAN OPAIN </td>";
                 $fila.="<td>".(($repu3->fields['ENERO']=='')?0:$repu3->fields['ENERO'])."</td>";
                 $fila.="<td>".(($repu3->fields['FEBRERO']=='')?0:$repu3->fields['FEBRERO'])."</td>";
                 $fila.="<td>".(($repu3->fields['MARZO']=='')?0:$repu3->fields['MARZO'])."</td>";
                 $fila.="<td>".($repu3->fields['ENERO']+$repu3->fields['FEBRERO']+$repu3->fields['MARZO'])."</td>";
                 $fila.="<td>".(($repu3->fields['ABRIL']=='')?0:$repu3->fields['ABRIL'])."</td>";
                 $fila.="<td>".(($repu3->fields['MAYO']=='')?0:$repu3->fields['MAYO'])."</td>";
                 $fila.="<td>".(($repu3->fields['JUNIO']=='')?0:$repu3->fields['JUNIO'])."</td>";
                 $fila.="<td>".($repu3->fields['ABRIL']+$repu3->fields['MAYO']+$repu3->fields['JUNIO'])."</td>";
                 $fila.="<td>".(($repu3->fields['JULIO']=='')?0:$repu3->fields['JULIO'])."</td>";
                 $fila.="<td>".(($repu3->fields['AGOSTO']=='')?0:$repu3->fields['AGOSTO'])."</td>";
                 $fila.="<td>".(($repu3->fields['SEPTIEMBRE']=='')?0:$repu3->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".($repu3->fields['JULIO']+$repu3->fields['AGOSTO']+$repu3->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".(($repu3->fields['OCTUBRE']=='')?0:$repu3->fields['OCTUBRE'])."</td>";
                 $fila.="<td>".(($repu3->fields['NOVIEMBRE']=='')?0:$repu3->fields['NOVIEMBRE'])."</td>";
                 $fila.="<td>".(($repu3->fields['DICIEMBRE']=='')?0:$repu3->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($repu3->fields['OCTUBRE']+$repu3->fields['NOVIEMBRE']+$repu3->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($repu3->fields['JULIO']+$repu3->fields['AGOSTO']+$repu3->fields['SEPTIEMBRE']+$repu3->fields['ABRIL']+$repu3->fields['MAYO']+$repu3->fields['JUNIO']+$repu3->fields['FEBRERO']+$repu3->fields['MARZO']+$repu3->fields['OCTUBRE']+$repu3->fields['NOVIEMBRE']+$repu3->fields['DICIEMBRE'])."</td>";
                 $fila.="</tr>";
              $tabla.=$fila;
              $fila="";
              $i++;
              $repu3->MoveNext();
              
            }
          
           $repu5 = $db->conn->Execute($_SESSION['totalaero']);
                while(!$repu5->EOF){
                $l=($repu5->fields['JULIO']+$repu5->fields['AGOSTO']+$repu5->fields['SEPTIEMBRE']+$repu5->fields['ABRIL']+$repu5->fields['MAYO']+$repu5->fields['JUNIO']+$repu5->fields['FEBRERO']+$repu5->fields['MARZO']+$repu5->fields['OCTUBRE']+$repu5->fields['NOVIEMBRE']+$repu5->fields['DICIEMBRE']);

                 $fila ="<tr class='listado1'>";
                 $fila.="<td> </td>";
                 $fila.="<td class='titulos3'> TOTAL PQR's AEROLINEAS</td>";
                 $fila.="<td>".(($repu5->fields['ENERO']=='')?0:$repu5->fields['ENERO'])."</td>";
                 $fila.="<td>".(($repu5->fields['FEBRERO']=='')?0:$repu5->fields['FEBRERO'])."</td>";
                 $fila.="<td>".(($repu5->fields['MARZO']=='')?0:$repu5->fields['MARZO'])."</td>";
                 $fila.="<td>".($repu5->fields['ENERO']+$repu5->fields['FEBRERO']+$repu5->fields['MARZO'])."</td>";
                 $fila.="<td>".(($repu5->fields['ABRIL']=='')?0:$repu5->fields['ABRIL'])."</td>";
                 $fila.="<td>".(($repu5->fields['MAYO']=='')?0:$repu5->fields['MAYO'])."</td>";
                 $fila.="<td>".(($repu5->fields['JUNIO']=='')?0:$repu5->fields['JUNIO'])."</td>";
                 $fila.="<td>".($repu5->fields['ABRIL']+$repu5->fields['MAYO']+$repu5->fields['JUNIO'])."</td>";
                 $fila.="<td>".(($repu5->fields['JULIO']=='')?0:$repu5->fields['JULIO'])."</td>";
                 $fila.="<td>".(($repu5->fields['AGOSTO']=='')?0:$repu5->fields['AGOSTO'])."</td>";
                 $fila.="<td>".(($repu5->fields['SEPTIEMBRE']=='')?0:$repu5->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".($repu5->fields['JULIO']+$repu5->fields['AGOSTO']+$repu5->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>".(($repu5->fields['OCTUBRE']=='')?0:$repu5->fields['OCTUBRE'])."</td>";
                 $fila.="<td>".(($repu5->fields['NOVIEMBRE']=='')?0:$repu5->fields['NOVIEMBRE'])."</td>";
                 $fila.="<td>".(($repu5->fields['DICIEMBRE']=='')?0:$repu5->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($repu5->fields['OCTUBRE']+$repu5->fields['NOVIEMBRE']+$repu5->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".$l."</td>";
             $fila.="</tr>";
                 $tabla.=$fila;
                $fila="";
                $repu5->MoveNext();
                
                }
                
                  $repu4 = $db->conn->Execute($_SESSION['totalgeneral']);
            while(!$repu4->EOF)
            {
           
                 $fila ="<tr class='listado1'>";
                 $fila.="<td> </td>";
                 $fila.="<td class='titulos3'> TOTAL </td>";
                 $fila.="<td>" .(($repu4->fields['ENERO']=='')?0:$repu4->fields['ENERO'])."</td>";
                 $fila.="<td>" .(($repu4->fields['FEBRERO']=='')?0:$repu4->fields['FEBRERO'])."</td>";
                 $fila.="<td>" .(($repu4->fields['MARZO']=='')?0:$repu4->fields['MARZO'])."</td>";
                 $fila.="<td>".($repu4->fields['ENERO']+$repu4->fields['FEBRERO']+$repu4->fields['MARZO'])."</td>";
                 $fila.="<td>".(($repu4->fields['ABRIL']=='')?0:$repu4->fields['ABRIL'])."</td>";
                 $fila.="<td>" .(($repu4->fields['MAYO']=='')?0:$repu4->fields['MAYO'])."</td>";
                 $fila.="<td>".(($repu4->fields['JUNIO']=='')?0:$repu4->fields['JUNIO'])."</td>";
                 $fila.="<td>" .($repu4->fields['ABRIL']+$repu4->fields['MAYO']+$repu4->fields['JUNIO'])."</td>";
                 $fila.="<td>" .(($repu4->fields['JULIO']=='')?0:$repu4->fields['JULIO'])."</td>";
                 $fila.="<td>" .(($repu4->fields['AGOSTO']=='')?0:$repu4->fields['AGOSTO'])."</td>";
                 $fila.="<td>" .(($repu4->fields['SEPTIEMBRE']=='')?0:$repu4->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>" .($repu4->fields['JULIO']+$repu4->fields['AGOSTO']+$repu4->fields['SEPTIEMBRE'])."</td>";
                 $fila.="<td>" .(($repu4->fields['OCTUBRE']=='')?0:$repu4->fields['OCTUBRE']). "</td>";
                 $fila.="<td>" .(($repu4->fields['NOVIEMBRE']=='')?0:$repu4->fields['NOVIEMBRE'])."</td>";
                 $fila.="<td>" .(($repu4->fields['DICIEMBRE']=='')?0:$repu4->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($repu4->fields['OCTUBRE']+$repu4->fields['NOVIEMBRE']+$repu4->fields['DICIEMBRE'])."</td>";
                 $fila.="<td>".($repu4->fields['JULIO']+$repu4->fields['AGOSTO']+$repu4->fields['SEPTIEMBRE']+$repu4->fields['ABRIL']+$repu4->fields['MAYO']+$repu4->fields['JUNIO']+$repu4->fields['FEBRERO']+$repu4->fields['MARZO']+$repu4->fields['OCTUBRE']+$repu4->fields['NOVIEMBRE']+$repu4->fields['DICIEMBRE'])."</td>";
             $fila.="</tr>";
              
              $i++;
              $tabla.=$fila;
              $fila="";
              $repu4->MoveNext();
              
            }
          
             $tabla.="</table>";
         
      
          
          
          
          
          
          }     
      
      
    
    
    
    


$dompdf = new DOMPDF();
$fechageneracion=date('Y-m-d H:i:s');

$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>  
  <link rel='stylesheet' href='../estilos/orfeo.css'>
 <link rel='stylesheet' type='text/css' href='../estilos/style.css'/>
  <link rel='stylesheet' type='text/css' href='../estilos/style.css'/>
</head>
<body>
<div>
<center>
<p>$entidad_largo</p>
<p> $titulo </p>";
if($_GET['tipo'] < 4){
  $html.="<p>Periodo Reportado: ".$_GET['fi']." Hasta ".$_GET['ff']."</p>";  
}
else{
    $html.="<p>Periodo Reportado:".$_GET['anio']."</p>";

}
$html.="</center>
<p>Generado por: ".utf8_decode($_SESSION["usua_nomb"])."</p>
<p>Dependencia: ".utf8_decode($_SESSION["depe_nomb"])."</p>
<p>Fecha de generaci&oacute;n: $fechageneracion <p> 
<p></p>
</div>
<center>";
if($_GET['tipo']< 4 ){
 
$html.="<img  width='400px'  height='400px' src='imagen1.png'/>";

} 

$html.="</center>
</div>
<div>
$tabla
</div>
</body>
</html>";
echo $html;
/*$dompdf->load_html($html);
$dompdf->set_paper("letter","landscape");
$dompdf->render();
$dompdf->stream('sample1.pdf');
 shell_exec('chmod -R 777 PdfReportes.php');*/
?>

