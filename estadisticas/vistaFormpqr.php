<?
//phpinfo();
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinz�n L�pez --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */ 
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeaci�n"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/
    
$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
if(!$tipoCarpOld) $tipoCarpOld= $tipo_carpt;
session_start();
if(!$krd) $krd=$krdOsld;
$ruta_raiz = "..";
//include "$ruta_raiz/rec_session.php";
error_reporting(7);
if(!$_GET['Estadistica']) $_GET['Estadistica'] =1;
if(!$dependencia_busq) $dependencia_busq =$_SESSION['dependencia'];
/** DEFINICION DE VARIABLES ESTADISTICA
	*	var $tituloE String array  Almacena el titulo de la Estadistica Actual
	* var $subtituloE String array  Contiene el subtitulo de la estadistica
	* var $helpE String Almacena array Almacena la descripcion de la Estadistica.
	*/
	//$tituloE[1] = "RADICACION - CONSULTA DE RADICADOS POR USUARIO";
	//$tituloE[2] = "RADICACION - ESTADISTICAS POR MEDIO DE RECEPCION-ENVIO";
	//$tituloE[3] = "RADICACION - ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS";
	//$tituloE[4] = "RADICACION - ESTADISTICAS DE DIGITALIZACION DE DOCUMENTOS";
	//$tituloE[5] = "RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA";
	//$tituloE[6] = "RADICADOS ACTUALES EN LA DEPENDENCIA";
	//$tituloE[7] = "ESTADISTICAS DE NUMERO DE DOCUMENTOS ENVIADOS";	  	  
	//$tituloE[8] = "REPORTE DE VENCIMIENTOS";	  	  
	//$tituloE[9] = "REPORTE PROCESO RADICADOS DE ENTRADA";
	//$tituloE[10] = "REPORTE ASIGNACION RADICADOS";
	//$tituloE[11] = "ESTADId jsSTICAS DE DIGITALIZACION";
	//$tituloE[12] = "DOCUMENTOS RETIPIFICADOS POR TRD";
	//$tituloE[13] = "EXPEDIENTES POR DEPENDENCIA";
	//$tituloE[14] = "REPORTE DE RADICADOS ASIGNADOS DETALLADOS (CARPETAS PERSONALES)";
        $tituloE[15] = "REPORTE PQR";

	$subtituloE[1] = "ORFEO - Generada el: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[2] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[3] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[4] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[5] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[6] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[8] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";  
	
	$helpE[1] = "Este reporte genera la cantidad de radicados por usuario. Se puede discriminar por tipo de radicaci&oacute;n. " ;
	$helpE[2] = "Este reporte genera la cantidad de radicados de acuerdo al medio de recepci&oacute;n o envio realizado al momento de la radicaci&oacute;n. " ;
	$helpE[3] = "Este reporte genera la cantidad de radicados enviados a su destino final por el &aacute;rea.  " ;
	$helpE[4] = "Este reporte genera la cantidad de radicados digitalizados por usuario y el total de hojas digitalizadas. Se puede seleccionar el tipo de radicaci&oacute;n." ;
	$helpE[5] = "Este reporte genera la cantidad de documentos de entrada radicados del &aacute;rea de correspondencia a una dependencia. " ;
	$helpE[6] = "Esta estadistica trae la cantidad de radicados \n generados por usuario, se puede discriminar por tipo de Radicacion. " ;
	$helpE[8] = "Este reporte genera la cantidad de radicados de entrada cuyo vencimiento esta dentro de las fechas seleccionadas. " ;
	$helpE[9] = "Este reporte muestra el proceso que han tenido los radicados tipo 2 que ingresaron durante las fechas seleccionadas. ";
	$helpE[10] = "Este reporte muestra cuantos radicados de entrada han sido asignados a cada dependencia. ";
	$helpE[11] = "Muestra la cantidad de radicados digitalizados por usuario y el total de hojas digitalizadas. Se puede seleccionar el tipo de radicaci&oacute;n y la fecha de digitalizaci&oacute;n." ;
	$helpE[12] = "Muestra los radicados que ten&iacute;an asignados un tipo documental(TRD) y han sido modificados";
	$helpE[13] = "Muestra todos los expedientes agrupados por dependencia que con el n&uacute;mero de radicados totales";
	$helpE[14] = "Muestra el total de radicados que tiene un usuario y 
				el detalle del radicado con respecto al Remitente(Detalle), 
				Predio(Detalle), ESP(Detalle) ";
    $helpE[15] = "Este reporte genera la cantidad de radicados con las diferentes pqr para cada dependencia.";
?>	  
<html>
<head>
<title>principal</title>
<link rel="stylesheet" href="../estilos/orfeo.css">
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
 <link rel="stylesheet" type="text/css" href="../estilos/style.css"/>
<link   rel="stylesheet" type="text/css" href="../estilos/jquery-ui.css">
<script type="text/javascript" src="../js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="../js/jquery-1ui.js"></script>
<script type='text/javascript' src='../js/codigo.js'></script>

<script>
    
    $(function(){
        $("#calen3").hide();
      
    });
  

    $(function(){
 
        $("#tipodereporte").change(function()
        {
            if($("#tipodereporte").val() == 5 || $("#tipodereporte").val()== 6)
            {   
                $("#calen2").hide();
                $("#calen1").hide();
                $("#calen3").show();
            }
         
            else{
                 $("#calen3").hide();
                  $("#calen2").show();
                $("#calen1").show();
            }
        }
    );
    
    });
    
    $(function(){
        debugger;
     
        $("#tipoEstadistica").change(function(){
             var estadistica = $("#tipoEstadistica").val();
             window.location.href="vistaFormConsulta.php?Estadistica="+estadistica;
        });
    });
   
 
 
  

function generar(){

 //var indice = document.formulario.tipodereporte.selectedIndex
            var tp=document.getElementById('tipodereporte').value;
            var dp=document.getElementById('dependencia').value;
            //var us=document.getElementById('codus').value;
          //  var tpr =   document.formulario.tipoRadicado.value;
            var pqr = document.formulario.tipoPQR.value;
         // var cau = document.formulario.tipoCAU.value;
            var fchi = document.formulario.fechainicial.value;
            var fchf = document.formulario.fechafinal.value;
      
            var anio = document.getElementById('year').value
	    var cadena="EstadisticasPQR.php?tipo="+tp+"&depe="+dp+"&pqrs="+pqr+"&fi="+fchi+"&ff="+fchf+"&year="+anio+"";	
            llamarasincrono(cadena, 'reporcontec');                 
}
</script>

<script>
  // $(function() {$("#datepicker" ).datepicker();});
   
 //$(function(){$('#datepickers').datepicker();});
   
function adicionarOp (forma,combo,desc,val,posicion){
	o = new Array;
	o[0]=new Option(desc,val );
	eval(forma.elements[combo].options[posicion]=o[0]);
	//alert ("Adiciona " +val+"-"+desc );
	
}
</script>
		 <script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
		  
		 <script language="javascript">
		 <!--
			<?
				date("m")==1?$ano_ini = date("Y")-1:$ano_ini = date("Y");
				$mes_ini = substr("00".(date("m")-1),-2);
				if ($mes_ini==0) {$ano_ini==$ano_ini-1; $mes_ini="12";}
				$dia_ini = date("d");
				if(!$fecha_ini) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
					$fecha_busq = date("Y/m/d") ;
				if(!$fecha_fin) $fecha_fin = $fecha_busq;
			?>
   var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "formulario", "fechainicial","btnDate1","<?=$fecha_ini?>",scBTNMODE_CUSTOMBLUE);
   var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "formulario", "fechafinal","btnDate2","<?=$fecha_fin?>",scBTNMODE_CUSTOMBLUE);

//--></script>
</head>
<?
error_reporting(0);
include "$ruta_raiz/envios/paEncabeza.php";
?>
<table><tr><TD></TD></tr></table>
<?
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include("$ruta_raiz/class_control/usuario.php");
$db = new ConnectionHandler($ruta_raiz);

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$objUsuario = new Usuario($db);
?>
<body bgcolor="#ffffff"  topmargin="0">
    <div id="norecargo">
    <div id="resultado"></div>
<div id="spiffycalendar" class="text"></div>
<form name="formulario"  method=post action='./vistaFormConsulta.php?<?=session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah"?>'>
<input name="feo" type="hidden" value="repor"> 
<table width="100%"  border="0" cellpadding="0" cellspacing="5" class="borde_tab">
  <tr>
    <td colspan="2" class="titulos4">POR ESTADISTICAS GENERALES 
        
         
                  <A href='vistaFormConsulta.php?<?=session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah"?>' style="color: #FFFFCC">Estadisticas </A> 
    
    </td>


  </tr>
  
  
  <tr>
    <td colspan="2" class="titulos3"><span class="cal-TextBox"><?=$helpE[$_GET['Estadistica']]?></span></td>
  </tr>
  <tr>
    <td width="30%" class="titulos2">Tipo de Consulta / Estadistica</td>
 <td class="listado2" align="left">
	   <select id="tipoEstadistica" name=tipoEstadistica  class="select">
		<?php   	
			foreach($tituloE as $key=>$value)
			{
		?>
	   <?php if($_GET['Estadistica']==$key) $selectE = " selected "; else $selectE = ""; ?>
			<option value=<?=$key?> <?=$selectE?>><?=$tituloE[$key]?></option>
		<?php
		}
		?>
	 </select>
	</td>
   </tr> 
   
	<tr>
            <td width="30%" class="titulos2" >
                Tipos de Reporte
             </td>
             <td>
                 <select name="tipodereporte" id="tipodereporte" >
                  
                    <option value="1">
                        Por PQR
                    </option>
                
                 
                </select>
             </td>
        </tr>
   
    <tr>
    <td width="30%" class="titulos2">Dependencia</td>
    <td class="listado2">
	<select id="dependencia" name=dependencia_busq  class="select">
<?
	if($usua_perm_estadistica>1)  {
		if($dependencia_busq==99999 )  {
			$datoss= " selected ";
		}
		?>
			<option value=99999  <?=$datoss?>>-- Todas las Dependencias --</option>
		<?
	}
    if($usua_perm_estadistica>1 && $_GET['Estadistica']==15)  {
		if($dependencia_busq==99998)  {
			$datoss1= " selected ";
		}
		?>
			<option value=99998  <?=$datoss1?>>-- Agrupar por Todas las Dependencias --</option>
		<?
	}

$whereDepSelect=" DEPE_CODI = $dependencia ";
if ($usua_perm_estadistica==1){
	$whereDepSelect=" $whereDepSelect or depe_codi_padre = $dependencia ";	
}
if ($usua_perm_estadistica==2) {
	$isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a ORDER BY a.DEPE_NOMB";
}
else {
//$whereDepSelect=
	$isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a 
						where $whereDepSelect ";
}
	//if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario "; 
//echo "--->".$isqlus;
$rs1=$db->query($isqlus);

do{
	$codigo = $rs1->fields["DEPE_CODI"]; 
	$vecDeps[]=$codigo;
	$depnombre = $rs1->fields["DEPE_NOMB"];
	$datoss="";
	if($dependencia_busq==$codigo){
		$datoss= " selected ";
	}
	echo "<option value=$codigo  $datoss>$depnombre</option>";		
	$rs1->MoveNext();
}while(!$rs1->EOF);
	?>
	</select>
</td>
</tr>
<?
		if ($dependencia_busq != 99999)  {
			$whereDependencia = " AND DEPE_CODI=$dependencia_busq ";
		}

if($_GET['Estadistica']==1 or $_GET['Estadistica']==2 or $_GET['Estadistica']==3 or 
	$_GET['Estadistica']==4 or $_GET['Estadistica']==5 or $_GET['Estadistica']==6 or 
	$_GET['Estadistica']==7 or $_GET['Estadistica']==11 or $_GET['Estadistica']==12 or $_GET['Estadistica']==15)
{
?>
<?php
  //if(!$_POST['tipodereporte'])
  //{
?>

  <?
  //}
}
  if($_GET['Estadistica']==1 or $_GET['Estadistica']==2 or $_GET['Estadistica']==3 or 
  		$_GET['Estadistica']==4 or $_GET['Estadistica']==6 or $_GET['Estadistica']==11 or 
  		$_GET['Estadistica']==12 or $_GET['Estadistica']==15)
  {
  ?>
  
   <?
  }
  if($_GET['Estadistica']==1 or $_GET['Estadistica']==6 or $_GET['Estadistica']==10 or 
  	$_GET['Estadistica']==12 or $_GET['Estadistica'] == 14) {
  ?>
  <tr id="tipopqrJ">
	<td width="30%" height="40" class="titulos2">Tipo de PQR </td>
	<td class="listado2">
	<?
		$rs = $db->conn->Execute('select PAR_SERV_NOMBRE ,PAR_SERV_SECUE  from PAR_SERV_SERVICIOS order by 2');
		print $rs->GetMenu2("tipoPQR", $tipoPQR,"0:-- Agrupar por tipo de PQR --",false,0,'class=select');
	?>&nbsp;
    </td>
</tr>

<?}
if($_GET['Estadistica']==9)
  {
  ?>
<tr>
	<td width="30%" height="40" class="titulos2">SERIE </td>
	<td class="listado2">
	<?
		$rs = $db->conn->Execute('select sgd_srd_descrip ,sgd_srd_codigo  from sgd_srd_seriesrd order by 1');
		print $rs->GetMenu2("idSerie", $idSerie,"0:-- Agrupar por SERIE --",false,0,'class=select');
	?>&nbsp;
    </td>
</tr>
<?}
  if($_GET['Estadistica']==1 or $_GET['Estadistica']==2 or $_GET['Estadistica']==3 or 
  		$_GET['Estadistica']==4 or $_GET['Estadistica']==5 or $_GET['Estadistica']==7 or 
  		$_GET['Estadistica']==8 or $_GET['Estadistica']==9 or $_GET['Estadistica']==10 or 
  		$_GET['Estadistica']==11 or $_GET['Estadistica']==12 or $_GET['Estadistica']==14 or $_GET['Estadistica']==15)
  {
  ?>  
  <tr id="calen2">
    <td width="30%" class="titulos2">Desde fecha (aaaa/mm/dd) </td>
    <td class="listado2">
    <!--    <input type="text" id="datepicker" name='fechainicial'placeholder="2013/01/01" value='<?php echo date('Y-m-d', strtotime('-1 month'))?>'/> -->
	<script language="javascript">
	dateAvailable.writeControl();
	dateAvailable.dateFormat="yyyy/MM/dd";
	</script> 
	&nbsp;
  </td>
  </tr>
  <tr id="calen1">
    <td width="30%" class="titulos2">Hasta  fecha (aaaa/mm/dd) </td>
    <td class="listado2">
       <!--  <input type="text" id="datepickers" name='fechafinal' placeholder="2013/01/01" value='<?= Date('Y-m-d');?>'/>-->
       <script language="javascript">
	dateAvailable2.writeControl();
	dateAvailable2.dateFormat="yyyy/MM/dd";
	</script> 
    
    </td>
  </tr>
    <?
  }
  ?>
  <tr id="calen3">
      <td width="30%" class="titulos2"><?php echo utf8_decode("Año"); ?> </td>
    <td class="listado2">
        <select id="year">
            <?php 
             $año=2009;
             for($i=0;$i<100;$i++){
             ?>
                 echo "<option value="<?=$año+$i ?>"> <?= ($año+$i) ?></option>";
             <?php
             }
            ?>
        </select>
    
    </td>
  </tr>
  <tr>
    <td colspan="2" class="titulos2">
	<center>
	<input name="Submit" type="reset" class="botones_funcion" value="Limpiar"> 
	 
        <input type="button" id="Generar" class="botones_funcion" value="Generar" onclick="generar()">
        
        </center>
</td>
  </tr>
  
</table>
</form>

<?
 $datosaenviar = "fechaf=$fechaf&tipoEstadistica='".$_GET['Estadistica']."'&codus=$codus&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&idSerie=$idSerie&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&tipoPQR=$tipoPQ&resol=".$resol;


if (isset($generarOrfeo) && $_GET['Estadistica'] == 12) {
	global $orderby;
	$orderby = 'ORDER BY NOMBRE';
	$whereDep = ($dependencia_busq != 99999) ? "AND h.DEPE_CODI = " . $dependencia_busq : '';
	//modificado idrd para postgres	
	$isqlus = "SELECT u.USUA_NOMB as NOMBRE, u.USUA_DOC, d.DEPE_CODI,
					COUNT(r.RADI_NUME_RADI) as TOTAL_MODIFICADOS
					  FROM USUARIO u, RADICADO r, HIST_EVENTOS h, DEPENDENCIA d, SGD_TPR_TPDCUMENTO s
					  WHERE u.USUA_DOC = h.USUA_DOC
					    AND h.SGD_TTR_CODIGO = 32
					    AND h.HIST_OBSE LIKE '*Modificado TRD*%'
					    AND h.DEPE_CODI = d.DEPE_CODI
					    $whereDep
					    AND s.SGD_TPR_CODIGO = r.TDOC_CODI
					    AND r.RADI_NUME_RADI = h.RADI_NUME_RADI
					    AND TO_CHAR(r.RADI_FECH_RADI,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
					  GROUP BY u.USUA_NOMB, u.USUA_DOC, d.DEPE_CODI $orderby";
	
	$rs1 = $db->query($isqlus);
	while(!$rs1->EOF)  {
		$usuadoc[] = $rs1->fields["USUA_DOC"]; 
		$dependencias[] = $rs1->fields["DEPE_CODI"]; 
		$rs1->MoveNext();
	}
}
?>
<div id="general">
<?php
if($_POST['feo'])
{ 
  include "genEstadistica1.php";
} 
 ?>   
</div>
<div id="reporcontec">
    
</div>
</body>
</html>
<?
 echo "<input type=hidden name=check value=$check>";
?>
<form name=jh >
 <input type=hidDEN name=jj value=0>
  <input type=hidDEN name=dS value=0>
 </form>
</div>