<?php
$ruta_raiz = "..";
include_once("$ruta_raiz/config.php");

//Almacenar datos para generar el archivo csv
$dataCsv = array();
$timeCSV = time();
?>
<link rel="stylesheet" href="../estilos/orfeo.css">
<table class="table">
<thead>
<tr>
  <th></th>
  <?
  $check=1;
  $fieldCount = $rsE->FieldCount();
  if($ascdesc=="") $ascdesc = " desc ";
  else $ascdesc = "";

  for($iE=0; $iE<=$fieldCount-1; $iE++) {
    $fld = $rsE->FetchField($iE);
    if(substr($fld->name,0,3)!="HID") {
      $titleCsv[] = $fld->name;
      ?>
      <th>
        <abbr>
          <? $linkPaginaActual = $_SERVER['PHP_SELF']; ?>
          <a href='<?=$linkPaginaActual?>?<?=$datosaenviar?>&ascdesc=<?=$ascdesc?>&orno=<?=($iE+1)?>
                  &generarOrfeo=Busquedasss&genDetalle=<?=$genDetalle?>&genTodosDetalle=<?=$genTodosDetalle?>
                  &fenvCodi=<?=$fenvCodi?>&tipoDocumento=<?=$tipoDocumento?>'>
            <?=$fld->name;?>
          </a>
        </abbr>
      </th>
      <?
    }
  }

  //Agregamos el arreglo de titulos al arreglo del csv
  $dataCsv[] = $titleCsv;
?>
  <th></th>
</tr>
</thead>

<?
$iRow     = 1;
$datosCod = 0;
$reporte  = $reporte;

//Guardar en el arreglo los datos que se le muetran al
//usuario para generar el contenido del csv
$contCsv = array();
$contLine = array();
while(!$rsE->EOF){
  /**  INICIO CICLO RECORRIDO DE LOS REGISTROS
   *  En esta seccion se recorre todo el query solicitado
   *  @numListado Int Variable que almacena 1 O 2 dependiendo
   *  de la clase requerida.(Resultado de modulo con doos )
   */

  $numListado = fmod($iRow,2);

  if($numListado==0){
    $numListado = 2;
  }


?>
<tr class='listado<?=$numListado?>' >
  <td width="1"><?=$iRow?></td>
<?
  $fieldCount = $rsE->FieldCount();
  for($iE=0; $iE<=$fieldCount-1; $iE++) {
    $fld = $rsE->FetchField($iE);

    //OMITE LAS COLUMNAS CON HID_
    if(substr($fld->name,0,3)!="HID") {
      echo "<td>";
      $pathImg = "";

      // busca el campo de numero de expediente para asignar en ver detalles
      if ($fld->name == "SGD_EXP_NUMERO"){
        $expedientes = $rsE->fields["$fld->name"];
      }

      if ($fld->name == "ESTADO"){
        $rsE->fields["$fld->name"]= ($rsE->fields["$fld->name"] ==0) ? 'Sin Archivar' : 'Archivado';
      }

      $contCsv[] = $rsE->fields["$fld->name"];
      echo $rsE->fields["$fld->name"];
      echo "</td>";
    }

    if($fld->name=="HID_DEPE_USUA"){
      $datosEnvioDetalle.="&depeUs=".$rsE->fields["$fld->name"];
      $depeUs=$rsE->fields["$fld->name"];
    }

    if($fld->name=="HID_COD_USUARIO"){
      $datosEnvioDetalle.="&codUs=".$rsE->fields["$fld->name"];
    }
    if($fld->name=="USUARIO"){
      $nombUs[($iRow-1)]=substr($rsE->fields["$fld->name"],0,21);
      $nombXAxis = "USUARIO";
    }

    if($fld->name=="MEDIO_RECEPCION"){
      $nombUs[($iRow-1)]=substr($rsE->fields["$fld->name"],0,21);
      $nombXAxis = "MED RECEPCION";
    }

    if($fld->name=="MEDIO_ENVIO"){
      $nombUs[($iRow-1)]=substr($rsE->fields["$fld->name"],0,21);
      $nombXAxis = "MED ENVIO";
    }

    if($fld->name=="RADICADOS"){
      $data1y[($iRow-1)]=$rsE->fields["$fld->name"];
      $nombYAxis = "RADICADOS";
    }

    if($fld->name=="TOTAL_ENVIADOS"){
      $data1y[($iRow-1)]=$rsE->fields["$fld->name"];
      $nombYAxis = "RADICADOS";
    }

    if($fld->name=="HOJAS_DIGITALIZADAS"){
      $data2y[($iRow-1)]=$rsE->fields["$fld->name"];
      $nombYAxis .= " / HOJAS DIGITALIZADAS";
    }

    if($fld->name=="HID_MREC_CODI") $datosEnvioDetalle.="&mrecCodi=".$rsE->fields["$fld->name"];
    if($fld->name=="HID_CODIGO_ENVIO") $datosEnvioDetalle.="&fenvCodi=".$rsE->fields["$fld->name"];
    if($fld->name=="HID_TPR_CODIGO") $datosEnvioDetalle.="&tipoDOCumento=".$rsE->fields["$fld->name"];
    if($fld->name=="HID_FECH_SELEC") $datosEnvioDetalle.="&fecSel=".$rsE->fields["$fld->name"];
  }

  if(!$genDetalle){
    if($genTodosDetalle==1) {
?>      <td align="center">
        <A href="genEstadistica.php?<?=$datosEnvioDetalle?>&genDetalle=1&<?=$datosaenviar?>" Target="VerDetalle<?=date("dmYHis")?>"></a>
      </td>
<?    } else {
  if ($tipoEstadistica == 13 ) {
    $datosaenviarCodExp = "fechaf=$fechaf&tipoEstadistica=$tipoEstadistica&codus=$usuadoc[$datosCod]&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&depCodigo=$dependencias[$datosCod]&expediente=$expedientes";
?>        <td align="center">
        <A href="genEstadistica.php?<?=$datosEnvioDetalle?>&genDetalle=1&<?=$datosaenviarCodExp?>" Target="VerDetalle<?=date("dmYHis")?>" class="vinculos">VER DETALLES</a>
          </td>
<?      } elseif (isset($usuadoc)) {
  $datosaenviarCod = "fechaf=$fechaf&tipoEstadistica=$tipoEstadistica&codus=$usuadoc[$datosCod]&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&depCodigo=$dependencias[$datosCod]";
  $datosCod++;
?>        <td align="center">
        <A href="genEstadistica.php?<?=$datosEnvioDetalle?>&genDetalle=1&<?=$datosaenviarCod?>" Target="VerDetalle<?=date("dmYHis")?>" class="vinculos">VER DETALLES</a>
        </td>
<?      }
else {
  if($reporte==1)
  {
    $datosaenviarArch = "fechaf=$fechaf&krd=$krd&fechaIni=$fechaIni&fechaInif=$fechaInif&dep_sel=$dep_sel&codigoUsuario=$codigoUsuario&trad=$trad&ruta_raiz=$ruta_raiz&tipoReporte=$reporte&depCodigo=$dependencias[$datosCod]";
?>                      <td align="center">
               <A href="../estadisticas/genEstadistica.php?<?=$datosEnvioDetalle?>&genDetalle=1&genTodosDetalle=1&<?=$datosaenviarArch?>" Target="VerDetalle<?=date("dmYHis")?>" class="vinculos">VER DETALLES</a>
            </td>
<?        } else {
  if($tipoEstadistica!=18){
?>        <td align="center">
                <A href='genEstadistica.php?&<?="&krd=$krd&datosaenviar=$datosaenviar&genDetalle=1&datosEnvioDetalle=$datosEnvioDetalle"?>' > VER DETALLES</A>
        </td>
<?           }
if($tipoEstadistica==18) {  $dependen=$rsE->fields['CODIGO'];
?>        <td align="center">
                <A href='genEstadistica.php?&<?="&krd=$krd&dependen=$dependen&datosaenviar=$datosaenviar&genDetalle=1&datosEnvioDetalle=$datosEnvioDetalle"?>' > VER DETALLES</A>
        </td>
<?           }
}
}
}
  }

  echo "</tr>";

  if($check<=20){
    $check=$check+1;
  }

  $rsE->MoveNext();

  $iRow++;
  $datosEnvioDetalle="";
  $dataCsv[] = $contCsv;
  unset($contCsv);
  //FIN CICLO RECORRIDO DE LOS REGISTROS
}

echo "</table><center>";

//Agregar el contenido en el arreglo que generara el csv
$_SESSION['csv'] = array($timeCSV => $dataCsv);
$_SESSION["data1y"] = $data1y;
$_SESSION["nombUs"] = $nombUs;
$noRegs = count($data1y);

$nombreGraficaTmp = "$ruta_raiz/$carpetaBodega/tmp/E_$krd.png";
$rutaImagen = $nombreGraficaTmp;
$notaSubtitulo = $subtituloE[$tipoEstadistica]."\n";
$tituloGraph = $tituloE[$tipoEstadistica];

if ($tipoEstadistica==1 or $tipoEstadistica==3 or $tipoEstadistica==6 or $tipoEstadistica==8 or $tipoEstadistica==9 or
$tipoEstadistica==10 or $tipoEstadistica==12  or $tipoEstadistica==13 or $tipoEstadistica==15 or $tipoEstadistica==8){

if ($genTodosDetalle==1 or $genDetalle==1) {
?>
    <br>
      <A href="genEstadistica.php?<?=$datosEnvioDetalle?>&genTodosDetalle=1&<?=$datosaenviar?>" Target="VerDetalle<?=date("dmYHis")?>"></a>
    </br>
<?
  } else {
?>
<form name=jh >
 <input type=hidden name=jj value=0>
  <input type=hidden name=dS value=0>
 </form>
    <br>
      <A href="genEstadistica.php?<?=$datosEnvioDetalle?>&genTodosDetalle=1&<?=$datosaenviar?>" Target="VerDetalle<?=date("dmYHis")?>" class="vinculos">VER TODOS LOS DETALLES</a>
    </br>
<?
  }
}
if ($reporte==1){
  $datosaenviarArch = "fechaf=$fechaf&krd=$krd&dep_sel=$dep_sel&codigoUsuario=$codigoUsuario&fechaIni=$fechaIni&fechaInif=$fechaInif&trad=$trad&ruta_raiz=$ruta_raiz&tipoReporte=$reporte&depCodigo=$dependencias[$datosCod]";
?>
    <br>
      <A href="../estadisticas/genEstadistica.php?<?=$datosEnvioDetalle?>&genTodosDetalle=1&<?=$datosaenviarArch?>" Target="VerDetalle<?=date("dmYHis")?>" class="vinculos">VER TODOS LOS DETALLES</a>
    </br>
<?
}

if(count($dataCsv) > 1){
  echo "
          </center>
          <div class='box'>
          <a class='button is-info is-outlined' href='./genCsv.php?xtime=$timeCSV' >
            <span>Csv</span>
            <span class='icon is-small'>
              <i class='el el-download-alt'></i>
            </span>
          </a>
          </div>
          </body>
        </html>";
}else{
  echo "    </center>
          </body>
        </html>";
}
?>
