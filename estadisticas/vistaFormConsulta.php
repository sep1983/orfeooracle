<?php
session_start();
$ruta_raiz = "..";

if($_SESSION['dependencia'] == null ){
 include "$ruta_raiz/cerrar_session.php";
}

if(!$tipoEstadistica) $tipoEstadistica =1;

if($_POST['dependencia_busq']){
  $dependencia_busq  = $_POST['dependencia_busq'];
}elseif($_GET['dependencia_busq']){
  $dependencia_busq  = $_GET['dependencia_busq'];
}elseif($dependencia){
  $dependencia_busq = $dependencia;
}else{
  $dependencia_busq =$_SESSION['dependencia'];
}


foreach ($_GET as $key=> $value) {
    $$key = $value;
}

foreach ($_POST as $key=> $value) {
    $$key = $value;
}

if(!$tipoEstadistica) $tipoEstadistica =1;
/** DEFINICION DE VARIABLES ESTADISTICA
	*	var $tituloE String array  Almacena el titulo de la Estadistica Actual
	* var $subtituloE String array  Contiene el subtitulo de la estadistica
	* var $helpE String Almacena array Almacena la descripcion de la Estadistica.
	*/
	$tituloE[1] = "1. RADICACION - CONSULTA DE RADICADOS POR USUARIO";
	$tituloE[2] = "2. RADICACION - ESTADISTICAS POR MEDIO DE RECEPCION-ENVIO";
	$tituloE[3] = "3. RADICACION - ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS";
	//$tituloE[4] = "RADICACION - ESTADISTICAS DE DIGITALIZACION DE DOCUMENTOS";
	$tituloE[5] = "5. RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA";
	$tituloE[6] = "6. RADICADOS ACTUALES EN LA DEPENDENCIA";
	//$tituloE[7] = "ESTADISTICAS DE NUMERO DE DOCUMENTOS ENVIADOS";
	$tituloE[8] = "8. REPORTE DE VENCIMIENTOS";
	$tituloE[9] = "9. REPORTE PROCESO RADICADOS DE ENTRADA";
	$tituloE[10] = "10. REPORTE ASIGNACION RADICADOS";
	$tituloE[11] = "11. ESTADISTICAS DE DIGITALIZACION";
	$tituloE[12] = "12. DOCUMENTOS RETIPIFICADOS POR TRD";
	$tituloE[13] = "13. EXPEDIENTES POR DEPENDENCIA";
	$tituloE[15] = "15. REPORTE DE RESPUESTAS ANEXADAS POR USUARIO";
	$tituloE[16] = "16. REPORTE REASIGNACION DE DOCUMENTOS A OTRO USUARIO";
	$tituloE[17] = "17. REPORTE DE RADICADOS QUE ME REASIGNARON";
	$tituloE[18] = "18. REPORTE DE RADICADOS SIN DIGITALIZAR";

	$subtituloE[1] = "ORFEO - Generada el: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[2] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[3] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[4] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[5] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[6] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[8] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";

	$helpE[1] = "Este reporte muestra la cantidad de radicados generados por usuario. Se puede discriminar por tipo de radicaci&oacute;n. " ;
	$helpE[2] = "Este reporte genera la cantidad de radicados de acuerdo al medio de recepci&oacute;n o envio realizado al momento de la radicaci&oacute;n. " ;
	$helpE[3] = "Este reporte genera la cantidad de radicados enviados a su destino final por el &aacute;rea.  " ;
	$helpE[4] = "Este reporte genera la cantidad de radicados digitalizados por usuario y el total de hojas digitalizadas. Se puede seleccionar el tipo de radicaci&oacute;n." ;
	$helpE[5] = "Este reporte genera la cantidad de documentos de entrada radicados del &aacute;rea de correspondencia a una dependencia. " ;
	$helpE[6] = "Esta estadistica trae la cantidad de radicados \n generados por usuario, se puede discriminar por tipo de Radicacion. " ;
	$helpE[8] = "Este reporte genera la cantidad de radicados de entrada cuyo vencimiento esta dentro de las fechas seleccionadas. " ;
	$helpE[9] = "Este reporte muestra los documentos actuales en las dependencias, que fueron radicados en la fecha seleccionada especifica.";
	$helpE[10] = "Este reporte muestra cuantos radicados de entrada han sido asignados a cada dependencia. ";
	$helpE[11] = "Muestra la cantidad de radicados digitalizados por usuario y el total de hojas digitalizadas. Se puede seleccionar el tipo de radicaci&oacute;n y la fecha de digitalizaci&oacute;n." ;
	$helpE[12] = "Muestra los radicados que ten&iacute;an asignados un tipo documental(TRD) y han sido modificados";
	$helpE[13] = "Muestra todos los expedientes agrupados por dependencia, con el n&uacute;mero de radicados totales";
	$helpE[15] = "Este reporte muestra la cantidad de respuestas anexadas por usuario. Se puede discriminar por tipo de radicaci&oacute;n. " ;
	$helpE[16] = "Muestra la cantidad de radicados que he asignado a otros usuarios" ;
	$helpE[17] = "Muestra la cantidad de radicados que se me han reasignado" ;

?>
<html>
<head>
<title>principal</title>
<link rel="stylesheet" href="../estilos/orfeo.css">
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script>
function adicionarOp (forma,combo,desc,val,posicion){
	o = new Array;
	o[0]=new Option(desc,val );
	eval(forma.elements[combo].options[posicion]=o[0]);
	//alert ("Adiciona " +val+"-"+desc );

}
</script>
		 <script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>

		 <script language="javascript">
		 <!--
			<?
				$ano_ini = date("Y");
				$mes_ini = substr("00".(date("m")-1),-2);
				if ($mes_ini==0) {$ano_ini==$ano_ini-1; $mes_ini="12";}
				$dia_ini = date("d");
				if(!$fecha_ini) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
					$fecha_busq = date("Y/m/d") ;
				if(!$fecha_fin) $fecha_fin = $fecha_busq;
			?>
   var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "formulario", "fecha_ini","btnDate1","<?=$fecha_ini?>",scBTNMODE_CUSTOMBLUE);
   var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "formulario", "fecha_fin","btnDate2","<?=$fecha_fin?>",scBTNMODE_CUSTOMBLUE);

//--></script>
</head>
<?
include "$ruta_raiz/envios/paEncabeza.php";

include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include("$ruta_raiz/class_control/usuario.php");
$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$objUsuario = new Usuario($db);
?>
<body onLoad="comboUsuarioDependencia(document.formulario,document.formulario.elements['dependencia_busq'].value,'codus');" topmargin="0">
<div id="spiffycalendar" class="text"></div>
<form name="formulario"  method=post action='./vistaFormConsulta.php?<?=session_name()."=".trim(session_id())."&krd=$krd&depeUs=$depeUs&fechah=$fechah"?>'>

<table class="table">
  <tr>
    <td colspan="2">POR RADICADOS  -  <A href='vistaFormProc.php?<?=session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah"?>'>POR PROCESOS </A> <?php if($_SESSION["perm_est_pqr"]){ echo "PQR PQRS - <A href='vistaFormpqr.php?".session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah'>POR PRQS </A>";}?>
 </td>

  </tr>
  <tr>
    <td colspan="2"><div class="notification is-info"><?=$helpE[$tipoEstadistica]?></div></td>
  </tr>
  <tr>
    <td width="30%"><label class="label"> Tipo de Consulta / Estadistica </label></td>
    <td align="left">
	   <select name=tipoEstadistica onChange="formulario.submit();">
		<?
			foreach($tituloE as $key=>$value)
			{
		?>
	   <? if($tipoEstadistica==$key) $selectE = " selected "; else $selectE = ""; ?>
			<option value=<?=$key?> <?=$selectE?>><?=$tituloE[$key]?></option>
		<?
		}
		?>
		</select>
	</td>
	</tr>
	<tr>
    <td width="30%"><label class="label">Dependencia</label></td>
    <td>
	<select name="dependencia_busq" onChange="formulario.submit();">
<?
	if($usua_perm_estadistica>1)  {
		if($dependencia_busq==99999)  {
			$datoss= " selected ";
		}

		?>
			<option value=99999  <?=$datoss?>>-- Todas las Dependencias --</option>
		<?
	}

$whereDepSelect=" DEPE_CODI = $dependencia ";
if ($usua_perm_estadistica==1){
	$whereDepSelect=" $whereDepSelect or depe_codi_padre = $dependencia ";
}
if ($usua_perm_estadistica==2) {
	$isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a ORDER BY a.DEPE_CODI";
}
else {
//$whereDepSelect=
	$isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a
						where $whereDepSelect ";
}
	//if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
//echo "--->".$isqlus;
$rs1=$db->query($isqlus);

do{
	$codigo = $rs1->fields["DEPE_CODI"];
	$vecDeps[]=$codigo;
	$depnombre = $rs1->fields["DEPE_NOMB"];
	$datoss="";
	if($dependencia_busq==$codigo){
		$datoss= " selected ";
	}
	echo "<option value=$codigo  $datoss>$codigo - $depnombre</option>";
	$rs1->MoveNext();
}while(!$rs1->EOF);
	?>
	</select>
</td>
</tr>
<?  if ($dependencia_busq != 99999)  {
			$whereDependencia = " AND DEPE_CODI=$dependencia_busq ";
		}

if($tipoEstadistica==1 or $tipoEstadistica==2 or $tipoEstadistica==3 or
	$tipoEstadistica==4 or $tipoEstadistica==5 or $tipoEstadistica==6 or
    $tipoEstadistica==7 or $tipoEstadistica==11 or $tipoEstadistica==12 or
    $tipoEstadistica==15 or $tipoEstadistica==16)
{
?>
<tr id="cUsuario">
	<td width="30%"><label class="label">Usuario</label>
	<?
	$datoss = "";
	if($usActivos){
		$datoss = " checked ";
	}
	?>
	<input name="usActivos" type="checkbox" <?=$datoss?> onChange="formulario.submit();">
	Incluir Usuarios Inactivos  </td>
	<td>
	<select name=codus onChange="formulario.submit();">
	<?
    if ($usua_perm_estadistica > 0){
		?>
		<option value=0> -- AGRUPAR POR TODOS LOS USUARIOS --</option>
		<?
	}

	$whereUsSelect = "";

  if(!$usActivos){
    $whereUsSelect = " where u.USUA_ESTA = 1 ";
  }

  if ($usua_perm_estadistica < 1){
    $whereUsSelect .= empty($whereUsSelect)? ' where ' : ' and ';
  	$whereUsSelect .= "  u.USUA_LOGIN='$krd' ";
  }

  if($dependencia_busq != 99999){
    $isqlus = "SELECT u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA FROM USUARIO u";

    if ($dependencia_busq != 99999)  {
      $whereUsSelect .= empty($whereUsSelect)? ' where ' : ' and ';
      $whereUsSelect .= " DEPE_CODI = $dependencia_busq";
    }

    $isqlus .= " $whereUsSelect ORDER BY u.USUA_NOMB ";
    $rs1=$db->query($isqlus);

    while(!$rs1->EOF) {
      $codigo = $rs1->fields["USUA_CODI"];
      $vecDeps[]=$codigo;
      $usNombre = $rs1->fields["USUA_NOMB"];
      $datoss="";
      if($codus==$codigo)  {
        $datoss= " selected";
      }
      echo "<option value=$codigo $datoss>$usNombre</option>";
      $rs1->MoveNext();
    }
  }
?>
		</select>
	&nbsp;</td>
  </tr>
  <?
  }

  if($tipoEstadistica==1 or $tipoEstadistica==2 or $tipoEstadistica==3 or
  		$tipoEstadistica==4 or $tipoEstadistica==6 or $tipoEstadistica==11 or
  		$tipoEstadistica==12 or $tipoEstadistica==15)
  {
  ?>
<tr>
	<td width="30%" height="40"><label class="label">Tipo de Radicado</label></td>
	<td>
	<?
		$rs = $db->conn->Execute('select SGD_TRAD_DESCR, SGD_TRAD_CODIGO  from SGD_TRAD_TIPORAD order by 2');
		$nmenu = "tipoRadicado";
		$valor = "";
		$default_str=$tipoRadicado;
		$itemBlanco = " -- Agrupar por Todos los Tipos de Radicado -- ";
		print $rs->GetMenu2($nmenu, $default_str, $blank1stItem = "$valor:$itemBlanco",false,0);
		?>&nbsp;</td>
</tr>
   <?
  }

  if($tipoEstadistica==1 or $tipoEstadistica==6 or $tipoEstadistica==10 or
  	$tipoEstadistica==12) {
  ?>
  <tr>
    <td width="30%" height="40"><label class="label">Agrupar por Tipo de Documento</label></td>
    <td>
	<select name=tipoDocumento>
        <?
 		$isqlTD = "SELECT SGD_TPR_DESCRIP, SGD_TPR_CODIGO
					from SGD_TPR_TPDCUMENTO
					WHERE SGD_TPR_CODIGO<>0
				    order by  SGD_TPR_DESCRIP";
	    //if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
		//echo "--->".$isqlus;
		$rs1=$db->query($isqlTD);
		$datoss = "";

		if($tipoDocumento!='9998'){
			$datoss= " selected ";
			$selecUs = " b.USUA_NOMB USUARIO, ";
			$groupUs = " b.USUA_NOMB, ";
		}

		if($tipoDocumento=='9999'){
			$datoss= " selected ";
		}
		?>
		<option value='9999'  <?=$datoss?>>-- No Agrupar Por Tipo de Documento</option>
		<?   $datoss = "";
		if($tipoDocumento=='9998'){
			$datoss= " selected ";
		}
		?>
		<option value='9998'  <?=$datoss?>>-- Agrupar Por Tipo de Documento</option>
		<?
		$datoss = "";
		if($tipoDocumento=='9997'){
			$datoss= " selected ";
		}
		?>
		<option value='9997'  <?=$datoss?>>-- Tipos Documentales No Definidos</option>
		<?
		do{
			$codigo = $rs1->fields["SGD_TPR_CODIGO"];
			$vecDeps[]=$codigo;
			$selNombre = $rs1->fields["SGD_TPR_DESCRIP"];
			$datoss="";
		if($tipoDocumento==$codigo){
				$datoss= " selected ";
			}
			echo "<option value=$codigo  $datoss>$selNombre</option>";
			$rs1->MoveNext();
		}while(!$rs1->EOF);
		?>
		</select>

	  </td>
  </tr>
  <?
  }
  if($tipoEstadistica==1 or $tipoEstadistica==2 or $tipoEstadistica==3 or
  		$tipoEstadistica==4 or $tipoEstadistica==5 or $tipoEstadistica==7 or
  		$tipoEstadistica==8 or $tipoEstadistica==9 or $tipoEstadistica==10 or
        $tipoEstadistica==11 or $tipoEstadistica==12 or
        $tipoEstadistica==15 or $tipoEstadistica==16 or $tipoEstadistica==17 or $tipoEstadistica==18)
  {
  ?>
  <tr>
    <td width="30%"><label class="label"> Desde fecha (aaaa/mm/dd)</label> </td>
    <td>
	<script language="javascript">
	dateAvailable.writeControl();
	dateAvailable.dateFormat="yyyy/MM/dd";
	</script>
	&nbsp;
  </td>
  </tr>
  <tr>
    <td width="30%"><label class="label"> Hasta  fecha (aaaa/mm/dd)</label> </td>
    <td>
	<script language="javascript">
	dateAvailable2.writeControl();
	dateAvailable2.dateFormat="yyyy/MM/dd";
	</script>&nbsp;</td>
  </tr>
    <?
  }
  ?>
  <tr>
    <td colspan="2">
	<center>
	<input name="Submit" type="submit" class="button" value="Limpiar">
	<input type="submit" class="button is-primary" value="Generar" name="generarOrfeo">
	</center>
	</td>
  </tr>
</table>
</form>
<?
$datosaenviar = "fechaf=$fechaf&tipoEstadistica=$tipoEstadistica&codus=$codus&depeUs=$depeUs&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento";

//
if (isset($generarOrfeo) && $tipoEstadistica == 12) {
	global $orderby;
	$orderby = 'ORDER BY NOMBRE';
	$whereDep = ($dependencia_busq != 99999) ? "AND h.DEPE_CODI = " . $dependencia_busq : '';
	$isqlus = "SELECT u.USUA_NOMB NOMBRE, u.USUA_DOC, d.DEPE_CODI, COUNT(r.RADI_NUME_RADI) TOTAL_MODIFICADOS
					  FROM USUARIO u, RADICADO r, HIST_EVENTOS h, DEPENDENCIA d, SGD_TPR_TPDCUMENTO s
					  WHERE u.USUA_DOC = h.USUA_DOC
					    AND h.SGD_TTR_CODIGO = 32
					    AND h.HIST_OBSE LIKE '*Modificado TRD*%'
					    AND h.DEPE_CODI = d.DEPE_CODI
					    $whereDep
					    AND s.SGD_TPR_CODIGO = r.TDOC_CODI
					    AND r.RADI_NUME_RADI = h.RADI_NUME_RADI
					    AND TO_CHAR(r.RADI_FECH_RADI,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
					  GROUP BY u.USUA_NOMB, u.USUA_DOC, d.DEPE_CODI $orderby";

	$rs1 = $db->query($isqlus);
	while(!$rs1->EOF)  {
		$usuadoc[] = $rs1->fields["USUA_DOC"];
		$dependencias[] = $rs1->fields["DEPE_CODI"];
		$rs1->MoveNext();
	}
}

if($generarOrfeo) {
  include "genEstadistica.php";
}

?>
</body>
</html>
<form name=jh >
  <input type=hidden name=jj value=0>
  <input type=hidden name=ds value=0>
</form>
