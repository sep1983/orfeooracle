<?
/** CONSUTLA 001
	* Estadiscas por medio de recepcion Entrada
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	*
	*/


if($_POST['dependencia_busq']){
  $dependencia_busq  = $_POST['dependencia_busq'];
}elseif($_GET['dependencia_busq']){
  $dependencia_busq  = $_GET['dependencia_busq'];
}

$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
	{
	case 'mssql':
	{	if($tipoDocumento=='9999')
			{	$queryE = "SELECT b.USUA_NOMB USUARIO, count(1) RADICADOS, MIN(USUA_CODI) HID_COD_USUARIO, MIN(depe_codi) HID_DEPE_USUA
						FROM RADICADO r
							INNER JOIN USUARIO b ON r.radi_usua_radi=b.usua_CODI AND $tmp_substr($radi_nume_radi,5,3)=b.depe_codi
						WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
							$whereDependencia $whereActivos $whereTipoRadicado
						GROUP BY b.USUA_NOMB ORDER BY $orno $ascdesc";
			}
			else
			{	$queryE = "SELECT b.USUA_NOMB USUARIO, t.SGD_TPR_DESCRIP TIPO_DOCUMENTO, count(1) RADICADOS,
							MIN(USUA_CODI) HID_COD_USUARIO, MIN(SGD_TPR_CODIGO) HID_TPR_CODIGO, MIN(depe_codi) HID_DEPE_USUA
						FROM RADICADO r
							INNER JOIN USUARIO b ON r.RADI_USUA_RADI = b.USUA_CODI AND $tmp_substr($radi_nume_radi, 5, 3) = b.DEPE_CODI
							LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.TDOC_CODI = t.SGD_TPR_CODIGO
						WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
							$whereDependencia $whereActivos $whereTipoRadicado
						GROUP BY b.USUA_NOMB,t.SGD_TPR_DESCRIP ORDER BY $orno $ascdesc";
			}
 			/** CONSULTA PARA VER DETALLES
	 		*/
			$condicionDep = " AND depe_codi = $depeUs ";
			$condicionE = " AND b.USUA_CODI=$codUs $condicionDep ";
			$queryEDetalle = "SELECT $radi_nume_radi RADICADO
					,r.RADI_FECH_RADI FECHA_RADICADO
					,t.SGD_TPR_DESCRIP TIPO_DE_DOCUMENTO
					,r.RA_ASUN ASUNTO
					,r.RADI_DESC_ANEX
					,r.RADI_NUME_HOJA
					,m.MREC_DESC MEDIO_RECEPCION
					,b.usua_nomb Usuario
					FROM RADICADO r
						INNER JOIN USUARIO b ON r.radi_usua_radi=b.usua_CODI AND $tmp_substr($radi_nume_radi,5,3)=b.depe_codi
						LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi=t.SGD_TPR_CODIGO
						LEFT OUTER JOIN MEDIO_RECEPCION m ON r.MREC_CODI = m.MREC_CODI
					WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' $whereTipoRadicado ";
					$orderE = "	ORDER BY $orno $ascdesc";
			 /** CONSULTA PARA VER TODOS LOS DETALLES
			 */

			$queryETodosDetalle = $queryEDetalle . $condicionDep . $orderE;
			$queryEDetalle .= $condicionE . $orderE;
		}
	break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
	if($tipoDocumento=='9999'){
	$queryE = "
	    SELECT b.USUA_NOMB USUARIO
			, count(1) RADICADOS
			, MIN(USUA_CODI) HID_COD_USUARIO
			, MIN(depe_codi) HID_DEPE_USUA
			FROM RADICADO r
			, USUARIO b
		WHERE
			r.radi_usua_radi=b.usua_CODI
			AND substr(r.radi_nume_radi,5,3)=b.depe_codi
			$whereDependencia
			AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
			$whereActivos
		$whereTipoRadicado
		GROUP BY b.USUA_NOMB
		ORDER BY $orno $ascdesc";
	} else {
		$queryE = "
	    SELECT b.USUA_NOMB USUARIO
			, t.SGD_TPR_DESCRIP TIPO_DOCUMENTO
			, count(1) RADICADOS
			, MIN(USUA_CODI) HID_COD_USUARIO
			, MIN(SGD_TPR_CODIGO) HID_TPR_CODIGO
			, MIN(depe_codi) HID_DEPE_USUA
			FROM RADICADO r
			, USUARIO b
			, SGD_TPR_TPDCUMENTO t
		WHERE
			r.radi_usua_radi=b.usua_CODI
			AND r.tdoc_codi=t.SGD_TPR_CODIGO
			AND substr(r.radi_nume_radi,5,3)=b.depe_codi
			$whereDependencia
			AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
			$whereActivos
		$whereTipoRadicado
		GROUP BY b.USUA_NOMB,t.SGD_TPR_DESCRIP
		ORDER BY $orno $ascdesc";
	}
 /** CONSULTA PARA VER DETALLES
	 */
	$condicionDep = " AND depe_codi = $dependencia_busq";
	if($dependencia_busq=="99999")
	    $condicionDep = "";
	$condicionE = " AND b.USUA_CODI=$codUs $condicionDep ";
  $queryEDetalle = "
    SELECT
      r.radi_nume_radi radicado,
      r.radi_fech_radi fecha_radicado,
      t.sgd_tpr_descrip tipo_de_documento,
      r.ra_asun asunto,
      m.mrec_desc medio_recepcion,
      r.radi_desc_anex anexos,
      b.usua_nomb usuario
    FROM
      radicado r
      left outer join sgd_tpr_tpdcumento t on (r.tdoc_codi = t.sgd_tpr_codigo)
      left outer join medio_recepcion m on (r.mrec_codi = m.mrec_codi),
      usuario b
    WHERE
      r.radi_usua_radi = b.usua_codi AND
      substr( r.radi_nume_radi, 5, 3 ) = b.depe_codi AND
			TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin' AND
      b.usua_esta = 1
		$whereTipoRadicado ";
		$orderE = "	ORDER BY $orno $ascdesc";

 /** CONSULTA PARA VER TODOS LOS DETALLES
	 */
	$queryETodosDetalle = $queryEDetalle . $condicionDep . $orderE;
	$queryEDetalle .= $condicionE . $orderE;
	break;
	}
?>
