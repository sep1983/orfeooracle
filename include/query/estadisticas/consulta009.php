<?
/** CONSUTLA 001
	* Estadiscas por medio de recepcion Entrada
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	*
	*/


if($_POST['dependencia_busq']){
  $dependencia_busq  = $_POST['dependencia_busq'];
}elseif($_GET['dependencia_busq']){
  $dependencia_busq  = $_GET['dependencia_busq'];
}

$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver){
	case 'mssql':
	{	if($tipoDocumento=='9999')
			{	$queryE = "SELECT b.USUA_NOMB USUARIO, count(1) RADICADOS, MIN(USUA_CODI) HID_COD_USUARIO, MIN(depe_codi) HID_DEPE_USUA
						FROM RADICADO r
							INNER JOIN USUARIO b ON r.radi_usua_radi=b.usua_CODI AND $tmp_substr($radi_nume_radi,5,3)=b.depe_codi
						WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
							$whereDependencia $whereActivos $whereTipoRadicado
						GROUP BY b.USUA_NOMB ORDER BY $orno $ascdesc";
			}
			else
			{	$queryE = "SELECT b.USUA_NOMB USUARIO, t.SGD_TPR_DESCRIP TIPO_DOCUMENTO, count(1) RADICADOS,
							MIN(USUA_CODI) HID_COD_USUARIO, MIN(SGD_TPR_CODIGO) HID_TPR_CODIGO, MIN(depe_codi) HID_DEPE_USUA
						FROM RADICADO r
							INNER JOIN USUARIO b ON r.RADI_USUA_RADI = b.USUA_CODI AND $tmp_substr($radi_nume_radi, 5, 3) = b.DEPE_CODI
							LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.TDOC_CODI = t.SGD_TPR_CODIGO
						WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
							$whereDependencia $whereActivos $whereTipoRadicado
						GROUP BY b.USUA_NOMB,t.SGD_TPR_DESCRIP ORDER BY $orno $ascdesc";
			}
 			/** CONSULTA PARA VER DETALLES
	 		*/
			$condicionDep = " AND depe_codi = $depeUs ";
			$condicionE = " AND b.USUA_CODI=$codUs $condicionDep ";
			$queryEDetalle = "SELECT $radi_nume_radi RADICADO
					,r.RADI_FECH_RADI FECHA_RADICADO
					,t.SGD_TPR_DESCRIP TIPO_DE_DOCUMENTO
					,r.RA_ASUN ASUNTO
					,r.RADI_DESC_ANEX
					,r.RADI_NUME_HOJA
					,m.MREC_DESC MEDIO_RECEPCION
					,b.usua_nomb Usuario
					FROM RADICADO r
						INNER JOIN USUARIO b ON r.radi_usua_radi=b.usua_CODI AND $tmp_substr($radi_nume_radi,5,3)=b.depe_codi
						LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi=t.SGD_TPR_CODIGO
						LEFT OUTER JOIN MEDIO_RECEPCION m ON r.MREC_CODI = m.MREC_CODI
					WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' $whereTipoRadicado ";
					$orderE = "	ORDER BY $orno $ascdesc";
			 /** CONSULTA PARA VER TODOS LOS DETALLES */

			$queryETodosDetalle = $queryEDetalle . $condicionDep . $orderE;
			$queryEDetalle .= $condicionE . $condicionDep . $orderE;
		}
	break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':


  /** CONSULTA PARA VER DETALLES */
  if($dependencia_busq=="99999"){
	    $condicionDep = "";
  }else{
	  $condicionDep = " and r.radi_depe_actu = $dependencia_busq";
  }

	$queryE = " SELECT
                  d.depe_nomb DEPENDENCIA
                , count(1)  as RADICADOS
                , d.depe_codi as HID_DEPE_USUA
              FROM RADICADO r
                , dependencia d
              WHERE
                r.radi_depe_actu = d.depe_codi and
                r.radi_nume_radi like '%2' and
                TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
                $condicionDep
              GROUP BY d.depe_nomb, d.depe_codi
              ORDER BY $orno $ascdesc ";

  //Filtro que se realiza cuando el usuario selecciona en la primera
  //consulta una dependnecia. Este dato es enviado por la variable
  //datosEnvioDetalles con el nombre de la variable depeus

  if($depeUs){
	  $condicionE = " and r.radi_depe_actu = $depeUs ";
  }

  $diasHabiles=" sumadiashabiles(r.radi_fech_radi,t.sgd_tpr_termino) ";

  $queryEDetalle = "SELECT
        r.radi_nume_radi radicado,
        TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd hh:mi:ss') fecha_radicado,
        dr.sgd_dir_nombre || dr.sgd_dir_nomremdes as Remitente,"
        .$diasHabiles. "as Fecha_Vencimiento,
        t.sgd_tpr_descrip tipo_de_documento,
        r.ra_asun asunto,
        an.radi_nume_salida as respuesta,
        an.anex_radi_fech as fecha_respuesta,
        d.depe_nomb dependencia_actual,
        d2.depe_nomb dependencia_anterior
      FROM

        radicado r

        LEFT OUTER JOIN sgd_tpr_tpdcumento t ON (
          r.tdoc_codi = t.sgd_tpr_codigo
        )

        LEFT OUTER JOIN medio_recepcion m ON (
          r.mrec_codi = m.mrec_codi
        )

        LEFT JOIN sgd_dir_drecciones dr ON (
          r.radi_nume_radi = dr.radi_nume_radi
        )

        LEFT JOIN anexos an ON (
          an.anex_radi_nume = r.radi_nume_radi and
          an.radi_nume_salida is not null and
          an.anex_estado >= 3
        )

        LEFT OUTER JOIN usuario b2 ON (
          r.radi_usu_ante = b2.usua_login
        )

        LEFT  OUTER JOIN dependencia d2 ON (
          b2.depe_codi = d2.depe_codi
          and r.radi_usu_ante = b2.usua_login
        ),

        usuario b,
        dependencia d
      WHERE
        r.radi_nume_radi like '%2' and
        b.usua_codi = r.radi_usua_actu and
        b.depe_codi = r.radi_depe_actu and
        b.depe_codi = d.depe_codi
        AND TO_CHAR(
          r.radi_fech_radi,
          'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'";

	  $orderE = "	ORDER BY $orno $ascdesc";

    /** CONSULTA PARA VER TODOS LOS DETALLES */
    $queryETodosDetalle = $queryEDetalle . $condicionE . $condicionDep . $orderE;
    $queryEDetalle .= $condicionDep . $condicionE . $orderE;
    break;
	}
?>
