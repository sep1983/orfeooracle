<?php

if($_POST['dependencia_busq']){
  $dependencia_busq  = $_POST['dependencia_busq'];
}elseif($_GET['dependencia_busq']){
  $dependencia_busq  = $_GET['dependencia_busq'];
}

$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
if(!$orno) $orno=2;

switch($db->driver){
        case 'oci8':
            $isql = '';
	    	global $orderby;
            $whereDependencia = ($dependencia_busq != 99999 && !empty($dependencia_busq)) ? " h.depe_codi = " . $dependencia_busq ." AND": "";
            $whereDependencia = ($dependencia_busq != 99999 && !empty($dependencia_busq)) ? " AND h.depe_codi_dest = " . $dependencia_busq : "";
            $whereUsua = ($codus != 0) ? "AND u.usua_codi = " . $codus : '';
            $whereTipoRadicado = ($tipoRadicado != '') ? "AND h.radi_nume_radi LIKE '%" . $tipoRadicado . "'": '';

            $queryE =

            "select u.usua_nomb AS USUARIO, count(h.radi_nume_radi) AS RADICADOS, d.depe_nomb AS DEPENDENCIA,
            	d.depe_nomb AS HID_DEPENDENCIA, u.usua_login AS HID_COD_USUARIO
			from hist_eventos h
				inner join usuario u on h.usua_codi_dest = u.usua_codi and h.depe_codi_dest = u.depe_codi
				inner join dependencia d on h.depe_codi_dest = d.depe_codi
			where h.sgd_ttr_codigo=9 and
				".$db->conn->SQLDate('Y/m/d', 'h.hist_fech')." BETWEEN '$fecha_ini' AND '$fecha_fin' AND
				h.depe_codi = (select depe_codi from usuario where usua_login='$krd') and
				h.usua_codi = (select usua_codi from usuario where usua_login='$krd')
				$whereDependencia
				$whereUsua
			group by u.usua_nomb, d.depe_nomb, u.usua_login";

 	    // Consulta para ver detalles
            $whereUsua = (!empty($_GET['codUs'])) ? "AND usua.usua_login='" . $_GET['codUs'] . "'" : '';
            $queryEDetalle =
				"SELECT h.radi_nume_radi           AS radicado,
                       r.radi_fech_radi           AS fecha_radicado,
                       r.ra_asun                  AS asunto,
                       t.sgd_tpr_descrip          AS tipo_documental,
                       h.hist_fech                AS fecha_reasignacion,
                       h.hist_obse                AS observacion_de_reasignacion,
                       usua.usua_nomb             AS usuario_destino,
                       usua2.usua_nomb            AS usuario_actual,
                       CASE(Lower(substr(r2.radi_path, -3, 5)))
                         WHEN 'pdf' THEN 'TRAMITADO'
                         WHEN 'tif' THEN 'TRAMITADO'
                         ELSE '.'
                       END                        AS estado,
                       a.radi_nume_salida         AS respuesta,
                       r2.radi_fech_radi          AS fecha_radicacion_anexo
                FROM   hist_eventos h
                       INNER JOIN radicado r
                         ON h.radi_nume_radi = r.radi_nume_radi
                       LEFT JOIN anexos a
                         ON r.radi_nume_radi = a.anex_radi_nume
                            AND a.anex_salida = 1
                            AND a.anex_borrado = 'N'
                            AND a.anex_radi_nume <> a.radi_nume_salida
                       LEFT JOIN radicado r2
                         ON a.radi_nume_salida = r2.radi_nume_radi
                       INNER JOIN usuario usua
                         ON h.usua_codi_dest = usua.usua_codi
                            AND h.depe_codi_dest = usua.depe_codi
                            $whereUsua
                       INNER JOIN usuario usua2
                         ON usua2.usua_codi = r.radi_usua_actu
                            AND usua2.depe_codi = r.radi_depe_actu
                       LEFT JOIN sgd_tpr_tpdcumento t
                         ON r.tdoc_codi = t.sgd_tpr_codigo
                WHERE  h.sgd_ttr_codigo = 9 and ".
					$db->conn->SQLDate('Y/m/d', 'h.hist_fech')." BETWEEN '$fecha_ini' AND '$fecha_fin'
					$whereDependencia $whereTipoRadicado and
					h.depe_codi = (select depe_codi from usuario where usua_login='$krd') and
					h.usua_codi = (select usua_codi from usuario where usua_login='$krd')";
			$queryETodosDetalle = $queryEDetalle;

		break;
	case 'oracle':
	case 'oci805':
	case 'ocipo':
		global $orderby;
		$whereDependencia = ($dependencia_busq != 99999) ? "AND h.DEPE_CODI = " . $dependencia_busq : '';
		$whereUsua = ($codus != 0) ? "AND u.USUA_CODI = " . $codus : '';
		$whereTipoRadicado = ($tipoRadicado != '') ? "AND r.RADI_NUME_RADI LIKE '%" . $tipoRadicado . "'": '';
		$whereTipoRadicado.= ($tipoDocumento &&
								($tipoDocumento!='9999' and $tipoDocumento!='9998'))
								? "AND s.SGD_TPR_CODIGO = $tipoDocumento " : '';
	 	/** CONSULTA PARA VER DETALLES
		 */
	 	$whereDependencia = (isset($dependencia_busq) &&  ($dependencia_busq != 99999)) ?
	 							'AND h.DEPE_CODI =' . $dependencia_busq : '';
	 	$whereUsua = ($codus != 0) ? "AND u.USUA_DOC = '" . $codus . "'" : '';
		$whereTipoRadicado = ($tipoDocumento &&
		($tipoDocumento!='9999' and $tipoDocumento!='9998'))
		? "AND r.TDOC_CODI = s.SGD_TPR_CODIGO (+) AND s.SGD_TPR_CODIGO = $tipoDocumento " : '';
		$queryEDetalle = "";
		$queryETodosDetalle = $queryEDetalle;
		break;
	}
?>
