<?php
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
$diasHabiles=" sumadiashabiles(b.radi_fech_radi::date,c.sgd_tpr_termino::integer) ";
//$diasHabiles=" diashabiles(".$db->conn->sysDate.", b.radi_fech_radi::date)-(c.sgd_tpr_termino)";
switch($db->driver)
{
  case 'mssql':
  {
            $diasHabiles=" CONVERT(VARCHAR(10), dbo.sumadiashabiles(b.radi_fech_radi,c.sgd_tpr_termino ), 111) ";
    $isql = 'select
        convert(char(14), b.RADI_NUME_RADI) as "IDT_Numero Radicado"
        ,b.RADI_PATH as "HID_RADI_PATH"
        ,'.$sqlFecha.' as "DAT_Fecha Radicado"
        ,convert(char(14), b.RADI_NUME_RADI) as "HID_RADI_NUME_RADI"
        ,cast(UPPER(b.RA_ASUN) as TEXT)  as "Asunto"'.
        $colAgendado.
        ',d.SGD_DIR_NOMREMDES  as "Destinatario"
        ,c.SGD_TPR_DESCRIP as "Tipo Documento"
                                ,'.$diasHabiles.' as "Fecha Vencimiento"
        ,b.RADI_USU_ANTE "Enviado Por"
        ,convert(char(14),b.RADI_NUME_RADI) "CHK_CHKANULAR"
        ,b.RADI_LEIDO "HID_RADI_LEIDO"
        ,b.RADI_NUME_HOJA "HID_RADI_NUME_HOJA"
        ,b.CARP_PER "HID_CARP_PER"
        ,b.CARP_CODI "HID_CARP_CODI"
        ,b.SGD_EANU_CODIGO "HID_EANU_CODIGO"
        ,b.RADI_NUME_DERI "HID_RADI_NUME_DERI"
        ,b.RADI_TIPO_DERI "HID_RADI_TIPO_DERI"
     from
       radicado b
    left outer join SGD_TPR_TPDCUMENTO c on b.tdoc_codi=c.sgd_tpr_codigo
    left outer join SGD_DIR_DRECCIONES d on b.radi_nume_radi=d.radi_nume_radi
      where
      b.radi_nume_radi is not null
      and b.radi_depe_actu='.$dependencia.
      $whereUsuario.$whereFiltro.'
      '.$whereCarpeta.'
      '.$sqlAgendado.'
      order by '.$order .' ' .$orderTipo;
  }break;
  case 'oracle':
  case 'oci8':
  case 'oci805':
        case 'oci8po':
  case 'ocipo':
  {
    $diasHabiles=" sumadiashabiles(b.radi_fech_radi,c.sgd_tpr_termino)";
    $isql = 'select
        distinct to_char(b.RADI_NUME_RADI) as "IDT_Numero Radicado"
        ,b.RADI_PATH as "HID_RADI_PATH"
        ,'.$sqlFecha.' as "DAT_Fecha Radicado"
        ,to_char(b.RADI_NUME_RADI) as "HID_RADI_NUME_RADI"
        ,UPPER(b.RA_ASUN)  as "Asunto"
        '.$colAgendado.'
        ,c.SGD_TPR_DESCRIP as "Tipo Documento"
        ,'.$diasHabiles.' as "Fecha Vencimiento"
        ,min(a.radi_nume_salida) "IDT_Numero_Salida"
        ,b.RADI_USU_ANTE "Enviado Por"
        ,to_char(b.RADI_NUME_RADI) "CHK_CHKANULAR"
        ,b.RADI_LEIDO "HID_RADI_LEIDO"
        ,b.RADI_NUME_HOJA "HID_RADI_NUME_HOJA"
        ,b.CARP_PER "HID_CARP_PER"
        ,b.CARP_CODI "HID_CARP_CODI"
        ,b.SGD_EANU_CODIGO "HID_EANU_CODIGO"
        ,b.RADI_NUME_DERI "HID_RADI_NUME_DERI"
        ,b.RADI_TIPO_DERI "HID_RADI_TIPO_DERI"
        ,b.SGD_RADI_PRIORIDAD "HID_RADI_PRIORIDAD"
     from radicado b
     left outer JOIN anexos a ON (b.radi_nume_radi = a.anex_radi_nume
                            AND a.radi_nume_salida IS NOT NULL
                            AND a.anex_estado = 4
                            AND a.anex_salida = 1)
     , SGD_TPR_TPDCUMENTO c
   where '.$whereCarpeta. $whereAsunt .'
    b.radi_depe_actu='.$_SESSION['depecodi'].
    $whereUsuario.$whereFiltro.
    'and b.tdoc_codi=c.sgd_tpr_codigo
    '.$sqlAgendado.'
    GROUP BY to_char(b.RADI_NUME_RADI),
      b.RADI_NUME_RADI,
      b.RADI_PATH,
      TO_CHAR(b.RADI_FECH_RADI,\'YYYY-MM-DD\'),
      b.RADI_FECH_RADI,
      TO_CHAR(b.RADI_FECH_RADI,\'YYYY-MM-DD\'),
      to_char(b.RADI_NUME_RADI),
      b.RADI_NUME_RADI,
      UPPER(b.RA_ASUN),
      b.RA_ASUN,
      c.SGD_TPR_DESCRIP,
      sumadiashabiles(b.radi_fech_radi,c.sgd_tpr_termino),
      b.radi_fech_radi,
      c.sgd_tpr_termino,
      b.RADI_USU_ANTE,
      to_char(b.RADI_NUME_RADI),
      b.RADI_NUME_RADI,
      b.RADI_LEIDO,
      b.RADI_NUME_HOJA,
      b.CARP_PER,
      b.CARP_CODI,
      b.SGD_EANU_CODIGO,
      b.RADI_NUME_DERI,
      b.RADI_TIPO_DERI,
      b.SGD_RADI_PRIORIDAD
    order by '.$order .' ' .$orderTipo;
  }break;
  case 'postgres':
  {
    $sesi = $_SESSION['depecodi'];
    $isql = 'select
        b.RADI_NUME_RADI as "'.utf8_encode("IDT_Numero Radicado").'"
        ,b.RADI_PATH as "HID_RADI_PATH"
        ,'.$sqlFecha.' as "DAT_Fecha Radicado"
        , b.RADI_NUME_RADI as "HID_RADI_NUME_RADI"
        ,UPPER(b.RA_ASUN)  as "Asunto"'.
        $colAgendado.
        ',d.SGD_DIR_NOMREMDES  as "Remitente"
        ,c.SGD_TPR_DESCRIP as "Tipo documento"
        ,'.$diasHabiles.' as "Fecha Vencimiento"
        ,b.RADI_USU_ANTE as "Enviado por"
        ,b.RADI_NUME_RADI as "CHK_CHKANULAR"
        ,b.RADI_LEIDO as "HID_RADI_LEIDO"
        ,b.RADI_NUME_HOJA as "HID_RADI_NUME_HOJA"
        ,b.CARP_PER as "HID_CARP_PER"
        ,b.CARP_CODI as "HID_CARP_CODI"
        ,b.SGD_EANU_CODIGO as "HID_EANU_CODIGO"
        ,b.RADI_NUME_DERI as "HID_RADI_NUME_DERI"
        ,b.RADI_TIPO_DERI as "HID_RADI_TIPO_DERI"
     from
     radicado b
  left outer join SGD_TPR_TPDCUMENTO c on b.tdoc_codi=c.sgd_tpr_codigo
  left outer join SGD_DIR_DRECCIONES d on b.radi_nume_radi=d.radi_nume_radi
    where
    b.radi_nume_radi is not null
    and b.radi_depe_actu='."$sesi
    $whereUsuario
    $whereFiltro
    $whereCarpeta
    $sqlAgendado
    order by ".$order .' ' .$orderTipo;
  }break;
}

?>
