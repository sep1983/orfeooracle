<?
switch($db->driver)
{	case 'mssql':
		{
		$query = "SELECT
        		m.depe_codi,
				d.depe_nomb,
				m.sgd_srd_codigo,
				s.sgd_srd_descrip,
				m.sgd_sbrd_codigo,
				su.sgd_sbrd_descrip,
				m.sgd_tpr_codigo,
				t.sgd_tpr_descrip,
				(CASE m.sgd_mrd_esta WHEN '1' THEN 'A' ELSE 'I' END) AS ESTADO,
				su.SGD_SBRD_PROCEDI,
				su.sgd_sbrd_tiemag,
				su.sgd_sbrd_tiemac,
				(CASE su.SGD_SBRD_DISPFIN WHEN '1' THEN 'C. TOTAL' WHEN '2' THEN 'ELIMINACION' WHEN '3' THEN 'M.TECNICO' ELSE 'MUESTREO' END) AS DISPOSICION,
				su.sgd_sbrd_soporte
				FROM SGD_MRD_MATRIRD m,SGD_SRD_SERIESRD s,SGD_SBRD_SUBSERIERD su, SGD_TPR_TPDCUMENTO t, DEPENDENCIA d
				WHERE m.depe_codi = d.depe_codi
				AND m.sgd_srd_codigo = s.sgd_srd_codigo
				AND m.sgd_sbrd_codigo = su.sgd_sbrd_codigo
				AND m.sgd_tpr_codigo = t.sgd_tpr_codigo
				AND s.sgd_srd_codigo = su.sgd_srd_codigo
				AND m.SGD_MRD_ESTA=1
				";
		}break;
	case 'oracle':
	case 'oci8':
  case 'oci8po':
	case 'oci805':
	case 'postgres':
		{
	 $query = "SELECT
        		m.depe_codi,
				d.depe_nomb,
				m.sgd_srd_codigo,
				s.sgd_srd_descrip,
				m.sgd_sbrd_codigo,
				su.sgd_sbrd_descrip,
				m.sgd_tpr_codigo,
				t.sgd_tpr_descrip,
				(CASE WHEN m.sgd_mrd_esta = '1' THEN 'A' ELSE 'I' END) AS ESTADO,
				su.sgd_sbrd_tiemag,
				su.sgd_sbrd_tiemac,
				(CASE WHEN su.SGD_SBRD_DISPFIN = '1' THEN 'C. TOTAL' ELSE CASE WHEN su.SGD_SBRD_DISPFIN = '2' THEN 'ELIMINACION' ELSE CASE WHEN su.SGD_SBRD_DISPFIN = '3' THEN 'M.TECNICO' ELSE  'MUESTREO' END END END) AS DISPOSICION,
				su.sgd_sbrd_soporte,
				su.SGD_SBRD_PROCEDI
				FROM SGD_MRD_MATRIRD m,SGD_SRD_SERIESRD s,SGD_SBRD_SUBSERIERD su, SGD_TPR_TPDCUMENTO t, DEPENDENCIA d
				WHERE m.depe_codi = d.depe_codi
				AND m.sgd_srd_codigo = s.sgd_srd_codigo
				AND m.sgd_sbrd_codigo = su.sgd_sbrd_codigo
				AND m.sgd_tpr_codigo = t.sgd_tpr_codigo
				AND s.sgd_srd_codigo = su.sgd_srd_codigo
				AND m.SGD_MRD_ESTA='1'";
		}break;
}
?>
