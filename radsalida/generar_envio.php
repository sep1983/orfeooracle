<?php
	session_start();
	error_reporting(7);
	$ruta_raiz = "..";
	if (!isset($_SESSION['dependencia']))	include "../rec_session.php";
   	include_once  "../include/db/ConnectionHandler.php";
   	$db = new ConnectionHandler("..");	 
  	if (!defined('ADODB_FETCH_ASSOC'))	define('ADODB_FETCH_ASSOC',2);
  	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

  	if(!$fecha_busq) $fecha_busq=date("Y-m-d");

    #Descargar nuevo formato de planilla masiva.
    if($_POST['reclistma'] && !empty($_POST['no_planilla'])){

        $planilla = $_POST['no_planilla'];

        $where =   " WHERE
                        SGD_RENV_PLANILLA = $planilla AND
                        SGD_RENV_NOMBRE NOT LIKE 'Varios' AND
                        (SGD_RENV_DESTINO LIKE 'Local' OR SGD_RENV_DESTINO LIKE 'Nacional')
                     ORDER 
                        BY RADI_NUME_SAL,SGD_RENV_CODIGO,SGD_RENV_VALOR";

        $queryl = "	
                SELECT 
                    'CERTIFICADO' 	  AS CERTIFICADO,
                    RADI_NUME_SAL 	as REGISTRO,
                    substr(SGD_RENV_NOMBRE,1,36) 			as DESTINATARIO,
                    SGD_RENV_DESTINO 			as DESTINO,
                    SGD_RENV_MPIO 			    as MPIO,
                    SGD_RENV_DEPTO  			as DEPTO,
                    SGD_RENV_DIR   			    as DIR,
                    SGD_RENV_PESO			 	as PESO,
                    SGD_RENV_VALOR 				as VALOR 
                FROM 
                    SGD_RENV_REGENVIO";

        $queryl .= $where;
        $res     = $db->conn->Execute($queryl);

        $queryc = "	
                SELECT 
                    count(1) as count
                FROM 
                    SGD_RENV_REGENVIO";

        $queryc .= $where;
        $resc    = $db->conn->Execute($queryc);
        $num_reg = ($resc)? $resc->fields['COUNT'] : 0; 

        if (!empty($num_reg)){
            // ============================================================+
            //  BEGIN OF FILE INICIO DE IMPRESION DE PLANILLA                                               
            //  UTILIZACION DE LIBRERIAS TCPDF PARA GESTIONAR HTML Y PDF
            // ============================================================+

            require('../fpdf/config/lang/spa.php');
            require_once('../fpdf/tcpdf.php');

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('SGD-ORFEO');
            $pdf->SetTitle('Planilla masiva');
            $pdf->SetSubject('Masiva desde correspondencia');
            $pdf->SetKeywords('planilla, nueva ,correspondencia');

            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

            // //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            //
            // //set some language-dependent strings
            $pdf->setLanguageArray($l);
            //
            if($res) 

                $i = 1;
                while (!$res->EOF){
                    $radicado = $res->fields['REGISTRO'];
                    $peso     = $res->fields['PESO'];
                    $valor    = $res->fields['VALOR'];
                    $ciuda    = substr($res->fields['MPIO'], 0, 8);
                    $depar    = substr($res->fields['DEPTO'], 0, 13);
                    $desti    = substr($res->fields['DESTINATARIO'], 0, 26);
                    $direc    = substr($res->fields['DIR'], 0, 28);
                    $numradi  .= empty($numradi)? $radicado : ', '.$radicado;
                    $nombre_us = empty($nombre_us)? $_SESSION["usua_nomb"] : $nombre_us;
                    $dependencianomb = empty($dependencianomb)? $_SESSION["depe_nomb"] : $dependencianomb;
                    $htmlContent = <<<EOF
                                <tr>
                                    <td> $i </td>
                                    <td> Cert </td>
                                    <td> $radicado </td>
                                    <td> $desti </td>
                                    <td> $ciuda </td>
                                    <td> $depar </td>
                                    <td> $direc </td>
                                    <td> $peso </td>
                                    <td> $valor </td>
                                </tr>
EOF;
                    $contetotal .= $htmlContent;

                    $i++;
                    $num_reg--;

                    if($i > 30 || $num_reg < 1){
                        $j = $i -1;
                        $gtotal = $j * $peso; 
                        $vtotal = "$".number_format($j * $valor);
                        $htmlfoot = <<<EOF
                                </table>
                                <table class="tamfont"  border="0.5">
                                    <tr style="background-color:silver;">
                                        <td align="center"><B> Dependencia </B></td>
                                        <td align="center"><B> Funcionario </B></td>
                                        <td align="center"><B> Peso Total </B></td>
                                        <td align="center"><B> Valor Total </B></td>
                                    </tr>
                                    <tr>
                                        <td align="center"> $dependencianomb </td>
                                        <td align="center"> $nombre_us </td>
                                        <td align="center"> $gtotal </td>
                                        <td align="center"> $vtotal </td>
                                    </tr>
                                    <tr>
                                        <td><br/><br/></td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                    </tr>
                                </table>
EOF;

                        //Encabezado del pdf
                        $htmlHeader = <<<EOF
                                <style>
                                    table.tamfont {
                                            font-family: helvetica;
                                            font-size: 11pt;
                                        }
                                </style>
                                <table class="tamfont" border="0.5">

                                    <tr>
                                        <td align="center"><B> LA ENTIDAD </B></td>
                                        <td colspan="2" align="center"> SERVICIOS POSTALES NACIONALES S.A.</td>
                                    </tr>
                                    <tr>
                                        <td> NIT 899999055-4</td>
                                        <td> PLANILLA PARA LA IMPOSICION DE ENVIOS</td>
                                        <td> Planilla No. $planilla</td>
                                    </tr>
                                    <tr>
                                        <td> TEL 3240800 Ext 1140</td>
                                        <td> LICENCIA DE CREDITO CONTRATO 093-2010</td>
                                        <td> Bogota $fecha_hoy</td>
                                    </tr>
                                </table>
                                <table class="tamfont" border="0.5">
                                    <tr style="background-color:silver;">
                                        <td align="center" width="12mm"> No. </td>
                                        <td align="center" width="12mm"> Tipo </td>
                                        <td align="center" width="34mm"> Radicado </td>
                                        <td align="center" width="75mm"> Destinatatio </td>
                                        <td align="center" width="30mm"> Ciudad </td>
                                        <td align="center" width="37mm"> Departamento </td>
                                        <td align="center" width="82mm"> Direcci&oacute;n</td>
                                        <td align="center" width="14mm"> Gr </td>
                                        <td align="center" width="14mm"> Valor </td>
                                    </tr>
EOF;
                        // add a page
                        $pdf->AddPage();
                        //pagina final
                        $html = $htmlHeader.$contetotal.$htmlfoot;
                        // output the HTML content
                        $pdf->writeHTML($html, true, false, true, false, '');
                        $pdf->lastPage();

                        //Cambiamos valores para nueva planilla
                        $contetotal = $numradi = '';
                        $planilla++;
                        $i = 1;
                    }
                    $res->MoveNext();
                } 
            //Close and output PDF document
            $rutFile = $ruta_raiz.'/'.$carpetaBodega.'/masiva/planMasiva'.date("d_m_Y").'_'.time("h_i_s").'.pdf';
            $pdf->Output($rutFile, 'F');
            //============================================================+
            // END OF FILE                                                
            //============================================================+
            $mensaje = "<h2><center><a href='$rutFile'> ==== Archivo para descarga ===== </a></center></h2>";
        }else{
            $mensaje = "<h2><center> No existe la planilla </center></h2>";
        }
    }
?>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<script>
function validar(action)
{
  if(action!=2)
  {
    document.new_product.action = "generar_envio.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_h=$fechah"?>&generar_listado_existente= Generar Plantilla existente ";
   }else{
     
    document.new_product.action = "generar_envio.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_h=$fechah"?>&generar_listado= Generar Nuevo Envio ";
  }
   solonumeros();
}

function rightTrim(sString)
{	while (sString.substring(sString.length-1, sString.length) == ' ')
	{	sString = sString.substring(0,sString.length-1);  }
	return sString;
}

function solonumeros()
{	jh =  document.getElementById('no_planilla');
	if(rightTrim(jh.value) == "" || isNaN(jh.value))
 	{	alert('S�lo introduzca n�meros.' );
		jh.value = "";
		jh.focus();
 		return false;
	}
	else
	{	document.new_product.submit();	}
}
</script>
<BODY>
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript">
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "new_product", "fecha_busq","btnDate1","<?=$fecha_busq?>",scBTNMODE_CUSTOMBLUE);
</script>
<table class=borde_tab width='100%' cellspacing="5"></table>
  <form name="new_product"  action='generar_envio.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_h=$fechah"?>' method=post>
<center>
<TABLE width="450" class="borde_tab" cellspacing="5">
    <tr><td class=titulos2 colspan="2"><center>GENERACION PLANILLAS Y GUIAS DE CORREO</center></td></tr
  <!--DWLayoutTable-->
  <TR>
    <TD width="125" height="21"  class='titulos2'> Fecha<br>
	<?
	  echo "(".date("Y-m-d").")";
	?>
	</TD>
    <TD width="225" align="right" valign="top" class='listado2'>

        <script language="javascript">
		        dateAvailable.date = "2003-08-05";
			    dateAvailable.writeControl();
			    dateAvailable.dateFormat="yyyy-MM-dd";
    	  </script>
</TD>
  </TR>
  <TR>
    <TD height="26" class='titulos2'> Desde la Hora</TD>
    <TD valign="top" class='listado2'>
	<?
	   if(!$hora_ini) $hora_ini = 01;
   	   if(!$hora_fin) $hora_fin = date("H");
	   if(!$minutos_ini) $minutos_ini = 01;
   	   if(!$minutos_fin) $minutos_fin = date("i");
	   if(!$segundos_ini) $segundos_ini = 01;
   	   if(!$segundos_fin) $segundos_fin = date("s");
	?>

	<select name=hora_ini class='select'>
        <?
			for($i=0;$i<=23;$i++)
			{
			if ($hora_ini==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
            <option value='<?=$i?>' '<?=$datoss?>'>
              <?=$i?>
            </option>
        <?
			}
			?>
      </select>:<select name=minutos_ini class='select'>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($minutos_ini==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>
      </TD>
  </TR>
  <Tr>
    <TD height="26" class='titulos2'> Hasta</TD>
    <TD valign="top" class='listado2'><select name=hora_fin class=select>
        <?
			for($i=0;$i<=23;$i++)
			{
			if ($hora_fin==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>:<select name=minutos_fin class=select>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($minutos_fin==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>
      </TD>
  </TR>
  <tr>
    <TD height="26" class='titulos2'>Tipo de Salida</TD>
    <TD valign="top" align="left" class='listado2'>
<select name=tipo_envio class='select' onChange="submit();" >
<?
$codigo_envio="101";
$datoss = "";
if($tipo_envio==1)
{
	$datoss = " selected ";
	$codigo_envio = "101";
}
?>
<option value=1 '<?=$datoss?>'>CERTIFICADO - PLANILLAS</option>
<?
$datoss = "";
if($tipo_envio==2)
{
	$datoss = " selected ";
	$codigo_envio = "105";
}
?>
<option value=2 '<?=$datoss?>'>POSTEXPRESS - Guias</option>
<?

$datoss = "";
if($tipo_envio==108)
{
	$datoss = " selected ";
	$codigo_envio = "108";
}
?>
<option value=108 '<?=$datoss?>'>NORMAL - PLANILLAS</option>	
<?
$datoss = "";
if($tipo_envio==109)
{
	$datoss = " selected ";
	$codigo_envio = "109";
}
?>
<option value=109 '<?=$datoss?>'>ACUSE DE RECIBO - PLANILLAS</option>	
<?
$datoss = "";
if($tipo_envio==105)
{
	$datoss = " selected ";
	$codigo_envio = "105";
}
?>
<option value=105 '<?=$datoss?>'>POSTEXPRESS - PLANILLAS</option>	

<?
$datoss = "";
if($tipo_envio==107)
{
	$datoss = " selected ";
	$codigo_envio = "107";
}
?>
<option value=107 '<?=$datoss?>'>CORRA - PLANILLAS</option>	

</select>
 </TD>
  </tr>

 <tr>
    <TD height="26" class='titulos2'>Numero de Planilla</TD>
    <TD valign="top" align="left" class='listado2'>
    	<input type="text" name="no_planilla" id="no_planilla" value='<?=$no_planilla?>' class='tex_area' size=11 maxlength="9" >
<?
	$fecha_mes = substr($fecha_busq,0,7) ;
	// conte de el ultimo numero de planilla generado.
	$sqlChar = $db->conn->SQLDate("Y-m","SGD_RENV_FECH");	
//$db->conn->debug = true;	
	//include "$ruta_raiz/include/query/radsalida/queryGenerar_envio.php";	
	$query = "SELECT sgd_renv_planilla, sgd_renv_fech FROM sgd_renv_regenvio
				WHERE DEPE_CODI=$dependencia AND $sqlChar = '$fecha_mes'
					AND ".$db->conn->length."(sgd_renv_planilla) > 0 
					AND sgd_fenv_codigo = $codigo_envio ORDER BY sgd_renv_fech desc, SGD_RENV_PLANILLA desc";
	$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
   	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	$rs = $db->query($query);
	if ($rs) {
		$planilla_ant       = $rs->fields["SGD_RENV_PLANILLA"];
		$fecha_planilla_ant = $rs->fields["SGD_RENV_FECH"];
	}

	if($codigo_envio=="101" or $codigo_envio =="108" or $codigo_envio =="109" or $codigo_envio =="105" or $codigo_envio =="107")
	{
	echo "<br><span class=etexto>&Uacute;ltima planilla generada : <b> $planilla_ant </b>  Fec:$fecha_planilla_ant";
	}
	else
	{

	}
	if($codigo_envio =="107")
	{
?>
    <tr>
    <TD height="26" class='titulos2'>Precinto Nro.</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="precinto" id="precinto" value='<?=$precinto?>' class='tex_area' size=11 maxlength="9" >
	</td>
	</tr>

    <tr>
    <TD height="26" class='titulos2'>Tula Nro.</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="tula" id="tula" value='<?=$tula?>' class='tex_area' size=11 maxlength="9" >
	</td>
	</tr>
	    <tr>
    <TD height="26" class='titulos2'>Comentario</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="coment0" id="coment0" value='<?=$coment0?>' class='tex_area' size=80 maxlength="80" >
	</td>
	</tr>
    <tr>
    <TD height="26" class='titulos2'>Comentario</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="coment1" id="coment1" value='<?=$coment1?>' class='tex_area' size=80 maxlength="80" >
	</td>
	</tr>
    <tr>
    <TD height="26" class='titulos2'>Comentario</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="coment2" id="coment2" value='<?=$coment2?>' class='tex_area' size=80 maxlength="80" >
	</td>
	</tr>
    <tr>
    <TD height="26" class='titulos2'>Comentario</TD>
	<TD valign="top" align="left" class='listado2'>
    	<input type="text" name="coment3" id="coment3" value='<?=$coment3?>' class='tex_area' size=80 maxlength="80" >
	</td>
	</tr>

<?
	}

	// Fin conteo planilla generada
?>
	</TD>
  <tr>
    <td height="26" colspan="2" valign="top" class='titulos2'> <center>
		<INPUT TYPE=button name=generar_listado_existente Value=' Generar Plantilla existente ' class='botones_funcion' onClick="validar(1);">
		</center>
		</td>
	</tr>
	<tr><td height="26" colspan="2" valign="top" class='titulos2'> <center>
        <INPUT TYPE=button name=generar_listado Value=' Generar Nuevo Envio ' class='botones_largo' onClick="validar(2);">
      </center></td>
  </tr>
</TABLE>
</form>

<form name="recuperarMasiva"  action='generar_envio.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_h=$fechah"?>' method=post>
     <TABLE width="450" class="borde_tab" cellspacing="5">
        <tr>
            <td height="26" class='titulos2'>
                Numero de Planilla masiva a recuperar
            </td>
            <td valign="top" align="left" class='listado2'>
               <input type="text" name="no_planilla" id="no_planilla" 
                value='<?=$no_planilla?>' class='tex_area' size=11 maxlength="9">
               <input type='submit' name='reclistma' 
                value='Recuperar Listado' class='botones_mediano'>
            </td>
        </tr>
<?php
    if(!empty($mensaje)){
        echo "<tr>
            <td height='26' colspan='2' class='titulos2'>
                $mensaje
            </td>
        </tr>";
    }
?>
    </table>
</form>

<?php
if(!$fecha_busq) $fecha_busq = date("Y-m-d");
if($generar_listado or $generar_listado_existente)
{
	if($tipo_envio==1)
	{

		error_reporting(7);
		if($generar_listado_existente)  
		{
            $generar_listado = "Genzzz";
            echo "<table class=borde_tab width='100%'><tr><td class=listado2><CENTER>Generar Listado Existente</td></tr></table>";
		}
		include "./listado_planillas.php";
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>FECHA DE BUSQUEDA $fecha_busq</cebter></td></tr></table>";
	}
	if($tipo_envio==2)
	{
		include "./listado_guias.php";
		echo "<table class=borde_tab width='100%'><tr><td class=listado2><CENTER>FECHA DE BUSQUEDA $fecha_busq </center></td></tr></table>";
	}
	if($tipo_envio==108)
	{
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>PLANILLA NORMAL</center></td></tr></table>";
		if($generar_listado_existente)  $generar_listado = "Genzzz";
		include "./listado_planillas_normal.php";
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>FECHA DE BUSQUEDA $fecha_busq </center></td></tr></table>";
	}	
	if($tipo_envio==109)
	{
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>PLANILLA ACUSE DE RECIBO</center></td></tr></table>";
		if($generar_listado_existente)  $generar_listado = "Genzzz";
		include "./lPlanillaAcuseR.php";
		echo "<table class=borde_tab width='100%'><tr><td class=listado2><CENTER>FECHA DE BUSQUEDA $fecha_busq </td></tr></table>";
	}		
	if($tipo_envio==105)
	{
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>PLANILLA POSTEXPRESS</center></td></tr></table>";
		if($generar_listado_existente)  $generar_listado = "Genzzz";
		include "./listado_planillas_postexpress.php";
		echo "<table class=borde_tab width='100%'><tr><td class=listado2><CENTER>FECHA DE BUSQUEDA $fecha_busq </td></tr></table>";
	}
	if($tipo_envio==107)
	{
		echo "<table class=borde_tab width='100%'><tr><td class=titulos2><CENTER>PLANILLA CORRA</center></td></tr></table>";
		if($generar_listado_existente)  $generar_listado = "Genzzz";
		include "./listado_planillas_corra.php";
		echo "<table class=borde_tab width='100%'><tr><td class=listado2><CENTER>FECHA DE BUSQUEDA $fecha_busq </td></tr></table>";
	}	
include "./generar_planos.php";	
}
?>
