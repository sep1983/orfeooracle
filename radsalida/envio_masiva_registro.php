<?php
session_start();
//para que no salgan nada en pantalla
//por que no genera el pdf.
$ruta_raiz = "..";

require_once("$ruta_raiz/config.php");
require_once("$ruta_raiz/include/db/ConnectionHandler.php");

$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

//De ser necesario recupera la sesión
if (!$codusuario or !$dependencia or !$usua_doc)
include "$ruta_raiz/rec_session.php";

$fecha_hoy = Date("Y-m-d");
$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);

//Si existen documentos nacionales
if ($nacional>0){
	$sgd_renv_destino  = "'Nacional'";
	$sgd_renv_valor = $valor   = "'$valor_unit_nacional'";
}

//Si existen documentos locales
if ($local>0){
	$sgd_renv_destino  = "'Local'";
	$sgd_renv_valor = $valor   = "'$valor_unit_local'";
}

//almacena el query
$isql = "  UPDATE
                SGD_RENV_REGENVIO
           set  sgd_renv_planilla    = '$planilla',
                sgd_renv_tipo        = '2',
                sgd_renv_peso        = '$envio_peso',
                usua_doc             = '$usua_doc',
                depe_codi            = '$dependencia',
                sgd_renv_codigo      = '$renv_codigo',
                sgd_fenv_codigo      = '$empresa_envio',
                sgd_renv_fech        = $sqlFechaHoy,
                sgd_renv_telefono    = '$telefono',
                sgd_renv_mail        = '$mail',
                sgd_renv_certificado = 0,
                sgd_renv_estado      = 1,
                sgd_dir_codigo       = 0,
                sgd_dir_tipo         = 1,
                sgd_renv_observa     = '$observaciones',
                sgd_renv_destino     = $sgd_renv_destino,
                sgd_renv_valor       = $sgd_renv_valor
            WHERE
            sgd_renv_planilla = '00' and
            sgd_renv_tipo = 1 and
            radi_nume_grupo = $grupo and
            radi_nume_sal not in (select
                                    sgd_rmr_radi
                                  from
                                    sgd_rmr_radmasivre
                                  where
                                    sgd_rmr_grupo=$grupo) ";
$rs = $db->query($isql);

if (!$rs){
		$db->conn->RollbackTrans();
        die ("<span class='etextomenu'>No se ha
            podido actualizar la base de datos
            SGD_RENV_REGENVIO");
}

$where =   " WHERE
            DEPE_CODI               =  ".$dependencia."
            AND SGD_RENV_CANTIDAD   =  0
            AND RADI_NUME_GRUPO     =  ".$grupo."
            ORDER BY RADI_NUME_SAL,SGD_RENV_CODIGO,SGD_RENV_VALOR";

$queryl = "
        SELECT
            'CERTIFICADO' 	  AS CERTIFICADO,
            RADI_NUME_SAL 	as REGISTRO,
            substr(SGD_RENV_NOMBRE,1,36) 			as DESTINATARIO,
            SGD_RENV_DESTINO 			as DESTINO,
            SGD_RENV_MPIO 			    as MPIO,
            SGD_RENV_DEPTO  			as DEPTO,
            SGD_RENV_DIR   			    as DIR,
            SGD_RENV_PESO			 	as PESO,
            SGD_RENV_VALOR 				as VALOR
        FROM
            SGD_RENV_REGENVIO";

$queryl .= $where;
$res     = $db->conn->Execute($queryl);

$queryc = "
        SELECT
            count(1) as count
        FROM
            SGD_RENV_REGENVIO";

$queryc .= $where;
$resc    = $db->conn->Execute($queryc);
$num_reg = ($resc)? $resc->fields['COUNT'] : 0;

if ($rs){

    // ============================================================+
    //  BEGIN OF FILE INICIO DE IMPRESION DE PLANILLA
    //  UTILIZACION DE LIBRERIAS TCPDF PARA GESTIONAR HTML Y PDF
    // ============================================================+

    require('../fpdf/config/lang/spa.php');
    require_once('../fpdf/tcpdf.php');

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('SGD-ORFEO');
    $pdf->SetTitle('Planilla masiva');
    $pdf->SetSubject('Masiva desde correspondencia');
    $pdf->SetKeywords('planilla, nueva ,correspondencia');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    //
    // //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    //
    // //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    //
    // //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    //
    // //set some language-dependent strings
    $pdf->setLanguageArray($l);
    //
    if($res)

        $i = 1;
        while (!$res->EOF){
            $radicado = $res->fields['REGISTRO'];
            $peso     = $res->fields['PESO'];
            $valor    = $res->fields['VALOR'];
            $ciuda    = substr($res->fields['MPIO'], 0, 8);
            $depar    = substr($res->fields['DEPTO'], 0, 11);
            $desti    = substr($res->fields['DESTINATARIO'], 0, 26);
            $direc    = substr($res->fields['DIR'], 0, 28);
            $numradi  .= empty($numradi)? $radicado : ', '.$radicado;
            $nombre_us = empty($nombre_us)? $_SESSION["usua_nomb"] : $nombre_us;
            $dependencianomb = empty($dependencianomb)? $_SESSION["depe_nomb"] : $dependencianomb;
            $htmlContent = <<<EOF
                        <tr>
                            <td> $i </td>
                            <td> Cert </td>
                            <td> $radicado </td>
                            <td> $desti </td>
                            <td> $ciuda </td>
                            <td> $depar </td>
                            <td> $direc </td>
                            <td> $peso </td>
                            <td> $valor </td>
                        </tr>
EOF;
            $contetotal .= $htmlContent;

            $i++;
            $num_reg--;

            if($i > 30 || $num_reg < 1){
                $j = $i -1;
                $gtotal = $j * $peso;
                $vtotal = "$".number_format($j * $valor);
                $htmlfoot = <<<EOF
                        </table>
                        <table class="tamfont"  border="0.5">
                            <tr style="background-color:silver;">
                                <td align="center"><B> Dependencia </B></td>
                                <td align="center"><B> Funcionario </B></td>
                                <td align="center"><B> Peso Total </B></td>
                                <td align="center"><B> Valor Total </B></td>
                            </tr>
                            <tr>
                                <td align="center"> $dependencianomb </td>
                                <td align="center"> $nombre_us </td>
                                <td align="center"> $gtotal </td>
                                <td align="center"> $vtotal </td>
                            </tr>
                            <tr>
                                <td><br/><br/></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </table>
EOF;

                //Encabezado del pdf
                $htmlHeader = <<<EOF
                        <style>
                            table.tamfont {
                                    font-family: helvetica;
                                    font-size: 11pt;
                                }
                        </style>
                        <table class="tamfont" border="0.5">

                            <tr>
                                <td align="center"><B> La entidad </B></td>
                                <td colspan="2" align="center"> SERVICIOS POSTALES NACIONALES S.A.</td>
                            </tr>
                            <tr>
                                <td> NIT 899999055-4</td>
                                <td> PLANILLA PARA LA IMPOSICION DE ENVIOS</td>
                                <td> Planilla No. $planilla</td>
                            </tr>
                            <tr>
                                <td> TEL 3240800 Ext 1140</td>
                                <td> LICENCIA DE CREDITO CONTRATO 093-2010</td>
                                <td> Bogota $fecha_hoy</td>
                            </tr>
                        </table>
                        <table class="tamfont" border="0.5">
                            <tr style="background-color:silver;">
                                <td align="center" width="12mm"> No. </td>
                                <td align="center" width="12mm"> Tipo </td>
                                <td align="center" width="34mm"> Radicado </td>
                                <td align="center" width="75mm"> Destinatatio </td>
                                <td align="center" width="30mm"> Ciudad </td>
                                <td align="center" width="37mm"> Departamento </td>
                                <td align="center" width="82mm"> Direcci&oacute;n</td>
                                <td align="center" width="14mm"> Gr </td>
                                <td align="center" width="14mm"> Valor </td>
                            </tr>
EOF;
                // add a page
                $pdf->AddPage();
                //pagina final
                $html = $htmlHeader.$contetotal.$htmlfoot;
                // output the HTML content
                $pdf->writeHTML($html, true, false, true, false, '');
                $pdf->lastPage();

                //Actulizar plantilla
                $sqlp = "UPDATE
                            SGD_RENV_REGENVIO
                         SET
                            SGD_RENV_PLANILLA = $planilla
                         WHERE
                            SGD_RENV_NOMBRE NOT LIKE 'VARIOS'
                            AND RADI_NUME_SAL IN ($numradi)";

                $db->conn->Execute($sqlp);

                //Cambiamos valores para nueva planilla
                $contetotal = $numradi = '';
                $planilla++;
                $i = 1;
            }
            $res->MoveNext();
        }
    //Close and output PDF document
    $rutFile = $ruta_raiz.'/'. $carpetaBodega . '/masiva/planMasiva'.date("d_m_Y").'_'.time("h_i_s").'.pdf';
    $pdf->Output($rutFile, 'F');
    //============================================================+
    // END OF FILE
    //============================================================+
    $enlaceret = "./cuerpo_masiva.php?".session_name()."=".session_id()."&krd=$krd";
echo <<<EOF
        <head>
            <title>
                Descarga de planilla
            </title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link rel="stylesheet" href="$ruta_raiz/estilos/orfeo.css" type="text/css">
        </head>
        <body>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="2" class="borde_tab">
            <tr>
                <td  class="titulos4">
                   Planilla masiva para descarga.
                </td>
            </tr>
           <tr>
                <td class="titulos5" align="center">
                    <br/>
                    <h2> <a href="$rutFile"> ====  Archivo para descarga =====</a> </h2>
                    <br/>
                </td>
           </tr>
        </table>
        </body>
EOF;

}
?>
