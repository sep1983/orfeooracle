<?php
session_start();
$ruta_raiz = "..";
require_once("$ruta_raiz/include/db/ConnectionHandler.php");
include "../Administracion/usuario/SecSuperClass.php";
$krdAnt = $krd;

if(!isset($_SESSION['dependencia'])) include "$ruta_raiz/cerrar_session.php";

$ruta_raiz = "..";
if (!$krd)
    $krd = $krdAnt;


//-------------------------------
// busqueda CustomIncludes begin

include ("common.php");
$fechah = date("ymd") . "_" . time("hms");
// busqueda CustomIncludes end
//-------------------------------

//===============================
// Save Page and File Name available into variables
//-------------------------------
$sFileName = "busquedaPiloto.php";
//===============================
//===============================

$usu = $krd;

$niv = $nivelus;
if (strlen($niv)) {
    set_session("UserID", $usu);
    set_session("krd", $krd);
    set_session("Nivel", $niv);
}

// busqueda PageSecurity end
//===============================
//Save the name of the form and type of action into the variables
//-------------------------------
$sAction          = $_POST["FormAction"];
$krd              = get_param("krd");
$sForm            = $_POST["FormName"];
$flds_ciudadano   = $_POST["s_ciudadano"];
$flds_empresaESP  = $_POST["s_empresaESP"];
$flds_oEmpresa    = $_POST["s_oEmpresa"];
$flds_FUNCIONARIO = $_POST["s_FUNCIONARIO"];
//Proceso de vinculacion al vuelo
$verrad      = get_param("verrad");
$carpAnt     = get_param("carpAnt");
$nomcarpeta  = get_param("nomcarpeta");

//Filtros de la busqueda
$whereFlds .= "0";
if ($flds_ciudadano == "CIU")
  $whereFlds .= "1,";
if ($flds_empresaESP == "ESP")
  $whereFlds .= "2,";
if ($flds_oEmpresa == "OEM")
  $whereFlds .= "3,";
if ($flds_FUNCIONARIO == "FUN")
  $whereFlds .= "4,";

$url = $_SERVER['REQUEST_URI'];

?>
<html>
  <head>
      <link rel="stylesheet" href="<?= $ruta_raiz ?>/estilos/orfeo.css" type="text/css">
      <script language="JavaScript" src="<?= $ruta_raiz ?>/js/jquery-1.8.3.min.js"></script>
  </head>
  <script>

  $( document  ).ready(function() {
    $('.vinculos').click(function(){
      history.pushState({}, '', '<?=$url?>');
    });
  });

  function limpiar(){
    document.Search.s_RADI_NUME_RADI.value = "";
    document.Search.s_RADI_NOMB.value = "";
    document.Search.s_RADI_DEPE_ACTU.value = "";
    document.Search.s_DOCTO.value = "";
    document.Search.s_TDOC_CODI.value = "9999";
    document.Search.s_SGD_EXP_SUBEXPEDIENTE.value = "";
    document.Search.s_entrada.value = 9999;
    <?
    $dia = intval(date("d"));
    $mes = intval(date("m"));
    if ($mes == 1){
      $ano_ini = intval(date("Y")) - 1;
    }else{
      $ano_ini = intval(date("Y"));
    }
    $ano = intval(date("Y"));
    ?>
    document.Search.elements['s_desde_dia'].value= "<?= $dia ?>";
    document.Search.elements['s_hasta_dia'].value= "<?= $dia ?>";
    document.Search.elements['s_desde_mes'].value= "<?= ($mes - 1) ?>";
    document.Search.elements['s_hasta_mes'].value= "<?= $mes ?>";
    document.Search.elements['s_desde_ano'].value= "<?= $ano_ini ?>";
    document.Search.elements['s_hasta_ano'].value= "<?= $ano ?>";
  }

  function noPermiso(tip){
    if(tip==0)
      alert ("No tiene permiso para acceder, su nivel es inferior al del usuario actual");
    if(tip==1)
      alert ("No tiene permiso para ver la imagen, el Radicado es confidencial");
    if(tip==2)
      alert("No tiene permiso para ver este radicado");
  }

  function pasar_datos(fecha,num){<?
      echo "if(num==1){";
      echo "opener.document.VincDocu.numRadi.value = fecha\n";
      echo "opener.focus(); window.close();\n }";
      echo "if(num==2){";
      echo "opener.document.insExp.numeroExpediente.value = fecha\n";
      echo "opener.focus(); window.close();}\n";
  ?>}

  </script>

  <body class="PageBODY" onLoad='document.getElementById("cajarad").focus();'>
  <?
    Search_show();
    if ($Busqueda or $s_entrada) {
      Ciudadano_show($nivelus, 9, $whereFlds);
    }
  ?>
  </body>
</html>
<?php

//-------------------------------
// Display Search Form
//-------------------------------
    function Search_show(){
      global $db;
      global $styles;
      global $db2;
      global $db3;
      global $sForm;
      $sFormTitle = "B&uacute;squeda Cl&aacute;sica";
      $sActionFileName = "busquedaPiloto.php";
      $ss_desde_RADI_FECH_RADIDisplayValue = "";
      $ss_hasta_RADI_FECH_RADIDisplayValue = "";
      $ss_TRAD_CODIDisplayValue = "Todos(1,2,3,...)";
      $ss_RADI_DEPE_ACTUDisplayValue = "Todas las dependencias";
      // Con esta variable se determina si la busqueda corresponde a vinculacion documentos
      $verrad = get_param("verrad");
      $carpeAnt = get_param("carpeAnt");
      $nomcarpeta = get_param("nomcarpeta");

      //-------------------------------
      // Set variables with search parameters
      //-------------------------------
      $flds_RADI_NUME_RADI = strip(get_param("s_RADI_NUME_RADI"));
      $flds_GUIA           = strip(get_param("s_GUIA"));
      $flds_DOCTO          = strip(get_param("s_DOCTO"));
      $flds_RADI_NOMB      = strip(get_param("s_RADI_NOMB"));
      $flds_METADATO       = strip(get_param("s_METADATO")); //Modificacion de Busqueda por Metadato para OPAIN S.A.
      $krd                 = get_param("krd");
      $flds_ciudadano      = strip(get_param("s_ciudadano"));

      if ($flds_ciudadano)
        $checkCIU = "checked";
      $flds_empresaESP = strip(get_param("s_empresaESP"));
      if ($flds_empresaESP)
        $checkESP = "checked";
      $flds_oEmpresa = strip(get_param("s_oEmpresa"));
      if ($flds_oEmpresa)
        $checkOEM = "checked";
      $flds_FUNCIONARIO = strip(get_param("s_FUNCIONARIO"));
      if ($flds_FUNCIONARIO)
        $checkFUN = "checked";

      $flds_entrada        = strip(get_param("s_entrada"));
      $flds_salida         = strip(get_param("s_salida"));
      $flds_solo_nomb      = strip(get_param("s_solo_nomb"));
      $Busqueda            = strip(get_param("Busqueda"));
      $flds_desde_dia      = strip(get_param("s_desde_dia"));
      $flds_hasta_dia      = strip(get_param("s_hasta_dia"));
      $flds_desde_mes      = strip(get_param("s_desde_mes"));
      $flds_hasta_mes      = strip(get_param("s_hasta_mes"));
      $flds_desde_ano      = strip(get_param("s_desde_ano"));
      $flds_hasta_ano      = strip(get_param("s_hasta_ano"));
      $flds_TDOC_CODI      = strip(get_param("s_TDOC_CODI"));
      $s_Listado           = strip(get_param("s_Listado"));
      $flds_RADI_DEPE_ACTU = strip(get_param("s_RADI_DEPE_ACTU"));

      /** * Busqueda por expediente */
      $flds_SGD_EXP_SUBEXPEDIENTE = strip(get_param("s_SGD_EXP_SUBEXPEDIENTE"));

      if (strlen($flds_desde_dia) && strlen($flds_hasta_dia) &&
        strlen($flds_desde_mes) && strlen($flds_hasta_mes) &&
        strlen($flds_desde_ano) && strlen($flds_hasta_ano)) {
          $desdeTimestamp = mktime(0, 0, 0, $flds_desde_mes, $flds_desde_dia, $flds_desde_ano);
          $hastaTimestamp = mktime(0, 0, 0, $flds_hasta_mes, $flds_hasta_dia, $flds_hasta_ano);
          $flds_desde_dia = Date('d', $desdeTimestamp);
          $flds_hasta_dia = Date('d', $hastaTimestamp);
          $flds_desde_mes = Date('m', $desdeTimestamp);
          $flds_hasta_mes = Date('m', $hastaTimestamp);
          $flds_desde_ano = Date('Y', $desdeTimestamp);
          $flds_hasta_ano = Date('Y', $hastaTimestamp);
        } else { /* DESDE HACE UN MES HASTA HOY */
          $desdeTimestamp = mktime(0, 0, 0, Date('m') - 1, Date('d'), Date('Y'));
          $flds_desde_dia = Date('d', $desdeTimestamp);
          $flds_hasta_dia = Date('d');
          $flds_desde_mes = Date('m', $desdeTimestamp);
          $flds_hasta_mes = Date('m');
          $flds_desde_ano = Date('Y', $desdeTimestamp);
          $flds_hasta_ano = Date('Y');
        }

      //-------------------------------
      // Search Show begin
      //-------------------------------


      //Select tipo radicado
      if (!$s_Listado){
        $s_Listado = "VerListado";
      }

      if ($flds_entrada == 0){
        $flds_entrada = "9999";
      }

      $optionTex = "<option value=\"9999\">" . $ss_TRAD_CODIDisplayValue . "</option>";

      $lookup_s_entrada = db_fill_array("select
        SGD_TRAD_CODIGO,
        SGD_TRAD_DESCR
        from
        SGD_TRAD_TIPORAD
        order by 2");

      if (is_array($lookup_s_entrada)){
        while (list($key, $value) = each($lookup_s_entrada)) {
          if ($key == $flds_entrada){
            $optionTex .= "<option selected value=\"$key\">$value</option>";
          } else{
            $optionTex .= "<option value=\"$key\">$value</option>";
          }
        }
      }

      $selectTipRad  = "<select name='s_entrada'>
        $optionTex
        </select>";

      if ($flds_TDOC_CODI == 0){
        $flds_TDOC_CODI = "9999";
      }
      $optionTD = "<option value=\"9999\"> Todos </option>";

      $lookup_s_TDOC_CODI = db_fill_array("select
        SGD_TPR_CODIGO,
        SGD_TPR_DESCRIP
        from
        SGD_TPR_TPDCUMENTO order by 2");

      if (is_array($lookup_s_TDOC_CODI)) {
        while (list($key, $value) = each($lookup_s_TDOC_CODI)) {
          if ($key == $flds_TDOC_CODI){
            $optionTD .= "<option SELECTED value=\"$key\">$value</option>";
          } else{
            $optionTD .= "<option value=\"$key\">$value</option>";
          }
        }
      }

      $selectTipDoc  = "<select class=\"select\" name=\"s_TDOC_CODI\">
        $optionTD
        </select>";

      $l = strlen($flds_RADI_DEPE_ACTU);

      if ($l == 0) {
        $optionDep = "<option value=\"\" selected>Todas las dependencias</option>";
      } else {
        $optionDep = "<option value=\"\">Todas las dependencias</option>";
      }

      $radiDepeActu = db_fill_array("select
        DEPE_CODI,
        DEPE_NOMB
        from
        DEPENDENCIA
        where
        depe_estado=1 order by 2");

      if (is_array($radiDepeActu)) {
        reset($radiDepeActu);
        while (list($key, $value) = each($radiDepeActu)) {
          if ($l > 0 && $key == $flds_RADI_DEPE_ACTU){
            $optionDep .= "<option SELECTED value=\"$key\">$value</option>";
          }
          else{
            $optionDep .=  "<option value=\"$key\">$value</option>";
          }
        }
      }

      $selectDepe = "<select name='s_RADI_DEPE_ACTU'>$optionDep</select>";

      for ($i = 1; $i <= 31; $i++) {
        if ($i == $flds_desde_dia){
          $optionInitDay .= "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionInitDay .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateInitDay = "<select name='s_desde_dia'>$optionInitDay</select>";

      for ($i = 1; $i <= 12; $i++) {
        if ($i == $flds_desde_mes){
          $optionInitMon .= "<option selected value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionInitMon .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateInitMon = "<select name='s_desde_mes'>$optionInitMon</select>";

      $agnoactual = Date('Y');

      for ($i = 2013; $i <= $agnoactual; $i++) {
        if ($i == $flds_desde_ano){
          $optionInitYear .= "<option selected value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionInitYear .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateInitYear = "<select name='s_desde_ano'>$optionInitYear</select>";


      for ($i = 1; $i <= 31; $i++) {
        if ($i == $flds_hasta_dia){
          $optionEndDay .= "<option selected value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionEndDay .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateEndDay = "<select name='s_hasta_dia'>$optionEndDay</select>";

      for ($i = 1; $i <= 12; $i++) {
        if ($i == $flds_hasta_mes){
          $optionEndMon .= "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionEndMon .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateEndMon = "<select name='s_hasta_mes'>$optionEndMon</select>";

      for ($i = 2013; $i <= $agnoactual; $i++) {
        if ($i == $flds_hasta_ano){
          $optionEndYear .= "<option SELECTED value=\"" . $i . "\">" . $i . "</option>";
        } else{
          $optionEndYear .= "<option value=\"" . $i . "\">" . $i . "</option>";
        }
      }

      $dateEndYear = "<select name='s_hasta_ano'>$optionEndYear</select>";

    ?>

    <form method="get" action="<?= $sActionFileName ?>?<?= session_name() . "=" . session_id() ?>&verrad=<?= $verrad ?>&carpeAnt=<?= $carpeAnt ?>&nomcarpeta=<?= $nomcarpeta ?>&dependencia=<?= $dependencia ?>&krd=<?= $krd ?>" name="Search">
      <input type="hidden" name=<?= session_name() ?> value=<?= session_id() ?>>
      <input type="hidden" name=krd value=<?= $krd ?>>
      <input type="hidden" name="FormName" value="Search">
      <input type="hidden" name="FormAction" value="search">
      <input type="hidden" name="s_Listado" value="VerListado">

      <div class="tabs">
        <ul>
          <li class="is-active">
            <a href="../busqueda/busquedaPiloto.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? echo "&fechah=$fechah&primera=1&ent=2&s_Listado=VerListado"; ?>">
              General
            </a>
          </li>

          <li>
            <a href="../busqueda/busquedaExp.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? ECHO "&fechah=$fechah&primera=1&ent=2"; ?>">
              Expediente
            </a>
          </li>

          <li>
            <a href="../busqueda/busquedaHist.php?<?= session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">
              Historico
            </a>
          </li>

          <!--
          <li>
            <a href="../busqueda/busquedaUsuActu.php?<?= session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">
              Usuario
            </a>
          </li>
          <li>
            <a href="../busqueda/busquedaUsuActu.php?<?= session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">
              Usuario
            </a>
          </li>
          <li>
            <a href="../expediente/consultaTransferenciaExp.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? ECHO "&fechah=$fechah&primera=1&ent=2"; ?>">
              Transferencia
            </a>
          </li>
          <li>
            <a href="../busqueda/busquedaMetaDato.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? ECHO "&fechah=$fechah&primera=1&ent=2"; ?>">
              Metadatos
            </a>
          </li>
          -->

        </ul>
      </div>
      <div class="columns">
        <div class="column">
          <!-- INIT PRIMERA COLUMNA -->
          <div class="field ">
            <label class="label">Radicado</label>
            <div class="control">
              <input class="input" type="number" min="0" step="1" name="s_RADI_NUME_RADI"
               value="<?= tohtml($flds_RADI_NUME_RADI) ?>" id="cajarad">
            </div>
          </div>

          <div class="field ">
            <label class="label">Asunto</label>
            <div class="control">
              <input class="input"
                type="text"
                name="s_RADI_NOMB"
                maxlength="70"
                value="<?= tohtml($flds_RADI_NOMB) ?>"
                size="70">
            </div>
          </div>


          <div class="field ">
            <label class="label">Dependencia actual</label>
            <div class="control">
              <div class="select">
                <?=$selectDepe?>
              </div>
            </div>
          </div>

          <div class="field ">
            <label class="label">Identificaci&oacute;n (T.I.,C.C.,Nit)</label>
            <div class="control">
              <input class="input"
                type="text"
                name="s_DOCTO"
                maxlength=""
                value="<?= tohtml($flds_DOCTO) ?>"
                size="" >
            </div>
          </div>

        </div>
        <!-- END PRIMERA COLUMNA -->

        <!-- INIT SEGUNDA COLUMNA -->
        <div class="column">

          <div class="field ">
            <label class="label">Desde fecha (dd/mm/yyyy)</label>
            <div class="control">
              <div class="select">
                <?=$dateInitDay?>
              </div>
              <div class="select">
                <?=$dateInitMon?>
              </div>
              <div class="select">
                <?=$dateInitYear?>
              </div>
            </div>
          </div>

          <div class="field ">
            <label class="label">Expediente</label>
            <div class="control">
              <input class="input"
                type="text"
                name="s_SGD_EXP_SUBEXPEDIENTE"
                maxlength=""
                value="<?= tohtml($flds_SGD_EXP_SUBEXPEDIENTE) ?>"
                size="" >
            </div>
          </div>

          <div class="field ">
            <label class="label">Guia</label>
            <div class="control">
              <input class="input"
                type="text"
                name="s_GUIA"
                maxlength="70"
                value="<?= tohtml($flds_GUIA) ?>"
                size="70" >
            </div>
          </div>

          <div class="field ">
            <label class="label">Metadato</label>
            <div class="control">
              <input class="input" type="text" name="s_METADATO" maxlength="70" value="<?= tohtml($flds_METADATO) ?>" size="70" >
            </div>
          </div>

        </div>
        <!-- END SEGUNDA COLUMNA -->

        <!-- INIT TERCERA COLUMNA -->
        <div class="column">
          <div class="field ">
            <label class="label">Hasta fecha (dd/mm/yyyy)</label>
            <div class="control">
              <div class="select">
                <?=$dateEndDay?>
              </div>
              <div class="select">
                <?=$dateEndMon?>
              </div>
              <div class="select">
                <?=$dateEndYear?>
              </div>
            </div>
          </div>

          <div class="field ">
            <label class="label">Tipo de radicado</label>
            <div class="control">
              <div class="select">
                <?=$selectTipRad?>
              </div>
            </div>
          </div>

          <div class="field ">
            <label class="label">Tipo Documental</label>
            <div class="control">
              <div class="select">
                <?=$selectTipDoc?>
              </div>
            </div>
          </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button" type="button" onclick="limpiar();">Limpiar</button>
            </div>
            <div class="control">
              <input class="button is-primary" type="submit" name=Busqueda value="B&uacute;squeda">
            </div>
          </div>

        </div>
      </div>
      <!-- END TERCERA COLUMNA -->
      </form>
      <?
    //-------------------------------
    // Search Show end
    //-------------------------------
    }

    //===============================
    // Display Grid Form
    //-------------------------------
    function Ciudadano_show($nivelus, $tpRemDes, $whereFlds) {
      //-------------------------------
      // Initialize variables
      //-------------------------------
      global $db2;
      global $db3;
      global $sRADICADOErr;
      global $sFileName;
      global $styles;
      global $ruta_raiz;
      $sWhere = "";
      $sOrder = "";
      $sSQL = "";
      $db = new ConnectionHandler($ruta_raiz);
      $UsrSecAux = new SecSuperClass($db);
      $UsrSecAux->SecSuperFill($_SESSION['usua_doc']);



      if ($tpRemDes == 1) {
        $tpRemDesNombre = "Por Ciudadano";
      }

      if ($tpRemDes == 2) {
        $tpRemDesNombre = "Por Otras Empresas";
      }
      if ($tpRemDes == 3) {
        $tpRemDesNombre = "Por Entidad";
      }
      if ($tpRemDes == 4) {
        $tpRemDesNombre = "Por Funcionario";
      }
      if ($tpRemDes == 9) {
        $tpRemDesNombre = "";
        $whereTrd = "   ";
      } else {
        $whereTrd = " and dir.sgd_trd_codigo = $whereFlds  ";
      }

      $sFormTitle = "Radicados encontrados $tpRemDesNombre";

      $HasParam        = false;
      $iRecordsPerPage = 50;
      $iCounter        = 0;
      $iPage           = 0;
      $bEof            = false;
      $iSort           = "";
      $iSorted         = "";
      $sDirection      = "";
      $sSortParams     = "";
      $iTmpI           = 0;
      $iTmpJ           = 0;
      $sCountSQL       = "";
      $transit_params  = "";

      //Proceso de Vinculacion documentos
      $verrad      = get_param("verrad");
      $carpeAnt    = get_param("carpeAnt");
      $nomcarpeta  = get_param("nomcarpeta");

      //-------------------------------
      // Build ORDER BY statement
      //-------------------------------

      $sOrder  = " order by r.radi_fech_radi ";
      $iSort   = get_param("FormCIUDADANO_Sorting");
      $iSorted = get_param("FormCIUDADANO_Sorted");
      $krd     = get_param("krd");

      $form_params = trim(session_name())."=".trim(session_id()) .
        "&krd=$krd&
        verrad=$verrad&
        carpeAnt=$carpeAnt&
        nomcarpeta=$nomcarpeta&
        s_RADI_DEPE_ACTU=". tourl(get_param("s_RADI_DEPE_ACTU")) . "&
        s_RADI_NOMB=" . tourl(get_param("s_RADI_NOMB")) . "&
        s_RADI_NUME_RADI=" . tourl(get_param("s_RADI_NUME_RADI")) . "&
        s_TDOC_CODI=" . tourl(get_param("s_TDOC_CODI")) . "&
        s_desde_dia=" . tourl(get_param("s_desde_dia")) . "&
        s_desde_mes=" . tourl(get_param("s_desde_mes")) . "&
        s_desde_ano=" . tourl(get_param("s_desde_ano")) . "&
        s_hasta_dia=" . tourl(get_param("s_hasta_dia")) . "&
        s_hasta_mes=" . tourl(get_param("s_hasta_mes")) . "&
        s_hasta_ano=" . tourl(get_param("s_hasta_ano")) . "&
        s_solo_nomb=" . tourl(get_param("s_solo_nomb")) . "&
        s_ciudadano=" . tourl(get_param("s_ciudadano")) . "&
        s_empresaESP=" . tourl(get_param("s_empresaESP")) . "&
        s_oEmpresa=" . tourl(get_param("s_oEmpresa")) . "&
        s_FUNCIONARIO=" . tourl(get_param("s_FUNCIONARIO")) . "&
        s_entrada=" . tourl(get_param("s_entrada")) . "&
        s_salida=" . tourl(get_param("s_salida")) . "&
        nivelus=$nivelus&
        s_Listado=" . get_param("s_Listado") . "&
        s_SGD_EXP_SUBEXPEDIENTE=" . get_param("s_SGD_EXP_SUBEXPEDIENTE") . "&";

      if (!$iSort) {
        $form_sorting = "";
      } else {
        if ($iSort == $iSorted) {
          $form_sorting = "";
          $sDirection = " DESC ";
          $sSortParams = "FormCIUDADANO_Sorting=" . $iSort . "&FormCIUDADANO_Sorted=" . $iSort . "&";
        } else {
          $form_sorting = $iSort;
          $sDirection = "  ";
          $sSortParams = "FormCIUDADANO_Sorting=" . $iSort . "&FormCIUDADANO_Sorted=" . "&";
        }

        switch ($iSort) {
        case 1: $sOrder = " order by r.radi_nume_radi" . $sDirection;
        break;
        case 2: $sOrder = " order by r.radi_fech_radi" . $sDirection;
        break;
        case 3: $sOrder = " order by r.ra_asun" . $sDirection;
        break;
        case 4: $sOrder = " order by td.sgd_tpr_descrip" . $sDirection;
        break;
        case 9: $sOrder = " order by dir.sgd_dir_nombre" . $sDirection;
        break;
        }
      }
      //-------------------------------
      // Encabezados HTML de las Columnas
      //-------------------------------
?>
    <table class="table">
     <thead>
      <tr>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=1&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Radicado</a>
          </abbr>
        </th>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=2&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Fecha radicaci&oacute;n</a>
          </abbr>
        </th>
        <th>
          <abbr>Expediente</abbr>
        </th>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=3&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Asunto</a>
          </abbr>
        </th>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=4&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Tipo de documento</a>
          </abbr>
        </th>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=9&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Nombre </a>
          </abbr>
        </th>
        <th>
          <abbr>
            <a href="<?= $sFileName ?>?<?= $form_params ?>FormCIUDADANO_Sorting=15&FormCIUDADANO_Sorted=<?= $form_sorting ?>">Usuario actual</a>
          </abbr>
        </th>
        <th>
          <abbr>
            Dependencia actual
          </abbr>
        </th>
        <th>
          <abbr>
            Usuario anterior
          </abbr>
        </th>
        <th>
          <abbr>
            Gu&iacute;a
          </abbr>
        </th>
      </tr>
     </thead>
<?
      //---------------------------------------------------------------
      // Build WHERE statement
      //-------------------------------

      require_once("../include/query/busqueda/busquedaPiloto1.php");
      $ps_RADI_NUME_RADI = get_param("s_RADI_NUME_RADI");
      //-------------------------------
      // Se crea la $ps_desde_RADI_FECH_RADI con los datos ingresados.
      //---------------------------------------------------------------
      $ps_desde_RADI_FECH_RADI = mktime(0, 0, 0, get_param("s_desde_mes"), get_param("s_desde_dia"), get_param("s_desde_ano"));
      $ps_hasta_RADI_FECH_RADI = mktime(23, 59, 59, get_param("s_hasta_mes"), get_param("s_hasta_dia"), get_param("s_hasta_ano"));

      if (strlen($ps_RADI_NUME_RADI) < 10 ) {
        $HasParam = true;
        $sWhere = $sWhere . $db->conn->SQLDate('Y-m-d', 'r.radi_fech_radi') . " >= " . $db->conn->DBDate($ps_desde_RADI_FECH_RADI);
        $sWhere .= " and ";
        $sWhere = $sWhere . $db->conn->SQLDate('Y-m-d', 'r.radi_fech_radi') . " <= " . $db->conn->DBDate($ps_hasta_RADI_FECH_RADI);
      }


    /****************************************************************
    /* Se recibe la dependencia actual para bsqueda */
      $ps_RADI_DEPE_ACTU = get_param("s_RADI_DEPE_ACTU");
      if (is_number($ps_RADI_DEPE_ACTU) && strlen($ps_RADI_DEPE_ACTU))
        $ps_RADI_DEPE_ACTU = tosql($ps_RADI_DEPE_ACTU, "Number");
      else
        $ps_RADI_DEPE_ACTU = "";

      if (strlen($ps_RADI_DEPE_ACTU)) {
        if ($sWhere != "")
          $sWhere .= " and ";
        $HasParam = true;
        $sWhere = $sWhere . "r.radi_depe_actu=" . $ps_RADI_DEPE_ACTU;
      }

    /****************************************************************
    /* Se recibe el nmero del radicado para bsqueda */
      $ps_DOCTO = get_param("s_DOCTO");
      if (strlen($ps_RADI_NUME_RADI)) {
        if ($sWhere != "")
          $sWhere .= " and ";
        $HasParam = true;
        $sWhere = $sWhere . "$radi_nume_radi like " . tosql("%" . trim($ps_RADI_NUME_RADI) . "%", "Text");
      }

      if (strlen($ps_DOCTO)) {
        if ($sWhere != "")
          $sWhere .= " and ";
        $HasParam = true;
        $sWhere = $sWhere . " dir.SGD_DIR_DOC = '$ps_DOCTO' ";
      }

      /****************************************************************
       * Se recibe el n�mero del expediente para b�squeda */

      $ps_SGD_EXP_SUBEXPEDIENTE =trim(get_param("s_SGD_EXP_SUBEXPEDIENTE"));

      if (strlen($ps_SGD_EXP_SUBEXPEDIENTE) != 0) {
        if ($sWhere != "") {
          $sWhere .= " and ";
        }
        $HasParam = true;
        $sWhere = $sWhere . " R.RADI_NUME_RADI = EXP.RADI_NUME_RADI";
        $sWhere = $sWhere . " AND EXP.SGD_EXP_NUMERO = SEXP.SGD_EXP_NUMERO";
        $sWhere = $sWhere . " AND EXP.SGD_EXP_ESTADO <> 2";
        $sWhere = $sWhere . " AND ( EXP.SGD_EXP_NUMERO LIKE '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%'";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_NOMBRE LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_ASUNTO LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP1 LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP2 LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP3 LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP4 LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " OR SEXP.SGD_SEXP_PAREXP5 LIKE UPPER( '%" . str_replace('\'', '', tosql(trim($ps_SGD_EXP_SUBEXPEDIENTE), "Text")) . "%' )";
        $sWhere = $sWhere . " )";
      }


      /****************************************************************
       * Se decide si busca en radicado de entrada o de salida o ambos
       * */
      $ps_entrada = strip(get_param("s_entrada"));
      $eLen = strlen($ps_entrada);
      $ps_salida = strip(get_param("s_salida"));
      $sLen = strlen($ps_salida);

      if ($ps_entrada != "9999") {
        if ($sWhere != "")
          $sWhere .= " and ";
        $HasParam = true;
        $sWhere = $sWhere . "($radi_nume_radi like " . tosql("%" . trim($ps_entrada), "Text") . ")";
      }


      /****************************************************************
       * Se recibe el tipo de documento para la busqueda
       * */
      $ps_TDOC_CODI = get_param("s_TDOC_CODI");
      if (is_number($ps_TDOC_CODI) && strlen($ps_TDOC_CODI) && $ps_TDOC_CODI != "9999")
        $ps_TDOC_CODI = tosql($ps_TDOC_CODI, "Number");
      else
        $ps_TDOC_CODI = "";
      if (strlen($ps_TDOC_CODI)) {
        if ($sWhere != "")
          $sWhere .= " and ";

        $HasParam = true;
        $sWhere = $sWhere . "r.tdoc_codi=" . $ps_TDOC_CODI;
      }

      /****************************************************************
       * Se recibe la cadena del metadato para la busqueda.
     */
      $ps_METADATO = strip(get_param("s_METADATO"));
      $yaentro = false;
      if (strlen($ps_METADATO)) {
        if ($sWhere != "") {
          $sWhere .= " and MM.SGD_MMR_DATO LIKE '%$ps_METADATO%'";
        }
        $HasParam = true;
        $sWhere .= " ";
      }

      /****************************************************************
       * Se recibe la cadena de busqueda de  la guia
     */
      $ps_GUIA = strip(get_param("s_GUIA"));
      $ps_GUIA=strtoupper($ps_GUIA);
      $yaentro = false;
      if (strlen($ps_GUIA)) {
        if ($sWhere != "")
          $sWhere .= " and ";
        $HasParam = true;
        $sWhere .= $sWhere . " UPPER (r.RADI_GUIA) LIKE '%$ps_GUIA%'";
      }

      /* Se recibe la caadena a buscar y el tipo de busqueda (All) (Any) */
      $ps_RADI_NOMB = trim(strip(get_param("s_RADI_NOMB")));
      $ps_RADI_NOMB = mb_strtoupper(trim($ps_RADI_NOMB), ini_get('default_charset'));
      $ps_solo_nomb = get_param("s_solo_nomb");
      $yaentro = false;
      if (trim($ps_RADI_NOMB))
        $inTD = ",2";
      if (strlen($ps_RADI_NOMB)) { //&& $ps_solo_nomb == "Any")
        if ($sWhere != "")
          $sWhere .= " and (";
        $HasParam = true;
        $sWhere .= " ";

        $ps_RADI_NOMB = strtoupper($ps_RADI_NOMB);
        $tok = strtok($ps_RADI_NOMB, " ");
        $sWhere .= "(";
        while ($tok) {
          $sWhere .= "";
          if ($yaentro == true) {
            $sWhere .= " and ";
          }
          $sWhere .= "UPPER(dir.sgd_dir_nomremdes) LIKE '%" . $tok . "%' ";
          $tok = strtok(" ");
          $yaentro = true;
        }
        $sWhere .=") or (";
        $tok = strtok($ps_RADI_NOMB, " ");
        $yaentro = false;
        while ($tok) {
          $sWhere .= "";
          if ($yaentro == true) {
            $sWhere .= " and ";
          }
          $sWhere .= "UPPER(dir.sgd_dir_nombre) LIKE '%" . $tok . "%' ";
          $tok = strtok(" ");
          $yaentro = true;
        }
        $sWhere .= ") or (";
        $yaentro = false;
        $tok = strtok($ps_RADI_NOMB, " ");
        if ($yaentro == true)
          $sWhere .= " and (";
        $sWhere .= "UPPER(" . $db->conn->Concat("r.ra_asun", "r.radi_cuentai", "dir.sgd_dir_telefono", "dir.sgd_dir_direccion") . ") LIKE '%" . $ps_RADI_NOMB . "%' ";
        $tok = strtok(" ");
        if ($yaentro == true)
          $sWhere .= ")";
        $yaentro = true;
        $sWhere .="))";
      }

      if (strlen($ps_RADI_NOMB) && $ps_solo_nomb == "AllTTT") {
        if ($sWhere != "")
          $sWhere .= " AND (";
        $HasParam = true;
        $sWhere .= " ";

        $ps_RADI_NOMB = strtoupper($ps_RADI_NOMB);
        $tok = strtok($ps_RADI_NOMB, " ");
        $sWhere .= "(";
        $sWhere .= "";
        if ($yaentro == true) {
          $sWhere .= " AND ";
        }
        $sWhere .= "UPPER(dir.sgd_dir_nomremdes) LIKE '%" . $ps_RADI_NOMB . "%' ";
        $tok = strtok(" ");
        $yaentro = true;
        $sWhere .=") OR (";
        $tok = strtok($ps_RADI_NOMB, " ");
        $yaentro = false;
        $sWhere .= "";
        if ($yaentro == true) {
          $sWhere .= " AND ";
        }
        $sWhere .= "UPPER(dir.sgd_dir_nombre) LIKE '%" . $ps_RADI_NOMB . "%' ";
        $tok = strtok(" ");
        $yaentro = true;
        $sWhere .= ") OR (";
        $yaentro = false;
        $tok = strtok($ps_RADI_NOMB, " ");
        if ($yaentro == true)
          $sWhere .= " AND (";
        $sWhere .= "UPPER(" . $db->conn->Concat("r.ra_asun", "r.radi_cuentai", "dir.sgd_dir_telefono", "dir.sgd_dir_direccion") . ") LIKE '%" . $ps_RADI_NOMB . "%' ";
        $tok = strtok(" ");
        if ($yaentro == true)
          $sWhere .= ")";
        $yaentro = true;
        $sWhere .="))";
      }
      if ($HasParam)
        $sWhere = " AND (" . $sWhere . ") ";

      //-------------------------------
      // Build base SQL statement
      //-------------------------------
      require_once("../include/query/busqueda/busquedaPiloto1.php");
      $sSQL = "SELECT " .
        $radi_nume_radi . " AS RADI_NUME_RADI," .
        $db->conn->SQLDate('Y-m-d H:i:s', 'R.RADI_FECH_RADI') . " AS RADI_FECH_RADI,
          r.RA_ASUN,
          td.sgd_tpr_descrip, " .
          $redondeo . " as diasr,
          r.RADI_PATH,
          dir.SGD_DIR_NOMREMDES,
          r.RADI_USU_ANTE,
          r.RADI_GUIA,
          dir.SGD_DIR_NOMBRE,
          dir.SGD_TRD_CODIGO,
          r.RADI_DEPE_ACTU,
          r.RADI_USUA_ACTU,
          r.CODI_NIVEL,
          D.DEPE_NOMB,
          US.USUA_NOMB,
          EXP.SGD_EXP_NUMERO,
          r.SGD_SPUB_CODIGO";


      //Modificacion de la conslta para trabajar con la mejora de la
      //busqueda por metadato.
      if (strlen($ps_SGD_EXP_SUBEXPEDIENTE) != 0) {
        $sSQL .= " FROM SGD_EXP_EXPEDIENTE EXP, SGD_SEXP_SECEXPEDIENTES SEXP, RADICADO R
          INNER JOIN SGD_DIR_DRECCIONES DIR ON R.RADI_NUME_RADI=DIR.RADI_NUME_RADI
          INNER JOIN SGD_TPR_TPDCUMENTO TD ON R.TDOC_CODI=TD.SGD_TPR_CODIGO
          INNER JOIN DEPENDENCIA D ON R.RADI_DEPE_ACTU = D.DEPE_CODI
          INNER JOIN USUARIO US ON R.RADI_DEPE_ACTU = US.DEPE_CODI AND R.RADI_USUA_ACTU = US.USUA_CODI ";
      } else {
        $sSQL .= " FROM RADICADO R
          LEFT  JOIN SGD_EXP_EXPEDIENTE EXP ON (EXP.RADI_NUME_RADI = R.RADI_NUME_RADI AND EXP.SGD_EXP_ESTADO <> 2)
          INNER JOIN SGD_DIR_DRECCIONES DIR ON R.RADI_NUME_RADI=DIR.RADI_NUME_RADI
          INNER JOIN SGD_TPR_TPDCUMENTO TD ON R.TDOC_CODI=TD.SGD_TPR_CODIGO
          INNER JOIN DEPENDENCIA D ON R.RADI_DEPE_ACTU = D.DEPE_CODI
          INNER JOIN USUARIO US ON R.RADI_DEPE_ACTU = US.DEPE_CODI AND R.RADI_USUA_ACTU = US.USUA_CODI ";
      }

      if (strlen($ps_METADATO) != 0) {
        $sSQL .= " LEFT JOIN SGD_MMR_MATRIMETARADI MM ON R.RADI_NUME_RADI = MM.RADI_NUME_RADI";
      }

      $sSQL .= " WHERE dir.sgd_dir_tipo in (7,1$inTD)";

      //---------------------------------
      // Assemble full SQL statement
      //-------------------------------
      $sSQL .= $sWhere . $whereTrd . $sOrder;

      $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
      $rs = $db->query($sSQL);

      $sqlC = "SELECT COUNT(1) AS NODATA FROM ($sSQL)";
      $rsC = $db->query($sqlC);

      $noRows = $rsC->fields['NODATA'];

      //En esta version la consulta esta realizada
      //para ser recorrida con identificacion numerica
      //de las columnas y no por nombre de referencia.
      //se debe rehacer el sistema de exprotación para
      //dejar una sola consulta.
      $db->conn->SetFetchMode(ADODB_FETCH_NUM);
      $bEof = $db->query($sSQL);

      //-------------------------------
      // Process empty recordset
      //-------------------------------
      if ($rs->EOF || !$rs) {
        echo "<tr>
          <td colspan='20' class='alarmas'>No hay resultados</td>
          </tr>";
        return;
      }

      $iCounter = 0;

      $iPage = get_param("FormCIUDADANO_Page");

      if (strlen(trim($iPage)) == 0){
        $iPage = 1;
      } else {
        if ($iPage == "last") {
          $db_count = get_db_value($sCountSQL);
          $dResult = intval($db_count) / $iRecordsPerPage;
          $iPage = intval($dResult);
          if ($iPage < $dResult)
            $iPage++;
        } else{
          $iPage = intval($iPage);
        }
      }

      if (($iPage - 1) * $iRecordsPerPage != 0) {
        do {
          $iCounter++;
          $rs->MoveNext();
        } while ($iCounter < ($iPage - 1) * $iRecordsPerPage && (!$rs->EOF && $rs));
      }

      $iCounter = 0;
      $i = 1;
      echo "<tbody>";
      while ((!$rs->EOF && $rs) && $iCounter < $iRecordsPerPage) {
        $fldRADI_NUME_RADI         = $rs->fields['RADI_NUME_RADI'];
        $fldRADI_FECH_RADI         = $rs->fields['RADI_FECH_RADI'];
        $fldsSGD_EXP_SUBEXPEDIENTE = $rs->fields['SGD_EXP_NUMERO'];
        $fldCUENTAI                = $rs->fields['CUENTAI'];
        $fldASUNTO                 = $rs->fields['RA_ASUN'];
        $fldNUME_HOJAS             = $rs->fields['RADI_NUME_HOJA'];
        $fldRADI_PATH              = $rs->fields['RADI_PATH'];
        $fldDIRECCION_C            = $rs->fields['SGD_DIR_DIRECCION'];
        $fldDIGNATARIO             = $rs->fields['SGD_DIR_NOMBRE'];
        $fldTELEFONO_C             = $rs->fields['SGD_DIR_TELEFONO'];
        $fldMAIL_C                 = $rs->fields['SGD_DIR_MAIL'];
        $fldNOMBRE                 = $rs->fields['SGD_DIR_NOMREMDES'];
        $fldCEDULA                 = $rs->fields['SGD_DIR_DOC'];
        $aRADI_DEPE_ACTU           = $rs->fields['RADI_DEPE_ACTU'];
        $aRADI_USUA_ACTU           = $rs->fields['RADI_USUA_ACTU'];
        $fldPAIS                   = $rs->fields['RADI_PAIS'];
        $fldDIASR                  = $rs->fields['DIASR'];
        $tipoReg                   = $rs->fields['SGD_TRD_CODIGO'];
        $nivelRadicado             = $rs->fields['CODI_NIVEL'];
        $fldDEPE_ACTU              = $rs->fields['DEPE_NOMB'];
        $fldUSUA_ACTU              = $rs->fields['USUA_NOMB'];
        $seguridadRadicado         = $rs->fields['SGD_SPUB_CODIGO'];
        $fldMETADATO               = $rs->fields['SGD_MMR_DATO']; // Busqueda por Metadato - Grupo Iyunxi Ltda

        $fldTIPO_DOC               = tohtml($rs->fields['SGD_TPR_DESCRIP']);
        $fldUSUA_ANTE              = tohtml($rs->fields['RADI_USU_ANTE']);
        $fldGUIA                   = tohtml($rs->fields['RADI_GUIA']);

        $fldNOMBRE = str_replace($ps_RADI_NOMB, "<font color=green><b>$ps_RADI_NOMB</b>", tohtml($fldNOMBRE));
        $fldASUNTO = str_replace($ps_RADI_NOMB, "<font color=green><b>$ps_RADI_NOMB</b>", tohtml($fldASUNTO));

        //-------------------------------
        // Busquedas Anidadas
        //-------------------------------
        $linkDocto = "<font style='color: red;'>Privado<br/>$fldRADI_NUME_RADI</font>";
        $linkInfGeneral = $fldRADI_FECH_RADI;

        if (strlen($fldRADI_PATH)){
          $linkDoctoImg = "<a class='vinculos' href='../seguridadImagen.php?fec=" . base64_encode($fldRADI_PATH) . "'>".$fldRADI_NUME_RADI."</a>";
        } else {
          $linkDoctoImg = "$fldRADI_NUME_RADI";
        }

        $linkInfGeneralRad = "<a class='vinculos' href='../verradicado.php?verrad=$fldRADI_NUME_RADI&" . session_name() . "=" . session_id() . "&krd=$krd&carpeta=8&nomcarpeta=Busquedas&tipo_carp=0'>$fldRADI_FECH_RADI</a>";

        if($seguridadRadicado == 0){
          $linkDocto = $linkDoctoImg;
          $linkInfGeneral = $linkInfGeneralRad;
        }elseif ($seguridadRadicado == 1 and $aRADI_USUA_ACTU == $_SESSION['codusuario'] && $aRADI_DEPE_ACTU == $_SESSION['dependencia']) {
          #Si el radicado esta en la bandeja del usuario actual
          $linkDocto = $linkDoctoImg;
          $linkInfGeneral = $linkInfGeneralRad;

        }elseif ($seguridadRadicado == 2) {
          if ($aRADI_DEPE_ACTU == $_SESSION['dependencia']) {
            $linkDocto = $linkDoctoImg;
            $linkInfGeneral = $linkInfGeneralRad;
          } else {
            $variable_inventada = $_SESSION['dependencia'];
            $linkInfGeneral = $linkInfGeneralRad;
          }

        }elseif ($seguridadRadicado == 3) {
          $sql = "select * from sgd_matriz_nivelrad where radi_nume_radi=$fldRADI_NUME_RADI and usua_login='" . $_SESSION['krd'] . "'";
          $rsVerif = $db->conn->Execute($sql);
          if ($rsVerif && !$rsVerif->EOF or ($aRADI_USUA_ACTU == $_SESSION['codusuario'] && $aRADI_DEPE_ACTU == $_SESSION['dependencia'])) {
            $linkDocto = $linkDoctoImg;
            $linkInfGeneral = $linkInfGeneralRad;
          }
        }

        if ($_SESSION['usua_super_perm'] != 0) {
          if ($UsrSecAux->SecureCheckBusq($fldRADI_NUME_RADI, $fldDEPE_ACTU , $fldUSUA_ACTU)) {
            $linkInfGeneral = $linkInfGeneralRad;
          }
        }

      if ($i == 1) {
        $formato = "listado1";
        $i = 2;
      } else {
        $formato = "listado2";
        $i = 1;
      }

      $fldUSUA_ACTU = tohtml($fldUSUA_ACTU);
      $fldDEPE_ACTU = tohtml($fldDEPE_ACTU);

      $content .=  "<tr class='$formato'>
                      <td>$linkDocto</td>
                      <td>$linkInfGeneral</a></td>
                      <td>$fldsSGD_EXP_SUBEXPEDIENTE</td>
                      <td>$fldASUNTO</td>
                      <td>$fldTIPO_DOC</td>
                      <td>$fldNOMBRE</td>
                      <td>$fldUSUA_ACTU</td>
                      <td>$fldDEPE_ACTU</td>
                      <td>$fldUSUA_ANTE</td>
                      <td>$fldGUIA</td>
                    </tr>";

      $iCounter++;
      $rs->MoveNext();
      }

      $content .= "</tbody></table>";

      if ($bEof && !$bEof->EOF) {
        $iCounter = 1;
        $iHasPages = $iPage;
        $sPages = "";
        $iDisplayPages = 0;
        $iPageCount = 1;
        $nowPage = $_GET['FormCIUDADANO_Page'];

        //Si el numero de registros es mayor al numero
        //de lineas por pagina, buscamos la cantidad
        //de paginas necesarias registros

        if($noRows > $iRecordsPerPage){
          $res = fmod($noRows, $iRecordsPerPage);
          $iNumberOfPages = (($noRows - $res) / $iRecordsPerPage) + 1;
        }else{
          $iNumberOfPages = 1;
        }

        while ((!$rs->EOF && $rs) && $iHasPages < $iPage + $iNumberOfPages) {
          if ($iCounter == $iRecordsPerPage) {
            $iCounter = 0;
            $iHasPages = $iHasPages + 1;
          }
          $iCounter++;
          $rs->MoveNext();
        }

        if (($rs->EOF || !$rs) && $iCounter > 1)
          $iHasPages++;
        if (($iHasPages - $iPage) < intval($iNumberOfPages / 2))
          $iStartPage = $iHasPages - $iNumberOfPages;
        else
          $iStartPage = $iPage - $iNumberOfPages + intval($iNumberOfPages / 2);

        while ($iDisplayPages < $iNumberOfPages && $iStartPage + $iDisplayPages < $iHasPages) {
          $showMark = '';

          if($iPageCount == $nowPage){
            $showMark = 'is-current';
          }

          $sPages .= "<li><a class='pagination-link $showMark'  href=\"" . $sFileName . "?" . $form_params . $sSortParams .
            "FormCIUDADANO_Page=" . $iPageCount . "#RADICADO\">" . $iPageCount . "</a></li>";
          $iDisplayPages++;
          $iPageCount++;
        }

        if ($iNumberOfPages > 1 ){

          if($nowPage != 1){
            $prePage = $iPage - 1;
            $sPages .= "<li><a class='pagination-next' href='$sFileName?$form_params"."$sSortParams"."FormCIUDADANO_Page=$prePage#RADICADO'>Anterior</a></li>";
          }

          if($nowPage != $iNumberOfPages){
            $nexPage = $iPage + 1;
            $sPages .= "<li><a class='pagination-next' href='$sFileName?$form_params"."$sSortParams"."FormCIUDADANO_Page=$nexPage#RADICADO'>Siguiente</a></li>";
          }

        }
      }

      if (!$bEof->EOF || $bEof){
        if (!isset($carpetaBodega)) {
          include "$ruta_raiz/config.php";
        }
        include_once("$ruta_raiz/adodb/toexport.inc.php");

        $ruta = "$ruta_raiz/" . $carpetaBodega . "tmp/Busqclasic" . date('Y_m_d_H_i_s') . ".csv";
        $f = fopen($ruta, 'w');
        if ($f) {
          rs2csvfile($bEof, $f);
          $sPages .= "<li>
                        <a class='button is-info is-outlined' href='$ruta' >
                          <span>Csv</span>
                          <span class='icon is-small'>
                            <i class='el el-download-alt'></i>
                          </span>
                        </a>
                      </li>";
        }
      }

      echo "<div class='box'><ul class='pagination-list'>".$sPages."</ul></div>";
      echo $content;
      echo "<div class='box'><ul class='pagination-list'>".$sPages."</ul></div>";

    }

//===============================
// Display Grid Form
//-------------------------------
function resolverTipoCodigo($tipo) {
  $salida;
  switch ($tipo) {
  case 1:
    $salida = "Ciudadano";
    break;
  case 2:
    $salida = "Empresa";
    break;
  case 3:
    $salida = "Entidad";
    break;
  case 4:
    $salida = "Funcionario";
    break;
  }
  return $salida;
}

function resalaltarTokens(&$tkens, $busqueda) {
  $salida = $busqueda;
  $tok = explode(" ", $tkens);
  foreach ($tok as $valor) {
    $salida = eregi_replace($valor, "<font color=\"green\"><b>" . strtoupper($valor) . "</b></font>", $salida);
  }
  return $salida;
}

?>
