<?php
session_start();
$verrad = "";
$ruta_raiz = "..";
define('ADODB_ASSOC_CASE',1);
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

//===================================Series - Subseries==============================
$seriesSubseries=1;
if($seriesSubseries == 1){
$a=$slc_srd;
$b = $slc_sbrd;

$id_srdDefa = ($_POST['slc_srd']) ? $_POST['slc_srd'] : ($slc_srd !="" ? $slc_srd :0);
$id_sbrdDefa = $_POST['slc_sbrd'] ? $_POST['slc_sbrd'] : ($slc_sbrd !="" ? $slc_sbrd : 0);
$id_tdocDefa = $_POST['slc_tdoc'];

}else {
	$id_srdDefa = 0;
	$id_sbrdDefa = 0;
	$id_tdocDefa = '';
	$srdCheck = "";
	$tdoCheck = "";
	$deshab_tdoc = "";
}

$sql = "SELECT SGD_SRD_CODIGO || '-' || SUBSTR(SGD_SRD_DESCRIP,0,40) as DESCRIP, SGD_SRD_CODIGO as CODI FROM SGD_SRD_SERIESRD ORDER BY SGD_SRD_DESCRIP";
$rs = $db->conn->Execute($sql);
$slc_srd = $rs->GetMenu2('slc_srd', $id_srdDefa, "0:Todos los tipos", false, 0, "id='slc_srd' onchange='this.form.submit()'");

$sqlSubserie = "SELECT SGD_SBRD_CODIGO ||'-'|| SGD_SBRD_DESCRIP as SGD_SBRD_DESCRIP, SGD_SBRD_CODIGO FROM SGD_SBRD_SUBSERIERD WHERE SGD_SRD_CODIGO = $id_srdDefa ORDER BY SGD_SBRD_DESCRIP";
$rss = $db->conn->Execute($sqlSubserie);
$slc_sbrd = $rss->GetMenu2('slc_sbrd', $id_sbrdDefa, ":", false, 0, "id='slc_sbrd' onchange='this.form.submit()'");

//================================================================================================

if (!$_SESSION['dependencia'])   include "$ruta_raiz/rec_session.php";

if($orden_cambio==1)
	(!$orderTipo) ? $orderTipo="desc" : $orderTipo="";

if(!$orderNo)
{
	$orderNo="0";
	$order = 1;
}else
{
	$order = $orderNo +1;
}

if(!isset($fecha_ini))$fecha_ini=date("Y/m/d",strtotime('-12 month'));
if(!isset($fecha_fin)) $fecha_fin = date("Y/m/d");

$encabezado1 = "$PHP_SELF?".session_name()."=".session_id()."&krd=$krd";
$linkPagina = "$encabezado1&n_nume_radi=$n_nume_radi&s_RADI_NOM=$s_RADI_NOM&s_solo_nomb=$s_solo_nomb&s_entrada=$s_entrada&s_salida=$s_salida&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&fecha1=$fecha1&tipoDocumento=$tipoDocumento&dependenciaSel=$dependenciaSel&orderTipo=$orderTipo&nume_expe=$nume_expe&txtExpe=$txtExpe&txtMetadato=$txtMetadato&insertaExp=$insertaExp&slc_srd=$id_srdDefa&slc_sbrd=$id_sbrdDefa&orderNo=$orderNo";
$encabezado = "".session_name()."=".session_id()."&krd=$krd&n_nume_radi=$n_nume_radi&s_RADI_NOM=$s_RADI_NOM&s_solo_nomb=$s_solo_nomb&insertaExp=$insertaExp&s_entrada=$s_entrada&s_salida=$s_salida&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&fecha1=$fecha1&tipoDocumento=$tipoDocumento&dependenciaSel=$dependenciaSel&nume_expe=$nume_expe&txtExpe=$txtExpe&txtMetadato=$txtMetadato&insertaExp=$insertaExp&orderTipo=$orderTipo&orderNo=";
$nombreSesion = "".session_name()."=".session_id();

/* Se recibe el numero del Expediente a Buscar */
if($nume_expe or $adodb_next_page or $orderNo or $orderTipo or $orden_cambio or $dependenciaSel){
	//Se valida rango de fechas
	$sqlFecha = $db->conn->SQLDate('Y/m/d',"F.SGD_SEXP_FECH");
	$where_general = " WHERE ".$sqlFecha . " BETWEEN '$fecha_ini' AND '$fecha_fin'" ;

	/* Se recibe la dependencia actual para bsqueda */
	if ($dependenciaSel == "99999")
		$where_general .= " AND F.DEPE_CODI IS NOT NULL ";
	else
		$where_general .= " AND F.DEPE_CODI = ".$dependenciaSel;

	// Se valida el expediente
	if ($nume_expe){
		$where_general.= " AND F.SGD_EXP_NUMERO LIKE '%".trim($nume_expe)."%' ";
	}

  if($txtExpe){
    $where_general.= " AND ( F.SGD_SEXP_NOMBRE LIKE UPPER( '%".str_replace( '\'', '',  trim( $txtExpe ) )."%' )";
    $where_general.= " OR F.SGD_SEXP_ASUNTO LIKE UPPER( '%".str_replace( '\'', '',trim( $txtExpe ) )."%' )";
    $where_general.= " ) ";
  }


  /* Busqueda por nivel y usuario
    $where_general .= " AND R.CODI_NIVEL <= ".$nivelus;
   */
  include($ruta_raiz."/include/query/busqueda/busquedaPiloto1.php");

  //Creamos la columna por el cual ORDENAR los resutados
  switch ($orderNo){
  case 0:
    $c_order = " ORDER BY 1 ";
    break;
  case 1:
    $c_order = " ORDER BY 2 ";
    break;
  case 2:
    $c_order = " ORDER BY 3 ";
    break;
  case 3:
    $c_order = " ORDER BY 4 ";
    break;
  case 4:
    $c_order = " ORDER BY 5 ";
    break;
  case 5:
    $c_order = " ORDER BY 6 ";
    break;
  case 6:
    $c_order = " ORDER BY 7 ";
    break;
  case 7:
    $c_order = " ORDER BY 8 ";
    break;
  case 8:
    $c_order = " ORDER BY 9 ";
    break;
  case 9:
    $c_order = " ORDER BY 10 ";
    break;
  case 10:
    $c_order = " ORDER BY 11 ";
    break;
  case 11:
    $c_order = " ORDER BY 12 ";
    break;
  }

  $c_order .= (!$orderTipo) ? "asc" : "desc";


  $from_normal = "FROM SGD_SEXP_SECEXPEDIENTES F";

  /*************************************************
   * Busqueda de Expediente por Serie, Subserie o Metadato
   * Para: OPAIN S.A.
   * Por: Grupo Iyunxi Ltda.
   */
  //Por Metadato
  if($txtMetadato){
    $where_metadato= " AND (sgd_mmr_dato LIKE '%$txtMetadato%')";
    $from_metadato = " left join sgd_mmr_matrimetaexpe me on f.sgd_exp_numero = me.sgd_exp_numero";
    $where_general.=$where_metadato;
    $from_normal.= $from_metadato;
  }

  //Por serie
  if($id_srdDefa){
    $where_serie  = " AND (sgd_srd_codigo = $id_srdDefa)";
    $where_general .= $where_serie;
  }

  //Por Subserie
  if($id_sbrdDefa){
    $where_subSerie = " AND (sgd_sbrd_codigo = $id_sbrdDefa)";
    $where_general .= $where_subSerie;
  }
  /*************************************************/

  $sql =
    'SELECT	F.SGD_EXP_NUMERO AS "No_expediente",
    F.SGD_SEXP_FECH AS "DEX_Fecha creacion",
    F.sgd_sexp_nombre AS "Nombre",
    F.sgd_sexp_asunto AS "Asunto",
    u.usua_nomb AS "Usuario responsable"
    '.$from_normal.'
    JOIN USUARIO u on u.usua_doc=F.usua_doc_responsable'.
    $where_general.$c_order;

  $ADODB_COUNTRECS = true;
  $rs    = $db->conn->Execute($sql);
  $rscsv =  clone $rs;

  if ($rs){
    $nregis = $rs->recordcount();
    $fldTotal = $nregis;
  } else{
    $fldTotal = 0;
  }
  $ADODB_COUNTRECS = false;
  $pager = new ADODB_Pager($db,$sql,'adodb', true,$orderNo,$orderTipo);
  $pager->checkAll    = false;
  $pager->checkTitulo = true;
  $pager->toRefLinks  = $linkPagina;
  $pager->toRefVars   = $encabezado;
  $pager->txtBusqueda = trim($txtExpe);
  if($insertaExp)$pager->pasarDatos = true;

}

$sql = "SELECT
          dep_sigla ".$db->conn->concat_operator."'-'".$db->conn->concat_operator."
          DEPE_NOMB,
          DEPE_CODI
        FROM
          DEPENDENCIA
        WHERE depe_estado=1 ORDER BY 1";

$rs = $db->conn->execute($sql);

$cmb_dep = $rs->GetMenu2('dependenciaSel',
  $dependenciaSel,
  $blank1stItem = "99999:Todas las dependencias",
  false,0,'class=select');

?>
<html>
<head>
  <title>Consultas</title>
  <link rel="stylesheet" href="../estilos/orfeo.css">
  <script>
    function limpiar(){
      document.Search.elements['nume_expe'].value = "";
      document.Search.elements['txtExpe'].value = "";
      document.Search.elements['txtMetadato'].value = "";
      document.Search.elements['dependenciaSel'].value = "99999";
      document.Search.elements['slc_sbrd'].value = "0";
      document.Search.elements['slc_srd'].value = "0";
    }

    function pasarDatos(numExp){
      opener.document.getElementById('numeroExpediente').value=numExp;
      window.close();
    }
  </script>
</head>

<body onload="document.getElementById('nume_expe').focus();">
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript"><!--
var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "Search", "fecha_ini","btnDate1","<?=$fecha_ini?>",scBTNMODE_CUSTOMBLUE);
var dateAvailable1 = new ctlSpiffyCalendarBox("dateAvailable1", "Search", "fecha_fin","btnDate2","<?=$fecha_fin?>",scBTNMODE_CUSTOMBLUE);
//-->
</script>
<form  name="Search"  action='<?=$_SERVER['PHP_SELF']?>?<?=$encabezado?>' method=post>
  <div class="tabs">
    <ul>

      <li>
        <a href="../busqueda/busquedaPiloto.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? echo "&fechah=$fechah&primera=1&ent=2&s_Listado=VerListado"; ?>">
          General
        </a>
      </li>

      <li class="is-active">
        <a href="../busqueda/busquedaExp.php?<?= $phpsession ?>&krd=<?= $krd ?>&<? ECHO "&fechah=$fechah&primera=1&ent=2"; ?>">
          Expediente
        </a>
      </li>

      <li>
        <a href="../busqueda/busquedaHist.php?<?= session_name() . "=" . session_id() . "&fechah=$fechah&krd=$krd" ?>">
          Historico
        </a>
      </li>

    </ul>
  </div>
  <div class="columns">
    <div class="column">
      <input type="hidden" name="FormName" value="Search">
      <input type="hidden" name="FormAction" value="search">
      <!-- INIT PRIMERA COLUMNA -->
      <div class="field ">
        <label class="label">No. expediente:</label>
        <div class="control">
          <input
            class="input is-small"
            type="text"
            name="nume_expe"
            id="nume_expe"
            maxlength="18"
            value="<?=$nume_expe?>"
            size="" >
        </div>
      </div>

      <div class="field ">
        <label class="label">Metadato:</label>
        <div class="control">
          <input
            class="input is-small"
            type="text"
            name="txtMetadato"
            id="txtMetadato"
            maxlength=""
            value="<?=$txtMetadato?>"
            size="" >
        </div>
      </div>


      <div class="field ">
        <label class="label">Desde fecha (dd/mm/yyyy)</label>
        <div class="control">
          <script language="javascript">
            dateAvailable.writeControl();
            dateAvailable.dateFormat="yyyy/MM/dd";
          </script>
        </div>
      </div>

    </div>
    <!-- END PRIMERA COLUMNA -->

    <!-- INIT SEGUNDA COLUMNA -->
    <div class="column">

      <div class="field ">
        <label class="label">Buscar por:</label>
        <div class="control">
          <input
            class="input is-small"
            type="text"
            name="txtExpe"
            id="txtExpe"
            maxlength=""
            value="<?=$txtExpe?>"
            size="" >
        </div>
      </div>

      <div class="field ">
        <label class="label">Serie:</label>
        <div class="control">
          <div class="select">
            <?=$slc_srd;?>
          </div>
        </div>
      </div>


      <div class="field ">
        <label class="label">Hasta fecha (dd/mm/yyyy)</label>
        <div class="control">
          <script language="javascript">
              dateAvailable1.writeControl();
              dateAvailable1.dateFormat="yyyy/MM/dd";
          </script>
        </div>
      </div>

    </div>
    <!-- END SEGUNDA COLUMNA -->

    <!-- INIT TERCERA COLUMNA -->
    <div class="column">

      <div class="field ">
        <label class="label">Dependencia creadora:</label>
        <div class="control">
          <div class="select">
            <?=$cmb_dep; ?>
          </div>
        </div>
      </div>

      <div class="field ">
        <label class="label">SubSerie:</label>
        <div class="control">
          <div class="select">
            <?=$slc_sbrd; ?>
          </div>
        </div>
      </div>

      <div class="field is-grouped">
        <div class="control">
          <button class="button" type="button" onclick="limpiar();">Limpiar</button>
        </div>
        <div class="control">
          <input class="button is-primary" type="submit" name='busqueda' value="B&uacute;squeda">
        </div>
      </div>

    </div>
    <!-- END TERCERA COLUMNA -->
  </div>

<?php

	if($rscsv && !$rscsv->EOF) {
 		if (!isset($carpetaBodega)) {
			include "$ruta_raiz/config.php";
		}
		include_once("$ruta_raiz/adodb/toexport.inc.php");

		$ruta = "$ruta_raiz/".$carpetaBodega."tmp/BusqExp".date('Y_m_d_H_i_s').".csv";

	  $f = fopen($ruta, 'w');
		if ($f) {
			rs2csvfile($rscsv, $f);
      $sPages .= "<li>
                    <a class='button is-info is-outlined' href='$ruta' >
                      <span>Csv</span>
                      <span class='icon is-small'>
                        <i class='el el-download-alt'></i>
                      </span>
                    </a>
                  </li>";
		}

 	}

if($nume_expe or $adodb_next_page or $orderNo or $orderTipo or $orden_cambio or $dependenciaSel){
    echo "<div class='box'><ul class='pagination-list'>".$sPages."</ul></div>";
    $pager->Render($rows_per_page=20,$linkPagina,$checkbox=chkAnulados);
    echo "<div class='box'><ul class='pagination-list'>".$sPages."</ul></div>";
}


?>
</form>
</body>
</html>
