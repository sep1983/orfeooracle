<?php
session_start();
$ruta_raiz = '.';

if(!isset($_SESSION['dependencia']) and !isset($_SESSION['cod_local'])){
  include "$ruta_raiz/rec_session.php";
}

$krd = $_SESSION["krd"];
$ssid = session_name()."=".session_id()."&krd=$krd";

if($autentica_por_LDAP != 1){
  $itemPass = "<a class='navbar-item'
                  href='./contraxx.php'
                  target='_top'>
                    Contrase&ntilde;a
               </a>";
}

?>

  <html>
    <head>
      <link rel="stylesheet" href="./estilos/orfeo.css">
    </head>

    <body>
      <nav class="navbar navMenuEdit">
        <div class="navbar-brand">
          <span class="navbar-item">
            <img src="./img/logoEntidad.jpg" alt="SGD-Orfeo" height="28">
          </span>
        </div>
        <div class="navbar-menu">
          <div class="navbar-end">
            <a class="navbar-item" href="./mod_datos.php?<?=$ssid?>" target="mainFrame">
              Usuario
            </a>
            <a class="navbar-item" href="./Manuales/index.php?<?=$ssid?>" target="mainFrame">
              Plantillas
            </a>
            <!--Init Mostrar cambio de clave -->
            <?=$itemPass?>
            <!--End -->
            <a class="navbar-item" href="./estadisticas/vistaFormConsulta.php?<?=$ssid?>" target="mainFrame">
              Estadisticas
            </a>
            <a class="navbar-item" href="./cerrar_session.php?<?=$ssid?>" target="_top">
              Cerrar
            </a>
          </div>
        </div>
      </nav>
    </body>

  </html>
