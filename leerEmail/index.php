<?PHP
/**
* Este script permite buscar en el correo cc_mailbox suministrado en el 
* archivo de configuración config.php las certificaciones emitidas
* por la entidad de los envios realizados desde el modulo de 
* correspondencia por correo electronico.
*
* Las rutas son absolutas por que se ejecuta el script mediante un cront
* y no permite rutas relativas.

* # VERIFICA CORREO CERTIFICADO Y GENERA LA ACCION EN EL SGD-ORFEO produccion.
  # *\5 * * * *    php -f /var/www/html/orfeo/secure/prod/leerEmail/index.php >> /var/log/sgd_orfeo_certimail_prod.txt;
*/

date_default_timezone_set('America/Bogota');
$ruta_raiz = "/var/www/html/orfeo/secure/prod/";

require_once($ruta_raiz."leerEmail/Imap.php");
require_once($ruta_raiz."config.php");
require_once($ruta_raiz."include/db/ConnectionHandler.php");


$db = new ConnectionHandler("$ruta_raiz");


$path1    = 'pdfs/resCertificados/';
$adjuntos = $ruta_raiz.$carpetaBodega.$path1;
$compName = date("YmdHms");

//Conexion al correo electronico
$imap = new Imap($cc_mailbox, $cc_username, $cc_password, $cc_encryption);
$re  = '/\d+/'; 
$dat = $radCer = array();
$log = '';

$sqlupdate = "UPDATE SGD_RENV_REGENVIO
              SET SGD_RENV_RUTA_ANEXO = '%s'
              WHERE RADI_NUME_SAL = %d ";

// stop on error
if($imap->isConnected()===false)
    die($imap->getError());

// select folder Inbox
$imap->selectFolder('INBOX');

// count messages in current folder
$overallMessages = $imap->countMessages();
$unreadMessages  = $imap->countUnreadMessages();

// fetch all messages in the current folder
$emails = $imap->getUnreadMessages(false);
foreach ($emails as $email) {
    $idemail = $email['uid'];
    $from   = preg_replace('[\s+]',"", $email['from']);
    $string = ' ' . $from; 
    $ini    = strpos($string, '<');

    if ($ini == 0) return '';
    $ini++;
    $len    = strpos($string, '>', $ini) - $ini;
    $fromEx = substr($string, $ini, $len);

    if($fromEx === $email_certimail){
	$namFile  = ''; 
	$radCer[] = $idemail;
	$subject  = $email['subject'];
	preg_match($re, $subject,  $matches);
        $radi_nume_radi = implode("",$matches);
	//Trae el primer adjunto
        $attac1 = $imap->getAttachment($idemail,0);
        $nameFile = $radi_nume_radi."_".$compName.".pdf";
	
	//Guardar el adjunto
	$file = fopen($adjuntos.$nameFile,"w");
        
	if (fwrite($file,  $attac1['content']) === false) {
             $log .="No se escribiro el archivo" .$radi_nume_radi."\n";
        }

	fclose($file);
	//Log para crontab
	$log .= "Numero de radicado".$radi_nume_radi."\n";

	$query = sprintf($sqlupdate, $nameFile, $radi_nume_radi);

        $db->conn->query($query);

    }else{
	$dat[] = $idemail;
    }

    $imap->setUnseenMessage($idemail);
} 

if(count($dat) > 0){
    $imap->deleteMessages($dat);
}

//Log para crontab
echo $log = "\nInicio de verificación de correo certificado \n
          Fecha: ".date("Y/m/d H:m:s")."\n".$log;
