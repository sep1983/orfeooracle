<?php
set_time_limit(0);
session_start();
$ruta_raiz = "..";
if(!$_SESSION['dependencia']) include "$ruta_raiz/rec_session.php";
if(!$coddepe) $coddepe=0;
if(!$tsub) $tsub=0;
if(!$codserie) $codserie=0;
$fecha_fin = date("Y/m/d") ;
$where_fecha="";

$tdcArray = $_POST['tdcOrden'];
$ruta_raiz = "..";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
$encabezado = "".session_name()."=".session_id()."&krd=$krd&filtroSelect=$filtroSelect&accion_sal=$accion_sal&dependencia=$dependencia&tpAnulacion=$tpAnulacion&orderNo=";
$linkPagina = "$PHP_SELF?$encabezado&accion_sal=$accion_sal&orderTipo=$orderTipo&orderNo=$orderNo";

/*  GENERACION LISTADO DE RADICADOS
 *  Aqui utilizamos la clase adodb para generar el listado de los radicados
 *  Esta clase cuenta con una adaptacion a las clases utiilzadas de orfeo.
 *  el archivo original es adodb-pager.inc.php la modificada es adodb-paginacion.inc.php
 */
error_reporting(7);
if(trim($orderTipo)=="") $orderTipo="ASC";
if($orden_cambio==1)
{
  if(trim($orderTipo)!="DESC")
  {
    $orderTipo="DESC";
  }else
  {
    $orderTipo="ASC";
  }
}

/************************************************************/
/**** ACTUALIZAR MATRIZ ORDEN TIPO DOCUMENTAL****************/
/************************************************************/
if(!empty($tdcArray)){
  foreach(array_keys($tdcArray) as $key) {
    $ordenTdc = $tdcArray[$key];
    $tdc      = $key;
    if($tdc and $ordenTdc){
      $oracle = "UPDATE
                  SGD_MRD_MATRIRD
                SET
                  SGD_MRD_ORDER = $ordenTdc
                WHERE
                  DEPE_CODI        = $coddepe  AND
                  SGD_SRD_CODIGO   = $codserie AND
                  SGD_SBRD_CODIGO  = $tsub     AND
                  SGD_TPR_CODIGO   = $tdc
                ";

      $rs = $db->query($oracle);

      if (!$rs) {
        echo "<hr><b><font color=red>Error no se inserto sobre radicado<br>SQL: " . $this->db->querySql . "</font></b><hr>";
      }
    }
  }
}

/************************************************************/
/**** ACTUALIZAR MATRIZ**************************************/
/************************************************************/
if($actu_mtrd && $coddepe !=0 && $codserie !=0 && $tsub !=0){
  $num = count($checkValue);
  $i = 0;
  while ($i < $num){
    $record_id = key($checkValue);
    $isqlCount = "select max(sgd_mrd_codigo) as NUMREGT from sgd_mrd_matrird";
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $rsC = $db->query($isqlCount);
    $numreg = $rsC->fields["NUMREGT"];
    $numreg = $numreg+1;
    $record = array(); # Inicializa el arreglo que contiene los datos a insertar
    $record["SGD_MRD_CODIGO"] = $numreg;
    $record["DEPE_CODI"]      = $coddepe;
    $record["SGD_SRD_CODIGO"] = $codserie;
    $record["SGD_SBRD_CODIGO"]= $tsub;
    $record["SGD_TPR_CODIGO"] = $record_id;
    $record["SOPORTE"]        = $med;
    $record["SGD_MRD_ESTA"] = '1';
    $record["SGD_MRD_FECHINI"] = $db->conn->OffsetDate(0);
    //$record["SGD_MRD_FECHFIN"] = $db->conn->OffsetDate(0);
    $insertSQL = $db->insert("SGD_MRD_MATRIRD", $record, "true");
    next($checkValue);
    $i++;
  }
}
/*********************/


?>
<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<script>
function markAll()
{
  if(document.formEnviar.elements['checkAll'].checked)
    for(i=1;i < document.formEnviar.elements.length;i++)
      document.formEnviar.elements[i].checked=true;
  else
    for(i=1;i< document.formEnviar.elements.length;i++)
      document.formEnviar.elements[i].checked=false;
}

</script>
<body bgcolor="#FFFFFF" topmargin="0" >
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="js/spiffyCal/spiffyCal_v2_1.css">
<form name=formEnviar action='../trd/cuerpoMatriTRD.php?<?=session_name()."=".session_id()."&krd=$krd" ?>&estado_sal=<?=$estado_sal?>&estado_sal_max=<?=$estado_sal_max?>&pagina_sig=<?=$pagina_sig?>&dep_sel=<?=$dep_sel?>&nomcarpeta=<?=$nomcarpeta?>&orderNo=<?=$orderNo?>' method=post>

<p class="title is-4">MATRIZ TRD</p>

<table class="table" cellspacing="5">
<tr>
    <td width="125" height="21">
      <label class="label">DEPENDENCIA</label>
    </td>
      <td colspan="3">
        <div class="select">
          <?
          include_once "$ruta_raiz/include/query/envios/queryPaencabeza.php";
          $sqlConcat = $db->conn->Concat($db->conn->substr."($conversion,1,5) ", "'-'",$db->conn->substr."(depe_nomb,1,100) ");
          $sql = "select $sqlConcat ,depe_codi from dependencia
            order by depe_codi";
          $rsDep = $db->conn->Execute($sql);
          if(!$depeBuscada) $depeBuscada=$dependencia;
          print $rsDep->GetMenu2("coddepe","$coddepe",false, false, 0," onChange='submit();' class='select'");
          ?>
        </div>
</td>
    </tr>
  <tr>
    <td width="125" height="21"> <label class="label">SERIE</label> </td>
      <td colspan="3">
        <div class="select">
          <?php
          if(!$codserie) $codserie = 0;
          $fechah=date("dmy") . " ". time("h_m_s");
          $fecha_hoy = Date("Y-m-d");
          $sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
          $check=1;
          $fechaf=date("dmy") . "_" . time("hms");
          $num_car = 4;
          $nomb_varc = "sgd_srd_codigo";
          $nomb_varde = "sgd_srd_descrip";
          include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
          $querySerie = "select distinct ($sqlConcat) as detalle, sgd_srd_codigo
            from sgd_srd_seriesrd order by detalle
            ";
          $rsD=$db->conn->query($querySerie);
          $comentarioDev = "Muestra las Series Docuementales";
          include "$ruta_raiz/include/tx/ComentarioTx.php";
          print $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
          ?>
        </div>
   <tr>
    <td width="125" height="21"> <label class="label">SUBSERIE</label></td>
      <td colspan="3">
        <div class="select">
          <?
          $nomb_varc = "sgd_sbrd_codigo";
          $nomb_varde = "sgd_sbrd_descrip";
          include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
          $querySub = "select distinct ($sqlConcat) as detalle, sgd_sbrd_codigo
            from sgd_sbrd_subserierd
            where sgd_srd_codigo = '$codserie'
            and ".$sqlFechaHoy." between $sgd_sbrd_fechini and $sgd_sbrd_fechfin
            order by detalle
            ";
          $rsSub=$db->conn->query($querySub);
          include "$ruta_raiz/include/tx/ComentarioTx.php";
          print $rsSub->GetMenu2("tsub", $tsub, "0:-- Seleccione --", false,"","onChange='submit()'" );

          ?>
        </div>
      </td>
    <tr>
    <td width="125" height="21"> <label class="label">SOPORTE<label></td>
      <td colspan="3">
        <div class="select">
          <select  name='med'>
          <?php
            if($med==1){$datosel=" selected ";}else {$datosel=" ";}
            echo "<option value='1' $datosel><font>1. PAPEL</font></option>";
            if($med==2){$datosel=" selected ";}else {$datosel=" ";}
            echo "<option value='2' $datosel><font>2. ELECTRONICO</font></option>";
            ?>
          </select>
        </div>
      </td>
  </tr>
 <tr>
       <td height="26" colspan="4">
     <center>
    <input type=submit name=actu_mtrd value='Actualizar' class='button is-primary' >
       <input name="aceptar" type="button"  class="button" id="envia22"  onClick="window.close();" value="Cancelar">
      </td>
    </tr>
  </table>

<br>
<?
$codigo=utf8_encode("c�digo");

$isql = "select
  t.sgd_tpr_codigo as CODIGO,
  t.sgd_tpr_descrip as DETALLE,
  m.sgd_mrd_order as ORDEN
  from
  sgd_mrd_matrird m,
  sgd_tpr_tpdcumento t
  where
  m.depe_codi       = '$coddepe' and
  m.sgd_srd_codigo  = '$codserie' and
  m.sgd_sbrd_codigo = '$tsub' and
  m.sgd_tpr_codigo  = t.sgd_tpr_codigo
  order by m.sgd_mrd_order";

$rs = $db->query($isql);

while (!$rs->EOF){
  $codigo = $rs->fields['CODIGO'];
  $detall = $rs->fields['DETALLE'];
  $order  = $rs->fields['ORDEN'];
  $sqltd .= "<tr>
    <td>$codigo</td>
    <td>$detall</td>
    <td>
    <input
    class='input'
      maxlength='4'
      value='$order'
      size='4'
      type='number'
      name='tdcOrden[$codigo]'
      placeholder='Numero de orden'>
      </td>
      </tr>";

  $rs->MoveNext();
}

if(!empty($sqltd)){
  echo
    "<p class='title is-4'>DOCUMENTOS ASIGNADOS A ESTOS PARAMETROS</p>
    <table class=\"table\">
    <thead>
    <tr>
    <th>CODIGO</th>
    <th>DETALLE</th>
    <th>ORDENAR</th>
    </tr>
    </thead>
    $sqltd
    <tr>
    <td colspan='3'>
    <div class='field is-grouped is-grouped-right'>
    <p class='control'>
    <button type='submit' name='Enviar_Orden' class='button is-primary'>Modificar</button>
    </p>
    </div>
    </td>
    </tr>
    </tbody>
    </table>";
}
?>
<p class="title is-4">DOCUMENTOS SIN ASIGNAR A ESTOS PARAMETROS</p>
<br>
<?
if(strlen($orderNo)==0) {
  $orderNo="1";
  $order = 1;
}else {
  $order = $orderNo +1;
}

$isqlF = "select a.sgd_tpr_codigo as codigo
  , a.sgd_tpr_descrip as DETALLLE
  , a.sgd_tpr_codigo AS \"CHK_SGD_TPR_CODIGO\"
  from sgd_tpr_tpdcumento a
  where a.sgd_tpr_codigo not in (select t.sgd_tpr_codigo
  from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
  where m.depe_codi = '$coddepe'
  and m.sgd_srd_codigo = '$codserie'
  and m.sgd_sbrd_codigo = '$tsub'
  and m.sgd_tpr_codigo = t.sgd_tpr_codigo)
  and a.sgd_tpr_codigo != '0' ";
$isqlF = $isqlF .  'order by '.$order .' ' .$orderTipo;

$encabezado = "".session_name()."=".session_id()."&krd=$krd&estado_sal=$estado_sal&estado_sal_max=$estado_sal_max&accion_sal=$accion_sal&coddepe=$coddepe&dep_sel=$dep_sel&codserie=$codserie&med=$med&tsub=$tsub&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
$linkPagina = "$PHP_SELF?$encabezado&orderTipo=$orderTipo&orderNo=$orderNo";

$pager = new ADODB_Pager($db,$isqlF,'adodb', true,$orderNo,$orderTipo);
$pager->checkAll = false;
$pager->checkTitulo = true;
$pager->toRefLinks = $linkPagina;
$pager->toRefVars = $encabezado;
$pager->Render($rows_per_page=500,$linkPagina,$checkbox=chkEnviar);

?>
</table>
  </form>
</body>
</html>
