<?php

session_start();
$anoActual = date("Y");
if(!$fecha_busq) $fecha_busq=date("Y-m-d");
if(!$fecha_busq2) $fecha_busq2=date("Y-m-d");
$ruta_raiz = "..";
if (!$_SESSION['dependencia'] and !$_SESSION['depe_codi_territorial'])	include "../rec_session.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
if (!defined('ADODB_FETCH_ASSOC'))	define('ADODB_FETCH_ASSOC',2);
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$sqlMS="select sgd_msa_codigo, sgd_msa_sigla from sgd_msa_medsoparchivo where sgd_msa_estado=1";
$medioSoporte=$db->conn->GetArray($sqlMS);
foreach ($medioSoporte as $i)
    $tdMedioSoporte.="<td>$i[SGD_MSA_SIGLA]</td>";
?>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<BODY>
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<h3 class='titulo titulos4' align="center">INFORME TABLAS DE RETENCION DOCUMENTAL</h3>

<form name="inf_trd"  action='../trd/informe_trd.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_h=$fechah"?>' method=post>
<table class='table'>

  <tr>
    <td><label class="label">Dependencia</label></TD>
    <td>
      <div class="select">
	<?
	$ss_RADI_DEPE_ACTUDisplayValue = "--- TODAS LAS DEPENDENCIAS ---";
	$valor = 0;
	include "$ruta_raiz/include/query/devolucion/querydependencia.php";
	$sqlD = "select $sqlConcat ,depe_codi from dependencia where depe_estado=1
							order by depe_codi";
			$rsDep = $db->conn->Execute($sqlD);
	       print $rsDep->GetMenu2("dep_sel","$dep_sel",$blank1stItem = "$valor:$ss_RADI_DEPE_ACTUDisplayValue", false, 0," onChange='submit();'");
	?>
      </div>
    </td>
  </tr>
  <tr>
    <td> <center>
		<input type=SUBMIT name=generar_informe value=' Generar Informe' class="button is-primary">
    </center>
		</td>
	</tr>
</table>

<?php
  if($_POST['generar_informe']) {
    if ($_POST['dep_sel'] == 0) {
      $where_depe = '';
    } else {
      if($entidad == "SSPD") {
        if($dep_sel=="527" || $dep_sel=="810" || $dep_sel=="820" || $dep_sel=="830" || $dep_sel=="840" || $dep_sel=="850") {
          $where_depe = " AND ( ( m.depe_codi = ". $_POST['dep_sel'] ."  AND m.SGD_SRD_CODIGO = 15) OR (m.depe_codi = " . $_POST['dep_sel'] . " AND m.SGD_SRD_CODIGO <> 15))";
        }else {
          $where_depe = " AND m.depe_codi = ". $_POST['dep_sel'] ." AND m.SGD_SRD_CODIGO <> 15 ";
        }
      }else {
        $where_depe = " and m.depe_codi = ".$_POST['dep_sel'];
      }
    }
    $generar_informe = 'generar_informe';
    $guion     = "' '";
    include "$ruta_raiz/include/query/trd/queryinforme_trd.php";
    $order_isql = " order by m.depe_codi, m.sgd_srd_codigo,m.sgd_sbrd_codigo,m.sgd_mrd_order ";
    $query_t = $query . $where_depe . $order_isql ;
    $ruta_raiz = "..";
    error_reporting(7);
    $rs = $db->query($query_t);
?>
<table class='table'>
<?
    $nSRD_ant = "";
    $nSBRD_ant = "";
    $openTR = "";
?>
<tr>
  <th><abbr>Codigo</abbr></th>
  <th rowspan="2">Series Y Tipos Documentales</th>
  <th>Retencion<br> A&#241;os</th>
  <th>Disposicion<br> Final</th>
  <th colspan="<?=count($medioSoporte)?>" align="center">Soporte</th>
  <th rowspan="2" >Procedimiento </th>
</tr>
<tr>
  <td>D</td><td align="center">S</td><td align="center">Sb</td>
  <td>AG</td><TD>AC</TD>
  <td>CT</TD><TD>E</TD><TD>M</TD><TD>S</TD>
  <?=$tdMedioSoporte?>
</tr>
<?
    $tdMedioSoporte='';
    $depTDR = $rs->fields['DEPE_CODI'];   //Dependencia
    while(!$rs->EOF and $rs) {
      $nSRD = strtoupper($rs->fields['SGD_SRD_DESCRIP']);	//Nombre Serie
      $nSBRD = $rs->fields['SGD_SBRD_DESCRIP'];			//Nombre SubSerie
      $cSRD = $rs->fields['SGD_SRD_CODIGO'];				//Codigo Serie
      $cSBRD = $rs->fields['SGD_SBRD_CODIGO'];			//Codigo Subserie
      $nTDoc = lcfirst_orf($rs->fields['SGD_TPR_DESCRIP']);	//Nombre Tipo Documental
      if($nSRD==$nSRD_ant and $depTDR == $rs->fields['DEPE_CODI']) {
        $pSRD="";
      } else {
        $pSRD = "$nSRD";
        if($openTR=="Si") {
          echo "$colFinales";
        }
        $depTDR = $rs->fields['DEPE_CODI'];
        echo "<tr><td><font size=2 face='Arial'>$depTDR</font></td>
          <td>&nbsp;<font size=2 face='Arial'>$cSRD</font></td>
          <td>&nbsp;</td><td colspan=11><font size=2 face='Arial'>$pSRD</font></td>";
        echo "</tr>";
        $openTR = "No";
        $band=1;
      }
      if($nSBRD==$nSBRD_ant and $band===0) {
        $pSBRD="&nbsp;&nbsp;&nbsp;- <font size=2 face='Arial'>$nTDoc</font><br>";
        echo "<tr class=leidos><td colspan=3></td><td><font size=2 face='Arial'>$pSBRD</font></td><td colspan=10></td></tr>";
      }else {
        $conservCT="&nbsp;";
        $conservE="&nbsp;";
        $conservI="&nbsp;";
        $conservS="&nbsp;";
        $conserv = strtoupper(substr(trim($rs->fields['DISPOSICION']),0,1));
        $pSBRD = "<a href=#><font size=2 face='Arial'>$nSBRD</font></a><br>&nbsp;&nbsp;&nbsp;-
          <font size=2 face='Arial'>$nTDoc</font>";
        echo "<tr valign=top class=leidos>
          <td><center><font size=2 face='Arial'>$depTDR</font></center></td>
          <td><center>&nbsp;<font size=2 face='Arial'>$cSRD</font></center></td>
          <td><center><font size=2 face='Arial'>$cSBRD</font></center></td>
          <td><font size=2 face='Arial'>$pSBRD</font><br>";
        $openTR = "Si";
        if($conserv=="C") $conservCT="X";
        if($conserv=="E") $conservE="X";
        if($conserv=="M") $conservI="X";
        if($conserv=="S") $conservS="X";
        $tiemag = $rs->fields['SGD_SBRD_TIEMAG'];
        $tiemac = $rs->fields['SGD_SBRD_TIEMAC'];
        $nObservacion = $rs->fields['SGD_SBRD_PROCEDI'];
        $conservacion = "<td><font size=2 face='Arial'><font size=2 face='Arial'>$conservCT</font></td>
          <td><center><font size=2 face='Arial'>$conservE</font></center></td>
          <td><center><font size=2 face='Arial'>$conservI</font></center></td>
          <td><center><font size=2 face='Arial'>$conservS</font></center></td>";
        foreach ($medioSoporte as $i){
          if($i['SGD_MSA_CODIGO']==$rs->fields['SGD_SBRD_SOPORTE'])$tdMedioSoporte.="<td><center><font size=2 face='Arial'>X</font></center></td>";
          else $tdMedioSoporte.="<td><center><font size=2 face='Arial'>&nbsp;</font></center></td>";
        }
        echo "<td><font size=2 face='Arial'>&nbsp;$tiemag</td>
          <td>&nbsp;<font size=2 face='Arial'>$tiemac</font></td>
          <font size=2 face='Arial'>$conservacion $tdMedioSoporte</font>
          <td>&nbsp;<font size=2 face='Arial'>".$nObservacion."</font>
          </td></tr>";
        $band=0;
        $tdMedioSoporte='';
      }
      $nSRD_ant = $nSRD;
      $nSBRD_ant = $nSBRD;
      $rs->MoveNext();
    }
    if($openTR=="Si")
    {
      echo "$colFinales";
    }
?>
</table>
<?
  }

function lcfirst_orf($str)
{
    require_once($GLOBALS['ruta_raiz']."/radsalida/masiva/OpenDocText.class.php");
    $odt = new OpenDocText();
   return mb_strtoupper(substr($str, 0, 1),ini_get("default_charset")) . mb_strtolower(substr($str, 1),ini_get("default_charset"));
}
?>
</form>
<HR>
