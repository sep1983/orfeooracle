<?php
	   require_once("$ruta_raiz/class_control/Transaccion.php");
		 require_once("$ruta_raiz/class_control/Dependencia.php");
		 require_once("$ruta_raiz/class_control/usuario.php");
	   error_reporting(7);
	   $trans = new Transaccion($db);
	   $objDep = new Dependencia($db);
	   $objUs = new Usuario($db);
	   $isql = "select USUA_NOMB from usuario where depe_codi=$radi_depe_actu and usua_codi=$radi_usua_actu";
	   $rs = $db->query($isql);
	   $usuario_actual = $rs->fields["USUA_NOMB"];
	   $isql = "select DEPE_NOMB from dependencia where depe_codi=$radi_depe_actu";
	   $rs = $db->query($isql);
	   $dependencia_actual = $rs->fields["DEPE_NOMB"];
	   $isql = "select USUA_NOMB from usuario u
                    join hist_eventos h on u.usua_doc=h.usua_doc
                    where h.radi_nume_radi=$verrad and sgd_ttr_codigo in (2,30) ";
	   $rs = $db->query($isql);
	   $usuario_rad = $rs->fields["USUA_NOMB"];
	   $isql = "select DEPE_NOMB from dependencia where depe_codi=$radi_depe_radicacion";
	   $rs = $db->query($isql);
	   $dependencia_rad = $rs->fields["DEPE_NOMB"];
	   $isql = "select DEPE_NOMB from dependencia where depe_codi=$radi_depe_radi";
	   $rs = $db->query($isql);
	   $dependencia_asig = $rs->fields["DEPE_NOMB"];
?>

<table class="table">
  <tr>
    <td width=10%><strong>Usuario Actual</strong></td>
    <td width=15%><?=$usuario_actual?></td>
    <td width=10%><strong>Dependencia Actual</strong></td>
    <td width=15%><?=$dependencia_actual?></td>
  </tr>
  <tr>
    <td><strong>Usuario Radicador</strong></td>
    <td><?=$usuario_rad?></td>
    <td><strong>Dependencia de Radicaci&oacute;n</strong></td>
    <td><?=$dependencia_rad?></td>
  </tr>
</table>

<h4 class="title is-4">Flujo Hist&oacute;rico del Documento</h4>
<table class="table">
  <thead>
    <tr>
      <th width=10%>Dependencia </th>
      <th width=5% >Fecha</th>
      <th width=15%>Transacci&oacute;n </th>
      <th width=15%>Usuario Origen</th>
      <th>Comentario</th>
    </tr>
  </thead>
  <?
  $sqlFecha = $db->conn->SQLDate("d-m-Y H:i:s A","a.HIST_FECH");

     $isql = "select
            $sqlFecha AS HIST_FECH1
            , a.DEPE_CODI
            , a.USUA_CODI
            ,a.RADI_NUME_RADI
            ,a.HIST_OBSE
            ,a.USUA_CODI_DEST
            ,a.USUA_DOC
            ,a.HIST_OBSE
            ,a.SGD_TTR_CODIGO
            from hist_eventos a
          where
            a.radi_nume_radi =$verrad
            order by hist_fech desc , sgd_ttr_codigo desc ";

	$i=1;
	$rs = $db->query($isql);
	if($rs) {
    while(!$rs->EOF){
      $usua_doc_dest = "";
      $usua_doc_hist = "";
      $usua_nomb_historico = "";
      $usua_destino = "";
      $numdata =  trim($rs->fields["CARP_CODI"]);
      if($data =="") $rs1->fields["USUA_NOMB"];
      $data = "NULL";
      $numerot = $rs->fields["NUM"];
      $usua_doc_hist = $rs->fields["USUA_DOC"];
      $usua_codi_dest = $rs->fields["USUA_CODI_DEST"];
      $usua_dest=intval(substr($usua_codi_dest,3,3));
      $depe_dest=intval(substr($usua_codi_dest,0,3));
      $usua_codi = $rs->fields["USUA_CODI"];
      $depe_codi = $rs->fields["DEPE_CODI"];
      $codTransac = $rs->fields["SGD_TTR_CODIGO"];
      $descTransaccion = $rs->fields["SGD_TTR_DESCRIP"];
      if(!$codTransac) $codTransac = "0";
      $trans->Transaccion_codigo($codTransac);
      $objUs->usuarioDocto($usua_doc_hist);
      $objDep->Dependencia_codigo($depe_codi);
      ?>
      <tr>
      <td>
        <?=$objDep->getDepe_nomb()?>
      </td>
      <td>
      <?=$rs->fields["HIST_FECH1"]?>
      </td>
      <td>
        <?=$trans->getDescripcion()?>
      </td>
      <td>
        <?=$objUs->get_usua_nomb()?>
      </td>
      <td>
        <?=$rs->fields["HIST_OBSE"]?>
        <?=$usua_destino?>
      </td>
      </tr>
      <?
      $rs->MoveNext();
    }
}

echo "</table>";

include "$ruta_raiz/include/query/queryver_historico.php";

$isql = "select $numero_salida from anexos a where a.anex_radi_nume=$verrad";
$rs = $db->query($isql);
$radicado_d= "";
while(!$rs->EOF)
	{
		$valor = $rs->fields["RADI_NUME_SALIDA"];
		if(trim($valor))
		   {
		      $radicado_d .= "'".trim($valor) ."', ";
		   }
		$rs->MoveNext();
	}

$radicado_d .= "$verrad";

include "$ruta_raiz/include/query/queryver_historico.php";
$sqlFechaEnvio = $db->conn->SQLDate("d-m-Y H:i A","a.SGD_RENV_FECH");
$isql = "select $sqlFechaEnvio AS SGD_RENV_FECH,
		a.DEPE_CODI,
		a.USUA_DOC,
		a.RADI_NUME_SAL,
		a.SGD_RENV_NOMBRE,
		a.SGD_RENV_DIR,
		a.SGD_RENV_MPIO,
		a.SGD_RENV_DEPTO,
		a.SGD_RENV_PLANILLA,
                a.SGD_RENV_RUTA_ANEXO,
		b.DEPE_NOMB,
		c.SGD_FENV_DESCRIP,
		$numero_sal,
		a.SGD_RENV_OBSERVA,
		a.SGD_DEVE_CODIGO
		from sgd_renv_regenvio a, dependencia b, sgd_fenv_frmenvio c
		where
		a.radi_nume_sal in($radicado_d)
		AND a.depe_codi=b.depe_codi
		AND a.sgd_fenv_codigo = c.sgd_fenv_codigo
		order by a.SGD_RENV_FECH desc ";
$rs = $db->query($isql);
?>

<h4 class="title is-4">Datos De Env&iacute;o</h4>

<table>
  <thead>
    <tr>
      <th width=10%>Radicado</th>
      <th width=10%>Dependencia</th>
      <th width=15%>Fecha </th>
      <th width=15%>Destinatario</th>
      <th width=15%>Direcci&oacute;n </th>
      <th width=15%>Departamento </th>
      <th width=15%>Municipio</th>
      <th width=15%>Tipo de Env&iacute;o</th>
      <th width=5%> No. planilla</th>
      <th> Observaciones</th>
    </tr>
  </thead>
  <tbody>
  <?
$i=1;
while(!$rs->EOF) {
	$radDev = $rs->fields["SGD_DEVE_CODIGO"];
	$radEnviado = $rs->fields["RADI_NUME_SAL"];
	if($radDev) {
		$imgRadDev = "<img src='$ruta_raiz/imagenes/devueltos.gif' alt='Documento Devuelto por empresa de Mensajeria' title='Documento Devuelto por empresa de Mensajeria'>";
	}else {
		$imgRadDev = "";
	}
	$numdata = trim($rs->fields["CARP_CODI"]);

  if($data ==""){
		$data = "NULL";
  }

  ?>
  <tr>
    <td>
      <?=$imgRadDev?><?=$radEnviado?>
    </td>
    <td>
      <?=$rs->fields["DEPE_NOMB"]?>
    </td>
    <td>
	    <?="<a class=vinculos href='./verradicado.php?verrad=$radEnviado&krd=$krd' target='verrad$radEnviado'><span class='timpar'>".$rs->fields["SGD_RENV_FECH"]."</span></a>";
	?> </td>
    <td>
      <?=$rs->fields["SGD_RENV_NOMBRE"]?>
    </td>
    <td>
      <?=$rs->fields["SGD_RENV_DIR"]?> </td>
    <td>
      <?=$rs->fields["SGD_RENV_DEPTO"] ?> </td>
    <td>
      <?=$rs->fields["SGD_RENV_MPIO"] ?> </td>
    <td>
      <?=$rs->fields["SGD_FENV_DESCRIP"] ?> </td>
    <td>
      <?=$rs->fields["SGD_RENV_PLANILLA"] ?>
      <?php
      $pathAnex = $rs->fields["SGD_RENV_RUTA_ANEXO"];
      if(!empty($pathAnex)){
        echo "<a href='$ruta_raiz/seguridadImagen.php?fec=".base64_encode("pdfs/resCertificados/$pathAnex")."'>Certificado Entrega</a>";
      }
      ?>
    </td>
    <td>
      <?=$rs->fields["SGD_RENV_OBSERVA"] ?>
    </td>
  </tr>
  <?
	$rs->MoveNext();
  }
	?>
  </tbody>
</table>
