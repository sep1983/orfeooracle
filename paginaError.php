<?php

if (session_id()){
	session_destroy();
}

?>
<html>
  <head>
    <title>SGD-Orfeo cerrado</title>
    <link rel="stylesheet" href="estilos/bulma.css">
    <link rel="stylesheet" href="estilos/login.css">
  </head>
  <body>
    <div class="container">
      <div class="centercontent">
        <div id="titleLogin">
          SGD-Orfeo Cerrado!
        </div>
        <div class="login-box">
          <br>
          <br>
          <br>
          <div class="login-form">
            <p><a href="<?=$ruta_raiz?>/login.php">
              Su sesion ha expirado o ha ingresado en otro equipo
              por favor cierre su navegador e intente de nuevo.</a></p>
            <a  href="<?=$ruta_raiz?>/login.php"
                class="button is-success is-inverted is-outlined" target="_parent"> Ingresar</a>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
    </div>
  </body>
</html>

