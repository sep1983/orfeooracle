<?php
session_start();

if ($_SESSION["usuaPermExpediente"]!=3){
  die('No tiene acceso a esta secci&oacute;n');
}

$ruta_raiz = "..";

include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");

include_once "$ruta_raiz/include/tx/Historico.php";
include_once ("$ruta_raiz/class_control/TipoDocumental.php");
include_once "$ruta_raiz/include/tx/Expediente.php";

$dependencia = $_POST['dependencia'];

/*Filtros iniciales expediente*/
$fecha_hoy   = Date("Y-m-d");
$sqlFechaHoy = $db->conn->DBDate($fecha_hoy);
$nomb_varc   = "s.sgd_srd_codigo";
$nomb_varde  = "s.sgd_srd_descrip";
$nomb_varcsub = "su.sgd_sbrd_codigo";
$nomb_vardesub = "su.sgd_sbrd_descrip";
$encabezado = "$PHP_SELF?".session_name()."=".session_id();

$subDependencia   = $db->conn->substr ."(depe_nomb,0,50)";
$sqlFechaHoy      = $db->conn->SQLDate('Y-m-d',$db->conn->sysTimeStamp);
$sgd_sbrd_fechini = $db->conn->SQLDate('Y-m-d','sgd_sbrd_fechini');
$sgd_sbrd_fechfin = $db->conn->SQLDate('Y-m-d','sgd_sbrd_fechfin');
$sgd_srd_fechini  = $db->conn->SQLDate('Y-m-d','sgd_srd_fechini');
$sgd_srd_fechfin  = $db->conn->SQLDate('Y-m-d','sgd_srd_fechfin');
$sqlConcat        = $db->conn->Concat("$nomb_varc","'-'","$nomb_varde");
$sqlConcatSub     = $db->conn->Concat("$nomb_varcsub","'-'","$nomb_vardesub");

$sql = "SELECT
		     DEPE_CODI ||'-'|| $subDependencia
		      ,DEPE_CODI
        FROM
          DEPENDENCIA
        where
          DEPE_ESTADO = 1
		    ORDER BY DEPE_CODI";

$rs = $db->conn->Execute($sql);
$depeCodes = $rs->GetMenu2('dependencia',"$dependencia", "0:-- Seleccione --", false, 0, " id='dependencia' onChange='submit()'");

if($dependencia){
  $queryUs = "SELECT
                USUA_NOMB,
                USUA_DOC
              FROM
                USUARIO
              WHERE
                DEPE_CODI=$dependencia
                AND USUA_ESTA='1'
              ORDER BY USUA_NOMB";

  $rsUs = $db->conn->Execute($queryUs);
  $userCodes =$rsUs->GetMenu2("usuaDocExp", "$usuaDocExp", "0:-- Seleccione --", false,""," id='usuaDocExp' onChange='activa()'");
}


$querySerie = "SELECT DISTINCT ($sqlConcat) AS DETALLE, S.SGD_SRD_CODIGO
        FROM SGD_MRD_MATRIRD M, SGD_SRD_SERIESRD S
        WHERE M.DEPE_CODI = '$dependencia'
        AND S.SGD_SRD_CODIGO = M.SGD_SRD_CODIGO
        AND M.SGD_MRD_ESTA = 1
        AND ".$sqlFechaHoy." BETWEEN $sgd_srd_fechini and $sgd_srd_fechfin
        ORDER BY DETALLE";

$rsD=$db->conn->Execute($querySerie);
$comentarioDev = "Muestra las Series Docuementales";
include "$ruta_raiz/include/tx/ComentarioTx.php";
$serieCodes = $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,""," id='codserie' onChange='submit()'");

if($codserie){
  $querySub = "select distinct ($sqlConcatSub) as detalle, su.sgd_sbrd_codigo
        from sgd_mrd_matrird m, sgd_sbrd_subserierd su
        where m.depe_codi = '$dependencia'
        and m.sgd_srd_codigo = '$codserie'
        and su.sgd_srd_codigo = '$codserie'
        and su.sgd_sbrd_codigo = m.sgd_sbrd_codigo
        and ".$sqlFechaHoy." between $sgd_sbrd_fechini and $sgd_sbrd_fechfin
        order by detalle";
  $rsSub=$db->conn->Execute($querySub);
  $subserieCodes = $rsSub->GetMenu2("tsub", $tsub, "0:-- Seleccione --", false,""," id='tsub' onChange='submit()'" );
}

if(!$anoExp) $anoExp = date("Y");

for ($ano=date('Y'); $ano>=2000; $ano--){
  $sel = ($ano==$_POST['anoExp']) ? ' selected ' : '';
  $opc .= "<option value='$ano' $sel>$ano</option>";
}


$arrNivel = array('0'=>"P&uacute;blico"
  ,'1'=>"Privado"
  ,'2'=>"Dependencia"
  ,'3'=>"Usuario Espec&iacute;fico"
);

foreach($arrNivel as $j=>$value){
  $nivelExp==$j?$est='selected':$est='';
  $slcNivel.="<option value=$j $est >$value</option>";
}

/* Crear el expediente */
if( $crearExpediente ){

  $expediente = new Expediente($db);

  if($dependencia and $codserie and $tsub and $anoExp){
    $secExp = $expediente->secExpediente($dependencia,$codserie,$tsub,$anoExp, $dependencia);
  }

  $trdExp = substr("000".$codserie,-3) . substr("00".$tsub,-2);
  $consecutivoExp = substr("00000".$secExp,-5);


  if($anoExp and $dependencia and $trdExp and $consecutivoExp){
    $resExp = $anoExp . $dependencia . $trdExp . $consecutivoExp. 'E';
  };

  //insertar nuevo expediente
  $codiSRD = $codserie;
  $codiSBRD = $tsub;
  $trdExp = substr("000".$codiSRD,-3) . substr("00".$codiSBRD,-2);
  $expediente = new Expediente($db);
  $cheka='checked';

  if(!$expManual){
    $cheka='';
    $secExp = $expediente->secExpediente($dependencia,$codiSRD,$codiSBRD,$anoExp);
  }else{
    $cheka='checked';
    $secExp = $consecutivoExp;
  }

  $consecutivoExp = substr("00000".$secExp,-5);
  $numeroExpediente = $anoExp . $dependencia . $trdExp . $consecutivoExp . 'E';

  foreach ( $_POST as $elementos => $valor ){
    if ( strncmp ( $elementos, 'parExp_', 7) == 0 ){
      $indice = ( int ) substr ( $elementos, 7 );
      $arrParametro[ $indice ] = $valor;
    }
  }

  /**  Procedimiento que Crea el Numero de  Expediente
   *  @param $numeroExpediente String  Numero Tentativo del expediente, Hya que recordar que en la creacion busca la ultima secuencia creada.
   *  @param $nurad  Numeric Numero de radicado que se insertara en un expediente.
   *  Modificado: 09-Junio-2006 Supersolidaria
   *  La funcion crearExpediente() recibe los parametros $codiPROC y $arrParametro
   */
  $numeroExpedienteE = $expediente->crearExpediente( $numeroExpediente,$nurad,$dependencia,$codusuario,$usua_doc,$usuaDocExp,$codiSRD,$codiSBRD,'false',$fechaExp,$_POST['codProc'], $arrParametro, $_POST['txtNombre'],$_POST['txt_asuExp'],$nivelExp);
  if($numeroExpedienteE==0) {
    echo "<CENTER><table class=borde_tab><tr><td class=titulosError>EL EXPEDIENTE QUE INTENTO CREAR YA EXISTE.</td></tr></table>";
  }
}
?>
<html>

<head>
  <link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
  <script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
  <script>
    var dateAvailable1 = new ctlSpiffyCalendarBox("dateAvailable1", "TipoDocu", "fechaExp","btnDate1","<?=$fechaExp?>",scBTNMODE_CUSTOMBLUE);

    function mayus(obj) {
      obj.value=obj.value.toUpperCase( );
    }

    function valida(){
      var band=true;
      var msg='';

      var usuaDocExp = document.getElementById('usuaDocExp');
      var codserie = document.getElementById('codserie');
      var dependencia = document.getElementById('dependencia');
      var tsub =  document.getElementById('tsub');
      var txtnombre = document.getElementById('txtNombre');
      var asunExp = document.getElementById('txt_asuExp');
      var anoExp = document.getElementById('anoExp');

      if(codserie.value==0){
        msg +='Seleccione serie!\n';
        band =false;
      }

      if(dependencia.value==0){
        msg +='Seleccione una dependencia!\n';
        band =false;
      }

      if (typeof(usuaDocExp) != 'undefined' && usuaDocExp != null){
        if(usuaDocExp.value==0){
          msg +='Seleccione un usuario!\n';
          band =false;
        }
      }else{
        msg +='Seleccione un usuario!\n';
        band =false;
      }


      if (typeof(tsub) != 'undefined' && tsub != null){
        if(tsub.value==0){
          msg +='Seleccione Subserie!\n';
          band =false;
        }
      }else{
        msg +='Seleccione Subserie!\n';
        band =false;
      }

      if(anoExp.value==0){
          msg +='Seleccione una fecha\n';
          band =false;
      }

      if(!txtnombre.value){
        msg +='Debe llenar el campo Nombre!\n';
        band =false;
      }

      if(!asunExp.value){
        msg +='Debe llenar el campo Asunto!\n';
        band =false;
      }

      if(!band) alert(msg);
      return band;

    }
  </script>
</head>

<body>
  <div id="spiffycalendar"></div>
  <form method="post" action="<?=$encabezadol?>" name="TipoDocu" id="TipoDocu">

    <div class="columns">
      <div class="column">
        <div class="field">
          <label class="label">Dependencia</label>
          <div class="control">
            <div class="select">
              <?=$depeCodes?>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">Usuario</label>
          <div class="control">
            <div class="select">
              <?=$userCodes?>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">A&ntilde;o</label>
          <div class="control">
            <div class="select">
              <select name=anoExp id=anoExp onChange="submit();">
                <option value='0'>Seleccione</option>
                <?=$opc?>
              </select>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">Nombre del Expediente</label>
          <div class="control">
            <input  type="text" name="txtNombre" class='input' id="txtNombre" size="60" maxlength="60" onchange="mayus(this)" onblur="mayus(this)" value="<?=$_POST['txtNombre'] ?>">
          </div>
        </div>

        <div class="field">
          <div class="control">
            <input name="crearExpediente"  id="crearExpediente" type=submit class="button is-info" value=" Crear Expediente "  onclick="return valida();">
          </div>
        </div>

      </div>

      <div class="column">
        <div class="field">
          <label class="label">Serie</label>
          <div class="control">
            <div class="select">
              <?=$serieCodes?>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">SubSerie</label>
          <div class="control">
            <div class="select">
              <?=$subserieCodes?>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">Seguridad</label>
          <div class="control">
            <div class="select">
              <select name='nivelExp' id='nivelExp' class='select' onchange='activa();'>
                <?=$slcNivel?>
              </select>
            </div>
          </div>
        </div>

        <div class="field">
          <label class="label">Asunto del Expediente</label>
          <div class="control">
            <textarea name="txt_asuExp" id="txt_asuExp" class="textarea" rows="2" cols="78" onchange="mayus(this)" onblur="mayus(this)"><?= $_POST['txt_asuExp']?></textarea>
          </div>
        </div>

      </div>
    </div>
    <div class="columns">
      <div class="column">
        <? if( $crearExpediente ){?>
          <p class="title is-5">Se creo el n&uacute;mero de Expediente <?=$resExp?></p>
        <? } ?>
      </div>
    </div>
  </form>

</body>

</html>
