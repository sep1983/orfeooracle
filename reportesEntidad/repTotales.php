<?php
/*********************************************************************************
 *      Filename: repTotales.php
 *		Autor : Lucia Ojeda Acosta
 *      19 diciembre de 2008
**********************************************************************************/

$krdOld = $krd;
session_start();
error_reporting(7);
$ruta_raiz = "..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";

include "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
include ("../busqueda/common.php");
	if ($krd=='LUCIA') {
//	echo "<br>line 394 $sSQLCount";
//$db->conn->debug = TRUE;
	}

//===============================
// Save Page and File Name available into variables
//-------------------------------
$sFileName = "repTotales.php";
//===============================

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
$ps_swTotales = get_param("swTotales");
	$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_SELECCION=$s_SELECCION&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hasta_RADI_FECH_RADI=$s_hasta_RADI_FECH_RADI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&FormRADICADO_Sorting=$FormRADICADO_Sorting&sFileName=$sFileName&form_sorting=$form_sorting&FormRADICADO_Sorted=$FormRADICADO_Sorted&generar=$generar&s_solo_nomb=$s_solo_nomb&ps_swTotales=$ps_swTotales";

?><html>
<head>
<title>Reporte Asignacion Radicados</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css"></head>
<link rel="stylesheet" href="../estilos/orfeo.css">
<body class="PageBODY">

 <table>
  <tr>
   <td valign="top">
<?php Search_show() ?>
    
   </td>
  </tr>
 </table>

 <table>
  <tr>
   <td valign="top">
<? 
  $ps_desde_RADI_FECH_RADI = get_param("s_desde_RADI_FECH_RADI");
  $ps_hasta_RADI_FECH_RADI = get_param("s_hasta_RADI_FECH_RADI");

if ($ps_desde_RADI_FECH_RADI &&  $ps_hasta_RADI_FECH_RADI) 	RADICADO_show(); else 		echo "<center><b>Por favor seleccione un rango de fechas</center></b>";	
	
?>
   </td>
  </tr>
 </table>
<?php

function Search_show()
{
	global $db;
	global $styles;
	global $sForm;
	global $encabezado;
	global $s_RADI_DEPE_ACTU;
	global $s_solo_nomb;
	global $swTotales;
	$sFormTitle = "Totales Radicados de Entrada Asignados de Correspondencia a las Dependencias";
	$sActionFileName = "repTotales.php";
	$ss_desde_RADI_FECH_RADIDisplayValue = "";

  $cadena="";
  for ($hace=730;$hace>=0;$hace--){
    $timestamp = mktime (0,0,0,date("m"),date("d")-$hace,date("Y"));
    $mes = Date('Y/m/d',$timestamp);
    $valormes = Date("M d Y", $timestamp);
    $cadena.=$mes.";". $valormes .";";
  }
  $cadena2="";
  for ($hace=0;$hace<=24;$hace++){
    $cadena2.= $hace .";" .$hace . ";";
  }
  $cadena3="";
  for ($hace=0;$hace<=24;$hace++){
    $cadena3.= $hace .";" .$hace;
	if ($hace!= 24)
		$cadena3.= ";";
  }
//-------------------------------
// Set variables with search parameters
//-------------------------------
  $flds_SELECCION = strip(get_param("s_SELECCION"));
  $flds_RADI_DEPE_ACTU = strip(get_param("s_RADI_DEPE_ACTU"));
  $flds_RADI_DEPE_RADI = strip(get_param("s_RADI_DEPE_RADI"));
  $flds_desde_RADI_FECH_RADI = strip(get_param("s_desde_RADI_FECH_RADI"));
  $flds_hasta_RADI_FECH_RADI = strip(get_param("s_hasta_RADI_FECH_RADI"));  

	$iSQLdep = "select depe_codi,depe_nomb from DEPENDENCIA ORDER BY DEPE_CODI";
// 	$db->query($iSQLdep);

?>
<form name='frmCrear' action='repTotales.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">

  <table class="FormTABLE" width="727">
    <tr>
      <td width="719" colspan="7" class="FormHeaderTD"><a name="Search"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
    </tr>
  </table>	
  
  <table class="FormTABLE" width="725">
    <tr>
      <td height="10" colspan="1"  align="left" class="FieldCaptionTD"><font class="FieldCaptionFONT">DEPENDENCIA</font></td>
      <td   colspan="3"  height="20">Todas las Dependencias
	  <? /*
	$sql = " SELECT 'Todas las dependencias' as DEPE_NOMB, 0 AS DEPE_CODI FROM DEPENDENCIA
			UNION  SELECT DEPE_NOMB, DEPE_CODI AS DEPE_CODI FROM DEPENDENCIA
					order by DEPE_NOMB";
	$rsDep = $db->conn->Execute($sql);
	if(!$s_RADI_DEPE_ACTU) $s_RADI_DEPE_ACTU= 0;
	print $rsDep->GetMenu2("s_RADI_DEPE_ACTU","$s_RADI_DEPE_ACTU",false, false, 0," class='select'");
	if(!$s_solo_nomb) $s_solo_nomb="tdoc";
*/	?>
      </td>
    </tr>

    <tr>
      <td  class="FieldCaptionTD"  width=26% align="left" height="25"><font class="FieldCaptionFONT">FECHA RADICACION DESDE</font></td>
      <td class="DataTD" width=13% height="25"><select name="s_desde_RADI_FECH_RADI">
          <? 
    echo "<option value=\"\">" . $ss_desde_RADI_FECH_RADIDisplayValue . "</option>";
    $LOV = split(";", "$cadena;");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      if($LOV[$i] == $flds_desde_RADI_FECH_RADI) 
        $option="<option SELECTED value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      else
        $option="<option value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];

      echo $option;
    }  ?>
      </select></td>
      <td  colspan="1" class="FieldCaptionTD" width=22% align="left" height="25"><font class="FieldCaptionFONT">FECHA RADICACION HASTA</font></td>
      <td class="DataTD" width=39% height="25"><select name="s_hasta_RADI_FECH_RADI">
          <?
    echo "<option value=\"\">" . $ss_hasta_RADI_FECH_RADIDisplayValue . "</option>";
    $LOV = split(";", "$cadena;");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      if($LOV[$i] == $flds_hasta_RADI_FECH_RADI) 
        $option="<option SELECTED value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      else
        $option="<option value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];

      echo $option;
    }
?>
      </select></td>
    </tr>
    <tr align="center">
      <td colspan="4"><input type="submit" value="BUSCAR">
      </td>
    </tr>
    <tr align="left">
      <td colspan="4"><b>Pendientes <font color="#0000FF"> = </font>  Asignados <font color="#FF0000"> Menos </font>  Archivados <font color="#FF0000"> Menos </font> NRR</b></td>
    </tr>

  </table>
</form>
<?


}


//===============================
// Display Grid Form
//-------------------------------
function RADICADO_show()
{
//-------------------------------
// Initialize variables  
//-------------------------------
  
  global $db;
  global $sRADICADOErr;
  global $sFileName;
  global $styles;
  global $ps_solo_nomb;
  global $s_solo_nomb;  
  global $swTotales;
  global $krd;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte";
  $HasParam = false;
  $bReq = true;
  $iRecordsPerPage = 2000;
  $iCounter = 0;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $sCountSQL = "";

  $transit_params = "";
  $form_params = "s_RADI_DEPE_RADI=" . tourl(get_param("s_RADI_DEPE_RADI")) . 
          "&s_RADI_DEPE_ACTU=" . tourl(get_param("s_RADI_DEPE_ACTU")) . 
          "&s_SELECCION=" . tourl(get_param("s_SELECCION")) . 
          "&s_desde_RADI_FECH_RADI=" . tourl(get_param("s_desde_RADI_FECH_RADI")) . 
		  "&s_hasta_RADI_FECH_RADI=" . tourl(get_param("s_hasta_RADI_FECH_RADI")) . "&";

//-------------------------------
// Build ORDER BY statement
//-------------------------------
  $sOrder = " order by entrada Asc";
  $iSort = get_param("FormRADICADO_Sorting");
  $iSorted = get_param("FormRADICADO_Sorted");
  if(!$iSort)
  {
    $form_sorting = "";
  }
  else
  {
    if($iSort == $iSorted)
    {
      $form_sorting = "";
      $sDirection = " DESC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . $iSort . "&";
    }
    else
    {
      $form_sorting = $iSort;
      $sDirection = " ASC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . "&";
    }
    if ($iSort == 1)  $sOrder = " order by entrada"   . $sDirection;
    if ($iSort == 2)  $sOrder = " order by fechae"    . $sDirection;
    if ($iSort == 3)  $sOrder = " order by rsalida"   . $sDirection;
    if ($iSort == 4)  $sOrder = " order by fechas"    . $sDirection;
    if ($iSort == 5)  $sOrder = " order by tipo"      . $sDirection;
    if ($iSort == 6)  $sOrder = " order by asunto"    . $sDirection;
    if ($iSort == 7)  $sOrder = " order by depe_actu" . $sDirection;
    if ($iSort == 8)  $sOrder = " order by nomb_actu" . $sDirection;		
    if ($iSort == 9)  $sOrder = " order by usant"     . $sDirection;
    if ($iSort == 10) $sOrder = " order by diasr"     . $sDirection;		
    if ($iSort == 11) $sOrder = " order by FECH_IMPR"    . $sDirection;		
    if ($iSort == 12) $sOrder = " order by FECH_ENVIO"    . $sDirection;		

  }

//-------------------------------
// HTML column headers
//-------------------------------
?>
     
<table class="FormTABLE" width="783">
     
  <tr align="center"> 
    <td class="ColumnTD" height="25" width=40%><font class="ColumnFONT">Dependencia</font></td>
    <td class="ColumnTD" height="25" width=40%><font class="ColumnFONT">Tipo</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Asignados</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Respondidos</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Archivados</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">NRR</font></td>
    <td class="ColumnTD" height="25" width=40%><font class="ColumnFONT">Pendientes</font></td>
  </tr>
<?
  
//-------------------------------
// Build WHERE statement
//-------------------------------
  $ps_desde_RADI_FECH_RADI = get_param("s_desde_RADI_FECH_RADI");
  $ps_hasta_RADI_FECH_RADI = get_param("s_hasta_RADI_FECH_RADI");

  if(strlen($ps_desde_RADI_FECH_RADI))
  {
    $desde = $ps_desde_RADI_FECH_RADI . " ". "00:00:00";
    $hasta = $ps_hasta_RADI_FECH_RADI . " ". "23:59:59";
    $HasParam = true;
	
    $sWhereFec =  " and R.RADI_FECH_RADI >= to_date('" .$desde . "','yyyy/mm/dd HH24:MI:ss')";
    $sWhereFec .= " and ";
    $sWhereFec = $sWhereFec . " R.RADI_FECH_RADI <= to_date('" . $hasta . "','yyyy/mm/dd HH24:MI:ss')";
  }

/* Seleccion Todo - Solo archivados - Solo NO archivados */
  $ps_SELECCION = get_param("s_SELECCION");
  if(strlen($ps_SELECCION))
  {
  	if ($ps_SELECCION == 1) $sSelec = "";
  	if ($ps_SELECCION == 2) $sSelec = " r.radi_depe_actu  = 999 ";	
  	if ($ps_SELECCION == 3) $sSelec = " r.radi_depe_actu  != 999 ";		
  }
/*FIN  /* Seleccion Todo - Solo archivados - Solo NO archivados */
 $sWDepe = "";
  $ps_RADI_DEPE_RADI = get_param("s_RADI_DEPE_ACTU");
  if(is_number($ps_RADI_DEPE_RADI) && strlen($ps_RADI_DEPE_RADI))
    $ps_RADI_DEPE_RADI = tosql($ps_RADI_DEPE_RADI, "Number");
  else 
    $ps_RADI_DEPE_RADI = "";

  if($ps_RADI_DEPE_RADI > 0)
  {
    $HasParam = true;//se busca en el radicado donde sea like 'yyyyDEP%'
    $sWDepe = " AND d.depe_codi = $ps_RADI_DEPE_RADI ";
  }
  else
  {

  }
  $ps_swTotales = get_param("swTotales");
  $ps_solo_nomb = get_param("s_solo_nomb");

	$sSQL = "  
	select count(UNIQUE(r.radi_nume_radi)) 	AS cantRads, 	
	MIN(d.depe_nomb)	AS dependen,
	MIN(d.depe_codi)	AS depe_codi
	 " ;
/*REVISAR ESTE sWhere*/
	$sFrom = " from dependencia d, hist_eventos h, radicado r ";
	$sWhere = " where 
	r.radi_nume_radi like '%2' 
	and r.radi_nume_radi = h.radi_nume_radi 
	AND r.radi_depe_radi = d.depe_codi 
	AND SUBSTR(h.radi_nume_Radi,5,3) <> 998 AND SUBSTR(h.radi_nume_Radi,5,3) <> 997
	";

	$sSQLt = ", MIN(r.tdoc_codi) AS codtipo, td.sgd_tpr_descrip 				AS tipo";
  	$sFromt= ", sgd_tpr_tpdcumento td";
  	$sFromt= "";
	$sWheret= " AND r.tdoc_codi=td.sgd_tpr_codigo ";
	$sWheret= " ";
	$sGroupt = " td.sgd_tpr_descrip ";	
	$sGroupt = "  ";	
	$sGroup = "	group by d.depe_nomb " . $sGroupt;
	$sOrder = " order by dependen, tipo ";
	$sOrder = " order by depe_codi ";	
	$sFrom .=  $sFromt;
	

	$sWhere .= $sWheret . $sWhereFec . $sWDepe ; 

	if ($ps_swTotales =="Totales")
	{
		$sOrder = " order by tipo " ;
		$sSQLDet = $sSQL . $sSQLt . $sFrom . $sWhere . " group by " . $sGroupt ;
	}else {
		$sSQLDet = $sSQL . $sSQLt . $sFrom . $sWhere . $sGroup ;
	}
/**Modificacion 22 Diciembre**/
	$sSQLDet = $sSQL . $sFrom . $sWhere . $sGroup ;
		
	$sSQLDet .=  $sOrder;

  if(!$bReq)
  {
?>
     <tr>
      
    <td colspan="5" class="DataTD" height="25"><font class="DataFONT">No records</font></td>
     </tr>
</table>
<?
    return;
  }
//-------------------------------

//-------------------------------
// Execute SQL statement
//-------------------------------

	/*CONTAR RADICADOS ASIGNADOS*/
	//$sSQLCount = "Select count(UNIQUE(r.radi_nume_radi)) as Total ". $sFrom . $sWhere; 
	$sSQLCount = " Select count(UNIQUE(r.radi_nume_radi)) as Total 
		from radicado r 
		where r.radi_nume_radi like '%2'  
		AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 " . $sWhereFec;
	$rs = $db->conn->Execute($sSQLCount);
	$fldTotal = $rs->fields["TOTAL"];

	/*CONTAR RADICADOS ENVIADOS*/
	$sFromE = $sFrom . ", anexos a ";
	$sWhereE = $sWhere . " AND r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4 " ;
//	$sSQLCountE = "Select count(UNIQUE(r.radi_nume_radi)) as TotalE ". $sFromE . $sWhereE;
	$sSQLCountE = "Select count(UNIQUE(r.radi_nume_radi)) as TotalE 
		from radicado r , anexos a 
		where r.radi_nume_radi like '%2'  
		AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 
		AND r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4  " . $sWhereFec;
	$rs = $db->conn->Execute($sSQLCountE);   
	$fldTotalE = $rs->fields["TOTALE"];

	/*CONTAR RADICADOS ARCHIVADOS*/
	$sSQLCountA = "Select count(UNIQUE(r.radi_nume_radi)) as TotalARC
		from radicado r 
		where r.radi_nume_radi like '%2'  
		AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 
		AND r.radi_depe_actu = 999 " . $sWhereFec;
	$rs = $db->conn->Execute($sSQLCountA);   
	$fldTotalARC = $rs->fields["TOTALARC"];
	
    $rs = $db->conn->Execute($sSQLDet); 

//-------------------------------
// Process empty recordset
//-------------------------------
   if(!$fldTotal > 0)
  {
?>
     <tr>
      <td colspan="5" class="DataTD"><font class="DataFONT">No records</font></td>
     </tr>
<?
  }
  
  else  {
  
		$iRecordsPerPage = 2000;
		$iCounter = 0;
		$DEPACTUAL = $rs->fields["DEPENDEN"];
		$DEPECODI  = $rs->fields["DEPE_CODI"];
		$CANTOTAL = 0;
		while(!$rs->EOF && $iCounter < $iRecordsPerPage)
		{
//-------------------------------
// Create field variables based on database fields
//-------------------------------
		$fldDEPENDEN 		= $rs->fields["DEPENDEN"];
		$fldDEPECODI 		= $rs->fields["DEPE_CODI"];	
		$fldTIPO 			= $rs->fields["TIPO"];
		$fldCODTIPO 		= $rs->fields["CODTIPO"];
		$fldCANTRADS    	= $rs->fields["CANTRADS"];	
		if ($DEPACTUAL == $fldDEPENDEN || $ps_swTotales =="Totales")  {
			//$sSQLDetE = $sSQL . $sSQLt . $sFromE . $sWhereE .  " AND d.depe_codi = $DEPECODI " . $sGroup . $sOrder;
		    $sSQLDetE = "select count(UNIQUE(r.radi_nume_radi)) AS cantRads, MIN(d.depe_nomb) AS dependen, MIN(d.depe_codi) AS depe_codi 
			from dependencia d, radicado r , anexos a 
			where r.radi_nume_radi like '%2'  
			AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 and
			r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4 AND r.radi_depe_radi = d.depe_codi and d.depe_codi = $DEPECODI " . $sWhereFec . "
			group by d.depe_nomb 
			order by depe_codi";
			
			$rsDetE = $db->conn->Execute($sSQLDetE);   // echo "<br>linea 461 --> $sSQLDetE";
		?>
		<tr>
<?  if ($ps_swTotales =="Totales") {
			$DEPACTUAL = " GENERAL ";
			$sSQLDetE = $sSQL . $sSQLt . $sFromE . $sWhereE . " AND r.tdoc_codi = $fldCODTIPO " . " group by " . $sGroupt . $sOrder;
			$rsDetE = $db->conn->Execute($sSQLDetE);
?>   	<td colspan="1" class="DataTD" width="212"><font class="DataFONT"><?= tohtml("--") ?>&nbsp;</font></td> 
<?	}else { 
?> 
 <? 
    } 
?>
		<? if(!$rsDetE->EOF) {
			$cantEnvTipo   = $rsDetE->fields["CANTRADS"];
			$canPendiente = $fldCANTRADS - $cantEnvTipo; 	
			$totalPendiente = $totalPendiente + $canPendiente; ?>
		<?	
		  } else {
		  		$cantEnvTipo = "";
		  	    $canPendiente = $fldCANTRADS  ?>
		<?	    $totalPendiente = $totalPendiente + $canPendiente; 
		  }   
		  $pendienteDep = $pendienteDep + $canPendiente;
		  $enviadosDep = $enviadosDep + $cantEnvTipo;
		  ?> 
	  	</tr>
		<?
		$CANTOTAL =  $CANTOTAL + $fldCANTRADS;
        $rs->MoveNext();       
		}else { // $DEPACTUAL diferente a $fldDEPENDEN
		?>
		<tr class="select"><td class="select" colspan="2"><font class="DataFONT">
		<?
			$sSQLEnv = $sSQL . $sFromE . $sWhereE . " AND d.depe_codi = $DEPECODI " . $sGroup ;
			$sSQLEnv = "select count(UNIQUE(r.radi_nume_radi)) AS cantRads, MIN(d.depe_nomb) AS dependen, MIN(d.depe_codi) AS depe_codi 
			from dependencia d, radicado r , anexos a 
			where r.radi_nume_radi like '%2'  
			AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 and
			r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4 AND r.radi_depe_radi = d.depe_codi and d.depe_codi = $DEPECODI " . $sWhereFec . "
			group by d.depe_nomb 
			order by depe_codi";
			$rsEnv = $db->conn->Execute($sSQLEnv);  
/*				if ($krd=='LUCIA') {
					echo "<br> lin 495 --> $sSQLEnv  </br>";
					echo "<br> </br>";
				}*/
			if(!$rsEnv->EOF) {
				$fldCANTRADSE   = $rsEnv->fields["CANTRADS"];	
			}
			echo "<b>$DEPECODI $DEPACTUAL</b>"; ?>&nbsp;</font></td>
			<td align="left" class="select" colspan="1">  <? 		echo "<b>$CANTOTAL</b>"; 	  ?>  &nbsp; </td>
			<?
			
				$sFromE = $sFrom . ", anexos a ";
	$sWhereE =  " AND r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4 " ;

			$sqlEnviado = "Select count(UNIQUE(r.radi_nume_radi)) as ENVIADO from dependencia d, radicado r , anexos a 
				where r.radi_nume_radi like '%2' AND r.radi_depe_radi = d.depe_codi 
			 " .  $sWhereFec . $sWhereE . " and r.radi_depe_radi = $DEPECODI" ;
 //echo "<br> linea 481 --> $sqlEnviado </br>";

			$rsEnviado = $db->conn->Execute($sqlEnviado);  
			if(!$rsEnviado->EOF) {
				$fldCANTENVIADO   = $rsEnviado->fields["ENVIADO"];	
			}



			$sqlArchi = "Select count(UNIQUE(r.radi_nume_radi)) as ARCHIVADO
				from dependencia d, radicado r 
				where r.radi_nume_radi like '%2'  AND r.radi_depe_radi = d.depe_codi 
				and radi_depe_actu = 999 and r.radi_nume_radi not in (select radi_nume_radi from hist_eventos h where h.sgd_ttr_codigo = 65) " .  $sWhereFec . 
				"and r.radi_depe_radi = $DEPECODI";
		$rsArchi = $db->conn->Execute($sqlArchi);   //if ($DEPECODI == 321) echo "<br> linea 492 --> $sqlArchi </br>";
			if(!$rsArchi->EOF) {
				$fldCANTARCHIV   = $rsArchi->fields["ARCHIVADO"];	
			}
	
			$sqlNRR = "Select count(UNIQUE(r.radi_nume_radi)) as CNRR
		from dependencia d, radicado r 
				where r.radi_nume_radi like '%2' AND r.radi_depe_radi = d.depe_codi 
		 " .  $sWhereFec . 
		"  and r.radi_depe_actu = 999 and r.radi_nume_radi in (select radi_nume_radi from hist_eventos h where h.sgd_ttr_codigo = 65) 
		     and r.radi_depe_radi = $DEPECODI";
			$rsNRR = $db->conn->Execute($sqlNRR);   //echo "<br> linea 492 --> $sqlArchi </br>";
			if(!$rsNRR->EOF) {
				$fldCANTNRR   = $rsNRR->fields["CNRR"];	
			}
			$PENDIENTES = $CANTOTAL - $fldCANTARCHIV - $fldCANTNRR
/*			
	$sqlEnviado = "Select count(UNIQUE(r.radi_nume_radi)) as RAD_SAL_GENERADOS " .  $sFromE . "
				where h.sgd_ttr_codigo = 2 and r.radi_nume_radi like '%2' and r.radi_nume_radi = h.radi_nume_radi AND h.depe_codi_dest = d.depe_codi 
				AND SUBSTR(h.radi_nume_Radi,5,3) <> 998  " .  $sWhereFec . $sWhereE . " and H.depe_codi_dest = $DEPECODI" ;			*/
			?>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$fldCANTENVIADO</b>"; 	  ?>  </td>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$fldCANTARCHIV</b>"; 	  ?>  </td>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$fldCANTNRR</b>"; 	  ?>  </td>			
			<td align="left" class="select" colspan="1">  <?	echo "<b>$PENDIENTES</b>"; 	  ?>  </td>						
		</tr>
		<?
		$CANTOTAL = 0;
		$DEPACTUAL = $fldDEPENDEN;
		$DEPECODI  = $fldDEPECODI;		
		$pendienteDep = 0;
		$enviadosDep = 0;
		}
    	$iCounter++;
	}
?>
	<tr>
	<td colspan="2" class="select"><font class="DataFONT">
    <?
	echo "<b>$DEPECODI $DEPACTUAL</b>"; 
	?>&nbsp;</font></td>

<?    if ($DEPACTUAL == " GENERAL ") {
?>		  	<td colspan="1" class="select">  <?	echo "<b>$CANTOTAL</b>"; 	  ?>  &nbsp; </td>
<?	  }else { ?>
			<td colspan="1" class="select"> <?		echo "<b>$CANTOTAL</b>"; 	  ?>  &nbsp; </td>
<?    }
		
			$sSQLEnv = $sSQL . $sFromE . $sWhereE . " AND d.depe_codi = $DEPECODI " . $sGroup ;
			$rsEnv = $db->conn->Execute($sSQLEnv);
			if(!$rsEnv->EOF) {
				$fldCANTRADSE   = $rsEnv->fields["CANTRADS"];	
			}
			$sqlArchi = "Select count(UNIQUE(r.radi_nume_radi)) as ARCHIVADO
				from dependencia d, radicado r 
				where r.radi_nume_radi like '%2'  AND r.radi_depe_radi = d.depe_codi 
				and radi_depe_actu = 999 and r.radi_nume_radi not in (select radi_nume_radi from hist_eventos h where h.sgd_ttr_codigo = 65) " .  $sWhereFec . 
				"and r.radi_depe_radi = $DEPECODI";
			$rsArchi = $db->conn->Execute($sqlArchi);
			if(!$rsArchi->EOF) {
				$fldCANTARCHIV   = $rsArchi->fields["ARCHIVADO"];	
			}

			$sqlNRR = "Select count(UNIQUE(r.radi_nume_radi)) as CNRR
		from dependencia d, radicado r 
				where r.radi_nume_radi like '%2' AND r.radi_depe_radi = d.depe_codi 
		 " .  $sWhereFec . 
		"  and r.radi_depe_actu = 999 and r.radi_nume_radi in (select radi_nume_radi from hist_eventos h where h.sgd_ttr_codigo = 65) 
		     and r.radi_depe_radi = $DEPECODI";
			$rsNRR = $db->conn->Execute($sqlNRR);   //echo "<br> linea 492 --> $sqlArchi </br>";
			if(!$rsNRR->EOF) {
				$fldCANTNRR   = $rsNRR->fields["CNRR"];	
			}


			?>
	<? $pendienteDep = $CANTOTAL - $fldCANTARCHIV - $fldCANTNRR; ?>
		<? if($enviadosDep > 0) { ?>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>"; 	  ?>  </td>
			<? }else {
			$enviadosDep = "";
			?><td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>";   ?>  </td>
			<? } ?>
        	<td align="left" class="select" colspan="1">  <?	echo "<b>$fldCANTARCHIV</b>"; 	  ?>  </td>	
			<td align="left" class="select" colspan="1">  <?	echo "<b>$fldCANTNRR</b>"; 	  ?>  </td>			
			<td align="left" class="select" colspan="1">  <?	echo "<b>$pendienteDep</b>"; 	  ?>  </td>						

	</tr>
	<tr></tr>
	<? $totalPendiente = $fldTotal - $fldTotalARC ?>
	 <tr>  <td  align="center" colspan="5" class="DataTD"><font size="2">                <b>TOTAL RADICADOS ASIGNADOS  : <?=$fldTotal?></b></font></td></tr>
	  <tr> <td  align="center" colspan="5" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS RESPONDIDOS: <?=$fldTotalE?></b></font></td></tr>
	  <tr> <td  align="center" colspan="5" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS ARCHIVADOS : <?=$fldTotalARC?></b></font></td></tr>
	  <tr> <td  align="center" colspan="5" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS PENDIENTES : <?=$totalPendiente?></b></font></td></tr>
 
	<?
	}
	?>
    </table>
  	<?
	}
	?>
</body>
</html>
