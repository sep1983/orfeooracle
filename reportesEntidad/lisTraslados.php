<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* C.R.A.  "COMISION DE REGULACION DE AGUA"                                          */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*                                                                                   */
/*************************************************************************************/
/* Listado de radicados trasladados de una dependencia a otra      lisTraslados.php  */
/* Nombre Desarrollador   Correo     				Fecha                            */
/*    Lucia Ojeda         lojedaster@gmail.com  	30 Marzo 2009	 			     */
$krdOld = $krd;
session_start();
error_reporting(0);
$ruta_raiz = "..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";

    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	include ("../busqueda/common.php");
//	$db->conn->debug = false;
?>
<html>
<head>
<title>Listado de radicados trasladados de una dependencia origen a otra</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?
$encabezado = "&krd=$krd&dep_inicial=$dep_inicial&dep_final=$dep_final&fecha_busq=$fecha_busq&fecha_busqH=$fecha_busqH&hora_ini=$hora_ini&minutos_ini=$minutos_ini&hora_fin=$hora_fin&minutos_fin=$minutos_fin&DEPE_ORIGEN=$DEPE_ORIGEN&tip_radi=$tip_radi";
?>
<form name='frmCrear' action='lisTraslados.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<?
	$fecha_ini = mktime($hora_ini,$minutos_ini,00,substr($fecha_busq,5,2),substr($fecha_busq,8,2),substr($fecha_busq,0,4));
	$fecha_fin = mktime($hora_fin,$minutos_fin,59,substr($fecha_busqH,5,2),substr($fecha_busqH,8,2),substr($fecha_busqH,0,4));
	$where_fecha = " and h.hist_fech BETWEEN
		".$db->conn->DBTimeStamp($fecha_ini)." and ".$db->conn->DBTimeStamp($fecha_fin) ;

if($generar)
{
		error_reporting(7);
		$ruta_raiz = "..";
		include ("$ruta_raiz/fpdf/html2pdf.php");
		$ruta_raiz = "..";
		$pdf=new PDF();
		$pdf->Open();
		$pdf->SetCreator("HTML2PDF");
		$pdf->AddPage();

	  	   $sql = "select DEPE_CODI, SUBSTR(DEPE_NOMB,1,30) AS DEPE_NOMB from DEPENDENCIA
					where DEPE_CODI = $DEPE_ORIGEN ";
	   $rsDep = $db->conn->Execute($sql);
	   if(!$rsDep->EOF)
	   {
	      $depe_nomb_ORIGEN = $rsDep->fields['DEPE_NOMB'];		
	   }		  
	   $sql = "select DEPE_CODI, SUBSTR(DEPE_NOMB,1,40) AS DEPE_NOMB from DEPENDENCIA
					where DEPE_CODI >= $dep_inicial AND DEPE_CODI <= $dep_final
					order by DEPE_codi ";
	   $rsDep = $db->conn->Execute($sql);
	   $siGeneroRegs = false;
	   while(!$rsDep->EOF)
	   {
	      $depe_codi = $rsDep->fields['DEPE_CODI'];
	      $depe_nomb = $rsDep->fields['DEPE_NOMB'];		  
		  RADICADO_show($fecha_ini,$fecha_fin,$where_fecha,$depe_codi,$depe_nomb,$DEPE_ORIGEN,$tip_radi,$depe_nomb_ORIGEN) ;
  		  $rsDep->MoveNext();
	   }
	   if($siGeneroRegs) {
			$noArchivo = "../$carpetaBodega".$noArchivo;
			$pdf->Output($noArchivo);
		}
}
?>
</form>

<?
function RADICADO_show($fecha_ini,$fecha_fin,$where_fecha,$depe_codi,$depe_nomb,$DEPE_ORIGEN,$tip_radi,$depe_nomb_ORIGEN)
{
  global $db;
  global $sFileName;
  global $DEPE_ORIGEN;
  global  $depe_nomb_ORIGEN;
  global $dep_inicial;
  global $dep_final;
  global $noArchivo;
  global $pdf;
  global $siGeneroRegs;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "LISTADO DE RADICADOS TRASLADADOS";
  if ($DEPE_ORIGEN == 0) {
	   $sFormSubTitle = "ORIGEN: TODAS LAS DEPENDENCIAS  -  DESTINO: ";
  } else {
	  $sFormSubTitle = "ORIGEN: $DEPE_ORIGEN $depe_nomb_ORIGEN  -  DESTINO: ";
  }
  $HasParam = false;
  $iTmpI = 0;
  $iTmpJ = 0;
  $sCountSQL = "";
  $feci = substr($db->conn->DBTimeStamp($fecha_ini),9,23); 
  $fecf = substr($db->conn->DBTimeStamp($fecha_fin),9,23); 
  $HasParam = true;
  if ($tip_radi>0) {
  	$where_rad = " AND r.radi_nume_radi like '%$tip_radi'" ;
  }
  if ($DEPE_ORIGEN==0) $where_dep = " ";  else 	$where_dep = " AND h.depe_codi = $DEPE_ORIGEN ";
	
//-------------------------------
// Build base SQL statement
//-------------------------------
 $sSQL =  " select unique(r.RADI_NUME_RADI) as R_RADI_NUME_RADI, h.depe_codi_dest as R_RADI_DEPE_RADI 
	,R.RADI_DESC_ANEX as R_RADI_DESC_ANEX
	,R.RADI_NUME_HOJA as R_RADI_NUME_HOJA
	,r.radi_fech_radi as R_RADI_FECH_RADI
	,R.RA_ASUN as R_RA_ASUN,
	dir.sgd_esp_codi as ESP,
	dir.sgd_oem_codigo AS OEM,
	dir.sgd_ciu_codigo AS CIU
	from RADICADO R, sgd_dir_drecciones dir, hist_eventos h
	where r.radi_nume_radi = h.radi_nume_radi
	and r.radi_nume_radi = dir.radi_nume_radi
	and h.depe_codi_dest = $depe_codi
	and h.depe_codi <> h.depe_codi_dest
	and h.sgd_ttr_codigo = 9
	" . $where_dep. $where_fecha . $where_rad;
//-------------------------------

$sOrder = " order by R.RADI_NUME_RADI";
//-------------------------------
// Assemble full SQL statement
//-------------------------------
 $sSQL .= $sOrder;
  if($sCountSQL == "")
  {
    $iTmpI = strpos(strtolower($sSQL), "select");
    $iTmpJ = strpos(strtolower($sSQL), "from") - 1;
    $sCountSQL = str_replace(substr($sSQL, $iTmpI + 6, $iTmpJ - $iTmpI - 6), " count(1) ", $sSQL);
    $iTmpI = strpos(strtolower($sCountSQL), "order by");
    if($iTmpI > 1) 
      $sCountSQL = substr($sCountSQL, 0, $iTmpI - 1);
  }
//-------------------------------
	$query_t = $sSQL;  
	$ruta_raiz = "..";
    $dbSel = new ConnectionHandler("$ruta_raiz");	
	$dbSel->conn->debug=false;
	$dbSel->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$rsSel = $dbSel->conn->Execute($query_t);  //echo "<br>$query_t";
	$i=0;
	if(!$rsSel->EOF)  {
		   $sFormSubTitle .= " $depe_codi $depe_nomb";
	   	   $siGeneroRegs = true;
?>
		<table class="FormTABLE" width="715" align="center">
		  <tr align="center">
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="ColumnTD" size="3"><b><?=$sFormTitle?></b></font></a></td>
		  </tr>
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="ColumnTD" size="3"><b><?=$sFormSubTitle?></b></font></a></td>
		  </tr>
			  
		  <tr align="center"> 
			<td class="ColumnTD" height="25" width="85">Radicado</td>
			<td class="ColumnTD" height="25" width="331">Asunto</td>
			<td class="ColumnTD" width="110" height="25">Fecha Radicaci�n</td>
			<td class="ColumnTD" height="25" width="23"># Hoj</td>
			<td width="142" height="25" class="ColumnTD">Anexos</td>
		  </tr>
<?   }
	while(!$rsSel->EOF)
	{
	   $radicado[$i] = $rsSel->fields['R_RADI_NUME_RADI'];
	   $asunto[$i]= substr($rsSel->fields['R_RA_ASUN'],0,100);
	   $hora[$i] = $rsSel->fields['R_RADI_FECH_RADI'];
	   $anexos[$i] = $rsSel->fields['R_RADI_DESC_ANEX'];
   	   $fldCODESP      = $rsSel->fields["ESP"];
	   $fldCODOEM      = $rsSel->fields["OEM"];
	   $fldCODCIU      = $rsSel->fields["CIU"];
	   //Determina el Remitente	   
	   if($fldCODESP != 0) {
	      $sSQL_BR = " select nombre_de_la_empresa as NOMBRE
			from bodega_empresas
			where identificador_empresa = '$fldCODESP' ";
		  $rsESP = $db->conn->Execute($sSQL_BR);
		  $fldNOMBRE[$i]   = substr($rsESP->fields["NOMBRE"],0,60);
	   }
	   else {
	   		if($fldCODOEM != 0) {
	     		$sSQL_BR = " select sgd_oem_oempresa as NOMBRE
					from sgd_oem_oempresas
					where sgd_oem_codigo = '$fldCODOEM' ";
		    	$rsOEM = $db->conn->Execute($sSQL_BR);
		  		$fldNOMBRE[$i]   = substr($rsOEM->fields["NOMBRE"],0,60);
	   		}
		   	else {
		         if($fldCODCIU != 0) {
	     	         $sSQL_BR = " select SGD_DIR_NOMREMDES as NOMBRE
						from SGD_DIR_DRECCIONES
						where radi_nume_radi = '$radicado[$i]' ";
			     	$rsCIU = $db->conn->Execute($sSQL_BR);
		  			$fldNOMBRE[$i]   = trim($rsCIU->fields["NOMBRE"]);
					$fldNOMBRE[$i]   = substr($fldNOMBRE[$i],0,60);
	   	        } 
		   }
	  } 
	//Fin Determina Remitente
		$i++;
		
	    $fldRA_ASUN = $rsSel->fields["R_RA_ASUN"];
	    $fldRADI_DESC_ANEX = $rsSel->fields["R_RADI_DESC_ANEX"];
	    $fldRADI_FECH_RADI = $rsSel->fields["R_RADI_FECH_RADI"];
	    $fldRADI_NUME_HOJA = $rsSel->fields["R_RADI_NUME_HOJA"];
	    $fldRADI_NUME_RADI = $rsSel->fields["R_RADI_NUME_RADI"];
	?>
    <tr>
		<td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRA_ASUN) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_HOJA) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_DESC_ANEX) ?>&nbsp;</font></td>
    </tr>
	<?
		$nregis = $nregis + 1 ;
		$rsSel->MoveNext();
	}

	if($radicado) {
		$noArchivo = "/pdfs/planillas/planillaEntregaTrasl". "$DEPE_ORIGEN" . ".pdf";
		define(FPDF_FONTPATH,'../fpdf/font/');
		error_reporting(7);

		$anoActual = date("Y");
		$radicadosPdf .= "</table>";

	$htmlA = "
		<b><center> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
		$sFormTitle </center></b>
		<br><b><center> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
		$sFormSubTitle </center></b></br>";
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlA);
	$htmlB = "<table>
	  <tr> 
		
		FECHA DESDE:<b> $feci </b> HASTA:<b> $fecf </b>  &nbsp;&nbsp;&nbsp; </tr></table>";
		$pdf->SetFont('Arial','',7);
		$pdf->WriteHTML($htmlB);
		

	 $htmlC = "<table>
	  <tr> 
		<td class='ColumnTD' height='25' width='85'><b>Radicado            Fec Rad                            Remitente</b></td>
	  </tr>
	";
		$htmlD = "
		<br>
		_________________________________________________ 				_________________________________________________<br>
		Entregado por:                                                                               Recibido por:";

		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlC);

		$cantRegs=0;
		foreach($radicado as $id=>$noRadicado)
		{
			$cantRegs++;
		}
	$pags = $cantRegs / 15;
	$pagAct = 1;
	$numreg = 0;
		foreach($radicado as $id=>$noRadicado)
		{
			$numreg++;		
			$radicadosPdf = "<tr height='25'><td>$numreg<b><font size='15'>" . "&nbsp;&nbsp;&nbsp;" . $radicado[$id] . "</font></b>&nbsp;$hora[$id]&nbsp;&nbsp;&nbsp;&nbsp;".$fldNOMBRE[$id]."</td></tr>";
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',9);
			$pdf->WriteHTML($html);
			$radicadosPdf = "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Asunto: </b>" .$asunto[$id]. "</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Anexos: </b>" .$anexos[$id]. "</td></tr>"; 
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',7);
			$pdf->WriteHTML($html);
			if($numreg==15 or $numreg==30 or $numreg==45 or $numreg==60 or $numreg==75 or $numreg==90) 
				{
					if($cantRegs > $numreg) {
						$htmlN = "<br>                                                                       p�g $pagAct de $pags";
						$pdf->SetFont('Arial','',8);
					    $pdf->WriteHTML($htmlN);
					} else {
		 			    $pdf->SetFont('Arial','',8);
				 		$pdf->WriteHTML($htmlD);
					}
				 $pagAct++;
				 $pdf->AddPage();
				 $pdf->SetFont('Arial','',9);
				 $pdf->WriteHTML($htmlA);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlB);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlC);
				}
		}
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlD);
		$pdf->AddPage();
	//save and redirect
}

	if($nregis>0) {
?>
     <tr>
      <td colspan="5" class="DataTD"><font class="DataFONT"><b>Total Registros: <?=$nregis?></b></font></td>
     </tr>
<?	}
}
  ?>
</table>
<?	   if($siGeneroRegs) {
?>
			<TABLE BORDER=0 WIDTH=100% class="borde_tab">
				<TR><TD class="listado2"  align="center"><center>
			<a href='<?=$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo pdf</a></center>
			</td>	</TR>
			</TABLE>
<?   }   ?>

</body>
</html>