<?php

$krdOld = $krd;
session_start();
error_reporting(0);
$ruta_raiz = "..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
include "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
include ("../busqueda/common.php");
$sFileName = "repAsignacionRadDetalle.php";
$sAction = get_param("FormAction");
$sForm = get_param("FormName");

?><html>
<head>
<title>Reporte Planilla</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?
	global $db;
	global $styles;
	global $sForm;
	$sFormTitle = "Listado de Radicados (-2) Asignados de Correspondencia a la Dependencia :";
	$sActionFileName = "repAsignacionRadDetalle.php";
	$ss_desde_RADI_FECH_RADIDisplayValue = "";
	$ss_hora_inicialDisplayValue = "";
	$ss_hora_finalDisplayValue = "";
	global $FormRADICADO_Sorting;
	global $FormRADICADO_Sorted;

	$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_SELECCION=$s_SELECCION&desde=$desde&hasta=$hasta&DEPACTUAL=$DEPACTUAL&DEPECODI=$DEPECODI&ps_solo_nomb=$ps_solo_nomb&FormRADICADO_Sorting=$FormRADICADO_Sorting&sFileName=$sFileName&form_sorting=$form_sorting&FormRADICADO_Sorted=$FormRADICADO_Sorted&generar=$generar";

?>
<form name='frmAsignacionRadDetalle' action='repAsignacionRadDetalle.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
     
  <table class="FormTABLE" width="722">
    <tr>
      <td class="FormHeaderTD" colspan="1"><a name="Search"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
    </tr>
    <tr>
	  <td class="DataTD"  width="198" height="20"><b><?=$DEPACTUAL?></b></td>
    </tr>
  </table>	
 <table  width="336">
	  <td width="58" height="20" align="left"><b>Entre el</b></td>
  	  <td width="110" height="20" align="left"><b><?=substr($desde,0,10)?></b></td>
	  <td width="35" height="20" align="left"><b> y el </b></td>
	  <td width="113" height="20" align="left"><b><?=substr($hasta,0,10)?></b></td>
    </tr>
  </table>
  
  <?
  RADICADO_show(); 
  ?>
</form>

<?php


//===============================
// Display Grid Form
//-------------------------------
function RADICADO_show()
{
//-------------------------------
// Initialize variables  
//-------------------------------
  
  global $db;
  global $sRADICADOErr;
  global $sFileName;
  global $styles;
  global $DEPECODI;
  global $FormRADICADO_Sorting;
  global $form_sorting;
  global $FormRADICADO_Sorted;
  global $desde;
  global $hasta;
  global $encabezado;
  global $ps_solo_nomb;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte";
  $HasParam = false;
  $bReq = true;
  $iRecordsPerPage = 2000;
  $iCounter = 0;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $iTmpI = 0;
  $iTmpJ = 0;
  //$sCountSQL = "";


  $sOrder = " order by entrada Asc";
  $iSort = $FormRADICADO_Sorting;

  $iSorted = $FormRADICADO_Sorted;
  if(!$iSort)
  {
    $form_sorting = "";
  }
  else
  {
    if($iSort == $iSorted)
    {
      $form_sorting = "";
      $sDirection = " DESC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . $iSort . "&";
    }
    else
    {
      $form_sorting = $iSort;
      $sDirection = " ASC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . "&";
    }
    if ($iSort == 1)  $sOrder = " order by entrada"   . $sDirection;
    if ($iSort == 2)  $sOrder = " order by fechae"    . $sDirection;
    if ($iSort == 3)  $sOrder = " order by rsalida"   . $sDirection;
    if ($iSort == 4)  $sOrder = " order by fechas"    . $sDirection;
    if ($iSort == 5)  $sOrder = " order by tipo"      . $sDirection;
    if ($iSort == 6)  $sOrder = " order by asunto"    . $sDirection;
    if ($iSort == 7)  $sOrder = " order by depe_actu" . $sDirection;
    if ($iSort == 8)  $sOrder = " order by nomb_actu" . $sDirection;		
    if ($iSort == 9)  $sOrder = " order by usant"     . $sDirection;
    if ($iSort == 10) $sOrder = " order by diasr"     . $sDirection;		
    if ($iSort == 11) $sOrder = " order by FECH_IMPR"    . $sDirection;		
    if ($iSort == 12) $sOrder = " order by FECH_ENVIO"    . $sDirection;		

  }

//-------------------------------
// HTML column headers
//-------------------------------
?>
<table class="FormTABLE" width="715">
	<tr align="center"> 
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=1&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Radicado</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=2&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Fecha Rad Entrada</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=3&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Rad Salida</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=4&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Fecha Rad Salida</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=5&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Tipo Doc</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=6&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Asunto</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=7&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Dependencia Actual</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=8&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Usuario Actual</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=9&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Usuario Anterior</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=10&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Dias Restantes</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=11&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Fec Impreso</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=12&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Fec Env�o</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=13&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Serie</font>	</a> </td>
	<td width="142" height="25" class="ColumnTD"><a href='repAsignacionRadDetalle.php?<?=$phpsession ?>&encabezado=<?=$encabezado?>&FormRADICADO_Sorting=14&FormRADICADO_Sorted=<?=$form_sorting?>&' target='mainFrame' class="vinculos"><font class="ColumnFONT">Subserie</font>	</a> </td>
	</tr>
<?
  
//-------------------------------
// Build WHERE statement
//-------------------------------
    $HasParam = true;
    $sWhere = $sWhere . " r.radi_depe_radi = $DEPECODI";
    $sWhereFec =  " and R.RADI_FECH_RADI >= to_date('" .$desde . "','yyyy/mm/dd HH24:MI:ss')";
    $sWhereFec .= " and ";
    $sWhereFec = $sWhereFec . " R.RADI_FECH_RADI <= to_date('" . $hasta . "','yyyy/mm/dd HH24:MI:ss')";

  $sSelec = "";
  
$sSQL_1 = "
select r.radi_nume_radi AS ENTRADA, 
	to_char(r.radi_fech_radi,'yyyy/mm/dd hh24:mi:ss') as FECHAE, 
	to_char(a.radi_nume_salida) as rsalida, 
	to_char(a.anex_radi_fech,'yyyy/mm/dd hh24:mi:ss') as FECHAS,
	td.sgd_tpr_descrip AS TIPO, 
	r.ra_asun AS ASUNTO, 
	d1.depe_nomb AS depe_actu, 
	u1.usua_nomb AS nomb_actu, 
	r.radi_usu_ante as usant, 
	round(((r.radi_fech_radi+(td.sgd_tpr_termino * 7/5))-sysdate)) as diasr, 
	r.radi_depe_actu, 
    to_char(a.sgd_fech_impres,'yyyy/mm/dd hh24:mi:ss') as FECH_IMPR,
    to_char(a.anex_fech_envio,'yyyy/mm/dd hh24:mi:ss') as fech_envio,
	a.anex_estado as estado
from radicado r, anexos a, sgd_tpr_tpdcumento td, usuario u1, dependencia d1
where r.radi_nume_radi = a.anex_radi_nume 
	AND a.radi_nume_salida > 0 AND a.anex_salida = 1
	AND r.radi_nume_radi like '%2' 
	AND r.codi_nivel <=5 
	AND r.radi_usua_actu=u1.usua_codi 
	AND r.radi_depe_actu=u1.depe_codi 
	AND u1.depe_codi=d1.depe_codi 
	AND substr(r.radi_nume_radi,5,3) != 998
	AND a.radi_nume_salida NOT IN(SELECT radi_nume_radi FROM SGD_ANU_ANULADOS AN)
";
//	AND r.radi_nume_radi = h.radi_nume_radi AND h.sgd_ttr_codigo = 2
$sSQL_2 = "
	SELECT 
		r.radi_nume_radi AS ENTRADA, 
		to_char(r.radi_fech_radi,'yyyy/mm/dd hh24:mi:ss') as FECHAE, 
		'' as rsalida,
		to_char('','yyyy/mm/dd hh24:mi:ss') as FECHAS, 
		td.sgd_tpr_descrip AS TIPO, 
		r.ra_asun AS ASUNTO, 
		d1.depe_nomb AS depe_actu, 
		u1.usua_nomb AS nomb_actu, 
		r.radi_usu_ante as usant,
		round(((r.radi_fech_radi+(td.sgd_tpr_termino * 7/5))-sysdate)) as diasr, 
		r.radi_depe_actu,
		to_char('','yyyy/mm/dd hh24:mi:ss') as FECH_IMPR,
        to_char('','yyyy/mm/dd hh24:mi:ss') as fech_envio,
	    0 as estado
	FROM 
		radicado r, 
		sgd_tpr_tpdcumento td, 
		usuario u1, 
		dependencia d1
	WHERE r.codi_nivel <=5 
		AND r.radi_usua_actu=u1.usua_codi AND r.radi_depe_actu=u1.depe_codi 
		AND u1.depe_codi=d1.depe_codi 
		AND substr(r.radi_nume_radi,14,1) = 2
		AND substr(r.radi_nume_radi,5,3) != 998
		AND (R.RADI_NUME_RADI NOT IN(SELECT ANEX_RADI_NUME FROM ANEXOS))
	";
	//		AND r.radi_nume_radi = h.radi_nume_radi AND h.sgd_ttr_codigo = 2
	//OR R.RADI_NUME_RADI IN(SELECT ANEX_RADI_NUME FROM ANEXOS WHERE ANEX_BORRADO = 'S')
$sSQL_3 = "
select r.radi_nume_radi AS ENTRADA, 
	to_char(r.radi_fech_radi,'yyyy/mm/dd hh24:mi:ss') as FECHAE, 
	' ANEXO ' as rsalida, 
	to_char(a.anex_radi_fech,'yyyy/mm/dd hh24:mi:ss') as FECHAS,
	td.sgd_tpr_descrip AS TIPO, 
	r.ra_asun AS ASUNTO, 
	d1.depe_nomb AS depe_actu, 
	u1.usua_nomb AS nomb_actu, 
	r.radi_usu_ante as usant, 
	round(((r.radi_fech_radi+(td.sgd_tpr_termino * 7/5))-sysdate)) as diasr, 
	r.radi_depe_actu, 
    to_char(a.sgd_fech_impres,'yyyy/mm/dd hh24:mi:ss') as FECH_IMPR,
    to_char(a.anex_fech_envio,'yyyy/mm/dd hh24:mi:ss') as fech_envio,
	a.anex_estado as estado
from radicado r, anexos a, sgd_tpr_tpdcumento td, usuario u1, dependencia d1
where r.radi_nume_radi = a.anex_radi_nume 
	AND a.radi_nume_salida is null AND a.anex_borrado = 'N'
	AND r.radi_nume_radi like '%2' 
	AND r.codi_nivel <=5 
	AND r.radi_usua_actu=u1.usua_codi 
	AND r.radi_depe_actu=u1.depe_codi 
	AND u1.depe_codi=d1.depe_codi 
	AND substr(r.radi_nume_radi,5,3) != 998
";
//	AND r.radi_nume_radi = h.radi_nume_radi AND h.sgd_ttr_codigo = 2
$sSQL_4 = "
select r.radi_nume_radi AS ENTRADA, 
	to_char(r.radi_fech_radi,'yyyy/mm/dd hh24:mi:ss') as FECHAE, 
	concat(to_char(a.radi_nume_salida),' ANULADO ')  as rsalida, 
	to_char(a.anex_radi_fech,'yyyy/mm/dd hh24:mi:ss') as FECHAS,
	td.sgd_tpr_descrip AS TIPO, 
	r.ra_asun AS ASUNTO, 
	d1.depe_nomb AS depe_actu, 
	u1.usua_nomb AS nomb_actu, 
	r.radi_usu_ante as usant, 
	round(((r.radi_fech_radi+(td.sgd_tpr_termino * 7/5))-sysdate)) as diasr, 
	r.radi_depe_actu, 
    to_char(a.sgd_fech_impres,'yyyy/mm/dd hh24:mi:ss') as FECH_IMPR,
    to_char(a.anex_fech_envio,'yyyy/mm/dd hh24:mi:ss') as fech_envio,
	a.anex_estado as estado
from radicado r, anexos a, sgd_tpr_tpdcumento td, usuario u1, dependencia d1
where r.radi_nume_radi = a.anex_radi_nume 
	AND a.radi_nume_salida > 0 AND a.anex_salida = 1
	AND r.radi_nume_radi like '%2' 
	AND r.codi_nivel <=5 
	AND r.radi_usua_actu=u1.usua_codi 
	AND r.radi_depe_actu=u1.depe_codi 
	AND u1.depe_codi=d1.depe_codi 
	AND substr(r.radi_nume_radi,5,3) != 998
	AND a.radi_nume_salida IN(SELECT radi_nume_radi FROM SGD_ANU_ANULADOS AN)
";
//	AND r.radi_nume_radi = h.radi_nume_radi AND h.sgd_ttr_codigo = 2
		
// $sOrder = " ORDER BY entrada " ;
  if ($ps_solo_nomb == "tdoc")  {
	$sSelec = " r.tdoc_codi=td.sgd_tpr_codigo ";
	}
  if ($ps_solo_nomb == "cReq")  {
	$sSelec = " td.sgd_tpr_tpuso = 2 AND (r.tdoc_codi=td.sgd_tpr_codigo OR r.SGD_TPR_COD_CORRESP = td.sgd_tpr_codigo) ";
	}

if ($sSelec != "") $sSelec = " AND " . $sSelec;
if ($sWhere != "") $sWhere = " AND " . $sWhere;
$sWhereC = $sSelec . $sWhere . $sWhereDep ;
//
$sWhereDep = "";
	if ($DEPECODI>0){ $sWhereDep = " and r.radi_depe_radi = $DEPECODI"; }
	
$sSQL = $sSQL_1 . $sWhereC . $sWhereFec . " UNION " . $sSQL_2 . $sWhereC . $sWhereFec . " UNION " .  $sSQL_3 . $sWhereC . $sWhereFec . " UNION " .  $sSQL_4 . $sWhereC . $sWhereFec . $sOrder;

$sFrom = " from dependencia d, hist_eventos h, radicado r, sgd_tpr_tpdcumento td ";
$sWhereCont = " where r.radi_nume_radi like '%2' 
	and r.radi_nume_radi = h.radi_nume_radi 
	AND r.radi_depe_radi = d.depe_codi 
	AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 
	"  . $sWhereC . $sWhereFec;
	/*CONTAR RADICADOS ASIGNADOS*/
	//$sSQLCount = "Select count(1) as Total ". $sFrom . $sWhereCont;
	$sWhereDep = "";
	if ($DEPECODI>0){ $sWhereDep = " and r.radi_depe_radi = $DEPECODI"; }
		$sSQLCount = " Select count(UNIQUE(r.radi_nume_radi)) as Total 
		from radicado r 
		where r.radi_nume_radi like '%2'  
		AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 998 AND SUBSTR(r.radi_nume_Radi, 5, 3) <> 997 " . $sWhereFec . $sWhereDep;

	$rs = $db->conn->Execute($sSQLCount);
	$fldTotal = $rs->fields["TOTAL"];
	/*CONTAR RADICADOS ENVIADOS*/
	$sFromE = $sFrom . ", anexos a ";
	$sWhereE = $sWhereCont . " AND r.radi_nume_radi = a.anex_radi_nume AND a.anex_estado = 4 " ;
	$sSQLCountE = "Select count(UNIQUE(r.radi_nume_radi)) as TotalE ". $sFromE . $sWhereE;
//	echo "<hr>linea 322 $sSQLCountE</hr>";
	$rs = $db->conn->Execute($sSQLCountE);
	$fldTotalE = $rs->fields["TOTALE"];
/*************/
  $iRecordsPerPage = 3000;
  $iCounter = 0;

 $rs=$db->conn->Execute($sSQL);
 while(!$rs->EOF && $iCounter < $iRecordsPerPage)
    {
    $fldRADI_NUME_RADI = $rs->fields["ENTRADA"];
    $fldRADI_FECH_RADI = $rs->fields["FECHAE"];
    $fldRADI_SALIDA    = $rs->fields["RSALIDA"];	
    $fldRADI_FECH_SALI = $rs->fields["FECHAS"];
    $fldTIPO_DOC       = $rs->fields["TIPO"];
    $fldRA_ASUN        = $rs->fields["ASUNTO"];
    $fldSALIDA         = $rs->fields["SALIDA"];	
    $fldDEPE_NOMB      = $rs->fields["DEPE_ACTU"];
    $fldUSUA_ACTUAL    = $rs->fields["NOMB_ACTU"];
	$fldUSUA_ANTER     = $rs->fields["USANT"]; 
	$fldDIASR          = $rs->fields["DIASR"]; 
	$fldRADI_DEPE_ACTU = $rs->fields["RADI_DEPE_ACTU"];
	$fldFECH_IMPR      = $rs->fields["FECH_IMPR"];	
	$fldFECH_ENVIO     = $rs->fields["FECH_ENVIO"];	
	$fldESTADO         = $rs->fields["ESTADO"];		
      $rs->MoveNext();  

//-------------------------------
// Process the HTML controls
//-------------------------------
?>
      <tr>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_SALIDA) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_SALI) ?>&nbsp;</font></td>
      <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldTIPO_DOC) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRA_ASUN) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldDEPE_NOMB) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldUSUA_ACTUAL) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldUSUA_ANTER) ?>&nbsp;</font></td>
      <td class="DataTD"><font class="DataFONT">
	  <?
       if ($fldRADI_DEPE_ACTU!=999){ echo tohtml($fldDIASR);
	  } else {
		 $sSqlNrr = "select radi_nume_radi from hist_eventos where radi_nume_radi = $fldRADI_NUME_RADI and sgd_ttr_codigo = 65 ";
		 $rsNrr=$db->conn->Execute($sSqlNrr);
		 if(!$rsNrr->EOF) {echo "<b>Archivado NRR</b>"; } else {echo "<b>Archivado</b>";} ?>&nbsp;</font></td>
	  <? } ?>
      <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldFECH_IMPR) ?>&nbsp;</font></td>
      <td class="DataTD"><font class="DataFONT">
      <? if ($fldESTADO==4){ echo tohtml($fldFECH_ENVIO);} ?>&nbsp;</font></td>
      <td class="DataTD"><font class="DataFONT">
	  <?
	  $sSqlTRD = "select rdf.SGD_MRD_CODIGO from SGD_RDF_RETDOCF rdf where  radi_nume_radi = $fldRADI_NUME_RADI";
      $rsTRD=$db->conn->Execute($sSqlTRD);
	  if(!$rsTRD->EOF) {
  	     $mrd_codigo = $rsTRD->fields["SGD_MRD_CODIGO"];	//echo "$mrd_codigo";
	     $sSqlTRD = "select mrd.SGD_SRD_CODIGO, mrd.SGD_SBRD_CODIGO from SGD_MRD_MATRIRD mrd where mrd.SGD_MRD_CODIGO = $mrd_codigo";
	  	 $rsTRD=$db->conn->Execute($sSqlTRD);
		 $serie = $rsTRD->fields["SGD_SRD_CODIGO"];
		 $subserie = $rsTRD->fields["SGD_SBRD_CODIGO"];
		 $sSqlTRD = "select SGD_SRD_DESCRIP from SGD_SRD_SERIESRD s where s.SGD_SRD_CODIGO = $serie";
		 $rsTRD=$db->conn->Execute($sSqlTRD);
		 $desc_serie = $rsTRD->fields["SGD_SRD_DESCRIP"];
		 $sSqlTRD = "select SGD_SBRD_DESCRIP from SGD_SBRD_SUBSERIERD s where s.SGD_SRD_CODIGO = $serie AND SGD_SBRD_CODIGO = $subserie";
		 $rsTRD=$db->conn->Execute($sSqlTRD);
		 $desc_subs = $rsTRD->fields["SGD_SBRD_DESCRIP"];
		 echo "$desc_serie"; ?>&nbsp;</font></td>
		    <td class="DataTD"><font class="DataFONT">
	  <? echo "$desc_subs"; ?>&nbsp;</font></td>
      <?
	  } else {echo "<b></b>";} ?>&nbsp;</font></td>

	  </tr><?
	  
//-------------------------------
// RADICADO Show end
//-------------------------------
    $iCounter++;
	}
	if ($iCounter >= $iRecordsPerPage) {
?>	   <tr>  <td  align="center" colspan="12" class="DataTD"><font size="2"><b>NO FUE POSIBLE DESPLEGAR TODOS LOS REGISTROS, SELECCIONE UN RANGO DE FECHAS MENOR</b></font></td></tr>
<?	}
  ?>
	 <tr></tr>
	 <tr>  <td  align="center" colspan="12" class="DataTD"><font size="2">                <b>TOTAL RADICADOS ASIGNADOS  : <?=$fldTotal?></b></font></td></tr>
	 <tr> <td  align="center" colspan="12" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS RESPONDIDOS: <?=$fldTotalE?></b></font></td></tr>
    </table>
  <?
	}
?>
</body>
</html>
