<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* C.R.A.  "COMISION DE REGULACION DE AGUA"                                          */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*   Luc�a Ojeda		lojedaster@gmail.com	25 Abril 2008						 */
/*************************************************************************************/

$krdOld = $krd;
session_start();
error_reporting(0);
$ruta_raiz = "..";

if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
if(!$fecha_busq) $fecha_busq=date("Y-m-d");
if(!$fecha_busqH) $fecha_busqH=date("Y-m-d");  
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

include ("../busqueda/common.php");
//	$db->conn->debug = TRUE;
?>
<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<body>
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript">
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "frmCrear", "fecha_busq","btnDate1","<?=$fecha_busq?>",scBTNMODE_CUSTOMBLUE);
  var dateAvailableH = new ctlSpiffyCalendarBox("dateAvailableH", "frmCrear", "fecha_busqH","btnDate1","<?=$fecha_busqH?>",scBTNMODE_CUSTOMBLUE);
</script>

<?
$encabezado = "&krd=$krd&dep_sel=$dep_sel&fecha_busq=$fecha_busq&fecha_busqH=$fecha_busqH";
?>
<form name='frmCrear' action='lisInformados.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="93%"  border="1" align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>LISTADO DE INFORMADOS</span></B> </p>
	</center>
	</td>
	</tr>
</table>
<table border=1 width=93% class=t_bordeGris align="center">
	<tr class=timparr>
		<td width="23%" height="26" class="titulos2">Documento Identidad Informador </td>
		    <TD width="225" align="right" valign="top" class='listado2'>

      <input type=text name=dep_inicial value="<?=$dep_inicial?>" class="tex_area" maxlength="12" size="12">
</td>
</tr>
<tr>
      <TD width="125" height="21"  class='titulos2'> Fecha Desde <br>
	<?
	  echo "(".date("Y-m-d").")";
	?>
	</TD>
    <TD width="225" align="right" valign="top" class='listado2'>

        <script language="javascript">
		        dateAvailable.date = "2003-08-05";
			    dateAvailable.writeControl();
			    dateAvailable.dateFormat="yyyy-MM-dd";
    	  </script>
</TD>
  </TR>
  <TR>
    <td width="125" height="21"  class='titulos2'> Fecha Hasta <br>
	</td>
   <TD width="225" align="right" valign="top" class='listado2'>
    <script language="javascript">
		dateAvailableH.date = "2003-08-05";
		dateAvailableH.writeControl();
		dateAvailableH.dateFormat="yyyy-MM-dd";
    </script>
	</td>
  </tr>
	
	
	
<tr>
    <td height="30" colspan="2" class="listado2"><span class="celdaGris"> <span class="e_texto1">
	  <center> <input class="botones" type=submit name=generar id=Continuar_button Value=Generar> </center> </span> </span></td>
	
	</tr>
</table>
<?

if($generar)
{
		error_reporting(7);
		$ruta_raiz = "..";
		include ("$ruta_raiz/fpdf/html2pdf.php");
		$ruta_raiz = "..";
		$pdf=new PDF();
		$pdf->Open();
		$pdf->SetCreator("HTML2PDF");
		$pdf->AddPage();

		  RADICADO_show() ;
	   if($siGeneroRegs) {
			$noArchivo = "../bodega".$noArchivo;
			$pdf->Output($noArchivo);
		}
}
?>
	
</form>

<?
//===============================
// Display Grid Form
//-------------------------------
function RADICADO_show()
{
//-------------------------------
// Initialize variables  
//-------------------------------
  
  
  global $db;
  global $sRADICADOErr;
  global $sFileName;
  global $styles;
  global $s_RADI_DEPE_RADI;
  global $s_hora_inicial;
  global $s_hora_final;
  global $dep_inicial;
  global $noArchivo;
  global $depe_codi;
  global $pdf;
  global $siGeneroRegs;
  global $fecha_busq;
  global $fecha_busqH;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte Radicados Informados";
  $HasParam = false;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $iTmpI = 0;
  $iTmpJ = 0;
  $sCountSQL = "";

//-------------------------------
// Build ORDER BY statement
//-------------------------------
//-------------------------------
// HTML column headers
//-------------------------------
  
//-------------------------------
// Build WHERE statement
//-------------------------------
  $s_hora_inicial = "00";
  $s_hora_final = "23";
  if(strlen($fecha_busq) && strlen($s_hora_inicial) && strlen($s_hora_final))
  {
    $desde = $fecha_busq . " ". $s_hora_inicial .":00:00";
    $hasta = $fecha_busqH . " ". $s_hora_final .":59:59";
    $HasParam = true;
    $sWhere = $sWhere . " AND INFO_FECH >=to_date('" .$desde . "','YYYY/mm/dd HH24:MI:ss')";
    $sWhere .= " and ";
    $sWhere = $sWhere . " INFO_FECH <=to_date('" . $hasta . "','YYYY/mm/dd HH24:MI:ss') 
	 ";
  }

  if($HasParam)  	$sWhere = " where i.info_codi = $dep_inicial " . $sWhere;
//-------------------------------
// Build base SQL statement
//-------------------------------
 $sSQL =  " select I.RADI_NUME_RADI as R_RADI_NUME_RADI, i.info_desc as R_RA_ASUN,  
	to_char(i.info_fech,'dd/mm/yyyy hh24:mi:ss') as R_RADI_FECH_RADI
	from informados i
	";
//-------------------------------

$sOrder = " order by RADI_NUME_RADI";
//-------------------------------
// Assemble full SQL statement
//-------------------------------
  $sSQL .= $sWhere . $sOrder;
  if($sCountSQL == "")
  {
    $iTmpI = strpos(strtolower($sSQL), "select");
    $iTmpJ = strpos(strtolower($sSQL), "from") - 1;
    $sCountSQL = str_replace(substr($sSQL, $iTmpI + 6, $iTmpJ - $iTmpI - 6), " count(1) ", $sSQL);
    $iTmpI = strpos(strtolower($sCountSQL), "order by");
    if($iTmpI > 1) 
      $sCountSQL = substr($sCountSQL, 0, $iTmpI - 1);
  }
//-------------------------------
$isql = $sSQL;
$query_t = $sSQL;   
//echo "<br>$query_t";
	$ruta_raiz = "..";
    $dbSel = new ConnectionHandler("$ruta_raiz");	
	$dbSel->conn->debug=false;
	$dbSel->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$rsSel = $dbSel->conn->Execute($query_t); 	   $radicado1 = $rsSel->fields['R_RADI_NUME_RADI'];
	if(!$radicado1) { 
	die("<P><span class=etextomenu><CENTER><FONT COLOR=RED>NO HAY RADICADOS PARA LISTAR</FONT></CENTER><span>");
	}
	else {
	$i=0;
	if(!$rsSel->EOF)  {
		   $depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);
		   $depe_codi      = $rsSel->fields["R_RADI_DEPE_RADI"];
		//   $sFormTitle .= "- $depe_codi - $depe_nomb";
	   	   $siGeneroRegs = true;

?>
     
		<table class="FormTABLE" width="715" align="center">
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
		  </tr>
			  
		  <tr align="center"> 
			<td class="ColumnTD" height="25" width="85">Radicado</td>
			<td class="ColumnTD" height="25" width="331">Descripcion Informado</td>
			<td class="ColumnTD" width="110" height="25">Fecha en que fue Informado</td>
		  </tr>
<?   }
	while(!$rsSel->EOF)
	{
	   $radicado[$i] = $rsSel->fields['R_RADI_NUME_RADI'];
	   $asunto[$i]= substr($rsSel->fields['R_RA_ASUN'],0,100);
	   $hora[$i] = $rsSel->fields['R_RADI_FECH_RADI'];
	   $anexos[$i] = $rsSel->fields['R_RADI_DESC_ANEX'];
	   $depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);

	    $i++;
		$rsSel->MoveNext();
	}

	if($radicado) {
		$noArchivo = "/pdfs/planillas/lisInformados". "$s_RADI_DEPE_RADI" . ".pdf";
		define(FPDF_FONTPATH,'../fpdf/font/');
		error_reporting(7);

		$anoActual = date("Y");
		$radicadosPdf .= "</table>";

	$htmlA = "
		<b><center> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
		RELACION DE INFORMADOS</center></b>";
		$pdf->SetFont('Arial','',9);
		$pdf->WriteHTML($htmlA);
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlB);
		

	 $htmlC = "<table>
	  <tr> 
		<td class='ColumnTD' height='25' width='85'><b>Radicado                       Hora                  </b></td>
	  </tr>
	";
		$htmlD = "
		<br>
		_________________________________________________ 				_________________________________________________<br>
		Entregado por:                                                                               Recibido por:";

		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlC);

		$cantRegs=0;
		foreach($radicado as $id=>$noRadicado)
		{
			$cantRegs++;
		}
	$pags = $cantRegs / 21;
	$pagAct = 1;
	$numreg = 0;
		foreach($radicado as $id=>$noRadicado)
		{
			$numreg++;		
			$radicadosPdf = "<tr height='25'><td>$numreg<b><font size='15'>" . "&nbsp;&nbsp;&nbsp;" . $radicado[$id] . "</font></b>&nbsp;$hora[$id]&nbsp;&nbsp;&nbsp;&nbsp;".$fldNOMBRE[$id]."</td></tr>";
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',9);
			$pdf->WriteHTML($html);
			$radicadosPdf = "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Descrip: </b>" .$asunto[$id]. "</td></tr>"; 
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',7);
			$pdf->WriteHTML($html);
			if($numreg==21 or $numreg==42 or $numreg==63 or $numreg==84 or $numreg==105) 
				{
					if($cantRegs > $numreg) {
						$htmlN = "<br>                                                                       p�g $pagAct de $pags";
						$pdf->SetFont('Arial','',8);
					    $pdf->WriteHTML($htmlN);
					} else {
		 			    $pdf->SetFont('Arial','',8);
				 		$pdf->WriteHTML($htmlD);
					}
				 $pagAct++;
				 $pdf->AddPage();
				 $pdf->SetFont('Arial','',9);
				 $pdf->WriteHTML($htmlA);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlB);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlC);
				}
		}
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlD);
		$pdf->AddPage();
	
?>

<?

}

//echo "$isql";
 $rs=$db->conn->Execute($isql);
 while(!$rs->EOF)
	{
    $fldRA_ASUN = $rs->fields["R_RA_ASUN"];
    $fldRADI_DESC_ANEX = $rs->fields["R_RADI_DESC_ANEX"];
    $fldRADI_FECH_RADI = $rs->fields["R_RADI_FECH_RADI"];
    $fldRADI_NUME_RADI = $rs->fields["R_RADI_NUME_RADI"];
	?>
    <tr>
		<td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRA_ASUN) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_HOJA) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_DESC_ANEX) ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
	if($nregis>0) {
?>
     <tr>
      <td colspan="5" class="DataTD"><font class="DataFONT"><b>Total Registros: <?=$nregis?></b></font></td>
     </tr>
<?	}
}
}
  ?>
</table>

<?	   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
	<a href='<?=$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo pdf</a></center>
	</td>	</TR>
	</TABLE>
<?   }   ?>

</body>
</html>