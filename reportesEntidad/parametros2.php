<?
/*  Nombre Desarrollador   Correo                 Fecha                              */
/*	Lucía Ojeda Acosta	   lojedaster@gmail.com	  2 nov 2010                         */
/*************************************************************************************/
?>
<?php
	session_start();
	error_reporting(7);
 	$ruta_raiz = "..";
	if(!$fecha_busqH) $fecha_busqH = date("Y-m-d");
	if(!$fecha_busq)  $fecha_busq = date("Y-m-d");
//	$fecha_busq = date("Y-m-d",mktime(0,0,0,date("m")  ,date("d")-1,date("Y"))); 
	if (!$_SESSION['dependencia'])	include "../rec_session.php";
   	include_once  "../include/db/ConnectionHandler.php";
   	$db = new ConnectionHandler("..");	 
  	define('ADODB_FETCH_ASSOC',2);
   	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	if (!$dep_sel) $dep_sel = $_SESSION['dependencia'];
	$dep_sel= $dependencia;
  	if(!$DEPE_ORIGEN) $DEPE_ORIGEN=321;
	if(!$dep_inicial) $dep_inicial=$dep_sel;
	if(!$dep_final) $dep_final=$dep_sel;
	$carpeta = 25;
	$nomcarpeta = "Asignados de Correspondencia";
?>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<body>
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript">
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "gen_listado", "fecha_busq","btnDate1","<?=$fecha_busq?>",scBTNMODE_CUSTOMBLUE);
  var dateAvailableH = new ctlSpiffyCalendarBox("dateAvailableH", "gen_listado", "fecha_busqH","btnDate1","<?=$fecha_busqH?>",scBTNMODE_CUSTOMBLUE);
</script>

<table class=borde_tab width='100%' cellspacing="5"><tr><td class=titulos2><center>
  RECEPCION DE RADICADOS ASIGNADOS 
</center></td></tr></table>
<table><tr><td></td></tr></table>
<form name="gen_listado"  action='./cuerpoAsig.php?<?=session_name()."=".session_id()."&krd=$krd&fecha_ini=$fecha_ini&indi_generar=indi_generar&dep_sel=$dep_sel&tip_radi=$tip_radi&fecha_h=$fechah&fecha_busq=$fecha_busq&fecha_busq2=$fecha_busq2&hora_ini=$hora_ini&minutos_ini=$minutos_ini&segundos_ini=$segundos_ini&hora_fin=$hora_fin&minutos_fin=$minutos_fin&segundos_fin=$segundos_fin&DEPE_ORIGEN=$DEPE_ORIGEN&carpeta=$carpeta&nomcarpeta=$nomcarpeta"?>' method=post>
<center>
<TABLE width='70%' class="borde_tab" cellspacing="5">
  <!--DWLayoutTable-->
          <TD width="185" height="21"  class='titulos2'> Fecha de Radicacion Desde <br>
	<?
	  echo "Hoy es (".date("Y-m-d").")";
	?>
	</TD>
    <TD width="327" align="right" valign="top" class='listado2'>

        <script language="javascript">
		        dateAvailable.date = "2003-08-05";
			    dateAvailable.writeControl();
			    dateAvailable.dateFormat="yyyy-MM-dd";
    	  </script>
	</TD>
  </TR>
  <TR>
    <td width="185" height="21"  class='titulos2'> Fecha de Radicacion Hasta <br>
	</td>
   <TD width="327" align="right" valign="top" class='listado2'>
    <script language="javascript">
		dateAvailableH.date = "2003-08-05";
		dateAvailableH.writeControl();
		dateAvailableH.dateFormat="yyyy-MM-dd";
    </script>
	</td>
  </tr>
  <tr>
    <td width="185" height="21"  class='titulos2'> Hora Inicial</td>
     <TD width="327" align="right" valign="top" class='listado2'>
	<?
	   if(!$hora_ini) $hora_ini = 00;
   	   if(!$hora_fin) $hora_fin = 23; //date("H");
	   if(!$minutos_ini) $minutos_ini = 00;
   	   if(!$minutos_fin) $minutos_fin = 59;//date("i");
	   if(!$segundos_ini) $segundos_ini = 00;
   	   if(!$segundos_fin) $segundos_fin = 59; //date("s");
	?>

	<select name=hora_ini class=select>
        <?
			for($i=0;$i<=23;$i++)
			{
			if ($hora_ini==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
            <option value='<?=$i?>' '<?=$datoss?>'>
              <?=$i?>
            </option>
        <?
			}
			?>
      </select>:<select name=minutos_ini class=select>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($minutos_ini==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>:<select name=segundos_ini class=select>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($segundos_ini==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>
    </td>
  </tr>
  <tr>
    <TD  width="185" height="26" class='titulos2'> Hora Final</td>
	<TD valign="top" class='listado2'><select name=hora_fin class=select>
        <?
			for($i=0;$i<=23;$i++)
			{
			if ($hora_fin==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>:<select name=minutos_fin class=select>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($minutos_fin==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>:<select name=segundos_fin class=select>
        <?
			for($i=0;$i<=59;$i++)
			{
			if ($segundos_fin==$i){ $datoss = " selected "; }else{ $datoss = " "; }?>
        <option value='<?=$i?>' '<?=$datoss?>'>
        <?=$i?>
        </option
			>
        <?
			}
			?>
      </select>
    </td>
  </tr>

	<?
	$tip_radi = '2';  
  	if(!$DEPE_ORIGEN) $DEPE_ORIGEN=321;
	if(!$dep_inicial) $dep_inicial=$dep_sel;
	if(!$dep_final) $dep_final=$dep_sel;
	?>
    <tr>
       <td height="26" colspan="2" valign="top" class='titulos2'> 
	   <center>
	    <INPUT TYPE=SUBMIT name=generar Value=' Generar ' class=botones_funcion>
    </td></tr>
</table>
<?php

if(!$fecha_busq) $fecha_busq = date("Y-m-d");
if(!$fecha_busqH) $fecha_busqH = date("Y-m-d");

?>
</form>