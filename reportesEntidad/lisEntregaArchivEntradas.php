<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* C.R.A.  "COMISION DE REGULACION DE AGUA"                                          */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*   Luc�a Ojeda		lojedaster@gmail.com	25 Abril 2008						 */
/*************************************************************************************/

$krdOld = $krd;
session_start();
error_reporting(0);
$ruta_raiz = "..";
//$s_hora_inicial = 0;
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
  $ss_desde_RADI_FECH_RADIDisplayValue = "";
    $cadena="";
  for ($hace=600;$hace>=0;$hace--){
    $timestamp = mktime (0,0,0,date("m"),date("d")-$hace,date("Y"));
    $mes = Date('d/m/Y',$timestamp);
    $valormes = Date("M d Y", $timestamp);
    $cadena.=$mes.";". $valormes .";";
  }
  //$flds_desde_RADI_FECH_RADI = strip(get_param("s_desde_RADI_FECH_RADI"));
    $flds_RADI_DEPE_RADI = $s_RADI_DEPE_RADI;
    $flds_desde_RADI_FECH_RADI = $s_desde_RADI_FECH_RADI;
	//$flds_HORA_INICIAL = $s_hora_inicial;
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

include ("../busqueda/common.php");
//	$db->conn->debug = false;
?>
<html>
<head>
<title>Listado de radicados entrantes para entregar en archivo central</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?
$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hora_inicial=$s_hora_inicial&s_hora_final=$s_hora_final&dep_inicial=$dep_inicial&dep_final=$dep_final";
?>
<form name='frmCrear' action='lisEntregaArchivEntradas.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="93%"  border="1" align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>RELACION DE ASIGNACION DIARIA DE CORRESPONDENCIA</span></B> </p>
	</center>
	</td>
	</tr>
</table>
<table border=1 width=100% class=t_bordeGris align="center">
	<tr class=timparr>
	<td width="12%" height="26" class="titulos2">Dependencia Radicadora</td>
	<td width="3%" height="1" class="listado2">
	<?
//	$sqlConcat = $db->conn->Concat($db->conn->substr."(depe_codi,1,5) ", "'-'",$db->conn->substr."(depe_nomb,1,15) ");
	$sql = "select DEPE_CODI from DEPENDENCIA
				where DEPE_RAD_TP2 > 0
				order by DEPE_codi ";
//	$sql = "select UBIC_DEPE_RADI, UBIC_DEPE_RADI from UBICACION_FISICA order by 2";
	$rsDep = $db->conn->Execute($sql);
	if(!$s_RADI_DEPE_RADI) $s_RADI_DEPE_RADI=$dependencia;
	print $rsDep->GetMenu2("s_RADI_DEPE_RADI","$s_RADI_DEPE_RADI",false, false, 0," class='select'");
	?>	
	</td>

	<td width="18%" height="26" class="titulos2">Fecha de Radicacion </td>
	<td width="21%" class="titulos2">
    <select name="s_desde_RADI_FECH_RADI">
	<?
    echo "<option value=\"\">" . $ss_desde_RADI_FECH_RADIDisplayValue . "</option>";
    $LOV = split(";", "$cadena;");
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      if($LOV[$i] == $flds_desde_RADI_FECH_RADI) 
        $option="<option SELECTED value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      else
        $option="<option value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      echo $option;
    }
    ?>
    </select>
    </td>

	<td width="23%" height="26" class="titulos2">Hora Inicio 
      <input type=text name=s_hora_inicial value="<?=$s_hora_inicial?>" class="tex_area" maxlength="2" size="5">
</td>
	


	<td width="23%" height="26" class="titulos2">
		Hora Final
		<input type=text name=s_hora_final value="<?=$s_hora_final?>" class="tex_area" maxlength="2" size="5">
	</td>
		<td width="23%" height="26" class="titulos2">Dependencia Inicial
      <input type=text name=dep_inicial value="<?=$dep_inicial?>" class="tex_area" maxlength="3" size="5">
</td>
		<td width="23%" height="26" class="titulos2">Dependencia Final 
      <input type=text name=dep_final value="<?=$dep_final?>" class="tex_area" maxlength="3" size="5">
</td>
	
    <td height="30" colspan="2" class="listado2"><span class="celdaGris"> <span class="e_texto1">
	  <center> <input class="botones" type=submit name=generar id=Continuar_button Value=Generar> </center> </span> </span></td>
	
	</tr>
</table>
<?

if($generar)
{
		error_reporting(7);
		$ruta_raiz = "..";
		include ("$ruta_raiz/fpdf/html2pdf.php");
		$ruta_raiz = "..";
		$pdf=new PDF();
		$pdf->Open();
		$pdf->SetCreator("HTML2PDF");
		//$pdf->SetTitle("Relacion de asignacion diraria de correspondencia");
		//$pdf->SetSubject("asignacion de correspondencia");
		//$pdf->SetAuthor("Correspondencia");
		//$pdf->SetFont('Arial','',9);
		$pdf->AddPage();
/*		if(ini_get('magic_quotes_gpc')=='1')
			$html=stripslashes($html);*/

	   $sql = "select DEPE_CODI from DEPENDENCIA
					where DEPE_CODI >= $dep_inicial AND DEPE_CODI <= $dep_final
					order by DEPE_codi ";
	   $rsDep = $db->conn->Execute($sql);
	   $siGeneroRegs = false;
	   while(!$rsDep->EOF)
	   {
	      $depe_codi = $rsDep->fields['DEPE_CODI'];
		  RADICADO_show() ;
  		  $rsDep->MoveNext();
	   }
	   if($siGeneroRegs) {
			$noArchivo = "../bodega".$noArchivo;
			$pdf->Output($noArchivo);
		}
}
?>
	
</form>

<?
//===============================
// Display Grid Form
//-------------------------------
function RADICADO_show()
{
//-------------------------------
// Initialize variables  
//-------------------------------
  
  
  global $db;
  global $sRADICADOErr;
  global $sFileName;
  global $styles;
  global $s_RADI_DEPE_RADI;
  global $s_desde_RADI_FECH_RADI;
  global $s_hora_inicial;
  global $s_hora_final;
  global $dep_inicial;
  global $dep_final;
  global $noArchivo;
  global $depe_codi;
  global $pdf;
  global $siGeneroRegs;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte Entradas";
  $HasParam = false;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $iTmpI = 0;
  $iTmpJ = 0;
  $sCountSQL = "";

//-------------------------------
// Build ORDER BY statement
//-------------------------------
//-------------------------------
// HTML column headers
//-------------------------------
  
//-------------------------------
// Build WHERE statement
//-------------------------------
  $ps_desde_RADI_FECH_RADI = $s_desde_RADI_FECH_RADI;
  $ps_hora_inicial = $s_hora_inicial;
  $ps_hora_final = $s_hora_final;
  if(strlen($s_desde_RADI_FECH_RADI) && strlen($s_hora_inicial) && strlen($s_hora_final))
  {
    $ps_hora_final -=1;
    $desde = $ps_desde_RADI_FECH_RADI . " ". $ps_hora_inicial .":00:00";
    $hasta = $ps_desde_RADI_FECH_RADI . " ". $ps_hora_final .":59:59";

    $HasParam = true;
    $sWhere = $sWhere . "R.RADI_FECH_RADI>=to_date('" .$desde . "','dd/mm/yyyy HH24:MI:ss')";
    $sWhere .= " and ";
    $sWhere = $sWhere . "R.RADI_FECH_RADI<=to_date('" . $hasta . "','dd/mm/yyyy HH24:MI:ss') AND 	R.RADI_NUME_RADI LIKE '%2'
	 AND R.RADI_DEPE_RADI = D.DEPE_CODI AND R.RADI_DEPE_RADI = $depe_codi
	 AND r.radi_nume_radi = dir.radi_nume_radi
	 ";
  }

  $ps_RADI_DEPE_RADI = $s_RADI_DEPE_RADI;

  if(strlen($ps_RADI_DEPE_RADI))
  {
    if($sWhere != "") 
      $sWhere .= " and ";
    $HasParam = true;//se busca en el radicado donde sea like 'yyyyDEP%'
    $sWhere = $sWhere . "R.RADI_NUME_RADI LIKE '" . substr($ps_desde_RADI_FECH_RADI,6,4) . $s_RADI_DEPE_RADI ."%'";
  }

  if($HasParam)
    $sWhere = " WHERE (" . $sWhere . ")";


//-------------------------------
// Build base SQL statement
//-------------------------------
 $sSQL =  " select unique(R.RADI_NUME_RADI) as R_RADI_NUME_RADI, R.RADI_DEPE_RADI as R_RADI_DEPE_RADI, R.RADI_DESC_ANEX as R_RADI_DESC_ANEX, 
	substr(to_char(R.RADI_FECH_RADI,'dd/mm/yyyy hh24:mi:ss'),11,10) as R_RADI_FECH_RADI, R.RADI_NUME_HOJA as R_RADI_NUME_HOJA, 
	R.RA_ASUN as R_RA_ASUN,
	dir.sgd_esp_codi as ESP,
	dir.sgd_oem_codigo AS OEM,
	dir.sgd_ciu_codigo AS CIU,
	d.depe_nomb
	from RADICADO R, sgd_dir_drecciones dir, dependencia d
	";
//-------------------------------

$sOrder = " order by R.RADI_NUME_RADI";
//-------------------------------
// Assemble full SQL statement
//-------------------------------
  $sSQL .= $sWhere . $sOrder;
  if($sCountSQL == "")
  {
    $iTmpI = strpos(strtolower($sSQL), "select");
    $iTmpJ = strpos(strtolower($sSQL), "from") - 1;
    $sCountSQL = str_replace(substr($sSQL, $iTmpI + 6, $iTmpJ - $iTmpI - 6), " count(1) ", $sSQL);
    $iTmpI = strpos(strtolower($sCountSQL), "order by");
    if($iTmpI > 1) 
      $sCountSQL = substr($sCountSQL, 0, $iTmpI - 1);
  }
//-------------------------------
$isql = $sSQL;
$query_t = $sSQL;   //echo "<br>$query_t";
	$ruta_raiz = "..";
    $dbSel = new ConnectionHandler("$ruta_raiz");	
	$dbSel->conn->debug=false;
	$dbSel->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$rsSel = $dbSel->conn->Execute($query_t);
	$i=0;
	if(!$rsSel->EOF)  {
		   $depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);
		   $depe_codi      = $rsSel->fields["R_RADI_DEPE_RADI"];
		   $sFormTitle .= "- $depe_codi - $depe_nomb";
	   	   $siGeneroRegs = true;

?>
     
		<table class="FormTABLE" width="715" align="center">
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
		  </tr>
			  
		  <tr align="center"> 
			<td class="ColumnTD" height="25" width="85">Radicado</td>
			<td class="ColumnTD" height="25" width="331">Asunto</td>
			<td class="ColumnTD" width="110" height="25">Fecha Radicaci�n</td>
			<td class="ColumnTD" height="25" width="23"># Hoj</td>
			<td width="142" height="25" class="ColumnTD">Anexos</td>
		  </tr>
<?   }
	while(!$rsSel->EOF)
	{
	   $radicado[$i] = $rsSel->fields['R_RADI_NUME_RADI'];
	   $asunto[$i]= substr($rsSel->fields['R_RA_ASUN'],0,100);
	   $hora[$i] = $rsSel->fields['R_RADI_FECH_RADI'];
	   $anexos[$i] = $rsSel->fields['R_RADI_DESC_ANEX'];
   	   $fldCODESP      = $rsSel->fields["ESP"];
	   $fldCODOEM      = $rsSel->fields["OEM"];
	   $fldCODCIU      = $rsSel->fields["CIU"];
	   $depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);
	   //Determina el Remitente	   
	   if($fldCODESP != 0)
	   {
	      $sSQL_BR = "
			select nombre_de_la_empresa as NOMBRE
			from bodega_empresas
			where identificador_empresa = '$fldCODESP'
          ";
		  $rsESP = $db->conn->Execute($sSQL_BR);
		  $fldNOMBRE[$i]   = substr($rsESP->fields["NOMBRE"],0,60);
	   }
	   else
	   {
	 	if($fldCODOEM != 0)
	   		{
	     	$sSQL_BR = "
				select sgd_oem_oempresa as NOMBRE
				from sgd_oem_oempresas
				where sgd_oem_codigo = '$fldCODOEM'
          	";
		    $rsOEM = $db->conn->Execute($sSQL_BR);
		  	$fldNOMBRE[$i]   = substr($rsOEM->fields["NOMBRE"],0,60);
	   		}
		   else {
		         if($fldCODCIU != 0)
	   		       {
	     	         $sSQL_BR = "
				 		select SGD_DIR_NOMREMDES as NOMBRE
						from SGD_DIR_DRECCIONES
						where radi_nume_radi = '$radicado[$i]'
          			";
					//echo "$sSQL_BR";
			        $rsCIU = $db->conn->Execute($sSQL_BR);
		  			$fldNOMBRE[$i]   = trim($rsCIU->fields["NOMBRE"]);
					$fldNOMBRE[$i]   = substr($fldNOMBRE[$i],0,60);
	   	        } 
		   }
	  } 
	//Fin Determina Remitente

	    $i++;
		$rsSel->MoveNext();
	}
//	if(!$radicado) die("<P><span class=etextomenu><CENTER><FONT COLOR=RED>NO HAY RADICADOS PARA LISTAR</FONT></CENTER><span>");
	if($radicado) {
		$noArchivo = "/pdfs/planillas/planillaEntrega". "$s_RADI_DEPE_RADI" . ".pdf";
		define(FPDF_FONTPATH,'../fpdf/font/');
		error_reporting(7);

		$anoActual = date("Y");
		$radicadosPdf .= "</table>";

	$htmlA = "
		<b><center> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
		RELACION DE ASIGNACION DIARIA DE CORRESPONDENCIA</center></b>";
		$pdf->SetFont('Arial','',9);
		$pdf->WriteHTML($htmlA);
	$htmlB = "<table>
	  <tr> 
		
		FECHA DE RADICACION:<b> $s_desde_RADI_FECH_RADI</b>  &nbsp;&nbsp;&nbsp; DEPENDENCIA: <b>$depe_codi - $depe_nomb</b></tr></table>";
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlB);
		

	 $htmlC = "<table>
	  <tr> 
		<td class='ColumnTD' height='25' width='85'><b>Radicado                       Hora                  Remitente</b></td>
	  </tr>
	";
		$htmlD = "
		<br>
		_________________________________________________ 				_________________________________________________<br>
		Entregado por:                                                                               Recibido por:";

		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlC);

		$cantRegs=0;
		foreach($radicado as $id=>$noRadicado)
		{
			$cantRegs++;
		}
	$pags = $cantRegs / 16;
	$pagAct = 1;
	$numreg = 0;
		foreach($radicado as $id=>$noRadicado)
		{
			$numreg++;		
			$radicadosPdf = "<tr height='25'><td>$numreg<b><font size='15'>" . "&nbsp;&nbsp;&nbsp;" . $radicado[$id] . "</font></b>&nbsp;$hora[$id]&nbsp;&nbsp;&nbsp;&nbsp;".$fldNOMBRE[$id]."</td></tr>";
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',9);
			$pdf->WriteHTML($html);
			$radicadosPdf = "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Asunto: </b>" .$asunto[$id]. "</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Anexos: </b>" .$anexos[$id]. "</td></tr>"; 
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',7);
			$pdf->WriteHTML($html);
			if($numreg==16 or $numreg==32 or $numreg==48 or $numreg==64 or $numreg==80) 
				{
					if($cantRegs > $numreg) {
						$htmlN = "<br>                                                                       p�g $pagAct de $pags";
						$pdf->SetFont('Arial','',8);
					    $pdf->WriteHTML($htmlN);
					} else {
		 			    $pdf->SetFont('Arial','',8);
				 		$pdf->WriteHTML($htmlD);
					}
				 $pagAct++;
				 $pdf->AddPage();
				 $pdf->SetFont('Arial','',9);
				 $pdf->WriteHTML($htmlA);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlB);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlC);
				}
		}
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlD);
		$pdf->AddPage();
	//save and redirect
	
?>

<?
  //  exit;
}

//echo "$isql";
 $rs=$db->conn->Execute($isql);
 while(!$rs->EOF)
	{
    $fldRA_ASUN = $rs->fields["R_RA_ASUN"];
    $fldRADI_DESC_ANEX = $rs->fields["R_RADI_DESC_ANEX"];
    $fldRADI_FECH_RADI = $rs->fields["R_RADI_FECH_RADI"];
    $fldRADI_NUME_HOJA = $rs->fields["R_RADI_NUME_HOJA"];
    $fldRADI_NUME_RADI = $rs->fields["R_RADI_NUME_RADI"];
	?>
    <tr>
		<td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRA_ASUN) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_HOJA) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_DESC_ANEX) ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
	if($nregis>0) {
?>
     <tr>
      <td colspan="5" class="DataTD"><font class="DataFONT"><b>Total Registros: <?=$nregis?></b></font></td>
     </tr>
<?	}
}
  ?>
</table>
<?	   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
	<a href='<?=$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo pdf</a></center>
	</td>	</TR>
	</TABLE>
<?   }   ?>

</body>
</html>