<?php
/*********************************************************************************
 *      Filename: repAsignacionRadicados.php
 *		Autor : Lucia Ojeda Acosta
 *      PHP 4.0 build 15 julio de 2005 Modificado 31 Octubre 2007
 *		esta es la que funciona para tipos documentales selectos
 *********************************************************************************/

$krdOld = $krd;
session_start();
error_reporting(7);
$ruta_raiz = "..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";

include "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
include ("../busqueda/common.php");
//$db->conn->debug = TRUE;
//===============================
// Save Page and File Name available into variables
//-------------------------------
$sFileName = "repAsignacionRadicados.php";
//===============================

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
$ps_swTotales = get_param("swTotales");
	$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_SELECCION=$s_SELECCION&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hasta_RADI_FECH_RADI=$s_hasta_RADI_FECH_RADI&s_RADI_DEPE_ACTU=$s_RADI_DEPE_ACTU&FormRADICADO_Sorting=$FormRADICADO_Sorting&sFileName=$sFileName&form_sorting=$form_sorting&FormRADICADO_Sorted=$FormRADICADO_Sorted&generar=$generar&s_solo_nomb=$s_solo_nomb&ps_swTotales=$ps_swTotales";

?><html>
<head>
<title>Reporte Asignacion Radicados</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css"></head>
<link rel="stylesheet" href="../estilos/orfeo.css">
<body class="PageBODY">

 <table>
  <tr>
   <td valign="top">
<?php Search_show() ?>
    
   </td>
  </tr>
 </table>

 <table>
  <tr>
   <td valign="top">
<? 
  $ps_desde_RADI_FECH_RADI = get_param("s_desde_RADI_FECH_RADI");
  $ps_hasta_RADI_FECH_RADI = get_param("s_hasta_RADI_FECH_RADI");

if ($ps_desde_RADI_FECH_RADI &&  $ps_hasta_RADI_FECH_RADI) 	RADICADO_show(); else 		echo "<center><b>Por favor seleccione un rango de fechas</center></b>";	
	
?>
   </td>
  </tr>
 </table>
<?php

function Search_show()
{
	global $db;
	global $styles;
	global $sForm;
	global $encabezado;
	global $s_RADI_DEPE_ACTU;
	global $s_solo_nomb;
	global $swTotales;
	$sFormTitle = "Reporte Asignacion Radicados (-2)";
	$sActionFileName = "repAsignacionRadicados.php";
	$ss_desde_RADI_FECH_RADIDisplayValue = "";

  $cadena="";
  for ($hace=1000;$hace>=0;$hace--){
    $timestamp = mktime (0,0,0,date("m"),date("d")-$hace,date("Y"));
    $mes = Date('Y/m/d',$timestamp);
    $valormes = Date("M d Y", $timestamp);
    $cadena.=$mes.";". $valormes .";";
  }
  $cadena2="";
  for ($hace=0;$hace<=24;$hace++){
    $cadena2.= $hace .";" .$hace . ";";
  }
  $cadena3="";
  for ($hace=0;$hace<=24;$hace++){
    $cadena3.= $hace .";" .$hace;
	if ($hace!= 24)
		$cadena3.= ";";
  }
//-------------------------------
// Set variables with search parameters
//-------------------------------
  $flds_SELECCION = strip(get_param("s_SELECCION"));
  $flds_RADI_DEPE_ACTU = strip(get_param("s_RADI_DEPE_ACTU"));
  $flds_RADI_DEPE_RADI = strip(get_param("s_RADI_DEPE_RADI"));
  $flds_desde_RADI_FECH_RADI = strip(get_param("s_desde_RADI_FECH_RADI"));
  $flds_hasta_RADI_FECH_RADI = strip(get_param("s_hasta_RADI_FECH_RADI"));  

	$iSQLdep = "select depe_codi,depe_nomb from DEPENDENCIA ORDER BY DEPE_CODI";
 	$db->query($iSQLdep);

?>
<form name='frmCrear' action='repAsignacionRadicados1.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">

  <table class="FormTABLE" width="727">
    <tr>
      <td width="719" colspan="7" class="FormHeaderTD"><a name="Search"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
    </tr>
  </table>	
  
  <table class="FormTABLE" width="725">
    <tr>
      <td height="10" colspan="1"  align="left" class="FieldCaptionTD"><font class="FieldCaptionFONT">DEPENDENCIA</font></td>
      <td   colspan="3"  height="20"><?
	$sql = " SELECT 'Todas las dependencias' as DEPE_NOMB, 0 AS DEPE_CODI FROM DEPENDENCIA
			UNION  SELECT DEPE_NOMB, DEPE_CODI AS DEPE_CODI FROM DEPENDENCIA
					order by DEPE_NOMB";
	$rsDep = $db->conn->Execute($sql);
	if(!$s_RADI_DEPE_ACTU) $s_RADI_DEPE_ACTU= 0;
	print $rsDep->GetMenu2("s_RADI_DEPE_ACTU","$s_RADI_DEPE_ACTU",false, false, 0," class='select'");
	if(!$s_solo_nomb) $s_solo_nomb="cReq";
	?>
      </td>
    </tr>
    <tr align="center">
      <td align="left" class="FieldCaptionTD" colspan="3"><font class="FieldCaptionFONT">
        <input type="radio" name="s_solo_nomb" value="cReq"
  		<?if($s_solo_nomb=="cReq"){echo ("CHECKED");} ?>>
      Tipos documentales selectos</font> </td>
	      <td height='26' width='39%' class='FieldCaptionTD'><font class="FieldCaptionFONT">
		<input type="checkbox" name="swTotales"  value="Totales" <? if($swTotales){ echo ("CHECKED");} else echo (""); ?>>
        Totales por Tipo Documental</font>
	    </td>
    </tr>
    <tr>
      <td  class="FieldCaptionTD"  width=26% align="left" height="25"><font class="FieldCaptionFONT">FECHA RADICACION DESDE</font></td>
      <td class="DataTD" width=13% height="25"><select name="s_desde_RADI_FECH_RADI">
          <? 
    echo "<option value=\"\">" . $ss_desde_RADI_FECH_RADIDisplayValue . "</option>";
    $LOV = split(";", "$cadena;");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      if($LOV[$i] == $flds_desde_RADI_FECH_RADI) 
        $option="<option SELECTED value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      else
        $option="<option value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];

      echo $option;
    }  ?>
      </select></td>
      <td  colspan="1" class="FieldCaptionTD" width=22% align="left" height="25"><font class="FieldCaptionFONT">FECHA RADICACION HASTA</font></td>
      <td class="DataTD" width=39% height="25"><select name="s_hasta_RADI_FECH_RADI">
          <?
    echo "<option value=\"\">" . $ss_hasta_RADI_FECH_RADIDisplayValue . "</option>";
    $LOV = split(";", "$cadena;");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      if($LOV[$i] == $flds_hasta_RADI_FECH_RADI) 
        $option="<option SELECTED value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];
      else
        $option="<option value=\"" . $LOV[$i] . "\">" . $LOV[$i + 1];

      echo $option;
    }
?>
      </select></td>
    </tr>
    <tr align="center">
      <td colspan="4"><input type="submit" value="BUSCAR">
      </td>
    </tr>
  </table>
</form>
<?


}


//===============================
// Display Grid Form
//-------------------------------
function RADICADO_show()
{
//-------------------------------
// Initialize variables  
//-------------------------------
  
  global $db;
  global $sRADICADOErr;
  global $sFileName;
  global $styles;
  global $ps_solo_nomb;
  global $s_solo_nomb;  
  global $swTotales;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte";
  $HasParam = false;
  $bReq = true;
  $iRecordsPerPage = 2000;
  $iCounter = 0;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $sCountSQL = "";

  $transit_params = "";
  $form_params = "s_RADI_DEPE_RADI=" . tourl(get_param("s_RADI_DEPE_RADI")) . 
          "&s_RADI_DEPE_ACTU=" . tourl(get_param("s_RADI_DEPE_ACTU")) . 
          "&s_SELECCION=" . tourl(get_param("s_SELECCION")) . 
          "&s_desde_RADI_FECH_RADI=" . tourl(get_param("s_desde_RADI_FECH_RADI")) . 
		  "&s_hasta_RADI_FECH_RADI=" . tourl(get_param("s_hasta_RADI_FECH_RADI")) . "&";

//-------------------------------
// Build ORDER BY statement
//-------------------------------
  $sOrder = " order by entrada Asc";
  $iSort = get_param("FormRADICADO_Sorting");
  $iSorted = get_param("FormRADICADO_Sorted");
  if(!$iSort)
  {
    $form_sorting = "";
  }
  else
  {
    if($iSort == $iSorted)
    {
      $form_sorting = "";
      $sDirection = " DESC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . $iSort . "&";
    }
    else
    {
      $form_sorting = $iSort;
      $sDirection = " ASC";
      $sSortParams = "FormRADICADO_Sorting=" . $iSort . "&FormRADICADO_Sorted=" . "&";
    }
    if ($iSort == 1)  $sOrder = " order by entrada"   . $sDirection;
    if ($iSort == 2)  $sOrder = " order by fechae"    . $sDirection;
    if ($iSort == 3)  $sOrder = " order by rsalida"   . $sDirection;
    if ($iSort == 4)  $sOrder = " order by fechas"    . $sDirection;
    if ($iSort == 5)  $sOrder = " order by tipo"      . $sDirection;
    if ($iSort == 6)  $sOrder = " order by asunto"    . $sDirection;
    if ($iSort == 7)  $sOrder = " order by depe_actu" . $sDirection;
    if ($iSort == 8)  $sOrder = " order by nomb_actu" . $sDirection;		
    if ($iSort == 9)  $sOrder = " order by usant"     . $sDirection;
    if ($iSort == 10) $sOrder = " order by diasr"     . $sDirection;		
    if ($iSort == 11) $sOrder = " order by FECH_IMPR"    . $sDirection;		
    if ($iSort == 12) $sOrder = " order by FECH_ENVIO"    . $sDirection;		

  }

//-------------------------------
// HTML column headers
//-------------------------------
?>
     
<table class="FormTABLE" width="783">
     
  <tr align="center"> 
    <td class="ColumnTD" height="25" width=40%><font class="ColumnFONT">Dependencia</font></td>
    <td class="ColumnTD" height="25" width=40%><font class="ColumnFONT">Tipo Doc</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Asignados</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Respondidos</font></td>
    <td class="ColumnTD" height="25" width=15%><font class="ColumnFONT">Archivados</font></td>
  </tr>
<?
  
//-------------------------------
// Build WHERE statement
//-------------------------------
  $ps_desde_RADI_FECH_RADI = get_param("s_desde_RADI_FECH_RADI");
  $ps_hasta_RADI_FECH_RADI = get_param("s_hasta_RADI_FECH_RADI");

  if(strlen($ps_desde_RADI_FECH_RADI))
  {
    $desde = $ps_desde_RADI_FECH_RADI . " ". "00:00:00";
    $hasta = $ps_hasta_RADI_FECH_RADI . " ". "23:59:59";
    $HasParam = true;
	
    $sWhereFec =  " and R.RADI_FECH_RADI >= to_date('" .$desde . "','yyyy/mm/dd HH24:MI:ss')";
    $sWhereFec .= " and ";
    $sWhereFec = $sWhereFec . " R.RADI_FECH_RADI <= to_date('" . $hasta . "','yyyy/mm/dd HH24:MI:ss')";
  }

/* Seleccion Todo - Solo archivados - Solo NO archivados */
  $ps_SELECCION = get_param("s_SELECCION");
  if(strlen($ps_SELECCION))
  {
  	if ($ps_SELECCION == 1) $sSelec = "";
  	if ($ps_SELECCION == 2) $sSelec = " r.radi_depe_actu  = 999 ";	
  	if ($ps_SELECCION == 3) $sSelec = " r.radi_depe_actu  != 999 ";		
  }
/*FIN  /* Seleccion Todo - Solo archivados - Solo NO archivados */
 $sWDepe = "";
  $ps_RADI_DEPE_RADI = get_param("s_RADI_DEPE_ACTU");
  if(is_number($ps_RADI_DEPE_RADI) && strlen($ps_RADI_DEPE_RADI))
    $ps_RADI_DEPE_RADI = tosql($ps_RADI_DEPE_RADI, "Number");
  else 
    $ps_RADI_DEPE_RADI = "";

  if($ps_RADI_DEPE_RADI > 0)
  {
    $HasParam = true;//se busca en el radicado donde sea like 'yyyyDEP%'
    $sWDepe = " AND d.depe_codi = $ps_RADI_DEPE_RADI ";
  }
  $ps_swTotales = get_param("swTotales");
  $ps_solo_nomb = get_param("s_solo_nomb");
if ($ps_solo_nomb == "cReq") {
$sSQLE1 = "  
	select count(UNIQUE(r.radi_nume_radi)) 	AS cantRads ";

$sFrom = " from dependencia d, radicado r ";
  	$sFromt= ", sgd_tpr_tpdcumento td";
    $sOrder = " order by dependen, tipo ";
	$sWheret= " AND td.sgd_tpr_tpuso = 2 AND  (r.SGD_TPR_COD_CORRESP = td.sgd_tpr_codigo or r.tdoc_codi=td.sgd_tpr_codigo) ";
	$sSQL1 = ", MIN(r.SGD_TPR_COD_CORRESP) AS codtipo ";
	$sSQL2 = ", MIN(r.tdoc_codi) AS codtipo ";
	$sSQL = " 
        select MIN(r.radi_nume_radi) as radi_nume_radi, 
		MIN(d.depe_nomb)				AS dependen,
		MIN(d.depe_codi)				AS depe_codi, 
		MIN(td.sgd_tpr_descrip)	AS tipo";
	$sFrom = "	from dependencia d, radicado r, sgd_tpr_tpdcumento td " ;
	$sWhere = " where r.radi_nume_radi like '%2' 
		and r.radi_depe_radi = d.depe_codi 
		AND SUBSTR(r.radi_nume_Radi,5,3) <> 998
		";

	$sW1 = "	 r.SGD_TPR_COD_CORRESP=td.sgd_tpr_codigo ";
	$sW2 = " 	 r.tdoc_codi=td.sgd_tpr_codigo";
	$sWhere .=  $sWhereFec . $sWDepe ; 
    $sSQLDet = $sSQL . $sSQL1 . $sFrom . $sWhere . " AND $sW1 " . " UNION " .  $sSQL . $sSQL2 . $sFrom . $sWhere . " AND $sW2 " ;
    $sSQLDet = $sSQL . $sSQL1 . $sSQL2 . $sFrom . $sWhere . " AND ($sW1 or $sW2) GROUP BY r.radi_nume_radi ORDER BY r.RADI_NUME_RADI" ;   

	$sWheret= " AND td.sgd_tpr_tpuso = 2 AND  (r.SGD_TPR_COD_CORRESP = td.sgd_tpr_codigo or r.tdoc_codi=td.sgd_tpr_codigo) ";
	$sWhereEnv = " AND r.radi_nume_radi = a.anex_radi_nume
	AND a.radi_nume_salida = env.RADI_NUME_SAL ";
	$sFromEnv = $sFrom . ", anexos a, sgd_renv_regenvio env ";

    $sWhereF = $sWhere . $sWheret;
    $sSQLDet1 = $sSQLDet . $sGroup . $sOrder;
	$iWhere = "";
if($ps_RADI_DEPE_RADI > 0) {
	$iWhere = " AND r.radi_depe_radi = $ps_RADI_DEPE_RADI";
}
if ($ps_swTotales =="Totales")
{ // echo "son totales";
	$sGroup = " group by tipo";
	$sOrder = " order by tipo " ;
	$sSQLDet = "SELECT COUNT(radi_nume_radi) AS CANTRADS, tipo, min(codtipo) as codtipo from($sSQLDet" ;
	$sSQLDet1 = $sSQLDet . ")" . $sGroup . $sOrder;

  	$sSQLDet1 = "SELECT MIN(RADI_DEPE_RADI) AS DEPE_CODI, MIN(D.DEPE_NOMB) AS DEPENDEN, TIPO, SUM(ASIGNADOS) as CANTRADS, SUM(RESPONDIDOS) as ENVIADOS, SUM(ARCHIVADOS) as ARCHIVADOS FROM(
SELECT RADI_DEPE_RADI, TIPO, COUNT(radicado) AS ASIGNADOS, 0 AS RESPONDIDOS, 0 AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) GROUP BY RADI_DEPE_RADI, TIPO
UNION
SELECT  RADI_DEPE_RADI, TIPO, 0 AS ASIGNADOS, COUNT(envio) AS RESPONDIDOS, 0 AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) GROUP BY RADI_DEPE_RADI, TIPO
UNION
SELECT  RADI_DEPE_RADI, TIPO, 0 AS ASIGNADOS, 0 AS RESPONDIDOS, COUNT(USUAR) AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) AS USUAR from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) WHERE USUAR = 'USUARIO PARA SALIDA DE DOCUMENTOS'  GROUP BY RADI_DEPE_RADI, TIPO
ORDER BY 1,2), DEPENDENCIA D WHERE RADI_DEPE_RADI = D.DEPE_CODI GROUP BY TIPO
ORDER BY 1,3";

} else { 
//echo "entro por else " ;

$sFromSelec = " from( select MIN(r.radi_nume_radi) as radi_nume_radi, MIN(d.depe_nomb) AS dependen, MIN(d.depe_codi) AS depe_codi, 
MIN(td.sgd_tpr_descrip) AS tipo, MIN(r.SGD_TPR_COD_CORRESP) AS codtipoc , MIN(r.tdoc_codi) AS codtipo ";

$sSQLDet1 = "
SELECT MIN(DEPE_CODI) as depe_codi,DEPENDEN,TIPO,SUM(CANTRADS) as cantrads, SUM(ENVIADOS) as enviados FROM
(select min(depe_codi) as depe_codi, dependen, min(codtipo) as codtipo, tipo, count(radi_nume_radi) as CANTRADS, 0 AS ENVIADOS " .
$sFromSelec . $sFrom .
$sWhereF . "
GROUP BY r.radi_nume_radi ORDER BY r.RADI_NUME_RADI) group by dependen,tipo 
UNION
select min(depe_codi) as depe_codi, dependen, min(codtipo) as codtipo, tipo, 0 AS CANTRADS, count(radi_nume_radi) as enviados " .
$sFromSelec . $sFrom .
" , anexos a, sgd_renv_regenvio env " .
$sWhereF .
" AND r.radi_nume_radi = a.anex_radi_nume
AND a.radi_nume_salida = env.RADI_NUME_SAL
GROUP BY r.radi_nume_radi ORDER BY r.RADI_NUME_RADI) group by dependen,tipo order by dependen,tipo) group by dependen,tipo";

//echo " <br> lin 431 $sSQLDet1 ";

  	$sSQLDet1 = "SELECT RADI_DEPE_RADI AS DEPE_CODI, MIN(D.DEPE_NOMB) AS DEPENDEN, TIPO, SUM(ASIGNADOS) as CANTRADS, SUM(RESPONDIDOS) as ENVIADOS, SUM(ARCHIVADOS) as ARCHIVADOS FROM(
SELECT RADI_DEPE_RADI, TIPO, COUNT(radicado) AS ASIGNADOS, 0 AS RESPONDIDOS, 0 AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) GROUP BY RADI_DEPE_RADI, TIPO
UNION
SELECT  RADI_DEPE_RADI, TIPO, 0 AS ASIGNADOS, COUNT(envio) AS RESPONDIDOS, 0 AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) GROUP BY RADI_DEPE_RADI, TIPO
UNION
SELECT  RADI_DEPE_RADI, TIPO, 0 AS ASIGNADOS, 0 AS RESPONDIDOS, COUNT(USUAR) AS ARCHIVADOS FROM(
SELECT  MIN(radi_depe_radi) AS RADI_DEPE_RADI, MIN(depe_nomb),  radicado, MIN(radi_fech_radi), MIN(tipo) AS TIPO, MIN(ra_asun), MIN(radi_nume_salida) as salida,MIN(renv.SGD_RENV_FECH) as envio, MIN(usua_nomb) AS USUAR from sgd_renv_regenvio renv,(
SELECT radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td, usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
 and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.TDOC_CODI = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
UNION
SELECT  radi_depe_radi, radi_nume_radi as radicado, radi_fech_radi, td.sgd_tpr_descrip AS tipo, r.ra_asun, u.usua_nomb
from radicado r, sgd_tpr_tpdcumento td,usuario u
where R.RADI_FECH_RADI >= to_date('" .$desde . "', 'yyyy/mm/dd HH24:MI:ss') and R.RADI_FECH_RADI <= to_date('" .$hasta . "', 'yyyy/mm/dd HH24:MI:ss')
and r.radi_nume_radi like '%2' AND SGD_TPR_TPUSO = 2 
and r.SGD_TPR_COD_CORRESP = td.SGD_TPR_CODIGO
AND r.radi_depe_actu = u.depe_codi and r.radi_usua_actu = u.usua_codi
" . $iWhere . "
order by 1), anexos a, dependencia d
where radicado = a.anex_radi_nume (+)
and  radi_nume_salida  = renv.RADI_NUME_SAL (+)
and  radi_depe_radi = d.depe_codi
group by radicado
ORDER BY 1,2) WHERE USUAR = 'USUARIO PARA SALIDA DE DOCUMENTOS'  GROUP BY RADI_DEPE_RADI, TIPO
ORDER BY 1,2), DEPENDENCIA D WHERE RADI_DEPE_RADI = D.DEPE_CODI GROUP BY RADI_DEPE_RADI, TIPO
ORDER BY 1,3";

}


  if(!$bReq)
  {
?>
     <tr>
      
    <td colspan="5" class="DataTD" height="25"><font class="DataFONT">No records</font></td>
     </tr>
</table>
<?
    return;
  }

    $rs = $db->conn->Execute($sSQLDet1);
$fldTotal = 1;
   if(!$fldTotal > 0)
     {
?>   <tr> <td colspan="5" class="DataTD"><font class="DataFONT">No records</font></td>  </tr>
<?   }
   else  {
  		if ($ps_swTotales =="Totales") 	 {
			while(!$rs->EOF ) {  
				 $TIPOACTUAL = $rs->fields["CODTIPO"];
				 $fldTIPO 			= $rs->fields["TIPO"];
				 $fldCANTRADS    	= $rs->fields["CANTRADS"];	
	             $cantEnvTipo   = $rs->fields["ENVIADOS"];
				 $archivados   = $rs->fields["ARCHIVADOS"];
				 $canPendiente = $fldCANTRADS - $cantEnvTipo; 	
				 if ($cantEnvTipo==0) $cantEnvTipo = "";
?>   	  		 <tr>
   	             <td colspan="1" class="DataTD" width="212"><font class="DataFONT"><?= tohtml("--") ?>&nbsp;</font></td> 
 	             <td colspan="1" align="left" class="DataTD" width="273"><font class="DataFONT"><?= tohtml($fldTIPO) ?>&nbsp;</font></td>
	   	         <td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($fldCANTRADS) ?>&nbsp;</font></td>
   	       	     <td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($cantEnvTipo) ?>&nbsp;</font></td>
      	         <td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($archivados) ?>&nbsp;</font></td>
				 </tr>
<?				 $asignados = $asignados + $fldCANTRADS;
				 $respondidos = $respondidos + $cantEnvTipo;
				 $pendientes = $pendientes + $canPendiente;
			   	 $fldArchivados  = $fldArchivados + $rs->fields["ARCHIVADOS"];
	    		 $rs->MoveNext();   
			} //fin del while		
?>			<tr>
			<td colspan="2" class="select"><font class="DataFONT">  <? 	echo "<b>TOTAL GENERAL</b>";  ?>&nbsp;</font></td>
 		  	<td colspan="1" class="select">  <?	echo "<b>$asignados</b>"; 	  ?>  &nbsp; </td>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$respondidos</b>";   ?>  </td>
  	        <td align="left" class="select" colspan="1">  <?	echo "<b>$fldArchivados</b>"; 	  ?>  </td>	
	        </tr>
<?		}else{
			 $iRecordsPerPage = 20000;
			 $iCounter = 0;
			 $DEPACTUAL = $rs->fields["DEPENDEN"];
			 $DEPECODI  = $rs->fields["DEPE_CODI"];
			 $CANTOTAL = 0;
			 $TIPOACTUAL = $rs->fields["CODTIPO"];
			 $TIPODES = $rs->fields["TIPO"];
			 $CANTXTIPO = 0;  $fldTotal = 0; $fldTotalE = 0; $fldArchivados = 0;
		     while(!$rs->EOF && $iCounter < $iRecordsPerPage)
		    {
//-------------------------------
// Create field variables based on database fields
//-------------------------------
				$fldDEPENDEN 		= $rs->fields["DEPENDEN"];
				$fldDEPECODI 		= $rs->fields["DEPE_CODI"];	
				$fldTIPO 			= $rs->fields["TIPO"];
				$fldCODTIPO 		= $rs->fields["CODTIPO"];
				$fldCANTRADS    	= $rs->fields["CANTRADS"];	
			  	$archivados   		= $rs->fields["ARCHIVADOS"];
				$TIPOACTUAL = $rs->fields["CODTIPO"];
				if ($DEPACTUAL == $fldDEPENDEN)  {
			?>
					<tr>
					   <td colspan="1" class="DataTD" width="212"><font class="DataFONT"><?= tohtml($fldDEPENDEN) ?>&nbsp;</font></td> 
					   <td colspan="1" align="left" class="DataTD" width="273"><font class="DataFONT"><?= tohtml($fldTIPO) ?>&nbsp;</font></td>
					   <td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($fldCANTRADS) ?>&nbsp;</font></td>
					<? if(!$rs->EOF) {
						  $cantEnvTipo   = $rs->fields["ENVIADOS"];
  						  $archivados   = $rs->fields["ARCHIVADOS"];
						  $canPendiente = $fldCANTRADS - $cantEnvTipo; 	
						  $totalPendiente = $totalPendiente + $canPendiente; ?>
						<td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($cantEnvTipo) ?>&nbsp;</font></td>
						<td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($archivados) ?>&nbsp;</font></td>
					<?	
					  } else {
							$cantEnvTipo = "";
							$canPendiente = $fldCANTRADS  ?>
							<td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($cantEnvTipo) ?>&nbsp;</font></td>
							<td colspan="1" class="DataTD" width="100"><font class="DataFONT"><?= tohtml($archivados) ?>&nbsp;</font></td>
					<?	    $totalPendiente = $totalPendiente + $canPendiente; 
					  }   			//  echo "<br>linea 491 -->   $cantEnvTipo";
					  $pendienteDep = $pendienteDep + $canPendiente;
					  $enviadosDep = $enviadosDep + $cantEnvTipo;
  					  $archivadosDep = $archivadosDep + $archivados;
					  ?> 
					</tr>
			<?
					$CANTOTAL =  $CANTOTAL + $fldCANTRADS;  $fldTotal = $fldTotal + $rs->fields["CANTRADS"];	
					$fldTotalE = $fldTotalE + $rs->fields["ENVIADOS"];	
					$fldArchivados = $fldArchivados + $rs->fields["ARCHIVADOS"];	
					$rs->MoveNext();       
			}else { // $DEPACTUAL diferente a $fldDEPENDEN
		?>
			<tr class="select"><td class="select" colspan="2"><font class="DataFONT">
		<?
			$sSQLEnv = $sSQL . $sFromE . $sWhereE . " AND d.depe_codi = $DEPECODI " . $sGroup ;
//			$rsEnv = $db->conn->Execute($sSQLEnv);
			if(!$rsEnv->EOF) {
				$fldCANTRADSE   = $rsEnv->fields["CANTRADS"];	
			}
							$fldCANTRADSE   = $rsEnv->fields["CANTRADS"];	

			echo "<b>TOTAL $DEPACTUAL</b>"; ?>&nbsp;</font></td>
			<td align="left" class="select" colspan="1">  <A href='repAsignacionRadDetalle.php?<?=session_name()."=".trim(session_id())."&krd=$krd&DEPACTUAL=$DEPACTUAL&DEPECODI=$DEPECODI&desde=$desde&hasta=$hasta&ps_solo_nomb=$ps_solo_nomb&CANTOTAL=$CANTOTAL&respondidos=$enviadosDep"?>' class=noLeidos><?
			echo "<b>$CANTOTAL</b>"; 	  ?>  </A>&nbsp; </td>
<!--			<? /*if($enviadosDep > 0) { ?>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>"; 	  ?>  </td>
			<? }else {
			$enviadosDep = "";
			?><td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>";   ?>  </td>
			<? } */?> -->
			<td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>"; 	  ?>  </td>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$archivadosDep</b>"; 	  ?>  </td>
		</tr>
		<?
		$CANTOTAL = 0;
		$DEPACTUAL = $fldDEPENDEN;
		$DEPECODI  = $fldDEPECODI;		
		$pendienteDep = 0;
		$enviadosDep = 0;
		$archivadosDep = 0;		
		}
    	$iCounter++;
	}
?>
	<tr>
	<td colspan="2" class="select"><font class="DataFONT">  <? 	echo "<b>TOTAL $DEPACTUAL</b>";  ?>&nbsp;</font></td>
<?    if ($DEPACTUAL == " GENERAL ") {
?>		  	<td colspan="1" class="select">  <?	echo "<b>$CANTOTAL</b>"; 	  ?>  &nbsp; </td>
<?	  }else { ?>
			<td colspan="1" class="select">  <A href='repAsignacionRadDetalle.php?<?=session_name()."=".trim(session_id())."&krd=$krd&DEPACTUAL=$DEPACTUAL&DEPECODI=$DEPECODI&desde=$desde&hasta=$hasta&ps_solo_nomb=$ps_solo_nomb&CANTOTAL=$CANTOTAL&respondidos=$enviadosDep"?>' class=noLeidos><?
			echo "<b>$CANTOTAL</b>"; 	  ?>  </A>&nbsp; </td>
<?    }
	  $sSQLEnv = $sSQL . $sFromE . $sWhereE . " AND d.depe_codi = $DEPECODI " . $sGroup ;
//	  $rsEnv = $db->conn->Execute($sSQLEnv);
	  if(!$rsEnv->EOF) {
				$fldCANTRADSE   = $rsEnv->fields["CANTRADS"];	
		}
	  $pendienteDep = $CANTOTAL - $enviadosDep; ?>
<?    if($enviadosDep > 0) { ?>
			<td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>"; 	  ?>  </td>
<?    }else {
			$enviadosDep = "";
			?><td align="left" class="select" colspan="1">  <?	echo "<b>$enviadosDep</b>";   ?>  </td>
<?    } ?>
	<td align="left" class="select" colspan="1">  <?	echo "<b>$archivadosDep</b>"; 	  ?>  </td>	
	</tr>
<?		}//fin del if ($ps_swTotales =="Totales") 	
?>	<tr></tr>
	<? $totalPendiente = $fldTotal - $fldTotalE ?>
<?	if ($ps_swTotales !="Totales") { ?>
	 <tr>  <td  align="center" colspan="5" class="DataTD"><font size="2">                <b>TOTAL RADICADOS ASIGNADOS  : <?=$fldTotal?></b></font></td></tr>
	  <tr> <td  align="center" colspan="5" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS RESPONDIDOS: <?=$fldTotalE?></b></font></td></tr>
	  <tr> <td  align="center" colspan="5" class="DataTD"><font color="#0033CC" size="2"><b>TOTAL RADICADOS ARCHIVADOS : <?=$fldArchivados?></b></font></td></tr>
 <? } ?>
	<?
	}
	?>
    </table>
  	<?
	}
} // FIN if ($ps_solo_nomb == "cReq") 
	?>

</body>
</html>
