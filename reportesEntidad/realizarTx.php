<?
session_start();
$ruta_raiz = "..";
if(!isset($_SESSION['dependencia']) or !isset($_SESSION['nivelus'])) include "$ruta_raiz/rec_session.php";
error_reporting(0);
/*  REALIZAR TRANSACCIONES
 *  Este archivo realiza la transaccion en Orfeo.
 */
?>
<html>
<head>
<title>Realizar Transaccion - Orfeo </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../estilos/orfeo.css">
</head>
<?
/**
  * Inclusion de archivos para utiizar la libreria ADODB
  *
  */
   include_once "$ruta_raiz/include/db/ConnectionHandler.php";
   $db = new ConnectionHandler("$ruta_raiz");
 /*
	* Genreamos el encabezado que envia las variable a la paginas siguientes.
	* Por problemas en las sesiones enviamos el usuario.
	* @$encabezado  Incluye las variables que deben enviarse a la singuiente pagina.
	* @$linkPagina  Link en caso de recarga de esta pagina.
	*/
	$encabezado = "".session_name()."=".session_id()."&krd=$krd&depeBuscada=$depeBuscada&filtroSelect=$filtroSelect&tpAnulacion=$tpAnulacion";

/*  FILTRO DE DATOS
 *  @$setFiltroSelect  Contiene los valores digitados por el usuario separados por coma.
 *  @$filtroSelect Si SetfiltoSelect contiene algunvalor la siguiente rutina realiza el arreglo de la condici� para la consulta a la base de datos y lo almacena en whereFiltro.
 *  @$whereFiltro  Si filtroSelect trae valor la rutina del where para este filtro es almacenado aqui.
 *
 */

?>
<body>
<?
//$db->conn->debug=true;
	$isql = "UPDATE radicado set radi_usu_recibe = '$krd', radi_fech_recibe = sysdate  WHERE radi_nume_radi in($listaRad)";
	$nombTx = "Recibo documento f�sico";
	$rsUp = $db->conn->Execute($isql);
	$codTx = 84;
	$radicadosSel = explode(",", $listaRad);
	$depsel  = $dependencia ;
	$usCodSelect = $codusuario;
	include "$ruta_raiz/include/tx/Historico.php";
	$hist = new Historico($db);
	$hist->insertarHistorico($radicadosSel,  $dependencia , $codusuario, $depsel, $usCodSelect, $observa, $codTx);


/**  IMPRESION DE RESULTADOS DE LA Transaccion
  */

?>
<form action='enviardatos.php?PHPSESSID=172o16o0o154oJH&krd=JH' method=post name=formulario>
<br>
<table border=0 cellspace=2 cellpad=2 WIDTH=50%  class="t_bordeGris" id=tb_general align="left">
	<tr>
	<td colspan="2" class="titulos4">ACCION REQUERIDA <?=$accionCompletada?>  <?=$nombTx?> COMPLETADA <?=$causaAccion ?> </td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">ACCION REQUERIDA :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$nombTx?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">RADICADOS INVOLUCRADOS :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa"><?=join("<BR> ",$radicadosSel)?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">USUARIO DESTINO :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$usua_nomb?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">FECHA Y HORA :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=date("m-d-Y  H:i:s")?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">USUARIO ORIGEN:
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$usua_nomb?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">DEPENDENCIA ORIGEN:
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$depe_nomb?>
	</td>
	</tr>
</table>
</form>
</body>
</html>
