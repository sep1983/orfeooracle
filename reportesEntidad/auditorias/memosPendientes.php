<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* "MINISTERIO DE TRANSPORTE"                                                        */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*   Luc�a Ojeda		lojedaster@gmail.com	25 Abril 2008						 */
/*************************************************************************************/

$krdOld = $krd;  
session_start();
error_reporting(0);
$ruta_raiz = "../..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
//	$db->conn->debug = TRUE;
?>
<html>
<head>
<title>Listado de radicados de salida pendientes por escanear o anular</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?   
$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hora_inicial=$s_hora_inicial&s_hora_final=$s_hora_final&dep_inicial=$dep_inicial&dep_final=$dep_final";
?>

<form name='frmCrear' action='lisEntregaArchivEntradas1.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="93%"  border="1" align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>RELACION DE MEMORANDOS PENDIENTES DE ESCANEAR O ANULAR</span></B> </p>
	<p>Este reporte muestra los memorandos generados en las dependencias de planta central hasta la fecha actual menos 5 d&iacute;as y que no han sido escaneados ni anulados. Se presenta ordenado por c&oacute;digo de dependencia y  n&uacute;mero de radicado </p>
	</center>
	</td>
	</tr>
</table>
<?
$generar = "SI";
if($generar)
{
		error_reporting(0);
		$ruta_raiz = "../..";
	   	$siGeneroRegs = false;

	$sSQL =  " 
		select unique(rad), MIN(fech) AS FECH, MIN(coddep) AS CODDEP, MIN(dependencia) AS DEPENDENCIA, MIN(creador) AS CREADOR from(
		select radicado.radi_nume_radi rad, MIN(radicado.radi_fech_radi) fech, MIN(radicado.radi_depe_radi) coddep, 
			MIN(DEPE_NOMB) DEPENDENCIA, MIN(anex_creador) CREADOR
		from radicado, anexos a, dependencia d
		where SGD_EANU_CODIGO is null
			AND (radicado.radi_nume_RADI like '%3')
			AND SUBSTR(radicado.RADI_PATH,26,3) <> 'tif' AND radi_depe_radi <> 998 AND radi_depe_radi <> 997
			AND radi_nume_radi = a.radi_nume_salida and a.ANEX_ESTADO <> 4
			AND radi_depe_radi = d.depe_codi
			AND radicado.RADI_FECH_RADI <= sysdate - 5
			AND depe_codi_territorial = 100 
		group by RADI_NUME_RADI
		union
		select radicado.radi_nume_radi rad, MIN(radicado.radi_fech_radi) fech, MIN(radicado.radi_depe_radi) coddep, 
			MIN(DEPE_NOMB) DEPENDENCIA, '' CREADOR
		from radicado, dependencia d
		where SGD_EANU_CODIGO is null
			AND (radicado.radi_nume_RADI like '%3')
			AND SUBSTR(radicado.RADI_PATH,26,3) <> 'tif' AND radi_depe_radi <> 998 AND radi_depe_radi <> 997
			AND radi_nume_radi not in(select anex_radi_nume from anexos)
			AND radi_depe_radi = d.depe_codi
			AND radicado.RADI_FECH_RADI <= sysdate - 5
			AND depe_codi_territorial = 100 
		group by RADI_NUME_RADI)
		group by RAD
		order by 3,1
	";

//and radicado.RADI_FECH_RADI >= TO_DATE('2008-09-29, 12:00:00 AM','RRRR-MM-DD, HH:MI:SS AM') AND radicado.RADI_FECH_RADI <= TO_DATE('2009-12-31, 11:59:59 PM','RRRR-MM-DD, HH:MI:SS AM') 
		
			$query_t = $sSQL ;
			RADICADO_show() ;
  	
}
?>
	
</form>
<?
function RADICADO_show()
{
 
  global $db;
  global $depe_codi;
  global $siGeneroRegs;
  global $query_t;
  $sSQL = "";
  $sFormTitle = "";


	$ruta_raiz = "../..";
	?>
		<table class="FormTABLE" width="900" align="center">
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
		  </tr>
			  
		  <tr > 
			<td class="ColumnTD" height="25" width="100"><font>Radicado</font></td>
			<td class="ColumnTD" height="25" width="70"><font>Fecha Rad </font></td>
			<td class="ColumnTD" width="70" height="25"><font size=2>codDep</font></td>
			<td class="ColumnTD" height="25" width="100"><font size=2>Dependencia</font></td>
			<td width="70" height="25" class="ColumnTD"><font size=2>Creador</font></td>
		  </tr>
<?
 $rs=$db->conn->Execute($query_t);
 while(!$rs->EOF)
	{
	$siGeneroRegs = true;
    $fldRAD = substr($rs->fields["RAD"],0,55); 
    $fldFECH = $rs->fields["FECH"];
    $fldCODDEP = $rs->fields["CODDEP"];
    $fldDEPENDENCIA= $rs->fields["DEPENDENCIA"];
    $fldCREADOR = $rs->fields["CREADOR"];
	
	?>
    <tr>
	  <td ><font size="-7">
      <?= $fldRAD ?>&nbsp;</font></td>
      <td ><font size="-7">
      <?= $fldFECH ?>&nbsp;</font></td>
      <td ><font size="-7">
      <?= $fldCODDEP ?>&nbsp;</font></td>
      <td ><font size="-7">
      <?= $fldDEPENDENCIA ?>&nbsp;</font></td>
      <td ><font size="-7">
      <?= $fldCREADOR ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
 <?= $nregis ?> Radicados</a></center>
	</td>	</TR>
	</TABLE>
<?   }   
}
  ?>
</table>


</body>
</html>