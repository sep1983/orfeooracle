<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/*************************************************************************************/

$krdOld = $krd;  
session_start();
error_reporting(0);
$ruta_raiz = "../..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
//	$db->conn->debug = TRUE;
?>
<html>
<head>
<title>Listado de radicados entrantes para entregar en archivo central</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?   
$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hora_inicial=$s_hora_inicial&s_hora_final=$s_hora_final&dep_inicial=$dep_inicial&dep_final=$dep_final";
?>

<form name='frmCrear' action='lisEntregaArchivEntradas1.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="93%"  border="1" align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>REPORTE DE DIAS PROMEDIO TERRITORIALES </span></B> </p>
	</center>
	</td>
	</tr>
	<tr>
	<td ><font >Fecha de generación: <?=date("Y-m-d - H:i:s")?></font></td>
	</tr>
</table>
<?
$generar = "SI";
if($generar)
{
		error_reporting(0);
		$ruta_raiz = "../..";
	   	$siGeneroRegs = false;

	$sSQL =  " SELECT MIN(d.DEPE_CODI) COD_DEPE, MIN(d.depe_nomb) DEPENDENCIA, COUNT(r.RADI_NUME_RADI) AS RADS, round(SUM(round(sysdate -r.radi_fech_radi))/COUNT(r.RADI_NUME_RADI)) diasPROM
		FROM RADICADO r, dependencia d
		WHERE d.depe_codi_territorial <> 100 
		AND d.DEPE_CODI=r.RADI_DEPE_ACTU AND r.RADI_NUME_RADI LIKE '%2' 
                and D.DEPE_CODI < 990
                GROUP BY d.DEPE_CODI
		order BY d.DEPE_CODI
				";

			$query_t = $sSQL ;
			RADICADO_show() ;
  	
}
?>
	
</form>
<?
function RADICADO_show()
{
 
  global $db;
  global $depe_codi;
  global $siGeneroRegs;
  global $query_t;
  $sSQL = "";
  $sFormTitle = "";


	$ruta_raiz = "../..";
	?>		
<table class="FormTABLE" width="800" align="center">
			  
		  <tr > 
			<td class="ColumnTD" height="25" width="117"><font><B>CODIGO</B></font></td>
			<td class="ColumnTD" height="25" width="362"><font><b>DEPENDENCIA</b></font></td>
			<td class="ColumnTD" width="190" height="25"><font><b>RADICADOS</b></font></td>
			<td class="ColumnTD" height="25" width="211"><font ><b>DIAS PROMEDIO</b></font></td>
		  </tr>
<?
// $archivo = fopen("../../bodega/pendientes.txt", "w");
// $linea = "COD DEP,DEPENDENCIA,RADS PENDIENTES,DIAS PROMEDIO\r\n";
// fputs($archivo, $linea);
 $rs=$db->conn->Execute($query_t);
 while(!$rs->EOF)
	{
	$siGeneroRegs = true;
    $fldDEPENDENCIA = substr($rs->fields["DEPENDENCIA"],0,55); 
    $fldRads = $rs->fields["RADS"];
    $fldCodigo = $rs->fields["COD_DEPE"];
    $fldDias = $rs->fields["DIASPROM"];
//	$linea = $fldDEPENDENCIA . "," . $fldUSUARIO . "," . $fldR30 . "," . $fldR3060 . "," . $fldR6090 . "," . $fldR90 . "\r\n";
//	fputs($archivo, $linea);
	
	?>
    <tr>
		<td ><font size="-1">
      <?= $fldCodigo ?>&nbsp;</font></td>
        <td ><font size="-1">
      <?= $fldDEPENDENCIA ?>&nbsp;</font></td>
       <td ><font size="-1">
      <?= $fldRads ?>&nbsp;</font></td>
       <td ><font size="-1">
      <?= $fldDias ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
//	fclose($archivo);
//	$noArchivo = "pendientes.txt";
/*   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
	<a href='<?= "../../bodega/".$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo Texto</a></center>
	</td>	</TR>
	</TABLE>
<?   }   */
}
  ?>
</table>


</body>
</html>