<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* C.R.A.  "COMISION DE REGULACION DE AGUA"                                          */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*   Luc�a Ojeda		lojedaster@gmail.com	25 Abril 2008						 */
/*************************************************************************************/

$krdOld = $krd;  
session_start();
error_reporting(0);
$ruta_raiz = "../..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
//	$db->conn->debug = TRUE;
?>
<html>
<head>
<title>Relacion de Radicados de Entrada Pendientes por Rangos de Dias</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?   
$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hora_inicial=$s_hora_inicial&s_hora_final=$s_hora_final&dep_inicial=$dep_inicial&dep_final=$dep_final";
?>

<form name='frmCrear' action='lisEntregaArchivEntradas1.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="93%"  border="1" align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>RELACION DE RADICADOS DE ENTRADA PENDIENTES POR RANGOS DE DIAS</span></B> </p>
	<p><B><span class=etexto>DIRECCION DE TRANSPORTE Y TRANSITO</span></B> </p>
	<p>Este reporte muestra los radicados de entrada que se encuentran en cada usuario, discriminando en columnas los que tienen fecha de radicaci&oacute;n menor a 30 d&iacute;as, entre 30 y 60, entre 60 y 90 y radicados hace m&aacute;s de 90 d&iacute;as </p>
	</center>
	</td>
	</tr>
</table>
<?
$generar = "SI";
if($generar)
{
		error_reporting(0);
		$ruta_raiz = "../..";
	   	$siGeneroRegs = false;

	$sSQL =  " SELECT MIN(DEPENDENCIA) DEPENDENCIA, USUARIO, SUM(RADS30) R30, SUM(RADS30A60) R3060, SUM(RADS60A90) R6090, SUM(RADSMAS90) R90 FROM (
		SELECT MIN(d.depe_nomb) DEPENDENCIA, MIN(b.USUA_NOMB) USUARIO ,MIN(b.DEPE_CODI) HID_DEPE_USUA ,COUNT(r.RADI_NUME_RADI) RADS30, 0 RADS30A60, 0 RADS60A90, 0 RADSMAS90 
		FROM RADICADO r, USUARIO b, dependencia d
		WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI and r.carp_codi <> 5 
		AND (d.DEPE_CODI=400 OR d.depe_codi_padre = 400 OR d.depe_codi_padre = 410 OR d.depe_codi_padre = 420 OR d.depe_codi_padre = 412)
		AND d.DEPE_CODI=r.RADI_DEPE_ACTU AND r.RADI_NUME_RADI LIKE '%2' AND b.depe_codi = d.depe_codi
		AND round(r.radi_fech_radi-sysdate) > -30
		GROUP BY b.DEPE_CODI,b.USUA_NOMB
		UNION
		SELECT MIN(d.depe_nomb) DEPENDENCIA, MIN(b.USUA_NOMB) USUARIO ,MIN(b.DEPE_CODI) HID_DEPE_USUA ,0 RADS30, COUNT(r.RADI_NUME_RADI) RADS30A60, 0 RADS60A90, 0 RADSMAS90
		FROM RADICADO r, USUARIO b, dependencia d
		WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI and r.carp_codi <> 5 
		AND (d.DEPE_CODI=400 OR d.depe_codi_padre = 400 OR d.depe_codi_padre = 410 OR d.depe_codi_padre = 420 OR d.depe_codi_padre = 412)
		AND d.DEPE_CODI=r.RADI_DEPE_ACTU AND r.RADI_NUME_RADI LIKE '%2' AND b.depe_codi = d.depe_codi
		AND round(r.radi_fech_radi-sysdate) <= -30 AND  round(r.radi_fech_radi-sysdate) > -60
		GROUP BY b.DEPE_CODI,b.USUA_NOMB
		UNION
		SELECT MIN(d.depe_nomb) DEPENDENCIA, MIN(b.USUA_NOMB) USUARIO ,MIN(b.DEPE_CODI) HID_DEPE_USUA ,0 RADS30, 0 RADS30A60, COUNT(r.RADI_NUME_RADI) RADS60A90, 0 RADSMAS90
		FROM RADICADO r, USUARIO b, dependencia d
		WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI and r.carp_codi <> 5 
		AND (d.DEPE_CODI=400 OR d.depe_codi_padre = 400 OR d.depe_codi_padre = 410 OR d.depe_codi_padre = 420 OR d.depe_codi_padre = 412)
		AND d.DEPE_CODI=r.RADI_DEPE_ACTU AND r.RADI_NUME_RADI LIKE '%2' AND b.depe_codi = d.depe_codi
		AND round(r.radi_fech_radi-sysdate) <= -60 AND  round(r.radi_fech_radi-sysdate) > -90
		GROUP BY b.DEPE_CODI,b.USUA_NOMB
		UNION
		SELECT MIN(d.depe_nomb) DEPENDENCIA, MIN(b.USUA_NOMB) USUARIO ,MIN(b.DEPE_CODI) HID_DEPE_USUA ,0 RADS30, 0 RADS30A60, 0 RADS60A90, COUNT(r.RADI_NUME_RADI) RADSMAS90
		FROM RADICADO r, USUARIO b, dependencia d
		WHERE r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI and r.carp_codi <> 5 
		AND (d.DEPE_CODI=400 OR d.depe_codi_padre = 400 OR d.depe_codi_padre = 410 OR d.depe_codi_padre = 420 OR d.depe_codi_padre = 412)
		AND d.DEPE_CODI=r.RADI_DEPE_ACTU AND r.RADI_NUME_RADI LIKE '%2' AND b.depe_codi = d.depe_codi
		AND round(r.radi_fech_radi-sysdate) <= -90 
		GROUP BY b.DEPE_CODI,b.USUA_NOMB)
		GROUP BY USUARIO ORDER BY 1,2
		
				";

			$query_t = $sSQL ;
			RADICADO_show() ;
  	
}
?>
	
</form>
<?
function RADICADO_show()
{
 
  global $db;
  global $depe_codi;
  global $siGeneroRegs;
  global $query_t;
  $sSQL = "";
  $sFormTitle = "";


	$ruta_raiz = "../..";
	?>
		<table class="FormTABLE" width="900" align="center">
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
		  </tr>
			  
		  <tr > 
			<td class="ColumnTD" height="25" width="270"><font>Dependencia</font></td>
			<td class="ColumnTD" height="25" width="150"><font>Usuario</font></td>
			<td class="ColumnTD" width="70" height="25"><font size=2>D30</font></td>
			<td class="ColumnTD" height="25" width="70"><font size=2>D30A60</font></td>
			<td width="70" height="25" class="ColumnTD"><font size=2>D60A90</font></td>
			<td width="70" height="25" class="ColumnTD"><font size=2>DMAS90</font></td>
		  </tr>
<?
 $archivo = fopen("../../bodega/pendientes.txt", "w");
 $linea = "DEPENDENCIA,USUARIO,D30,D30A60,D60A90,DMAS90\r\n";
 fputs($archivo, $linea);
 $rs=$db->conn->Execute($query_t);
 while(!$rs->EOF)
	{
	$siGeneroRegs = true;
    $fldDEPENDENCIA = substr($rs->fields["DEPENDENCIA"],0,55); 
    $fldUSUARIO = $rs->fields["USUARIO"];
    $fldR30 = $rs->fields["R30"];
    $fldR3060 = $rs->fields["R3060"];
    $fldR6090 = $rs->fields["R6090"];
    $fldR90 = $rs->fields["R90"];	
	$linea = $fldDEPENDENCIA . "," . $fldUSUARIO . "," . $fldR30 . "," . $fldR3060 . "," . $fldR6090 . "," . $fldR90 . "\r\n";
	fputs($archivo, $linea);
	
	?>
    <tr>
		<td ><font size="-7">
      <?= $fldDEPENDENCIA ?>&nbsp;</font></td>
        <td ><font size="-7">
      <?= $fldUSUARIO ?>&nbsp;</font></td>
       <td ><font size="-7">
      <?= $fldR30 ?>&nbsp;</font></td>
       <td ><font size="-7">
      <?= $fldR3060 ?>&nbsp;</font></td>
       <td ><font size="-7">
      <?= $fldR6090 ?>&nbsp;</font></td>
       <td ><font size="-7">
      <?= $fldR90 ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
	fclose($archivo);
	$noArchivo = "pendientes.txt";
   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
	<a href='<?= "../../bodega/".$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo Texto</a></center>
	</td>	</TR>
	</TABLE>
<?   }   
}
  ?>
</table>


</body>
</html>
