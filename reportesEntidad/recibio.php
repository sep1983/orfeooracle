<?
$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
session_start();

if(!$krd) $krd=$krdOsld;

$ruta_raiz = "..";
$mensaje_error = false;
if(!isset($_SESSION['dependencia']))  include "$ruta_raiz/rec_session.php";

/**
  * Inclusion de archivos para utilizar la libreria ADODB
  *
  */

include_once "$ruta_raiz/include/db/ConnectionHandler.php";
if (!isset($db))	$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
require_once("$ruta_raiz/class_control/Dependencia.php");
$objDep = new Dependencia($db);
?>
<html>
<head>
<link rel="stylesheet" href="<?=$ruta_raiz; ?>/estilos/orfeo.css">
<script>
function valMaxChars(maxchars)
{	document.realizarTx.observa.focus();
 	if(document.realizarTx.observa.value.length > maxchars)
 	{	/*  alert('Demasiados caracteres en el texto ! Por favor borre '+
    	(document.realizarTx.observa.value.length - maxchars)+ ' caracteres pues solo se permiten '+ maxchars);*/
 		alert('Demasiados caracteres en el texto, solo se permiten '+ maxchars);
 		setSel(maxchars,document.realizarTx.observa.value.length);
   	return false;
 	}
 	else	return true;
}
function okTx()
{
//	valCheck = Anular(0);
//	if(valCheck==0) return 0;
	numCaracteres = document.realizarTx.observa.value.length;
	if(numCaracteres>=6)
	{	//alert(document.realizarTx.usCodSelect.options.selected);
		if (valMaxChars(550))
			document.realizarTx.submit();
	}else
	{
		alert("Atenci�n: El n�mero de Caracteres m�nimo en la observaci�n es de 6. (Digit� :"+numCaracteres+")");
	}
}
</script>
<?
/*
* Generamos el encabezado que envia las variable a la paginas siguientes.
* Por problemas en las sesiones enviamos el usuario.
* @$encabezado  Incluye las variables que deben enviarse a la singuiente pagina.
* @$linkPagina  Link en caso de recarga de esta pagina.
*/
$encabezado = "".session_name()."=".session_id()."&krd=$krd&depeBuscada=$depeBuscada&checkValue=$checkValue&listaRad=$listaRad";
$linkPagina = "$PHP_SELF?$encabezado&orderTipo=$orderTipo&orderNo=";

if(isset($_POST['checkValue']))
{	$num = count($_POST['checkValue']);
	reset($_POST['checkValue']);
	$i = 0;
	$RadCont = 0;
	$resultadoJGL = "";
	while (list($recordid,$tmp) = each($_POST['checkValue']))
	{	$record_id = $recordid;
		if ($listaRad) $listaRad .= ",";
		$listaRad .= "$record_id";
		if ($radRecibidos) $radRecibidos .= " - ";
		$radRecibidos .= "$record_id";
		if ($whereFiltro) $whereFiltro .= " OR ";
		$whereFiltro.= " r.radi_nume_radi = " . $record_id;
		$RadCont++;
	}
	$isql = "select unique(R.RADI_NUME_RADI) as IDT_Radicado, RADI_PATH as HID_RADI_PATH, R.RADI_DESC_ANEX as ANEXOS, 
	to_char(R.RADI_FECH_RADI,'dd/mm/yyyy hh24:mi:ss') as FECH_RAD, R.RADI_NUME_HOJA as HOJAS, R.RA_ASUN as ASUNTO, 
	dir.SGD_DIR_NOMREMDES as remitente, to_char(r.RADI_NUME_RADI) CHK_CHKANULAR, RADI_LEIDO HID_RADI_LEIDO 
	from RADICADO R, sgd_dir_drecciones dir, dependencia d ";

	$whereFiltro = " where ( $whereFiltro ) AND R.RADI_DEPE_RADI = D.DEPE_CODI  
	AND r.radi_nume_radi = dir.radi_nume_radi";
	$isql = $isql . $whereFiltro;
?>
<table border=0 width=100% cellpadding="0" cellspacing="0">
	<tr><td width=100%>
		<form action= 'realizarTx.php?<?=$encabezado?>' method=post name="realizarTx">
			<input type='hidden' name=codTx value='<?=$codTx?>'>
			<input type='hidden' name=EnviaraV value='<?=$EnviaraV?>'>
			<input type='hidden' name=listaRad value='<?=$listaRad?>'>
	
			<table width="98%" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
			<tr>
				<TD width=30% class="titulos4">USUARIO:<br><br><?=$_SESSION['usua_nomb']?> </TD>
				<TD width='30%' class="titulos4">DEPENDENCIA:<br><br><?=$_SESSION['depe_nomb']?><br></TD>
				<? $accion = "Recibir documentos";  ?>
				<TD width='30%' class="titulos4">ACCION:<br><br><?=$accion?><br></TD>
				<td width='5' class="grisCCCCCC">
				<input type=button value=REALIZAR onClick="okTx();" name=enviardoc align=bottom class=botones id=REALIZAR>
				</td>
			</tr>
			</table>
			
			<table width="98%" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
				<TR bgcolor="White"><TD width="100">
								<center>
				<img src="<?=$ruta_raiz?>/iconos/tuxTx.gif" alt="Tux Transaccion" title="Tux Transaccion">
				</center>
				</td>
				
				<TD align="left">
				<span class="etextomenu">
				</span>
						<textarea name=observa cols=70 rows=3  class=ecajasfecha></textarea>
				</TD>
				</TR>
			</table>				
			
			<? $MsgCuenta = "Se reciben $RadCont Radicados:"; ?>
			<table width="98%" border="0" cellpadding="0" cellspacing="5" >
			<tr>
				<TD width=30% ><font color="#FF0000" size="+1"> Resumen: <?=$MsgCuenta?></font>
				<br> <?= $radRecibidos  ?></br>
				</TD>
			</tr>
			</table>
				<input type=hidden name=enviar value=enviarsi>
			</TABLE>
		</form>
	</td></tr>
</TABLE>
<?
} else {
if($checkValue)
{
	echo "si encontro radicados ";
}
if(isset($_POST['checkValue']))
{	$num = count($_POST['checkValue']);
	reset($_POST['checkValue']);
	$i = 0;
	$RadCont = 0;
	$resultadoJGL = "";
	while (list($recordid,$tmp) = each($_POST['checkValue']))
	{	$record_id = $recordid;
		$listaRad .= " $record_id - ";
		echo "$listaRad ";
	}
}
?> <center><tr> <td> <font color="#FF0000" size="+1"> NO SELECCIONO NINGUN RADICADO </font></td></tr></center>
<? 
}
