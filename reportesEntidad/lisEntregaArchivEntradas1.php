<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                                 */
/* C.R.A.  "COMISION DE REGULACION DE AGUA"                                          */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/*																					 */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     			Fecha   Modificacion                 */
/*   Luc�a Ojeda		lojedaster@gmail.com	25 Abril 2008						 */
/*************************************************************************************/

$krdOld = $krd;  
session_start();
error_reporting(0);
$ruta_raiz = "..";
if(!$krd) $krd=$krdOld;
if(!isset($_SESSION['dependencia']))	include "$ruta_raiz/rec_session.php";
    include "$ruta_raiz/config.php";
	include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    if (!defined('ADODB_FETCH_ASSOC'))define('ADODB_FETCH_ASSOC',2);
    $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

	include ("../busqueda/common.php");
	$db->conn->debug = false;
?>
<html>
<head>
<title>Listado de radicados entrantes para entregar en archivo central</title>
<meta name="GENERATOR" content="YesSoftware CodeCharge v.2.0.5 build 11/30/2001">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><link rel="stylesheet" href="../busqueda/Site.css" type="text/css">
</head>
<body class="PageBODY">
<?   
$encabezado = "&krd=$krd&dep_sel=$dep_sel&s_desde_RADI_FECH_RADI=$s_desde_RADI_FECH_RADI&s_hora_inicial=$s_hora_inicial&s_hora_final=$s_hora_final&dep_inicial=$dep_inicial&dep_final=$dep_final";
?>

<form name='frmCrear' action='lisEntregaArchivEntradas1.php?<?=session_name()."=".session_id()."&$encabezado"?>' method="post">
<table width="95%"  align="center">
  	<tr bordercolor="#FFFFFF">
    <td colspan="2" class="titulos4">
	<center>
	<p><B><span class=etexto>RELACION DE ASIGNACION DIARIA DE CORRESPONDENCIA</span></B> </p>
	</center>
	</td>
	</tr>
</table>
<?
if($generar)
{
		error_reporting(0);
		$ruta_raiz = "..";
		include ("$ruta_raiz/fpdf/html2pdf.php");
		$ruta_raiz = "..";
		$pdf=new PDF();
		$pdf->Open();
		$pdf->SetCreator("HTML2PDF");
		$pdf->AddPage();

	   	$sql = "select DEPE_CODI from DEPENDENCIA
					where DEPE_CODI >= $dep_inicial AND DEPE_CODI <= $dep_final
					order by DEPE_codi ";
	   	$rsDep = $db->conn->Execute($sql);
	   	$siGeneroRegs = false;

		$fecha_ini = mktime($hora_ini,$minutos_ini,$segundos_ini,substr($fecha_busq,5,2),substr($fecha_busq,8,2),substr($fecha_busq,0,4));
		$fecha_fin = mktime($hora_fin,$minutos_fin,$segundos_fin,substr($fecha_busqH,5,2),substr($fecha_busqH,8,2),substr($fecha_busqH,0,4));

		$sSQL =  " select unique(R.RADI_NUME_RADI) as R_RADI_NUME_RADI, R.RADI_DEPE_RADI as R_RADI_DEPE_RADI, R.RADI_DESC_ANEX as R_RADI_DESC_ANEX, 
			to_char(R.RADI_FECH_RADI,'dd/mm/yyyy hh24:mi:ss') as R_RADI_FECH_RADI, R.RADI_NUME_HOJA as R_RADI_NUME_HOJA, 
			R.RA_ASUN as R_RA_ASUN, R.RADI_PATH as R_IMAGEN,
			dir.SGD_DIR_NOMREMDES,
			d.depe_nomb
			from RADICADO R, sgd_dir_drecciones dir, dependencia d
		";

		$sOrder = " order by R.RADI_NUME_RADI";
		$radicacion = "Desde " . $fecha_busq . " " . $hora_ini .":". $minutos_ini .":". $segundos_ini . " Hasta " . $fecha_busqH . " " . $hora_fin .":". $minutos_fin .":". $segundos_fin;

	   while(!$rsDep->EOF)
	   {
	      	$depe_codi = $rsDep->fields['DEPE_CODI'];
    		$sWhere = "R.RADI_FECH_RADI BETWEEN
				".$db->conn->DBTimeStamp($fecha_ini)." and ".$db->conn->DBTimeStamp($fecha_fin) . " AND 	R.RADI_NUME_RADI LIKE '%2'
		 		AND R.RADI_DEPE_RADI = D.DEPE_CODI AND R.RADI_DEPE_RADI = $depe_codi
				AND r.radi_nume_radi = dir.radi_nume_radi
				AND substr(R.RADI_NUME_RADI,5,3) = " . $DEPE_ORIGEN;
				
			$sWhere = " WHERE (" . $sWhere . ")";

			$query_t = $sSQL . $sWhere . $sOrder;  
			RADICADO_show() ;
  			$rsDep->MoveNext();
	   }
	   if($siGeneroRegs) {
			$noArchivo = "../$carpetaBodega".$noArchivo;
			$pdf->Output($noArchivo);
	   }
}
?>
	
</form>
<?
function RADICADO_show()
{
 
  global $db;
  global $sFileName;
  global $styles;
  global $radicacion;
  global $noArchivo;
  global $depe_codi;
  global $pdf;
  global $siGeneroRegs;
  global $fecha_busq;
  global $fecha_busqH;
  global $DEPE_ORIGEN;  
  global $query_t;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $sFormTitle = "Reporte Entradas";
  $HasParam = false;
  $iSort = "";
  $iSorted = "";
  $sDirection = "";
  $sSortParams = "";
  $iTmpI = 0;
  $iTmpJ = 0;
  $sCountSQL = "";

	$ruta_raiz = "..";
    $dbSel = new ConnectionHandler("$ruta_raiz");	
	$dbSel->conn->debug=false;
	$dbSel->conn->SetFetchMode(ADODB_FETCH_ASSOC);
	$rsSel = $dbSel->conn->Execute($query_t); 
	$i=0;

	if(!$rsSel->EOF)  { 
		   $depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);
		   $sFormTitle .= "- $depe_codi - $depe_nomb";
	   	   $siGeneroRegs = true ;
?>
		<table class="FormTABLE" width="90%" align="center">
		  <tr>
		  <td class="FormHeaderTD" colspan="5"><a name="RADICADO"><font class="FormHeaderFONT"><?=$sFormTitle?></font></a></td>
		  </tr>
			  
		  <tr align="center"> 
			<td class="ColumnTD" height="25" width="85">No</td>
			<td class="ColumnTD" height="25" width="85">Radicado</td>
			<td class="ColumnTD" height="25" width="331">Asunto</td>
			<td class="ColumnTD" width="110" height="25">Fecha Radicaci&oacute;n</td>
			<td class="ColumnTD" height="25" width="23"># Hoj</td>
			<td width="142" height="25" class="ColumnTD">Anexos</td>
			<td width="142" height="25" class="ColumnTD">Digitalizado</td>
		  </tr>
<?  } 
	while(!$rsSel->EOF)
	{
	   	$radicado[$i] = $rsSel->fields['R_RADI_NUME_RADI'];
	   	$asunto[$i]= substr($rsSel->fields['R_RA_ASUN'],0,100);
	   	$hora[$i] = $rsSel->fields['R_RADI_FECH_RADI'];   
	   	$anexos[$i] = $rsSel->fields['R_RADI_DESC_ANEX'];
	   	$imagen[$i] = empty($rsSel->fields['R_IMAGEN'])? 'No': 'Si';
   	   	$fldNOMBRE[$i]  = substr($rsSel->fields["SGD_DIR_NOMREMDES"],0,60);
	   	$depe_nomb      = substr($rsSel->fields["DEPE_NOMB"],0,50);
	    $i++;
		$rsSel->MoveNext();
	}

	if($radicado) {  
		$noArchivo = "/pdfs/planillas/planillaEntrega". $DEPE_ORIGEN .  ".pdf";
		define(FPDF_FONTPATH,'../fpdf/font/');

		error_reporting(7);

		$radicadosPdf .= "</table>";

	$htmlA = "
		<b><center> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
		RELACI&Oacute;N DE ASIGNACIOacute;N DIARIA DE CORRESPONDENCIA</center></b>";
		$pdf->SetFont('Arial','',9);
		$pdf->WriteHTML($htmlA);
	$htmlB = "<table>
	  <tr> 
		
		$radicacion <b> $s_desde_RADI_FECH_RADI</b>  &nbsp;&nbsp;&nbsp; DEPENDENCIA: <b>$depe_codi - $depe_nomb</b></tr></table>";
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlB);
		

	 $htmlC = "<table>
	  <tr> 
		<td class='ColumnTD' height='25' width='85'><b>Radicado                       Hora                  Remitente</b></td>
	  </tr>
	";
		$htmlD = "
		<br>
		_________________________________________________ 				_________________________________________________<br>
		Entregado por:                                                                               Recibido por:";

		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlC);

		$cantRegs=0;
		foreach($radicado as $id=>$noRadicado)
		{
			$cantRegs++;
		}
	$pags = round($cantRegs / 15,1);
	$pagAct = 1;
	$numreg = 0;
		foreach($radicado as $id=>$noRadicado)
		{
			$numreg++;		
			$radicadosPdf = "<tr height='25'><td>$numreg<b><font size='15'>" . "&nbsp;&nbsp;&nbsp;" . $radicado[$id] . "</font></b>&nbsp;$hora[$id]&nbsp;&nbsp;&nbsp;&nbsp;".$fldNOMBRE[$id]."</td></tr>";
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',9);
			$pdf->WriteHTML($html);
			$radicadosPdf = "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Asunto: </b>" .$asunto[$id]. "</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Anexos: </b>" .$anexos[$id]. "</td></tr>
 <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Digitalizado: </b>" .$imagen[$id]. "</td></tr>"; 
			$html = "$radicadosPdf";
			$pdf->SetFont('Arial','',7);
			$pdf->WriteHTML($html);
			if($numreg==15 or $numreg==30 or $numreg==45 or $numreg==60 or $numreg==75 or $numreg==90) 
				{
					if($cantRegs > $numreg) {
						$htmlN = "<br>                                                                       p�g $pagAct de $pags";
						$pdf->SetFont('Arial','',8);
					    $pdf->WriteHTML($htmlN);
					} else {
		 			    $pdf->SetFont('Arial','',8);
				 		$pdf->WriteHTML($htmlD);
					}
				 $pagAct++;
				 $pdf->AddPage();
				 $pdf->SetFont('Arial','',9);
				 $pdf->WriteHTML($htmlA);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlB);
				 $pdf->SetFont('Arial','',8);
				 $pdf->WriteHTML($htmlC);
				}
		}
		$pdf->SetFont('Arial','',8);
		$pdf->WriteHTML($htmlD);
		$pdf->AddPage();

}
 $rs=$db->conn->Execute($query_t);
 while(!$rs->EOF)
	{
    $fldRA_ASUN = $rs->fields["R_RA_ASUN"];
    $fldRADI_DESC_ANEX = $rs->fields["R_RADI_DESC_ANEX"];
    $fldRADI_FECH_RADI = $rs->fields["R_RADI_FECH_RADI"];
    $fldRADI_NUME_HOJA = $rs->fields["R_RADI_NUME_HOJA"];
    $fldRADI_NUME_RADI = $rs->fields["R_RADI_NUME_RADI"];
    $fldRADI_NUME_IMAG = empty($rs->fields["R_IMAGEN"])? 'No': 'Si';
	?>
    <tr>
        <td class="DataTD"><font class="DataFONT">
      <?= tohtml($nregis + 1) ?>&nbsp;</font></td>
        <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRA_ASUN) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_FECH_RADI) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_HOJA) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_DESC_ANEX) ?>&nbsp;</font></td>
       <td class="DataTD"><font class="DataFONT">
      <?= tohtml($fldRADI_NUME_IMAG) ?>&nbsp;</font></td>
    </tr>
	<?
    $rs->MoveNext();  
	$nregis = $nregis + 1 ;
	}
	if($nregis>0) {
?>
     <tr>
      <td colspan="5" class="DataTD"><font class="DataFONT"><b>Total Registros: <?=$nregis?></b></font></td>
     </tr>
<?	}
}
  ?>
</table>
<?	   if($siGeneroRegs) {
?>
  	<TABLE BORDER=0 WIDTH=100% class="borde_tab">
		<TR><TD class="listado2"  align="center"><center>
	<a href='<?=$noArchivo?>' target='<?=date("dmYh").time("his")?>'>Abrir Archivo pdf</a></center>
	</td>	</TR>
	</TABLE>
<?   }   ?>

</body>
</html>
