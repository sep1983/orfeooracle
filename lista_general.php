<?
include_once "class_control/AplIntegrada.php";
$objApl = new AplIntegrada($db);
$lkGenerico = "&usuario=$krd&nsesion=".trim(session_id())."&nro=$verradicado"."$datos_envio";
$ADODB_COUNTRECS = true;

if($nivelRad==0){
	$textNiv = "P&uacute;blico";
}

if($nivelRad==1){
	$textNiv = "Privado";
}

if($nivelRad==2){
	$textNiv = "Dependencia";
}

if($nivelRad==3){
	$textNiv = "Usuario Especifico";
}

$query="select SGD_RADI_PRIORIDAD from radicado where radi_nume_radi=$verrad";
$rsprio=$db->conn->Execute($query);
$nivelPrioridad=$rsprio->fields["SGD_RADI_PRIORIDAD"];

if($nivelPrioridad==1){
  $textPri = "Alta";
} else{
  $textPri = "Normal";
}

?>
<script src="js/popcalendar.js"></script>

<script>
function regresar()
{	//window.history.go(0);
	window.location.reload();
}
</script>

<table class="table">
<tr>
	<td><strong>Fecha de radicado</strong></td>
    <td><?=substr($radi_fech_radi , 0, 19)?></td>
    <td><strong>Asunto</strong></td>
    <td colspan='4'><?=$ra_asun?></td>
</tr>
<tr>
	<td><strong>Destinatario</strong></td>
	<td><?=$nombret_us1 ?></td>
	<td><strong>Direcci&oacute;n Correspondencia</strong></td>
	<td><?=$direccion_us1 ?></td>
	<td><strong>Mun/Dpto</strong></td>
	<td><?=$dpto_nombre_us1."/".$muni_nombre_us1 ?></td>
</tr>
<tr>
	<td><strong>N&ordm; de p&aacute;ginas</strong></td>
  <td><?=$radi_nume_hoja ?></td>
  <td><strong>Descripci&oacute;n anexos</strong></td>
  <td><?=$radi_desc_anex ?></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
	<td><strong>Anexo/Asociado</strong></td>
	<td>
  <?
    if($radi_tipo_deri!=1 and $radi_nume_deri){
      echo "<a class='button is-small'
        href='$ruta_raiz/verradicado.php?verrad=$radi_nume_deri&session_name()=session_id()&krd=$krd'
        target='VERRAD$radi_nume_deri_".date("Ymdhi")."'>$radi_nume_deri</a>";
    }

    if($verradPermisos == "Full") {
      echo " <a type=button class='button is-small' onClick='verVinculoDocto();'>
            <i class='el el-share'></i>
          </a>";
    }
    ?>
    </td>
    <td><strong>Ref/Oficio/Cuenta Interna</strong></td>
    <td colspan='3'><?=$cuentai?><?
		$muniCodiFac = "";
		$dptoCodiFac = "";
    if($sector_grb==6 and $cuentai and $espcodi){
      if($muni_us2 and $codep_us2){
        $muniCodiFac = $muni_us2;
				$dptoCodiFac = $codep_us2;
      }else{
        if($muni_us1 and $codep_us1){
          $muniCodiFac = $muni_us1;
					$dptoCodiFac = $codep_us1;
				}
			}
	?>
		<a href="./consultaSUI/facturacionSUI.php?cuentai=<?=$cuentai?>&muniCodi=<?=$muniCodiFac?>&deptoCodi=<?=$dptoCodiFac?>&espCodi=<?=$espcodi?>" target="FacSUI<?=$cuentai?>"><span class="vinculos">Ver Facturaci&oacute;n</span></a>
	<?
		}
	?>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Imagen</strong>
    </td>
    <td>
      <?=$imagenv ?>
    </td>
    <td>
      <strong>Nivel de Seguridad</strong>
    </td>
    <td>
      <?=$textNiv?>
      <?
      if($verradPermisos == "Full"){
        $varEnvio = "krd=$krd&numRad=$verrad&nivelRad=$nivelRad";
        echo "<a class='button is-small' onClick=\"window.open('$ruta_raiz/seguridad/radicado.php?$varEnvio','Cambio Nivel de Seguridad Radicado', 'height=350, width=700,scrollbars=yes,resizable=yes')\">
                <i class='el el-share'></i>
              </a>";
      }
      ?>
	  </td>
    <td>
      <strong>Prioridad</strong>
    </td>
	  <td>
      <?=$textPri?>
      <?php
      if($verradPermisos == "Full"){
        $varEnvio = "krd=$krd&numRad=$verrad&nivelPrioridad=$nivelPrioridad";
        echo "<a name=mostrar_prioridad class='button is-small'
              onClick=\"window.open('$ruta_raiz/seguridad/prioridad.php?$varEnvio','Cambio Nivel de Prioridad Radicado', 'height=220, width=300,left=350,top=300')\">
                <i class='el el-share'></i>
              </a>";
      }
	    ?>
	  </td>
  </tr>
  <tr>
	<td><strong>TRD<strong></td>
	<td>
	<?
    if( strlen($tpdoc_nombreTRD) > 0) {
      echo  ucwords(strtolower($serie_nombre))
        .' / '. ucwords(strtolower($subserie_nombre))
        . ' / '. ucwords(strtolower($tpdoc_nombreTRD)). '&nbsp;';
    }

		if($verradPermisos == "Full") {
      if(!$codserie) $codserie = "0";
      if(!$tsub) $tsub = "0";
      echo "<a name=mosrtar_tipo_doc2 class='button is-small' onClick=\"ver_tipodocuTRD($codserie,$tsub);\">
             <i class='el el-share'></i>
           </a>";
    }
	?>
	</td>
	<td><strong>Tr&aacute;mites</strong></td>
	<td colspan='3'>
    <a class='button is-small' onClick="window.open('<?=$ruta_raiz?>/seguridad/tramites.php?<?=$varEnvio?>','Tramites del radicado', 'height=350, width=500,left=150,top=300, scrollbars=1')">
      <i class='el el-share'></i>
    </a>
	</td>
</tr>
<!--
<tr>
	<td><strong>Relacion Procedimental</strong></td>
	<td>
    <?
    if(!empty($tpdoc_nombre) and !empty($funcion_nombre)){
      echo "$tpdoc_nombre/$funcion_nombre/$proceso_nombre/$procedimiento_nombre";
    }

	  if($verradPermisos == "Full"){
		  echo "<a class=\"is-small button\" onClick=\"ver_tipodocumento();\">
             <i class='el el-share'></i>
           </a>";
	  }
	  ?>
	</td>
</tr>
-->

<tr>
    <td>Metadato</td>
    <td colspan="6">
        <?
        $sqlMetadato = "select sgd_mmr_dato from sgd_mmr_matrimetaradi where radi_nume_radi = $verradicado ";
        $rsMetadato = $db->conn->Execute($sqlMetadato);
        while(!$rsMetadato->EOF)
        {
            $metadato = $rsMetadato->fields["SGD_MMR_DATO"];
            echo $metadato. " / ";
            $rsMetadato->MoveNext();
        }
        ?>
    </td>
</tr>

<tr>
	<td>Notificaci&oacute;n</td>
	<td colspan="6">
      <?
       if ($sgd_apli_codi==1){
       		$objApl->AplIntegrada_codigo($sgd_apli_codi);
        	$lksancionados = $objApl->get_sgd_apli_lk1();
       		$lkagotamiento = $objApl->get_sgd_apli_lk2();
       		$lksancion = $objApl->get_sgd_apli_lk3();
       		$lkfirmesa = $objApl->get_sgd_apli_lk4();

       }

	  if ($tipoNotDesc){
	  	echo("$tipoNotDesc "); if ($tFechNot) echo ("Notificacion($tFechNot)/"); if ($tFechFija) echo ("Fijacion($tFechFija)/"); if ($tFechDesFija) echo ("Desfij($tFechDesFija)"); echo("/  Notificador($tNotNotifica)/  Notificado($tNotNotificado)/  "); if ($tNotEdicto) echo ("Edicto ($tNotEdicto)"); }
	  if(($verradPermisos == "Full") &&  $usua_perm_notifica==1 )
	  {
	  ?>
      <input type=button name=mostrarNotificacion value='...' class='button is-small' onClick="verNotificacion();">
      <?
       //Mira si la decision actual permite ver agotamiento
       if (!$sgd_tdes_codigo)
       		$sgd_tdes_codigo = 'null';
       $sql =  "select * from SGD_TDEC_TIPODECISION where SGD_APLI_CODI=1 and SGD_TDEC_VERAGOTA=1
       			and SGD_TDEC_CODIGO = $sgd_tdes_codigo ";
       $rs=$db->query($sql);

       if (strlen (trim($tipoNotDesc)) > 0) {
       	$swDecAgotam = false;

       	if ($rs && !$rs->EOF)
       		$swDecAgotam = true;

       		//Mira si ya se ha tipificado agotamiento
       	$swYaAgotam = false;

       	$sql = "select a.hist_obse
					from hist_eventos a
				 	where
					a.radi_nume_radi =$verradicado and
					a.hist_obse  like  '%SE HA AGOTADO LA VIA GUBERNATIVA%' and
					a.sgd_ttr_codigo = 35
					order by hist_fech desc ";
	 	$rs=$db->query($sql);

	 	if ( $rs && !$rs->EOF )
	 			$swYaAgotam = true;



       	if ( $swDecAgotam==true &&  $swYaAgotam==false  ) {
       	$datos_enviobk = $etiqueta_body = str_replace("&", "|", $datos_envio);

       	$lkagotamiento = $lkagotamiento.$lkGenerico;

       	$lkagotamiento = str_replace("/", "|", $lkagotamiento);
       	$lkgen=str_replace("&", "|", $lkGenerico);
       	?>
      	<a class='button is-small' href='javascript:abrirAgotamiento()'>Agotamiento Via
      	Gubernativa</a>
      	<script>
         function abrirAgotamiento (){
       		window.open('abre_en_frame.php?lkparam=<?=$lkagotamiento?>&datoenvio=<?=$lkgen?>',"Agotamiento",'top=0,height=580,width=850,scrollbars=yes');
       	  }

       	</script>
      <? }
	  }
       if ($tipoNotDesc && $sgd_tdec_firmeza && $tipoNotific){
	  	   	$lkfirmesa = $lkfirmesa .$lkGenerico;
    	?>
      <?
	// Si se ha notificado y se ha tipificado decisi�n como   Agotamiento de via gubernativa


	 			if ( $swYaAgotam == true ) {
					$sql =  "select * from SGD_TDEC_TIPODECISION where SGD_APLI_CODI=1  and SGD_TDEC_FIRMEZA=1
					 	and SGD_TDEC_CODIGO = $sgd_tdes_codigo ";
	  				$rs=$db->query($sql);
	 				$lkfirmesa = str_replace("/", "|", $lkfirmesa);
	 					$lkgen=str_replace("&", "|", $lkGenerico);
	  				if  ($rs && !$rs->EOF && $usua_perm_sancionad>=3 ){
	 					print ("  <script> ");
	 					print ("  function abrirFirmeza(){ ");
	 					//print (" 	window.open('$lkfirmesa"."$lkGenerico','Firmeza','top=0,height=580,width=850,scrollbars=yes'); ");
	 					print ("    window.open('abre_en_frame.php?lkparam=$lkfirmesa&datoenvio=$lkgen','Agotamiento','top=0,height=580,width=850,scrollbars=yes'); ");
	 					print ("  }  </script> ");
	 					print (" <a class='button is-small' href='javascript:abrirFirmeza()'>Registrar Firmeza</a> ");
		  			}
	 			}

		}
	  }
	  ?>
    </td>
  </tr>
  <?
  //Si el radicado est� relacionado con el aplicativo de sancionados
  if ($sgd_apli_codi==1){ ?>
  <tr>
    <td>Sancionados</td>
    <td colspan="6">
      <?
	  if ($sgd_tdes_descrip)
	  	echo("$sgd_tdes_descrip");
	  if(($verradPermisos == "Full") && $usua_perm_sancionad>=1){
	  ?>
      <input type=button name=mostrarSubtipo value='...' class='button is-small' onClick="verDecision();">
      <?
	  }

	  if ( $sgd_tdec_versancion ) {
	  ?>
      <a href='javascript:abrirSancion()' class='is-small button'>
      <script>
       function abrirSancion(){

      //alert ("Se selecciona " +  document.form_decision.decis.value);

	swVerSancion=1;

	if (swVerSancion==1)

		window.open('<?=$lksancion.$lkGenerico?>',"Sancion",'top=0,height=580,width=850,scrollbars=yes');

       }
       </script>
      </a>
      <a class='button is-small' href="javascript:abrirSancion();">Ver Sancion</a>
      <?
	  }
	  ?>
    </td>
  </tr>

  <tr>
   <? }?>
    <td>Resoluci&oacute;n</td>
    <td colspan="6">
      <?
    if ($sgd_tres_codigo){
      include "include/class/resoluciones.class.php";
      $objRes = new Resoluciones($db->conn);
      $sgd_tres_descrip = $objRes->Get_Descripcion($sgd_tres_codigo);
      echo("$sgd_tres_descrip");
    }

	  if($verradPermisos == "Full"){
	  ?>
      <input type=button name=mostrarSubtipo value='...' class='is-small button' onClick="verResolucion();">
      <?
	  }
	  ?>
    </td>
  </tr>
  <tr>
    <td>Tipo PQR</td>
    <td colspan="6">
      <?=$sector_nombre?>
      <?
		$nombreSession = session_name();
		$idSession = session_id();
		if ($verradPermisos == "Full") {
	  		$sector_grb = (isset($sector_grb)) ? $sector_grb : 0;
	  		$causal_grb = (isset($causal_grb) ||$causal_grb !='') ? $causal_grb : 0;
	  		$deta_causal_grb = (isset($deta_causal_grb) || $deta_causal_grb!='') ? $deta_causal_grb : 0;

			$datosEnviar = "'$ruta_raiz/causales/mod_causal.php?" .
											$nombreSession . "=" . $idSession .
											"&krd=" . $krd .
											"&verrad=" . $verrad .
											"&sector=" . $sector_grb .
											"&sectorCodigoAnt=" . $sector_grb .
											"&sectorNombreAnt=" . $sector_nombre .
											"&causal_grb=" . $causal_grb .
											"&causal_nombre=" . $causal_nombre .
											"&deta_causal_grb=" . $deta_causal_grb .
											"&dcausal_nombre=". $dcausal_nombre . "'";
	  ?>
      <input type=button name="mostrar_causal" value="..." class="button is-small" onClick="window.open(<?=$datosEnviar?>,'Tipificacion_Documento','height=150,width=400,scrollbars=no')">
      <input type="hidden" name="mostrarCausal" value="N">
      <?
	   }
	   ?>
    </td>
  </tr>
  <tr>
    <td>TEMA</td>
    <td colspan="6">
      <?=$tema_nombre ?>
      <?
	  if ($verradPermisos == "Full") {
	  ?>
      <input type=button name="mostrar_temas" value='...' class='is-small button' onClick="ver_temas();">
      <?
	  }
	  ?>
    </td>
  </tr>
</table>

</form>
<table>
  <tr>
    <td>
<?
 $ruta_raiz = ".";
 if(($verradPermisos=="Full")&&$SecSuperAux->UsrPerm==0) {
 	include ("tipo_documento.php");
 }
?>
    </td>
  </tr>
</table>
