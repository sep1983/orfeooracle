/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'editing',     groups: [ 'undo' ,'find', 'selection', 'spellchecker' ] },
    { name: 'links',       groups: [ 'Links', 'Unlink']},
		{ name: 'others' },
		{ name: 'basicstyles', groups: [ 'Justify','basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode','doctools' ] },
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	config.forcePasteAsPlainText = true;

	config.extraPlugins = 'justify';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

  config.removeFormatTags = 'big,code,del,dfn,em,font,ins,kbd';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

  config.basicEntities       = false;
  config.entities_greek      = false;
  config.entities_latin      = false;
  config.entities_additional = '';
};
