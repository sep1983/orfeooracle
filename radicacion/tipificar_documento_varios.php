<?
  $krdOld=$krd;
 	session_start();
 	if(!$krd) $krd = $krdOld;
 	$ruta_raiz = "..";
 	if(!$dependencia){
 		include "$ruta_raiz/rec_session.php";
 	}

	if (!$nurad) $nurad= $rad;
	if($nurad){
		$ent = substr($nurad,-1);
	}
	include_once("$ruta_raiz/include/db/ConnectionHandler.php");

	$db = new ConnectionHandler("$ruta_raiz");

	if (!defined('ADODB_FETCH_ASSOC')) define('ADODB_FETCH_ASSOC',2);
   	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

	include_once "$ruta_raiz/include/tx/Historico.php";
  include_once ("$ruta_raiz/class_control/TipoDocumental.php");
  include_once "$ruta_raiz/include/tx/Expediente.php";
  $coddepe = $dependencia;
	$codusua = $codusuario;
	$nomcarpeta = "Entrada";
  $trd = new TipoDocumental($db);
	$encabezadol = "$PHP_SELF?".session_name()."=".session_id()."&krd=$krd&nurad=$nurad&coddepe=$coddepe&codusuario=$codusua&codusua=$codusua&codusuario=$codusuario&depende=$depende&ent=$ent&tdoc=$tdoc&codiTRDModi=$codiTRDModi&codiTRDEli=$codiTRDEli&codserie=$codserie&tsub=$tsub&ind_ProcAnex=$ind_ProcAnex&texp=$texp&contRad=$contRad&nomcarpeta=$nomcarpeta";

	$trdExp = new Expediente($db);
	$trdExpediente= $trdExp->descSerie." / ".$trdExp->descSubSerie;
	$descPExpediente = $trdExp->descTipoExp;
	$descFldExp = $trdExp->descFldExp;
	$codigoFldExp = $trdExp->codigoFldExp;
	$expUsuaDoc = $trdExp->expUsuaDoc;

  if(isset($_POST['checkValue'])){
    $num = count($_POST['checkValue']);
    reset($_POST['checkValue']);
    $i = 0;
    $jglCounter = 0;
    $resultadoJGL = "";
    $contRad = 0;
    $lista = "";
    while (list($recordid,$tmp) = each($_POST['checkValue'])){
      $record_id = $recordid;
      $nurad = $recordid;
      $sqlSubstDescS =  $db->conn->substr."(s.sgd_srd_descrip, 0, 30)";
      $sqlSubstDescSu = $db->conn->substr."(su.sgd_sbrd_descrip, 0, 30)";
      $sqlSubstDescT =  $db->conn->substr."(t.sgd_tpr_descrip, 0, 30)";
      $sqlSubstDescD =  $db->conn->substr."(d.depe_nomb, 0, 30)";
      $sqlConcat = $db->conn->Concat("m.depe_codi","m.sgd_srd_codigo","m.sgd_sbrd_codigo","m.sgd_tpr_codigo");

      $isqlC = 'select
        '. $sqlConcat .  '      AS "CODIGO"
        , '. $sqlSubstDescS .  '  AS "SERIE"
        , '. $sqlSubstDescSu .  ' AS "SUBSERIE"
        , '. $sqlSubstDescT .  '  AS "TIPO_DOCUMENTO"
        , '. $sqlSubstDescD .  '  AS "DEPENDENCIA"
        , m.sgd_mrd_codigo        AS "CODIGO_TRD"
        , mf.usua_codi             AS "USUARIO"
        , mf.depe_codi             AS "DEPE"
        from
        SGD_RDF_RETDOCF mf,
        SGD_MRD_MATRIRD m,
        DEPENDENCIA d,
        SGD_SRD_SERIESRD s,
        SGD_SBRD_SUBSERIERD su,
        SGD_TPR_TPDCUMENTO t
        where d.depe_codi     = mf.depe_codi
        and s.sgd_srd_codigo  = m.sgd_srd_codigo
        and su.sgd_sbrd_codigo = m.sgd_sbrd_codigo
        and su.sgd_srd_codigo = m.sgd_srd_codigo
        and t.sgd_tpr_codigo  = m.sgd_tpr_codigo
        and mf.sgd_mrd_codigo = m.sgd_mrd_codigo
        and mf.radi_nume_radi = '. $nurad;

      $dtipodo = '';
      $rsC=$db->query($isqlC);
      if(!$rsC->EOF){
        $coddocu  =$rsC->fields["CODIGO"];
        $dserie   =$rsC->fields["SERIE"];
        $dsubser  =$rsC->fields["SUBSERIE"];
        $dtipodo  =$rsC->fields["TIPO_DOCUMENTO"];
        $ddepend  =$rsC->fields["DEPENDENCIA"];
        $codiTRDEli  =$rsC->fields["CODIGO_TRD"];
        $codiTRDModi =$codiTRDEli;
      }
      if ($dtipodo == '') {
        $contRad++;
        if ($lista == "") $lista .= $nurad;
        else $lista.= "," . $nurad;
      }
    }
  }
?>
<html>
<head>
<title>Tipificar Documento</title>
<link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
<script>

function regresar(){
	document.TipoDocu.submit();
}
</script>
</head>
<body bgcolor="#FFFFFF">
<form method="post" action="<?=$encabezadol?>" name="TipoDocu">
 <input type=hidden name=contRad value='<?=$contRad?>'>
   <input type=hidden name=lista value='<?=$lista?>'>

  <?
  /*
  * Adicion nuevo Registro
  */

if ($insertar_registro && $tdoc !=0 && $tsub !=0 && $codserie !=0 )
{
		if ($contRad > 0) {
			$listapas = explode(",",$lista);
			for($Z=0;$Z<$contRad;$Z++)  	{
				$nurad = $listapas[$Z];

				$isqlTRD = "select SGD_MRD_CODIGO
						from SGD_MRD_MATRIRD
						where DEPE_CODI = '$dependencia'
						   and SGD_SRD_CODIGO = '$codserie'
						   and SGD_SBRD_CODIGO = '$tsub'
						   and SGD_TPR_CODIGO = '$tdoc'";
				$rsTRD = $db->conn->Execute($isqlTRD);
        $codiTRDU = $rsTRD->fields['SGD_MRD_CODIGO'];

				$i=0;
				while(!$rsTRD->EOF){
					$codiTRDS[$i] = $rsTRD->fields['SGD_MRD_CODIGO'];
					$codiTRD = $rsTRD->fields['SGD_MRD_CODIGO'];
					$i++;
					$rsTRD->MoveNext();
				}

				$radicados = $trd->insertarTRD($codiTRDS,$codiTRD,$nurad,$coddepe, $codusua);
				$TRD = $codiTRD;
				include "$ruta_raiz/radicacion/detalle_clasificacionTRD.php";
        $observa = "*Modificado TRD " . $deta_serie . "/" . $deta_subserie . "/" . $deta_tipodocu;
				$sqlH = "SELECT $radi_nume_radi RADI_NUME_RADI
					FROM SGD_RDF_RETDOCF r
					WHERE r.RADI_NUME_RADI = '$nurad'
				    AND r.SGD_MRD_CODIGO =  '$codiTRD'";
				$rsH=$db->conn->query($sqlH);
				$i=0;
				while(!$rsH->EOF){
					$codiRegH[$i] = $rsH->fields['RADI_NUME_RADI'];
					$i++;
					$rsH->MoveNext();
				}
				$Historico = new Historico($db);
				$radiModi = $Historico->insertarHistorico($codiRegH, $dependencia, $codusuario, $dependencia, $codusuario, $observa, 32);
				/*
				*Actualiza el campo tdoc_codi de la tabla Radicados
				*/
				$radiUp = $trd->actualizarTRD($codiRegH,$tdoc);


        $busqAnexos = "	SELECT
          distinct(RADI_NUME_SALIDA) AS RADI_NUME_SALIDA
          FROM
          ANEXOS
          WHERE
          ANEX_RADI_NUME = $nurad
          AND RADI_NUME_SALIDA != 0
          AND ANEX_BORRADO = 'N'";


        $busAnex = $db->conn->Execute($busqAnexos);

        while (!$busAnex->EOF) {
          $anexRadTRD[] = $busAnex->fields['RADI_NUME_SALIDA'];
          $busAnex->MoveNext();
        }

        #######################################################
        # Si tenemos radicados anexos les agregamos la TRD
        #######################################################
        if(!empty($anexRadTRD)){

          foreach ($anexRadTRD as $rad){
            $trd->insertarTRD($codiTRDS, $codiTRD, $rad, $coddepe, $codusua);

            $sqlUA = "UPDATE SGD_RDF_RETDOCF SET SGD_MRD_CODIGO = $codiTRDU, USUA_CODI = $codusua
              WHERE RADI_NUME_RADI in ($rad) AND DEPE_CODI = $coddepe";
            $rsUp = $db->conn->query($sqlUA);
          }
          $trd->actualizarTRD($anexRadTRD, $tdoc);
        }

				$codserie = 0 ;
				$tsub = 0  ;
			}
			if ($Z > 0) {
			$nombTx = "Aplicación de TRD en masa";
?>
<br>
<table border=0 cellspace=2 cellpad=2 WIDTH=50%  class="t_bordeGris" id=tb_general align="left">
	<tr>
	<td colspan="2" class="titulos4">ACCION REQUERIDA <?=$accionCompletada?>  <?=$okTxDesc?> COMPLETADA <?=$causaAccion ?> </td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">ACCION REQUERIDA :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$nombTx?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">RADICADOS INVOLUCRADOS :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa"><?=$lista?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">FECHA Y HORA :
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=date("m-d-Y  H:i:s")?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">USUARIO ORIGEN:
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$usua_nomb?>
	</td>
	</tr>
	<tr>
	<td align="right" bgcolor="#CCCCCC" height="25" class="titulos2">DEPENDENCIA ORIGEN:
	</td>
	<td  width="65%" height="25" class="listado2_no_identa">
	<?=$depe_nomb?>
	</td>
	</tr>
</table>
<?
			}
		}
	}
if (!$Z) {
	?>
	<table border=0 width=70% align="center" class="borde_tab" cellspacing="0">
	  <tr align="center" class="titulos2">
	    <td height="15" class="titulos2">APLICACION DE LA TRD</td>
      </tr>
  </table>
<? if($lista)  { ?>
 	<table width="70%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
      <tr >
	  <td class="titulos5" >SERIE</td>
	  <td class=listado5 >
        <?php

    if(!$tdoc) $tdoc = 0;
    if(!$codserie) $codserie = 0;
	if(!$tsub) $tsub = 0;
	$fechah=date("dmy") . " ". time("h_m_s");
	$fecha_hoy = Date("Y-m-d");
	$sqlFechaHoy=$db->conn->DBDate($fecha_hoy);
	$check=1;
	$fechaf=date("dmy") . "_" . time("hms");
	$num_car = 4;
	$nomb_varc = "s.sgd_srd_codigo";
	$nomb_varde = "s.sgd_srd_descrip";
   	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	$querySerie = "select distinct ($sqlConcat) as detalle, s.sgd_srd_codigo
	         from sgd_mrd_matrird m, sgd_srd_seriesrd s
			 where m.depe_codi = '$dependencia'
			 	   and s.sgd_srd_codigo = m.sgd_srd_codigo
			       and ".$sqlFechaHoy." between s.sgd_srd_fechini and s.sgd_srd_fechfin
			 order by detalle
			  ";
	$rsD=$db->conn->query($querySerie);
	$comentarioDev = "Muestra las Series Documentales";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsD->GetMenu2("codserie", $codserie, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
 ?>
      </td>
     </tr>
   <tr>
     <td class="titulos5" >SUBSERIE</td>
	 <td class=listado5 >
	<?
	$nomb_varc = "su.sgd_sbrd_codigo";
	$nomb_varde = "su.sgd_sbrd_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
   	$querySub = "select distinct ($sqlConcat) as detalle, su.sgd_sbrd_codigo
	         from sgd_mrd_matrird m, sgd_sbrd_subserierd su
			 where m.depe_codi = '$dependencia'
			       and m.sgd_srd_codigo = '$codserie'
				   and su.sgd_srd_codigo = '$codserie'
			       and su.sgd_sbrd_codigo = m.sgd_sbrd_codigo
 			       and ".$sqlFechaHoy." between su.sgd_sbrd_fechini and su.sgd_sbrd_fechfin
			 order by detalle
			  ";
	$rsSub=$db->conn->query($querySub);
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsSub->GetMenu2("tsub", $tsub, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );

?>
     </td>
     </tr>
   <tr>
     <td class="titulos5" >TIPO DE DOCUMENTO</td>
 	 <td class=listado5 >
        <?
	$nomb_varc = "t.sgd_tpr_codigo";
	$nomb_varde = "t.sgd_tpr_descrip";
	include "$ruta_raiz/include/query/trd/queryCodiDetalle.php";
	$queryTip = "select distinct ($sqlConcat) as detalle, t.sgd_tpr_codigo
	         from sgd_mrd_matrird m, sgd_tpr_tpdcumento t
			 where m.depe_codi = '$dependencia'
			       and m.sgd_mrd_esta = '1'
 			       and m.sgd_srd_codigo = '$codserie'
			       and m.sgd_sbrd_codigo = '$tsub'
 			       and t.sgd_tpr_codigo = m.sgd_tpr_codigo
	  			   and t.sgd_tpr_tp2='1'
			 order by detalle
			 ";

	$rsTip=$db->conn->query($queryTip);
	$ruta_raiz = "..";
	include "$ruta_raiz/include/tx/ComentarioTx.php";
	print $rsTip->GetMenu2("tdoc", $tdoc, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
	?>
    </td>
    </tr>
   </table>
<br>
	<table border=0 width=70% align="center" class="borde_tab">
	  <tr align="center">
		<td width="33%" height="25" class="listado2" align="center">
         <center><input name="insertar_registro" type=submit class="botones_funcion" value=" Insertar "></center></TD>
        <td width="33%" class="listado2" height="25">
         <center>
		  <a href='../cuerpo.php?<?="$encabezado1"?>&nomcarpeta=<?="$nomcarpeta"?>'>
		  <input class="botones" type="reset" name="Cerrar" value="Cerrar"></a></center>
      </tr>
	</table>
<? }
?>
<br>
	<center><TABLE class="borde_tab"><tr>
	  <td class="alarmas">
	Solo se aplica clasificacion TRD en masa, a los radicados que NO tengan TRD</td>
	</tr></table></center>

<? if($lista)  { ?>
	<br>
	<center>
	<TABLE class="borde_tab"><tr><td class="titulos2">
	SE APLICARA LA CLASIFICACION A LOS SIGUIENTES RADICADOS </td></tr></table>
	<TABLE class="borde_tab"><tr><td class="listado2"><?=$lista ?></td></tr></table>
	</center>
<? }else {
?>
	<center><TABLE class="borde_tab"><tr><td class="alarmas">
	Los radicados seleccionados NO cumplen requisitos para aplicar TRD en masa </td></tr></table></center>
<?
}
}
?>
	<script>
function borrarArchivo(anexo,linkarch){
	if (confirm('Esta seguro de borrar este Registro ?'))
	{
		nombreventana="ventanaBorrarR1";
		url="tipificar_documentos_transacciones.php?borrar=1&usua=<?=$krd?>&codusua=<?=$codusua?>&coddepe=<?=$coddepe?>&codusuario=<?=$codusuario?>&dependencia=<?=$dependencia?>&nurad=<?=$nurad?>&codiTRDEli="+anexo+"&linkarchivo="+linkarch;
		window.open(url,nombreventana,'height=250,width=300');
	}
return;
}
function procModificar()
{
if (document.TipoDocu.tdoc.value != 0 &&  document.TipoDocu.codserie.value != 0 &&  document.TipoDocu.tsub.value != 0)
  {
  <?php
      $sql = "SELECT RADI_NUME_RADI
					FROM SGD_RDF_RETDOCF
					WHERE RADI_NUME_RADI = '$nurad'
				    AND  DEPE_CODI =  '$coddepe'";
		$rs=$db->conn->query($sql);
		$radiNumero = $rs->fields["RADI_NUME_RADI"];
		if ($radiNumero !='') {
			?>
			if (confirm('Esta Seguro de Modificar el Registro de su Dependencia ?'))
				{
					nombreventana="ventanaModiR1";
					url="tipificar_documentos_transacciones.php?modificar=1&usua=<?=$krd?>&codusua=<?=$codusua?>&tdoc=<?=$tdoc?>&tsub=<?=$tsub?>&codserie=<?=$codserie?>&coddepe=<?=$coddepe?>&codusuario=<?=$codusuario?>&dependencia=<?=$dependencia?>&nurad=<?=$nurad?>";
					window.open(url,nombreventana,'height=200,width=300');
				}
			<?php
	 		}else
			{
			?>
			alert("No existe Registro para Modificar ");
			<?php
			}
       ?>
     }
   else
   {
    alert("Campos obligatorios ");
   }
return;
}

</script>
</form>
</span>
<p>
<?=$mensaje_err?>
</p>
</span>
</body>
</html>
