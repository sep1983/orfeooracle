<?php
session_start();
$ruta_raiz = "..";
if(!$_SESSION['dependencia']) include "$ruta_raiz/rec_session.php";

foreach ($_GET as $key => $valor)   ${$key} = $valor;
foreach ($_POST as $key => $valor)   ${$key} = $valor;

?>
<html>
<head>
<link rel="stylesheet" href="../estilos/orfeo.css">
<title>REGISTRO DE TRAMITES</title>
</head>

<body bgcolor="#FFFFFF" topmargin="0" >
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="js/spiffyCal/spiffyCal_v2_1.css">
<script>
    function validac(){
        if (document.forma.elements['canTram'].value == '' || document.forma.elements['canTram'].value == 0)
        {	alert('La cantidad de tr\xe1mites es obligatoria');
        return false;
        }
    }
</script>
<?php
$ruta_raiz = "..";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");	

$encabezado = "".session_name()."=".session_id()."&krd=$krd&dependencia=$dependencia&nurad=$nurad&codTramite=$codTramite&codNov=$codNov&canTram=$canTram&coddepe=$coddepe";
$linkPagina = "$PHP_SELF?$encabezado"; //echo "<br> $linkPagina";
error_reporting(7);      
?> 
  <form name='forma' action='uploadTramite.php?<?=$encabezado?>' method='post'>
<?PHP
$query0 = "SELECT RAD_CANT_TRAM, radi_tipo_deri FROM RADICADO WHERE radi_nume_radi = $nurad";
$rs0=$db->conn->query($query0);
if(!$rs0->EOF) { 
    $canTramM = $rs0->fields["RAD_CANT_TRAM"];
    if($canTramM > 0) $canTram = $canTramM;
    $tipoanexo = $rs0->fields["RADI_TIPO_DERI"];
}
if($tipoanexo=="0")  //radicado Anexo de
{
    $radicadoCons = $radicadopadre;
    $varTramRad = "Complemento/Anexo del Radicado No. $radicadopadre";
}else {
    $radicadoCons = $nurad;
}

?>
 <table class=borde_tab width='100%' cellspacing="5"><tr><td class=titulos2><center>REGISTRO DE TRAMITES <?=$varTramRad?></center></td></tr></table>
 <table><tr><td></td></tr></table>
 <center>
<TABLE width="211" class="borde_tab" cellspacing="5">       
  <TR class=e_tablas>
    <td width="61%" class="titulos5" height="25" align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Cantidad de Tr&aacute;mites</font></td>
    <td width="39%" bgcolor="#FFFFFF" height="25" class="listado5">
        <INPUT type=text name='canTram' value='<?=$canTram?>' class="tex_area" size=4 maxlength="4">
    </td> 
  </tr>
</table>
 <table><tr><td></td></tr></table>
<TABLE width="550" class="borde_tab" cellspacing="5">   
<tr>
    <td width="13%" class="titulos5" height="19" align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">Placa</font></td>
    <td width="17%" bgcolor="#FFFFFF" height="19" class="listado5">
        <INPUT type=text name='placa' value='<?=$placa ?>' class="tex_area" size=14 maxlength="10">
    </td> 
    <td width="15%" class="titulos5" height="19" align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">No. Factura SIREV</font></td>
    <td width="26%" bgcolor="#FFFFFF" height="19" class="listado5">
        <INPUT type=text name='fSIREV' value='<?=$fSIREV ?>' class="tex_area" size=24 maxlength="16">
    </td> 

</tr>    
  <TR> 	<td width="13%" class="titulos5" height="20" align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">TRAMITE</font></td>
      <td colspan="3"  class="listado5"> 
<?php
if(!$codTramite) $codTramite = 0;
$sqlConcat = $db->conn->Concat("sgd_mtra_codigo","'-'","sgd_mtra_descripcion");

$query1 = "select sgd_mtra_descripcion, sgd_mtra_codigo 
    from sgd_mtra_tramite, SGD_TRADEP_TRAMITEDEP td
    where td.SGD_TRADEP_DEPECODI = $coddepe
    AND td.SGD_TRADEP_TRACODI = sgd_mtra_codigo
    order by sgd_mtra_descripcion";

$rs1=$db->conn->query($query1);
print $rs1->GetMenu2("codTramite", $codTramite, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
?>  </td> </tr>
   <TR> 	<td width="13%" class="titulos5" height="20" align="right"><font face="Arial, Helvetica, sans-serif" class="etextomenu">NOVEDAD</font></td>
        <td colspan="3"  class="listado5"> 
<?    
$query2 = "select distinct(NOV.sgd_mnovt_descripcion) as descripcion, NOV.sgd_mnovt_codigo
    from sgd_mnovt_novedadtram NOV, SGD_RELT_RELACIONTRAM REL
    where REL.SGD_RELT_CODTRAMITE = '$codTramite'
    AND REL.SGD_RELT_CODNOVEDAD = nov.SGD_mNOVT_CODIGO
    order by descripcion";
$rs2=$db->conn->query($query2);
print $rs2->GetMenu2("codNov", $codNov, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
?>   </td> </tr>
  </table>
<?
if($codTramite and $codNov) {
    if ($tipoanexo <> "0") { 	 /*ASIGNACION A UN FUNCIONARIO*/
        $sqlCons = "SELECT USUA_CODI, DEPE_CODI FROM USUARIO U WHERE u.DEPE_CODI = $coddepe AND U.USUA_ESTA = 1 AND U.USUA_DOC IN (SELECT FUNOV_USUA_DOC FROM SGD_FUNOV_FUNCNOVEDAD
            WHERE FUNOV_CODTRAMITE = $codTramite AND FUNOV_CODNOVEDAD = $codNov AND FUNOV_DEPE_CODI = $coddepe AND FUNOV_ESTADO=1)";
        $rsCons = $db->query($sqlCons); //echo "<br> $sqlCons ";
        if($rsCons->EOF) { 
            echo "<table class=borde_tab width='100%'><tr><td class=titulosError><center>Se asignar&aacute al Jefe, porque no hay funcionarios definidos para tramitar esta novedad en la dependencia</center></td></tr></table>";
        }						  
    }
?>
      <table border=1 width=100% class=t_bordeGris>
      <tr><td colspan="2" class="titulos4" align="center"><p><B><span class=etexto>Requisitos</span></B></p></td></tr>
      <tr><td colspan="1" class="titulos4" align="center"><p><B><span class=etexto>Requisitos exigidos para este tramite</span></B></p></td>
          <td colspan="1" class="titulos4" align="center"><p><B><span class=etexto>Observacion</span></B></p></td>
      </tr>
<?php
    $sql = "SELECT req.SGD_mREQT_CODIGO as codrequisito, substr(req.SGD_mREQT_DESCRIPCION,1,70) as descripcion,
        rel.SGD_RELT_CODIGO as codrelacion
        FROM SGD_RELT_RELACIONTRAM rel, SGD_mREQT_REQUISITOTRAM req
        WHERE rel.SGD_RELT_CODREQUISITO = req.SGD_mREQT_CODIGO
        AND rel.SGD_RELT_CODTRAMITE = '$codTramite'
        AND rel.SGD_RELT_CODNOVEDAD = '$codNov'
        AND rel.sgd_relt_estado = 1
        ORDER BY codrequisito";
    $ADODB_COUNTRECS = true;
    $rs_req = $db->query($sql);
    if ($rs_req->RecordCount() >= 0)
    {	
        $cad = "req";
        $cad2= "obs";
        $swInserto = "NO";
        while ($arr = $rs_req->FetchRow())
        {	print "<TR align='left'>";
        $varReq = ${$cad.$arr['CODREQUISITO']};
        $varObs = ${$cad2.$arr['CODREQUISITO']};
        $varRel = $arr['CODRELACION'];
        if(!$guardar) {
            //PRIMERO consulta si existe el registro
            $sqlCons = "SELECT *
                FROM SGD_RADT_RADICATRAMITE
                WHERE SGD_RADT_RADICADO = $nurad 
                AND SGD_RADT_RELTRAMITE = $varRel ";
            $rsCons = $db->query($sqlCons);
            if(!$rsCons->EOF) { 
                $varReq = "ok";
                ${varObs} = $rsCons->fields["SGD_RADT_OBSERVACION"];
            }
        }
        if($varReq) $chk = "checked"; else $chk = "";
        if($guardar) {
            //PRIMERO consulta si existe el registro
            if(!$fSIREV) $fSIREV = 0;
            $sqlCons = "SELECT *
                FROM SGD_RADT_RADICATRAMITE
                WHERE SGD_RADT_RADICADO = $nurad 
                AND SGD_RADT_RELTRAMITE = $varRel ";
            $rsCons = $db->query($sqlCons);
            if($rsCons->EOF) { 
                if($varReq) {
                    $sqlIns = "INSERT INTO SGD_RADT_RADICATRAMITE (SGD_RADT_RADICADO,SGD_RADT_RELTRAMITE,SGD_RADT_OBSERVACION,SGD_RADT_PLACA, SGD_RADT_FACT_SIREV) 
                        VALUES($nurad,$varRel,'${varObs}','$placa', $fSIREV)"; //echo " $sqlIns";
                    $rsIns = $db->query($sqlIns);
                    $swInserto = "SI";
                }
            }else { 
                if($varReq) {
                    $sqlIns = "UPDATE SGD_RADT_RADICATRAMITE SET SGD_RADT_OBSERVACION = '${varObs}',SGD_RADT_PLACA = '$placa', SGD_RADT_FACT_SIREV= $fSIREV
                        WHERE SGD_RADT_RADICADO = $nurad 
                        AND SGD_RADT_RELTRAMITE = $varRel "; 
                    $rsIns = $db->query($sqlIns);
                }else {
                    $sqlIns = "DELETE FROM SGD_RADT_RADICATRAMITE 
                        WHERE SGD_RADT_RADICADO = $nurad 
                        AND SGD_RADT_RELTRAMITE = $varRel ";
                    $rsIns = $db->query($sqlIns);
                }
            }

        }
?>      <td height='26' width='40%' class='listado2'>
        <input type="checkbox" name="<?=$cad.$arr['CODREQUISITO']?>"  value="<?=$cad.$arr['CODREQUISITO']?>" <? if ($varReq) echo "checked"; else echo "";?>>
        <?=$arr['DESCRIPCION']?>
        </td>
        <td width="75%" class="listado5" >
        <INPUT type='text' name="<?=$cad2.$arr['CODREQUISITO']?>" value="<?=$varObs?>" class='tex_area' size='80' maxlength='80'></td>
<?  
        print "</TR>" ;
        }  //fin del WHILE 
        if($guardar) {
            $sqlIns = "UPDATE RADICADO SET RAD_CANT_TRAM = $canTram
                WHERE RADI_NUME_RADI = $nurad";  
            $rsIns = $db->query($sqlIns);
            if ($swInserto == "SI" AND $tipoanexo <> "0") { 	 /*ASIGNACION A UN FUNCIONARIO*/
                $sqlCons = "SELECT USUA_CODI, DEPE_CODI FROM USUARIO U WHERE u.DEPE_CODI = $coddepe AND U.USUA_ESTA = 1 AND U.USUA_DOC IN (SELECT FUNOV_USUA_DOC FROM SGD_FUNOV_FUNCNOVEDAD
                    WHERE FUNOV_CODTRAMITE = $codTramite AND FUNOV_CODNOVEDAD = $codNov AND FUNOV_DEPE_CODI = $coddepe AND FUNOV_ESTADO = 1)";
                $rsCons = $db->query($sqlCons); //echo "<br> $sqlCons ";
                if(!$rsCons->EOF) { 
                    $varUsuaCodi = $rsCons->fields["USUA_CODI"];
                    $sqlIns = "UPDATE RADICADO SET radi_usua_actu = $varUsuaCodi
                        WHERE RADI_NUME_RADI = $nurad ";  // echo "<br> $sqlIns $varUsuaCodi";
                    $rsIns = $db->query($sqlIns);
                    echo "<table class=borde_tab width='100%'><tr><td class=listado5><center> se asigo al funcionario definido</center></td></tr></table>";
                }else   echo "<table class=borde_tab width='100%'><tr><td class=titulosError><center>NO se puede asignar porque no hay funcionarios definidos</center></td></tr></table>";

            }
        }
    }else  echo "<tr><td align='center'> NO TIENE REQUISITOS</td></tr>"; 
    $ADODB_COUNTRECS = false;  
?>
  <tr><td height="26" colspan="4" valign="top" class='titulos2'> 
      <center> 
      <input type="submit" name=guardar value='Guardar' class=botones_funcion onClick="return validac();">
      <input name="aceptar" type="button"  class="botones_funcion"   onClick="window.close();" value="Cerrar"> 
   </td>    
  </tr>
<?  }  	?>
  </table>

</form>
</body>
</html>
