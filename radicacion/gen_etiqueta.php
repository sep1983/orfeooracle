<?php
session_start();

$ruta_raiz="..";
include "$ruta_raiz/config.php";
include_once "../include/db/ConnectionHandler.php";
define('ADODB_ASSOC_CASE', 1);
$db = new ConnectionHandler("$ruta_raiz");

if ($db){
  if (isset($_GET['r']) && substr(base64_decode($_GET['r']),-1)==='%'){
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $sqlfn = $db->conn->SQLDate('Y-m-d H:i:s','r.RADI_FECH_RADI');
    $sql = "SELECT r.RADI_NUME_RADI, $sqlfn as RADI_FECH_RADI, r.RADI_DESC_ANEX, d.DEP_SIGLA, d.DEPE_NOMB,dir.SGD_DIR_NOMREMDES,d.DEPE_CODI
      FROM RADICADO r
      left join DEPENDENCIA  d  on d.depe_codi=r.radi_depe_actu
      left join SGD_DIR_DRECCIONES dir on r.RADI_NUME_RADI=dir.RADI_NUME_RADI
      WHERE r.RADI_NUME_RADI=".substr(base64_decode($_GET['r']),0,strlen(base64_decode($_GET['r']))-1)." ";
    $vct = $db->conn->GetRow($sql);
    //tamano del remitente
    $cadena=$vct['SGD_DIR_NOMREMDES'];

    $tamcadena=strlen($cadena);
    if($tamcadena>30){
      $cadena=substr($cadena,0,30);
    }
  } else {
    $tblSinPermiso="<html>
      <head><title>Seguridad Radicado</title><link href='../estilos/orfeo.css' rel='stylesheet' type='text/css'></head>
      <body>
      <table border=0 width=100% align='center' class='borde_tab' cellspacing='0'>
      <tr align='center' class='titulos2'>
      <td height='15' class='titulos2'>!! SEGURIDAD !!</td>
      </tr>
      <tr >
      <td width='38%' class=listado5 ><center><p><font class='no_leidos'>ACCESO INCORRECTO.</font></p></center></td>
      </tr>
      </table>
      </body>
      </html>";
    die($tblSinPermiso);
  }
}

$numrad   = $vct['RADI_NUME_RADI'];
$fecharad = $vct['RADI_FECH_RADI'];
$depecodi = $vct['DEPE_CODI'];

if (!empty($vct['RADI_DESC_ANEX'])){
  $radiDesA = "Anexos: ".$vct['RADI_DESC_ANEX'];
}

$cadena  = ucwords(strtolower($cadena));
$entidad = ucwords(strtolower($entidad));

echo "
  <html>
    <body style='margin-left: 6px;margin-top: 0px;margin-right: 0px;margin-bottom: 0px; line-height: 12px;'>
      <FONT FACE='Code3of9' size='6'>$numrad</FONT><FONT FACE='Arial' size='3'>$entidad</FONT><br>
      <FONT FACE='Arial' size='2'>No.$numrad<br></font>
      <FONT FACE='Arial' size='1'>Fecha Radicado: $fecharad Destino: $depecodi<br></font>
      <FONT FACE='Arial' size='1'>Anexos: $radiDesA - RTE:$cadena</font>
    </body>
  </html>";
?>

