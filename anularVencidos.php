<?php
ini_set('date.timezone', 'America/Bogota');
$ruta_raiz = "/var/www/html/orfeo/secure/prod/";
//$ruta_raiz = ".";

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/include/tx/Historico.php";
include_once "$ruta_raiz/anulacion/Anulacion.php";
include "$ruta_raiz/config.php";

$db        = new ConnectionHandler("$ruta_raiz");
if (!($db && $db instanceof ConnectionHandler)) {
    die("Error creando objeto de conexion a BD");
}
$Anulacion = new Anulacion($db);
$hist      = new Historico($db);

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

if (!defined('ADODB_ASSOC_CASE')){
     define('ADODB_ASSOC_CASE', 1);
}

$fecha   = date('Y-m-j');
$restadias = "- $diasanulacion day";
$dia     = strtotime ( $restadias , strtotime ( $fecha ) ) ; //Resta 30 dias 
$dia_ini = date('Y-m-d',$dia); //Formatea dia

echo $fecha;
echo $dia ;
echo $dia_ini;

    $sql     = "SELECT 
                    RADI_NUME_RADI, 
                    RADI_FECH_RADI, 
                    RADI_USUA_ACTU,
		    RADI_PATH, 
                    RADI_DEPE_ACTU
                FROM 
                    RADICADO 
                WHERE 
                    RADI_FECH_RADI < TO_DATE('$dia_ini','YYYY-MM-DD') AND
	            SGD_EANU_CODIGO IS NULL AND
                    (RADI_NUME_RADI LIKE '%1' OR RADI_NUME_RADI LIKE '%3') AND
                    (  ".$db->conn->substr."(radi_path, -3, 5) like 'doc'
			or ".$db->conn->substr."(radi_path, -3, 5) like 'odt')";

$rs = $db->conn->Execute($sql);
if (!$rs) die ("no existe $sql ..");

	while (!$rs->EOF) {
	    $depen = $rs->fields["RADI_DEPE_ACTU"];
	    $radicadosSel[] = $rs->fields["RADI_NUME_RADI"];
	    $rs->MoveNext();
	}

      if(!empty($radicadosSel)){
        $usuaActu  = 1;   //$res->fields["RADI_USUA_ACTU"];
	$depeActu  = 999; //$res->fields["RADI_DEPE_ACTU"];      
	$usua_doc  = "9991";
	$diasreales = $diasanulacion - 2;
        $observa = "Se solicita anulacion del documento por vencimiento de plazos - no 
                    se digitalizo el documento en $diasreales dias de plazo";
        $codTx   =  26;

        $hist->insertarHistorico($radicadosSel,  $depeActu , $usuaActu, 999, 1, $observa, $codTx);

        for($j=0;$j<count($radicadosSel);$j++){
            $iupt = "UPDATE 
                            RADICADO 
                         SET 
                            SGD_EANU_CODIGO = 1
                         WHERE
                            RADI_NUME_RADI  =  $radicadosSel[$j]";

            $db->conn->Execute($iupt); # Actualiza el registro en la base de datos
        }
        
        $resul = "El radicado ha sido anulado por superar el tiempo de tramite";
	$systemDate = $db->conn->OffsetDate(0,$db->conn->sysTimeStamp);

 	$radicados = $Anulacion->solAnulacion($radicadosSel,
            $depeActu,
            $usua_doc,
            $resul,
            $usuaActu,
            $systemDate,
            7);
    }else{
        $resul =  "No hay radicados pendientes";
    }

    echo "\n=========================================== 
          \n********* INICIO ".date('y-m-d')." *****************";

    if(isset($radicadosSel)){
        echo "\n\nDOCUMENTOS PARA ANULAR\n".
              implode($radicadosSel,", ");
    }

    echo "\n\n$resul\n\n";
?>
