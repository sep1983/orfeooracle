<?
session_start();

$ruta_raiz = "../..";
if (!$dependencia)   include "../../rec_session.php";
?>
<html>
  <head>
    <link rel="stylesheet" href="../../estilos/orfeo.css">
  </head>
<body onLoad="window_onload();">

<?
$ruta_raiz = "../..";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

$nomcarpeta = "Consulta Historico";

if ($orden_cambio==1)  {
  if (!$orderTipo)  {
    $orderTipo="desc";
  }else  {
    $orderTipo="";
  }
}

$encabezado = "".session_name()."=".session_id()."&krd=$krd&pagina_sig=$pagina_sig&usuLogin=$usuLogin&nombre=$nombre&dependencia=$dependencia&dep_sel=$dep_sel&selecdoc=$selecdoc&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
$linkPagina = "$PHP_SELF?$encabezado&usuLogin=$usuLogin&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=$orderNo";
?>
<h4 class="titulo titulos4">ADMINISTRACION DE USUARIOS Y PERFILES - Historicos</h4>

<table align="center" class=table>
  <tr>
    <td align="left">
      <label class='label'>Usuario:</label> <?=$usuLogin?>
    </td>

    <td align="left">
      <label class='label'>Nombre:</label> <?=$nombre?>
    </td>
  </tr>
</table>

<form name=formHistorico action='consultaHistorico.php?<?=$encabezado?>' method=post>
<?
if ($orderNo==98 or $orderNo==99) {
  $order=1;
  if ($orderNo==98)   $orderTipo="desc";

  if ($orderNo==99)   $orderTipo="";
} else  {
  if (!$orderNo)  {
    $orderNo=0;
  }
  $order = $orderNo + 1;
}

$sqlChar = $db->conn->SQLDate("d-m-Y H:i A","SGD_RENV_FECH");
include "$ruta_raiz/include/query/administracion/queryConsultaHistorico.php";

$rs=$db->conn->Execute($isql);

$nregis = $rs->fields["ADMINISTRADOR"];

if (!$nregis)  {
  echo "<hr><center><b>NO se encontro nada con el criterio de busqueda</center></b></hr>";}
else  {
  $pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo);
  $pager->toRefLinks = $linkPagina;
  $pager->toRefVars = $encabezado;
  $pager->Render($rows_per_page=20,$linkPagina,$checkbox=chkEnviar);
}
$encabezado = "".session_name()."=".session_id()."&krd=$krd&pagina_sig=$pagina_sig&usuLogin=$usuLogin&dependencia=$dependencia&dep_sel=$dep_sel&selecdoc=$selecdoc&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=";
?>
    <table class='table'>
      <tr>
          <td>
              <center>
                <a href="../formAdministracion.php?krd=<?=$krd?>" target='mainFrame' class="vinculos">  << Regresar >> </a>
              </center>
          </td>
      </tr>
    </table>

  </form>
</body>
</html>
