<?php
/*
 * Archivo auxiliar para la administracion de repositorios de ciudadanos y entidades
 * @autor Ing.John Guerrero
 */
session_start();
$ruta_raiz = "../..";
//if($_SESSION['usua_admin_sistema'] !=1 ) die(include "$ruta_raiz/errorAcceso.php");
if (!isset($krd))
    $krd = $_POST['krd']; else
    $krd = $_GET['krd'];
$ruta_raiz = "../..";
if (!isset($_SESSION['dependencia']))
    include "$ruta_raiz/rec_session.php";
define('ADODB_ASSOC_CASE', 1);
require_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler($ruta_raiz);
$error = 0;

if ($db) {
    $CiuWhere = " where sgd_ciu_codigo=" . $_POST['ciudadano'];
    $EntWhere = " where sgd_oem_codigo=" . $_POST['entidad'];
    $sqlCiu = "SELECT   sgd_ciu_nombre nombre, 
                        sgd_ciu_codigo codigo,
                        sgd_ciu_direccion direccion, 
                        sgd_ciu_apell1 apell1, 
                        sgd_ciu_apell2 apell2, 
                        sgd_ciu_telefono  telefono,
                        sgd_ciu_email mail,
                        sgd_ciu_cedula doc,
                        sgd_ciu_act act,
                        muni_codi,
                        dpto_codi,
                        id_cont,
                        id_pais
                        FROM sgd_ciu_ciudadano $CiuWhere
                        ORDER BY sgd_ciu_nombre ";
    $sqlEnt = "SELECT   sgd_oem_oempresa nombre,
                        sgd_oem_codigo codigo,
                        sgd_oem_nit NIT,
                        sgd_oem_rep_legal repre,
                        sgd_oem_telefono telefono,
                        sgd_oem_sigla sig,
                        sgd_oem_direccion direccion,
                        sgd_oem_act act,
                        muni_codi,
                        dpto_codi,
                        id_cont,
                        id_pais
                        FROM sgd_oem_oempresas $EntWhere
                        ORDER BY sgd_oem_oempresa ";
    /*
     *     $.post("CiuEntAux.php",{
      ModType:$('select[name="ModType"]').val()
      ,
      ciudadano:$("#sl_ciu").val()
      ,
      entidad:$("#sl_ent").val()
      },function(result){
      $("#SearchResult").empty().html(result);
      });
     */
    if (isset($_POST['ModType']) && !$_POST['Act']) {
        if ($_POST['ModType'] == 1) {
            $RsCiu = $db->conn->Execute($sqlCiu);
            ?>
            <script type="text/javascript">
                $("#txt_name").val('<?= $RsCiu->fields['NOMBRE'] ?>');
                $("#txt_id").val('<?= $RsCiu->fields['DOC'] ?>');
                $("#Codi").val('<?= $RsCiu->fields['CODIGO'] ?>');
                $("#txt_apell1").val('<?= $RsCiu->fields['APELL1'] ?>');
                $("#txt_apell2").val('<?= $RsCiu->fields['APELL2'] ?>');
                $("#txt_mail").val('<?= $RsCiu->fields['MAIL'] ?>');
                $("#txt_tel").val('<?= $RsCiu->fields['TELEFONO'] ?>');
                $("#txt_dir").val('<?= $RsCiu->fields['DIRECCION'] ?>');
                $("#idcont1").val('<?= $RsCiu->fields['ID_CONT'] ?>');
            <? if ($RsCiu->fields['ACT'] == 't') { ?>
                    $("#Slc_act").val('S');
            <? } else { ?>
                    $("#Slc_act").val('N');
            <? } ?>
                crea_var_idlugar_defa('<?= $RsCiu->fields['ID_CONT'] ?>'+'-'+'<?= $RsCiu->fields['ID_PAIS'] ?>'+'-'+'<?= $RsCiu->fields['DPTO_CODI'] ?>'+'-'+'<?= $RsCiu->fields['MUNI_CODI'] ?>');
                                                                                                                                                                
                                                                                                                                                                                
            </script>
            <?
        } else {
            $RsEnt = $db->conn->Execute($sqlEnt);
            ?>
            <!--             <script language="JavaScript" src="<?= $ruta_raiz ?>/js/crea_combos_2.js"></script>-->
            <script type="text/javascript">
                $("#txt_name").val('<?= $RsEnt->fields['NOMBRE'] ?>');
                $("#txt_id").val('<?= $RsEnt->fields['NIT'] ?>');
                $("#Codi").val('<?= $RsEnt->fields['CODIGO'] ?>');
                $("#txt_rep").val('<?= $RsEnt->fields['REPRE'] ?>');
                $("#txt_sig").val('<?= $RsEnt->fields['SIG'] ?>');
                $("#txt_tel").val('<?= $RsEnt->fields['TELEFONO'] ?>');
                $("#txt_dir").val('<?= $RsEnt->fields['DIRECCION'] ?>');
            <? if ($RsEnt->fields['ACT'] == 't') { ?>
                    $("#Slc_act").val('S');
            <? } else { ?>
                    $("#Slc_act").val('N');
            <? } ?>
                crea_var_idlugar_defa('<?= $RsEnt->fields['ID_CONT'] ?>'+'-'+'<?= $RsEnt->fields['ID_PAIS'] ?>'+'-'+'<?= $RsEnt->fields['DPTO_CODI'] ?>'+'-'+'<?= $RsEnt->fields['MUNI_CODI'] ?>');
                                                                                                                                 
                                                                                                                                                                                
            </script>
            <?
        }
    } else {
        if ($_POST['Act'] == 'S') {
            $value = 'TRUE';
        } else {
            $value = 'FALSE';
        }
        if ($_POST['ModType'] == 1) {
            $SQL = " UPDATE SGD_CIU_CIUDADANO SET SGD_CIU_ACT = $value $CiuWhere ";
            $Rs = $db->conn->Execute($SQL);
            if ($Rs) {
                echo '<script type="text/javascript">alert("Se actualizo el registro satisfactoriamente");window.location.reload();</script>';
            } else {
                echo '<script type="text/javascript">alert("Falla al actualizar el registro ");</script>';
            }
        } else {
            $SQL = " UPDATE SGD_OEM_OEMPRESAS SET SGD_OEM_ACT = $value $EntWhere ";
            $Rs = $db->conn->Execute($SQL);
            if ($Rs) {
                echo '<script type="text/javascript">alert("Se actualizo el resgistro satisfactoriamente");window.location.reload();</script>';
            } else {
                echo '<script type="text/javascript">alert("Falla al actualizar el registro ");</script>';
            }
        }
    }
} else {
    echo "<script type='text/javascript'>alert(\"Error de conexion a base de datos\");</script>";
}
?>
