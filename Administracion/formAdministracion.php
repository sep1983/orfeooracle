<?php
$ruta_raiz = "..";
session_start();
if (!isset($_SESSION['dependencia']))
    include "$ruta_raiz/rec_session.php";
if ($_SESSION['usua_admin_sistema'] != 1)
    die(include "$ruta_raiz/sinacceso.php");
?>
<html>
    <head>
        <link rel="stylesheet" href="../estilos/orfeo.css">
    </head>
    <body>
        <h4 class="titulo titulos4">M&Oacute;DULO DE ADMINISTRACI&Oacute;N</h4>
        <table class="table">
            <tr >
                <td >
                    <a href='usuario/mnuUsuarios.php?krd=<?= $krd ?>' target='mainFrame'>1. USUARIOS Y PERFILES</a>
                </td>
                <td><a href="tbasicas/adm_dependencias.php" target="mainFrame">2. DEPENDENCIAS</a></td>
            </tr>
            <tr >
                <td> <a   href="tbasicas/tipSoporte.php"target='mainFrame'>3. TIPO RECEP/ENV&Iacute;O </a></td>
                <td><a  href="tbasicas/adm_fenvios.php" target='mainFrame'>4. ENV&Iacute;O DE CORRESPONDENCIA</a> </td>
            </tr>
            <tr >
                <td><a href="tbasicas/adm_tsencillas.php" target='mainFrame'>5. TABLAS SENCILLAS</a></td>
                <td><a href="tbasicas/adm_trad.php?krd=<?= $krd ?>" target='mainFrame'>6. TIPOS DE RADICACI&Oacute;N</a></td>
            </tr>
            <tr >
                <td><a href="tbasicas/adm_paises.php" target='mainFrame'>7. PA&Iacute;SES</a></td>
                <td><a href="tbasicas/adm_dptos.php" target='mainFrame'>8. DEPARTAMENTOS</a></td>
            </tr>
            <tr >
                <td><a href="tbasicas/adm_mcpios.php" target='mainFrame'>9. MUNICIPIOS</a></td>
                <td><a href="tbasicas/adm_tarifas.php" target='mainFrame'>10. TARIFAS</a></td>
            </tr>

            <tr >

                <td><a href="tbasicas/adm_temas.php" target='mainFrame'>11. TABLAS TEM&Aacute;TICAS</a></td>
                <td><a href="tbasicas/adm_plantillas.php" target='mainFrame'>12. PLANTILLAS</a></td>
            <tr >
                <td><a href="tbasicas/adm_eaplicativos.php" target='mainFrame'>13. APLICATIVOS ENLACE</a></td>
                <td><a href="tbasicas/adm_contactos.php" target='mainFrame'>14. CONTACTOS</a></td>
            </tr>
            <tr >
                <td><a href="tbasicas/adm_esp.php?krd=<?= $krd ?>" target='mainFrame'>15. REMITENTES / TERCEROS</a></td>
                <td><a href="tbasicas/adm_msarchivo.php" target='mainFrame'>16. MEDIOS DE SOPORTE ARCHIVO</a></td>
            </tr>
            <tr >
                <td><a href="tbasicas/adm_mime.php" target='mainFrame'>17. MIME (Tipos de Archivos)</a></td>
                <td> <a  href="adm_nohabiles.php" target='mainFrame'>18. D&Iacute;AS NO H&Aacute;BILES</a></td>
            <tr >
                <td><a href="tbasicas/adm_detalleCausal.php" target='mainFrame'>19. DETALLES CAUSAL</a></td>
                <td><a href="tbasicas/adm_metadatos.php" target='mainFrame'>20. METADATOS</a></td>
            </tr>
            <tr >
                <td><a href="RemDest/adm_rem_dest.php" target='mainFrame'>21. CIUDADANOS Y EMPRESAS</a></td>
                <td><a href="../opciones/reasignar.php" target='mainFrame'>22. REASIGNAR</a></td>
            </tr>
        </table>
    </body>
</html>
