<?php
//variable para almacenar los enlaces registrados
//con los permisos asignados al usuario
$menu22 = '';

if ($_SESSION["usua_perm_tpx"] == 1 or $_SESSION["usua_perm_tpx"] == 3) {
  $menu22 .= "<li><a href='radicacion/NEW_express.php?$phpsession&krd=$krd' alt='Radicaci&oacute;n R&aacute;pida de entrada'
  title='Radicaci&oacute;n R&aacute;pida de entrada' target='mainFrame'>
  <i class=\"el el-chevron-right el-fw\"></i>
  Radicaci&oacute;n r&aacute;pida</a></li>";
}
if ($_SESSION["usua_perm_tpx"] == 2 or $_SESSION["usua_perm_tpx"] == 3) {
  $menu22 .= "<li><a href='radicacion/edtradicado_express.php?$phpsession&krd=$krd'
    alt='Modificaci&oacute;n de Radicaci&oacute;n R&aacute;pida de entrada'
    title='Modificaci&oacute;n Radicaci&oacute;n R&aacute;pida de entrada'
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Modificaci&oacute;n r&aacute;pida</a></li>";
}

$i = 0;

foreach ($_SESSION["tpNumRad"]as $key => $valueTp){
  $valueImg   = "";
  $valueDesc  = $tpDescRad[$key];
  $valueImg   = $tpImgRad[$key];
  $encabezado = "$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=$valueTp&depende=$dependencia";
  if ($valueTp != 2 and ($tpPerRad[$valueTp] == 1 or $tpPerRad[$valueTp] == 3)){
    $menu22 .= "<li><a href=\"radicacion/NEW.php?$encabezado\" alt='$valueDesc' title='$valueDesc'
      target='mainFrame'>
      <i class=\"el el-chevron-right el-fw\"></i>
      $valueDesc</a></li>";
  }elseif($tpPerRad[$valueTp] == 1 or $tpPerRad[$valueTp] == 3){
    $menu22 .= "<li><a href=\"radicacion/chequear.php?$encabezado\" alt='$valueDesc' title='$valueDesc'
      target='mainFrame'>
      <i class=\"el el-chevron-right el-fw\"></i>
      $valueDesc</a></li>";
  }
  $i++;
}

if ($_SESSION["usua_perm_reprad"] > 0) {
  $menu22 .= "<li><a href=\"reportes/listado_express.php?$phpsession&krd=$krd\" alt='Reporte de Radicaci&oacute;n R&aacute;pida de entrada'
    title='Reporte Radicaci&oacute;n de entrada' target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Reporte radicaci&oacute;n</a></li>";
}

if ($_SESSION["perm_radmemelec"] > 0) {
  $menu22 .= "<li><a onclick='cambioMenu($i);' href='borradores/index.php?$phpsession&krd=$krd' alt='iMemorando electr&oacute;nico' 
    title='Memorando electronico' target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Memorando Elec..</a></li>";
}

if ($_SESSION["usua_masiva"] == 1) {
  $menu22 .= "<li><a  href=\"radsalida/masiva/menu_masiva.php?$phpsession?>&krd=$krd&krd=$krd&fechah=$fechah\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Masiva</a></li>";
}

if ($_SESSION["perm_radi"] >= 1) {
  $menu22 .= "<li><a href=\"uploadFiles/uploadFileRadicado.php?$phpsession&krd=$krd&fechah=$fechah&
    primera=1&ent=2&depende=$dependencia\" alt='Asociar imagen de radicado'  target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Asociar im&aacute;genes</a></li>";
}

?>
  <p
      v-on:click="showTab(2)"
      class="menu-label">
        <i class="el el-file-new el-fw"></i>
        Radicaci&oacute;n
        <i v-if="!show2" class="el el-chevron-up"></i>
        <i v-if="show2" class="el el-chevron-down"></i>
  </p>
  <ul
    v-if="show2"
    class="menu-list">
    <?=$menu22?>
  </ul>
