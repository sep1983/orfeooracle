<?php
if(!isset($_SESSION['dependencia'])){
  include "../rec_session.php";
}

//variable para almacenar los enlaces que se asignan al usuario
//segun sus privilegios
$menu1 = '';
$menu2 = '';
$depe_codi = $_SESSION["dependencia"];

if($_SESSION["usua_admin_sistema"]==1){
  $menu1 .= "<li><a href=\"Administracion/formAdministracion.php?$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=1\"
    target='mainFrame'> <i class=\"el el-chevron-right el-fw\"></i>
    Administraci&oacute;n</a></li>";
}

if($_SESSION["codusuario"]==1){
  $menu1 .= "<li><a href=\"admonDep/formAdministracion.php?$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=1\"
    target='mainFrame'> <i class=\"el el-chevron-right el-fw\"></i>
    Admon Dependencia</a></li>";
}

if($_SESSION["usua_perm_adminflujos"]==1){
  $menu1 .= "<li><a href=\"Administracion/flujos/texto_version2/mnuFlujosBasico.php?$phpsession&krd=$krd\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Editor flujos</a></li>";
}

if($_SESSION["usua_perm_envios"]>=1){
  $menu1 .= "<li><a href=\"radicacion/formRadEnvios.php?$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=1\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Env&iacute;os</a></li>";
}

if($_SESSION["usua_perm_modifica"] >=1){
  $menu1 .= "<li><a href=\"radicacion/edtradicado.php?$phpsession&krd=$krd&fechah=$fechah&primera=1&ent=2\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Modificaci&oacute;n</a></li>";
}

if($_SESSION["usua_perm_firma"]==1 || $_SESSION["usua_perm_firma"]==3){
  $menu1 .= "<li><a href=\"firma/cuerpoPendientesFirma.php?$phpsession&krd=$krd&fechaf=$fechah&carpeta=8&nomcarpeta=Documentos Para Firma Digital&orderTipo=desc&orderNo=3\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Firma digital</a></li>";
}

if($_SESSION["usua_perm_intergapps"]==1 ){
  $menu1 .= "<li><a href=\"aplintegra/cuerpoApLIntegradas.php?$phpsession&krd=$krd&fechaf=$fechah&carpeta=8&nomcarpeta=Aplicaciones integradas&orderTipo=desc&orderNo=3\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Aplicaciones integradas</a></li>";
}

if($_SESSION["usua_perm_impresion"] >= 1){
  $usua_perm_impresion = $_SESSION["usua_perm_impresion"];
  $nomcarpeta= "Documentos Para Impresi&oacute;n";
  $menu1 .= "<li><a href=\"envios/cuerpoMarcaEnviar.php?$phpsession&krd=$krd&fechaf=$fechah&usua_perm_impresion=$usua_perm_impresion&carpeta=8&nomcarpeta=
    $nomcarpeta&orderTipo=desc&orderNo=3\" target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Impresi&oacute;n</a></li>";
}

if ($_SESSION["usua_perm_anu"]==3 or $_SESSION["usua_perm_anu"]==1){
  $menu1 .= "<li><a href=\"anulacion/cuerpo_anulacion.php?$phpsession&krd=$krd&tpAnulacion=1&fechah=$fechah\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Anulaci&oacute;n</a></li>";
}

if ($_SESSION["usua_perm_trd"]==1){
  $menu1 .= "<li><a href=\"trd/menu_trd.php?$phpsession&krd=$krd&fechah=$fechah\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Tablas retenci&oacute;n</a></li>";
}


if ($_SESSION["usuaPermExpediente"]==3){
  $menu1 .= "<li><a href=\"expediente/admCreaExpediente.php?$phpsession\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Crear Expedientes</a></li>";
}


if($_SESSION["usua_admin_archivo"]>=1){
  $isql = "select count(1) as CONTADOR
    from SGD_SEXP_SECEXPEDIENTES exp JOIN DEPENDENCIA d ON exp.DEPE_CODI=d.DEPE_CODI
    where exp.SGD_SEXP_FASEEXP = 1";
  $rs=$db->conn->Execute($isql);
  $num_exp = $rs->fields["CONTADOR"];
  $warning='';
  $warning = ((int)$num_exp > 0)? 'is-warning' : '';
  $menu1 .= "<li>
    <a href=\"archivo/archivo.php?$phpsession&krd=$krd&carpeta=8&nomcarpeta=Expedientes&orno=1&adodb_next_page=1\"
    target='mainFrame'>
      <span class='tag tag-cont is-info $warning'>$num_exp</span>
      Archivo
    </a>
    </li>";
}


if($_SESSION["usua_perm_prestamo"]==1){
  $isql = "select count(1) as CONTADOR
    from PRESTAMO p JOIN DEPENDENCIA d ON p.DEPE_CODI=d.DEPE_CODI
    where p.PRES_ESTADO=1 and p.SGD_EXP_NUMERO is not null";
  $rs=$db->conn->Execute($isql);
  $num_prest = $rs->fields["CONTADOR"];

  $warning='';
  $warning = ((int)$num_prest > 0)? 'is-warning' : '';

  $menu1 .= "<li>
    <a href=\"prestamo/menu_prestamo.php?$phpsession&etapa=1&s_Listado=VerListado&krd=$krd&fechah=$fechah\"
    target='mainFrame'>
    <span class='tag tag-cont is-info $warning'>$num_prest</span>
    Pr&eacute;stamo
    </a>
    </li>";
}

if($_SESSION["usua_perm_dev"]==1){
  $menu1 .= "<li><a href=\"devolucion/cuerpoDevCorreo.php?$phpsession&krd=$krd&fechaf=$fechah
    &carpeta=8&devolucion=2&estado_sal=4&nomcarpeta=Documentos Para Impresion&orno=1&adodb_next_page=1\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Dev correo</a></li>";
}

if($_SESSION["usua_perm_desarch"]==1){
  $menu1 .= "<li><a href=\"opciones/desarchivar.php?$phpsession&krd=$krd\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Desarchivar</a></li>";
}

$menu1 .= "<li><a href=\"reportesEntidad/menu.php?$phpsession&etapa=1&s_Listado=VerListado&krd=$krd&fechah=$fechah\"
  target='mainFrame'>
  <i class=\"el el-chevron-right el-fw\"></i>
  Reportes</a></li>";

if ($_SESSION["usua_perm_remit_tercero"]==1){
  $menu1 .= "<li><a href=\"Administracion/tbasicas/adm_esp.php?$phpsession&krd=$krd&fechah=$fechah\"
    target='mainFrame'>
    <i class=\"el el-chevron-right el-fw\"></i>
    Remitente / Terceros</a></li>";
}

$menu1 .= "<li><a href=\"busqueda/busquedaPiloto.php?$phpsession&etapa=1&s_Listado=VerListado&krd=$krd&fechah=$fechah\"
  target='mainFrame'>
  <i class=\"el el-chevron-right el-fw\"></i>
  Consultas</a></li>";

?>
  <p
      v-on:click="showTab(1)"
      class="menu-label">
        <i class="el el-list el-fw"></i>
        General
        <i v-if="!show1" class="el el-chevron-up"></i>
        <i v-if="show1" class="el el-chevron-down"></i>
  </p>
  <ul v-if="show1" class="menu-list">
    <?=$menu1?>
  </ul>
