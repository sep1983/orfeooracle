<?php
$dependencia = $_SESSION['dependencia'];
$codusuario  = $_SESSION['codusuario'];
$krd         = $_SESSION['krd'];
$usua_doc    = $_SESSION['usua_doc'];

include_once("$ruta_raiz/include/db/ConnectionHandler.php");

if(!$db){
  $db = new ConnectionHandler("$ruta_raiz");
}

if (!defined('ADODB_ASSOC_CASE')){
  define('ADODB_ASSOC_CASE', 1);
}

$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$ADODB_COUNTRECS  = false;

$error = 0;
//variable para almacenar los enlaces que pertenecen
//al usuario
$menuCarpetas  = '';
$menuPersonal  = '';

// Esta consulta selecciona las carpetas Basicas de
// DocuImage que son extraidas de la tabla Carp_Codi
$isql = "select CARP_CODI,CARP_DESC from carpeta order by carp_codi ";
$rs = $db->conn->Execute($isql);
$addadm = "";

$hayCambiosCarpetas = false;
while (!$rs->EOF) {
  //almacena los datos de cada una de las carpetas en cada ciclo
  $data = "";

  $numdata = trim($rs->fields["CARP_CODI"]);
  $sqlCarpDep = "select
                  SGD_CARP_DESCR
                 from
                  SGD_CARP_DESCRIPCION
                 where
                  SGD_CARP_DEPECODI = $dependencia
                  and SGD_CARP_TIPORAD = $numdata";
  $rsCarpDesc = $db->conn->Execute($sqlCarpDep);

  if(!$rsCarpDesc->EOF){
    $descripcionCarpeta = $rsCarpDesc->fields["SGD_CARP_DESCR"];
  }

  $data = (!empty($descripcionCarpeta)) ? $descripcionCarpeta : trim($rs->fields["CARP_DESC"]);

  if ($numdata == 11) {   // Se realiza la cuenta de radicados en Visto Bueno VoBo
    if ($_SESSION["usua_vobo_perm"] == 1) {//$codusuario ==1
      $isql = "select count(1) as CONTADOR from radicado
        where carp_per=0 and carp_codi=$numdata
        and  radi_depe_actu=$dependencia
        and radi_usua_actu=$codusuario";
    } else {
      $isql = "select count(1) as CONTADOR from radicado
        where carp_per=0
        and carp_codi=$numdata
        and radi_depe_actu=$dependencia
        and radi_usu_ante='$krd'";
    }
    $addadm = "&adm=1";
  } else {
    $isql = "select count(1) as CONTADOR from radicado
      where carp_per=0 and carp_codi=$numdata
      and  radi_depe_actu=$dependencia
      and radi_usua_actu=$codusuario and radi_nume_radi is not null  ";
    $addadm = "&adm=0";
  }

  $rs1 = $db->conn->Execute($isql);
  $numerot = $rs1->fields["CONTADOR"];

  $warning='';
  $warning = ((int)$numerot > 0)? 'is-warning' : '';
  $menuCarpetas .= "<li><a href='cuerpo.php?$phpsession&krd=$krd&adodb_next_page=1&
    nomcarpeta=$data&carpeta=$numdata&tipo_carpt=0&adodb_next_page=1'
    target='mainFrame' alt='Seleccione una Carpeta'>
    <span class='tag tag-cont is-info $warning'> $numerot </span> $data </a></li>";
  $rs->MoveNext();
}

$sqlFechaHoy = $db->conn->DBTimeStamp(time());
$sqlAgendado = " and (agen.SGD_AGEN_FECHPLAZO > " . $sqlFechaHoy . ")";

$isql = "select
            count(1) as CONTADOR
         from
            SGD_AGEN_AGENDADOS agen
         where
            usua_doc='$usua_doc' and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";
$rs = $db->conn->Execute($isql);
$numerot = $rs->fields["CONTADOR"];
$data = "Agendado";

$warning='';
$warning = ((int)$numerot > 0)? 'is-warning' : '';
$menuCarpetas .= "<li>
                    <a href='cuerpoAgenda.php?$phpsession&agendado=1&krd=$krd&nomcarpeta=$data&tipo_carpt=0'
                    target='mainFrame' alt='Seleccione una Carpeta'>
                    <span class='tag tag-cont is-info $warning'> $numerot </span>
                    Agendado</a></li>";

$sqlAgendado = " and (agen.SGD_AGEN_FECHPLAZO <= " . $sqlFechaHoy . ")";

$isql = "select count(1) as CONTADOR from SGD_AGEN_AGENDADOS agen
    where  usua_doc='$usua_doc'
    and agen.SGD_AGEN_ACTIVO=1 $sqlAgendado";

$rs = $db->conn->Execute($isql);
$numerot = $rs->fields["CONTADOR"];
$data = "Agendados vencidos";

$warning='';
$warning = ((int)$numerot > 0)? 'is-warning' : '';
$menuCarpetas .= "<li><a href='cuerpoAgenda.php?$phpsession&agendado=2&krd=$krd&fechah=$fechah&
  nomcarpeta=$data&&tipo_carpt=0&adodb_next_page=1' target='mainFrame' alt='Seleccione una Carpeta'> <span class='tag tag-cont is-info $warning'> $numerot </span> Agendado vencido </a></li>";

// Coloca el mensaje de Informados y cuenta cuantos registros hay en informados
$isql = "select count(1) as CONTADOR from informados where depe_codi=$dependencia and usua_codi=$codusuario ";
$rs1 = $db->conn->Execute($isql);
$numerot = $rs1->fields["CONTADOR"];
$data = "Informados";

$warning='';
$warning = ((int)$numerot > 0)? 'is-warning' : '';
$menuCarpetas .= "<li><a href='cuerpoinf.php?krd=$krd&mostrar_opc_envio=1&carpeta=8&
  nomcarpeta=Informados&orderTipo=desc&adodb_next_page=1'target='mainFrame'
  alt='Documentos De Informacion' title='Documentos De Informacion'>
  <span class='tag tag-cont is-info $warning'> $numerot </span> $data</a></li>";


$crearCarpeta = "<a href='crear_carpeta.php?$phpsession&krd=$krd&fechah=$fechah&adodb_next_page=1'
  target='mainFrame' alt='Creacion de Carpetas Personales' title='Creacion de Carpetas Personales'>Crear carpeta <i class='el el-plus '></i></a>";

// Busca las carpetas personales de cada usuario y las coloca contando el numero de documentos en cada carpeta.
$isql = "select
          CODI_CARP,DESC_CARP,NOMB_CARP
         from
          carpeta_per
         where usua_codi=$codusuario and depe_codi=$dependencia order by codi_carp  ";
$rs = $db->conn->Execute($isql);
while (!$rs->EOF) {
  //almacena los enlaces que peretenecen
  //al usuairo de las carpetas personales
  $data = '';
  $data = trim($rs->fields["NOMB_CARP"]);
  $numdata = trim($rs->fields["CODI_CARP"]);
  $detalle = trim($rs->fields["DESC_CARP"]);
  $isql = "select count(1) as CONTADOR
    from radicado
    where
    carp_per=1 and carp_codi=$numdata and
    radi_depe_actu=$dependencia and
    radi_usua_actu=$codusuario ";

  $rs1 = $db->conn->Execute($isql);
  $numerot = $rs1->fields["CONTADOR"];

  $warning='';
  $warning = ((int)$numerot > 0)? 'is-warning' : '';
  $menuPersonal .="<li><a href=\"cuerpo.php?$phpsession&krd=$krd&fechah=$fechah&nomcarpeta=$data(Personal)
    &tipo_carp=1&carpeta=$numdata&adodb_next_page=1\" alt='$detalle' title='$detalle'
    target='mainFrame'>
    <span class='tag tag-cont is-info $warning'> $numerot </span>
    $data</a></li>";
  $rs->MoveNext();
}

if(!empty($menuCarpetas)){
  echo "
    <p
      v-on:click='showTab(3)'
      class='menu-label'>
        <i class='el el-folder el-fw'></i>
        Carpetas
        <i v-if='!show3' class='el el-chevron-up'></i>
        <i v-if='show3' class='el el-chevron-down'></i>
    </p>
      <ul
        v-if='show3'
        class='menu-list'>
        $menuCarpetas
        <li>
          $crearCarpeta
          <ul>
            $menuPersonal
          </ul>
        </li>
      </ul>";
}
?>
