<?php
//Data base and get var
include_once('./inCommon.php');
include_once('./class_draft.php');

$draft = new Draft($db);

switch ($method) {
case 'GET':
  switch ($nameField){
    case 'folder':
      $data = $draft->GetFolderByUser();
    break;
  }
  break;
case 'POST':
  //Get value post vars
  $data = json_decode(file_get_contents('php://input'), true);

  //Selection function
  switch ($nameField) {
  case 'create':
    $data = $draft->SetNewFolder($data);
    //Create a new draft
    break;
  case 'delete':
    $data = $draft->deleteFolder($data);
    //Create a new draft
    break;
  }
  break;
}

// print results
header('Content-Type: application/json');
echo json_encode($data);
?>
