<?php
//Data base and get var
include_once('./inCommon.php');
include_once('./class_draft.php');


$draft = new Draft($db);

switch ($method) {

case 'GET':

  switch ($nameField) {

  case 'draft':
    //get vars depecodi and usuacodi from incommon
    $resData = $draft->GetDraftByUser();
    break;

  case 'idDraft':
    //get vars depecodi and usuacodi from incommon
    $resData = $draft->GetDraftById($id);
    break;
  }
  break;

  case 'POST':
    //Get value post vars
    $data = json_decode(file_get_contents('php://input'), true);
    //Seleccition function
    switch ($nameField) {
    //new draft
    case 'save':
      $resData = $draft->SetSaveDraft($data);
      break;
    //Set state read and unread
    case 'stateRead':
      $resData = $draft->SetStateRead($data);
      break;
    //set folder
    case 'folder':
      $resData = $draft->SetFolder($data);
      break;
    //set approve or discart
    case 'approve':
      $resData = $draft->SetApproveState($data);
      break;
    //send  and share to others users
    case 'send':
      $resData = $draft->SendDraft($data);
      break;
    //Memo save id draft to generate memo
    case 'memo':
      $resData = $draft->SendMemo($data);
      break;
    //Discard
    case 'disc':
      $resData = $draft->DiscDraft($data);
      break;
    }
    break;
}

// print results
header('Content-Type: application/json');
echo json_encode($resData);
?>
