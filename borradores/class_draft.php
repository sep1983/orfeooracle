<?php
class Draft {

  //handle draft for user
  private $depe_codi;
  private $usua_codi;
  private $usua_doc;
  private $nowDate;
  private $draft = array();

  //handle general folder
  private $tx = array(
    0  => 'Guardado',
    1  => 'Enviado',
    2  => 'Descartado',
    3  => 'Creado',
    4  => 'Leido',
    5  => 'Aprobado',
    6  => 'Memorando generado',
    7  => 'Enviado para generar Memorando',
    8  => 'Compartido para aprobación',
    9  => 'Compartido por notificación',
    10 => 'Guardado el borrador anterior'
  );

  //handle general folder
  private $folder = array(

      array(
        name  => 'Por Aprobación',
        nodrf => 0,
        state => false,
        id    => 1000,
        type  => 'system',
        value => 'approve'),

      array(
        name  => 'Notificados',
        nodrf => 0,
        state => false,
        id    => 1001,
        type  => 'system',
        value => 'notification'),

      array(
        name  => 'Enviados',
        nodrf => 0,
        state => false,
        id    => 1003,
        type  => 'system',
        value => 'send'),

      array(
        name  => 'Guardados',
        nodrf => 0,
        state => false,
        id    => 1004,
        type  => 'system',
        value => 'save'),

      array(
        name  => 'Aprobados',
        nodrf => 0,
        state => false,
        id    => 1005,
        type  => 'system',
        value => 'approved'),

      array(
        name  => 'Descartados',
        nodrf => 0,
        state => false,
        id    => 1006,
        type  => 'system',
        value => 'discarded'),

      array(
        name  => 'Memorandos',
        nodrf => 0,
        state => false,
        id    => 1007,
        type  => 'system',
        value => 'memorando'),
      );


  /**
   * Constructor de la classe.
   * @param ConnectionHandler $db */
  public function __construct($db){
    session_start();
    $this->db = $db;
    $this->db->conn->debug = false;
    $this->usua_codi = $_SESSION["codusuario"];
    $this->usua_doc  = $_SESSION["usua_doc"];
    $this->depe_codi = $_SESSION["dependencia"];
    $this->nowDate   = $this->db->conn->OffsetDate(0, $this->db->conn->sysTimeStamp);
  }


  // @param depe_codi Dependencia del usuario
  // @param usua_codi codigo del usuario
  // @return array draft
  public function GetDraftByUser(){
    $sql3  = "
      SELECT
        SD.ID,
        TO_CHAR (SD.DRAFT_CREATE_DATE, 'MM-DD-YYYY HH24:MI:SS'),
        SD.DRAFT_CONTENT,
        SD.DRAFT_FOLDER,
        SD.DRAFT_NAME,
        SD.DRAFT_READ,
        DE.DEPE_NOMB,
        US.USUA_NOMB
      FROM
        SGD_DRAFT SD,
        DEPENDENCIA DE,
        USUARIO US
      WHERE
        DE.DEPE_CODI = SD.DRAFT_DEPE_CODE AND
        US.USUA_CODI = SD.DRAFT_USER_CODE AND
        DE.DEPE_CODI = US.DEPE_CODI AND
        SD.DRAFT_USER_CODE = $this->usua_codi and
        SD.DRAFT_USUA_DOC  = $this->usua_doc  and
        SD.DRAFT_DEPE_CODE = $this->depe_codi";

    $res3 = $this->db->conn->query($sql3);

    while($res3 && !$res3->EOF){
      $data = array();
      $name = html_entity_decode($res3->fields['DRAFT_NAME']);
      $data['name']   = utf8_encode($name);
      $data['id']     = (int)$res3->fields['ID'];
      $data['date']   = $res3->fields['DRAFT_CREATE_DATE'];
      $data['user']   = $res3->fields['USUA_NOMB'];
      $data['depe']   = $res3->fields['DEPE_NOMB'];
      $data['read']   = empty($res3->fields['DRAFT_READ'])? false : true;
      $data['folder'] = (int)$res3->fields['DRAFT_FOLDER'];
      $this->draft[] = $data;
      $res3->MoveNext();
    }

    //DRAFT_ASSIG_STATE
    //0:Activo, 1:Aprobado, 2: Memorando Generado, 3: Descartado
    $sql4  = "
      SELECT
        SG.DRAFT_ASSIG_STATE,
        SG.DRAFT_ID AS ID,
        SG.DRAFT_ASSIG_APPROVALL,
        SG.DRAFT_ASSIG_DATE,
        SG.DRAFT_ASSIG_READ,
        SG.DRAFT_ASSIG_FOLDER,
        SD.DRAFT_NAME,
        US.USUA_NOMB,
        DE.DEPE_NOMB
      FROM
        SGD_DRAFT_ASSIGNMENT SG,
        SGD_DRAFT SD,
        USUARIO US,
        DEPENDENCIA DE
      WHERE
        SG.DRAFT_ASSIG_USER_CODE = $this->usua_codi AND
        SG.DRAFT_ASSIG_DEPE_CODE = $this->depe_codi AND
        SD.ID = SG.DRAFT_ID AND
        SD.DRAFT_DEPE_CODE = US.DEPE_CODI AND
        SD.DRAFT_USER_CODE = US.USUA_CODI AND
        SD.DRAFT_DEPE_CODE = DE.DEPE_CODI AND
        SG.DRAFT_ASSIG_STATE < 2";

    $res4 = $this->db->conn->query($sql4);

    while($res4 && !$res4->EOF){
      $data = array();
      $name = html_entity_decode($res4->fields['DRAFT_NAME']);
      $data['name']   = utf8_encode($name);
      $data['id']     = (int)$res4->fields['ID'];
      $data['date']   = $res4->fields['DRAFT_ASSIG_DATE'];
      $data['user']   = $res4->fields['USUA_NOMB'];
      $data['depe']   = $res4->fields['DEPE_NOMB'];
      $data['read']   = empty($res4->fields['DRAFT_ASSIG_READ'])? false : true;
      $data['folder'] = (int)$res4->fields['DRAFT_ASSIG_FOLDER'];

      $this->draft[] = $data;

      $res4->MoveNext();
    }

    return $this->draft;
  }

  public function GetDraftById($id){

    $comment = array();
    $history = array();
    $log = array();

    $sql39  = "
      SELECT
        SD.DRAFT_CONTENT,
        SD.DRAFT_NAME
      FROM
        SGD_DRAFT SD
      WHERE
        SD.ID              = $id and
        SD.DRAFT_USER_CODE = $this->usua_codi and
        SD.DRAFT_USUA_DOC  = $this->usua_doc  and
        SD.DRAFT_DEPE_CODE = $this->depe_codi";

    $res39 = $this->db->conn->query($sql39);

    if(!$res39->EOF){
      $iamown = true;
      $name = html_entity_decode($res39->fields['DRAFT_NAME']);
      $cont = html_entity_decode($res39->fields['DRAFT_CONTENT']);
      $data['name']    = utf8_encode($name);
      $data['content'] = utf8_encode($cont);

    }else{
      $sql87  = "
          SELECT
            SD.DRAFT_CONTENT,
            SD.DRAFT_NAME
          FROM
            SGD_DRAFT SD,
            SGD_DRAFT_ASSIGNMENT SG
          WHERE
            SD.ID = SG.DRAFT_ID AND
            SD.ID = $id AND
            SG.DRAFT_ASSIG_STATE < 2 AND
            SG.DRAFT_ASSIG_USER_CODE = $this->usua_codi AND
            SG.DRAFT_ASSIG_DEPE_CODE = $this->depe_codi";

      $res87 = $this->db->conn->query($sql87);

      if(!$res87->EOF){
        $name = html_entity_decode($res87->fields['DRAFT_NAME']);
        $cont = html_entity_decode($res87->fields['DRAFT_CONTENT']);
        $data['name']    = utf8_encode($name);
        $data['content'] = utf8_encode($cont);
      }
    }

    //if i'm owner of the draft i can create memorando
    $data['genMemo'] = false;

    if($iamown){
      $sql71 = "
        SELECT
          LISTAGG( DRAFT_ASSIG_STATE, ';'  ) WITHIN GROUP
          (ORDER BY DRAFT_ASSIG_STATE) AS APPRO
        FROM
          SGD_DRAFT_ASSIGNMENT
        WHERE
          DRAFT_ID = $id AND
          DRAFT_ASSIG_APPROVALL = 1";

      $res71 = $this->db->conn->query($sql71);

      if(!$res71->EOF){
        $arrAp = explode(";", $res71->fields['APPRO']);
        $noRow = count($arrAp);
        $sumAp = array_sum($arrAp);

        if (($sumAp / $noRow ) === 1){
          $data['genMemo'] = true;
        }
      }
    }

    $sql40 = "
        SELECT
          US.USUA_LOGIN || ' - ' || US.USUA_NOMB as NOMB,
          DRAFT_ASSIG_APPROVALL as APRO
        FROM
          SGD_DRAFT_ASSIGNMENT SA,
          USUARIO US
        WHERE
          SA.DRAFT_ID  = $id AND
          US.USUA_CODI = SA.DRAFT_ASSIG_USER_CODE AND
          US.DEPE_CODI = SA.DRAFT_ASSIG_DEPE_CODE ";

    $res40 = $this->db->conn->query($sql40);

    while($res40 && !$res40->EOF){
      if($res40->fields['APRO'] == 1){
        $data['aprob'][] = $res40->fields['NOMB'];
      }else{
        $data['notf'][] = $res40->fields['NOMB'];
      }
      $res40->MoveNext();
    }

    $sql92 = "
        SELECT
          DF.ID,
          DF.DRAFT_COMMENT AS COMMT,
          TO_CHAR (DF.DRAFT_COM_DATE, 'MM-DD-YYYY HH24:MI:SS') AS DAT,
          US.USUA_NOMB AS NAM
        FROM SGD_DRAFT_COMMENT DF,
          USUARIO US
        WHERE
          DF.DRAFT_ID = $id  AND
          DF.DRAFT_COM_USER_CODE = $this->usua_codi AND
          DF.DRAFT_COM_DEPE_CODE = $this->depe_codi AND
          DF.DRAFT_COM_USER_CODE = US.USUA_CODI AND
          DF.DRAFT_COM_DEPE_CODE = US.DEPE_CODI";

    $res92 = $this->db->conn->query($sql92);

    while($res92 && !$res92->EOF){
      $cont = html_entity_decode($res92->fields['COMMT']);
      $tmpComm['id'] = $res92->fields['ID'];
      $tmpComm['user'] = $res92->fields['NAM'];
      $tmpComm['date'] = $res92->fields['DAT'];
      $tmpComm['comment'] =utf8_encode($cont);
      $comment[] = $tmpComm;
      $res92->MoveNext();
    }

    $data['comment'] = $comment;

    $sql101 = "
      SELECT
      ID,
      DF.DRAFT_VER_CONTENT,
      TO_CHAR (DF.DRAFT_VER_DATE, 'MM-DD-YYYY HH24:MI:SS') AS DAT,
      US.USUA_NOMB
      FROM
      USUARIO US,
      SGD_DRAFT_VERSION DF
      WHERE
      US.USUA_CODI = DF.DRAFT_VER_USER_CODE AND
      US.DEPE_CODI = DF.DRAFT_VER_DEPE_CODE AND
      DF.DRAFT_ID = $id";

    $res95 = $this->db->conn->query($sql101);

    while($res95 && !$res95->EOF){
      $cont = html_entity_decode($res95->fields['DRAFT_VER_CONTENT']);
      $tmpComm['id']   = $res95->fields['ID'];
      $tmpComm['user'] = $res95->fields['USUA_NOMB'];
      $tmpComm['date'] = $res95->fields['DRAFT_VER_DATE'];
      $tmpComm['content'] =utf8_encode($cont);
      $history[] = $tmpComm;
      $res95->MoveNext();
    }

    $data['history'] = $history;

    $sql854 = "
      SELECT
        LG.ID,
        US.USUA_NOMB,
        LG.DRAFT_LOG_TX,
        LG.DRAFT_LOG_OBSER,
        TO_CHAR (LG.DRAFT_LOG_DATE, 'MM-DD-YYYY HH24:MI:SS')
      FROM
        USUARIO US,
        SGD_DRAFT_LOG LG
      WHERE
        US.USUA_CODI = LG.DRAFT_LOG_USUA_CODE AND
        US.DEPE_CODI = LG.DRAFT_LOG_DEPE_CODE AND
        LG.DRAFT_ID = $id";

    $res200 = $this->db->conn->query($sql854);

    while($res200 && !$res200->EOF){
      $tmpComm['id']   = $res200->fields['ID'];
      $tmpComm['user'] = $res200->fields['USUA_NOMB'];
      $tmpComm['date'] = $res200->fields['DRAFT_LOG_DATE'];
      $tmpComm['tx']   = $this->tx[$res200->fields['DRAFT_LOG_TX']];
      $tmpComm['obser'] = $res200->fields['DRAFT_LOG_OBSER'];
      $log[] = $tmpComm;
      $res200->MoveNext();
    }

    $data['log'] = $log;

    return $data;
  }

  // create a new draft
  // @param  array $data
  // @return Boolean true | false
  public function SetSaveDraft($data){
    $this->db->conn->debug = true;
    $idDraft  = $data['idDraft'];
    $table = 'SGD_DRAFT';

    $sql2 = " SELECT
                max(id) as ID
              from
                SGD_DRAFT";

    $res2 = $this->db->conn->query($sql2);
    $id2  = $res2->fields['ID'];

    if(empty($id2)){
      $id2 = 1;
    }else{
      $id2++;
    }

    $cont = htmlentities($data['content'], ENT_QUOTES, "UTF-8");
    $name = htmlentities($data['name'], ENT_QUOTES, "UTF-8");

    $cont = "'".$cont."'";
    $name = "'".$name."'";

    if(empty($idDraft)){

      $record["ID"]                = $id2;
      $record["DRAFT_CREATE_DATE"] = $this->nowDate;
      $record["DRAFT_CONTENT"]     = $cont;
      $record["DRAFT_FOLDER"]      = 1004;
      $record["DRAFT_NAME"]        = $name;
      $record["DRAFT_DEPE_CODE"]   = $this->depe_codi;
      $record["DRAFT_USER_CODE"]   = $this->usua_codi;
      $record["DRAFT_USUA_DOC"]    = $this->usua_doc;
      $record["DRAFT_READ"]        = 0;
      $ok = $this->db->insert($table, $record);
      $res['state'] = true;
      $res['idDraft'] = $id2;

      //log events
      $this->logDraft($id2, 0);

      return $res;
    }else{
      //save actualy draft
      $this->savePrevDraft($idDraft);

      $sqlU90 = "
        UPDATE
          SGD_DRAFT
        SET
          DRAFT_CONTENT = $cont,
          DRAFT_NAME    = $name
        WHERE
         ID = $idDraft";

      $this->db->conn->query($sqlU90);
      $res['state'] = true;
      $res['idDraft'] = $idDraft;

      //log events
      $this->logDraft($idDraft, 0);

      return $res;
    }
  }

  // create a new comment draft
  // @param  $data with id draft
  // @return Boolean true | false
  public function SetNewComment($data){
    if(empty($data['content']) || empty($data['idDraft'])){
      return false;
    };

    $sql102 = " SELECT
                max(id) as ID
              from
                SGD_DRAFT_COMMENT";

    $res1 = $this->db->conn->query($sql102);
    $id1  = $res1->fields['ID'];

    if(empty($id1)){
      $id1 = 1;
    }else{
      $id1++;
    }

    $cont = htmlentities($data['content'], ENT_QUOTES, "UTF-8");
    $cont = "'".$cont."'";

    $record["ID"] = $id1;
    $record["DRAFT_ID"]   = $data['idDraft'];
    $record["DRAFT_COMMENT"] = $cont;
    $record["DRAFT_COM_USER_CODE"] = $this->usua_codi;
    $record["DRAFT_COM_DEPE_CODE"] = $this->depe_codi;
    $record["DRAFT_COM_DATE"]= $this->nowDate;

    $table = 'SGD_DRAFT_COMMENT';

    $ok = $this->db->insert($table, $record, 'true');
    if($ok){
      return true;
    }else{
      return false;
    }
  }

  // change folder draft
  // @param id draft
  // @param newFolder
  // return Boolean true | false
  public function SetFolder($data){
    $idFolder = '';
    $idDraft  = $data['idsDraft'];
    $nameFolder = $data['folder'];

    foreach ($this->folder as $value){
      if($value['name'] == $nameFolder){
        $idFolder = $value['id'];
      }
    }

    if(empty($idFolder)){

      $sql46 = "
        SELECT
          ID
        FROM
          SGD_DRAFT_FOLDER
        WHERE
          DRAFT_FOLDER_STATE = 1 AND
          DRAFT_FOLDER_NAME LIKE '$nameFolder' AND
          DRAFT_USER_CODE = $this->usua_codi AND
          DRAFT_DEPE_CODE = $this->depe_codi";

      $res46 = $this->db->conn->query($sql46);

      if(!$res46->EOF){
        $idFolder = $res46->fields['ID'];
      }
    }

    if(!empty($idFolder)){
      //search draft for id and set new folder id
      $sqlSar = "SELECT COUNT(1) AS EXIST
                  FROM SGD_DRAFT
                 WHERE ID = $idDraft";

      $resSar = $this->db->conn->query($sqlSar);

      if(!$resSar->EOF){
        $sqlU90 = "
          UPDATE SGD_DRAFT SET DRAFT_FOLDER = $idFolder
          WHERE ID = $idDraft";

        $this->db->conn->query($sqlU90);

      }else{
        $sqlU28 = "UPDATE
          SGD_ASSIG-DRAFT
          SET DRAFT_ASIGG_FOLDER =  $idFolder
          WHERE DRAFT_ID = $idDraft";
        $this->db->conn->query($sqlU28);
      }

    }

    return true;
  }

  // change state read or not
  // @param id draft
  // @param string read | unread
  // return boolean true | false
  public function SetStateRead($data){

    $idDraft  = $data['idDraft'];
    if($idDraft){
      $sqlU70 = "
        UPDATE
          SGD_DRAFT
        SET
          DRAFT_READ = 1
        WHERE
        ID = $idDraft and
        DRAFT_USER_CODE = $this->usua_codi and
        DRAFT_USUA_DOC  = $this->usua_doc  and
        DRAFT_DEPE_CODE = $this->depe_codi";

      $oks = $this->db->conn->query($sqlU70);

      $sqlU3 = "
        UPDATE
          SGD_DRAFT_ASSIGNMENT
        SET
          DRAFT_ASSIG_READ = 1
        WHERE
          DRAFT_ASSIG_USER_CODE = $this->usua_codi AND
          DRAFT_ASSIG_DEPE_CODE = $this->depe_codi AND
          DRAFT_ID = $idDraft";

      $resU9 = $this->db->conn->query($sqlU3);

      if($oks || $resU9){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  // change approve state
  // @param id draft
  // @param string discard, approve
  // return boolean true | false
  public function SetApproveState($data){
    $id = $data['idDraft'];
    //DRAFT_ASSIG_STATE
    //0:Activo, 1:Aprobado, 2: Memorando Generado, 3: Descartado
    $sqlU3 = "
      UPDATE
        SGD_DRAFT_ASSIGNMENT
      SET
        DRAFT_ASSIG_READ = 1,
        DRAFT_ASSIG_STATE = 1,
        DRAFT_ASSIG_FOLDER = 1005
      WHERE
        DRAFT_ASSIG_USER_CODE = $this->usua_codi AND
        DRAFT_ASSIG_DEPE_CODE = $this->depe_codi AND
        DRAFT_ASSIG_STATE <= 1 AND
        DRAFT_ASSIG_APPROVALL = 1 AND
        DRAFT_ID = $id";

    $resU9 = $this->db->conn->query($sqlU3);

    //log events
    $this->logDraft($id, 5);

    return true;
  }

  // return all history from draft by id
  // @param id identification draft
  // @return array history draft
  public function GetHistoryById($id){
    return $this->histo;
  }

  // return all comment from draft by id
  // @param id identification draft
  // @return array comment draft
  public function GetCommentById($id){
    return $this->comment;
  }

  // return all logs from draft by id
  // @param id identification draft
  // @return array log draft
  public function GetLogById($id){
    return $this->log;
  }

  // return all folder from system and
  // users
  // @return array log draft
  public function GetFolderByUser(){

    $sqlf = 'SELECT
              ID,
              DRAFT_FOLDER_NAME
             FROM
              SGD_DRAFT_FOLDER
             WHERE
              DRAFT_FOLDER_STATE = 1';

    $res = $this->db->conn->query($sqlf);

    while($res && !$res->EOF ){
      $data['id']    = $res->fields['ID'];
      $data['nodrf'] = 0;
      $data['name']  = $res->fields['DRAFT_FOLDER_NAME'];
      $data['state'] = false;
      $data['type']  = 'person';
      $data['value'] = str_replace(' ', '', strtolower($res->fields['DRAFT_FOLDER_NAME']));

      $this->folder[] = $data;

      $res->MoveNext();
    }

    return $this->folder;
  }

  // create folder
  // @param name new folder
  // @return boolean true | false
  public function SetNewFolder($data){
    if(empty($data['name'])){
      return false;
    };

    $sql1 = " SELECT
                max(id) as ID
              from
                SGD_DRAFT_FOLDER";

    $res1 = $this->db->conn->query($sql1);
    $id1  = $res1->fields['ID'];

    if(empty($id1)){
      $id1 = 1;
    }else{
      $id1++;
    }

    $record["ID"] = $id1;
    $record["DRAFT_USER_CODE"]   = $this->usua_codi;
    $record["DRAFT_DEPE_CODE"]   = $this->depe_codi;
    $record["DRAFT_USUA_DOC"]    = $this->usua_doc;
    $record["DRAFT_FOLDER_NAME"] = "'".$data['name']."'";
    $record["DRAFT_FOLDER_STATE"]= 1;
    $table = 'SGD_DRAFT_FOLDER';
    $ok = $this->db->insert($table, $record, 'true');
    if($ok){
      return true;
    }else{
      return false;
    }
  }

  // delete folder
  // @param name folder to delete
  // @return boolean true | false
  public function deleteFolder($data){
    $id = $data['id'];
    $sql2 = "delete from sgd_draft_folder
             where id = $id";

    $res2 = $this->db->conn->query($sql2);
    if(!$rs->EOF){
      return true;
    }else{
      return false;
    }
  }


  // memo from draft to others users
  // @param data with id draft
  // retun boolean true | false
  public function SendMemo($data){
    $idDraft = $data['idDraft'];
    if($idDraft){
      $_SESSION["draftMemo"] = $idDraft;
      //log events
      $this->logDraft($idDraft, 7);
      return true;
    }
    return false;
  }

  // Discard draft and move the same folder
  // @param data with id draft
  // return boolean true | false
  public function DiscDraft($data){
    $idDraft  = $data['idDraft'];
    if($idDraft){
      $sqlU70 = "
        UPDATE
          SGD_DRAFT
        SET
          DRAFT_READ = 1,
          DRAFT_FOLDER = 1006
        WHERE
         ID = $idDraft";

      $oks = $this->db->conn->query($sqlU70);

      if($oks){

        $sql36 = "
          UPDATE
            SGD_DRAFT_ASSIGNMENT
          SET
            DRAFT_ASSIG_READ = 1,
            DRAFT_ASSIG_STATE = 3,
            DRAFT_ASSIG_FOLDER = 1006
          WHERE
            DRAFT_ID = $idDraft";

        $resU36 = $this->db->conn->query($sql36);

        //log events
        $this->logDraft($idDraft, 2);
        return true;

      }else{
        $sqlU3 = "
          UPDATE
            SGD_DRAFT_ASSIGNMENT
          SET
            DRAFT_ASSIG_READ = 1,
            DRAFT_ASSIG_STATE = 3,
            DRAFT_ASSIG_FOLDER = 1006
          WHERE
            DRAFT_ASSIG_USER_CODE = $this->usua_codi AND
            DRAFT_ASSIG_DEPE_CODE = $this->depe_codi AND
            DRAFT_ID = $idDraft";

        $resU9 = $this->db->conn->query($sqlU3);

        if($resU9){
          //log events
          $this->logDraft($idDraft, 2);
          return true;
        }else{
          return false;
        }
      }
    }
  }

  // send draft to others users
  // @param data with id draft
  // return boolean true | false
  public function SendDraft($data){

    $id2    = $data['idDraft'];
    $table  = 'SGD_DRAFT';
    $table2 = 'SGD_DRAFT_ASSIGNMENT';

    if(empty($id2)){

      $sql2 = " SELECT
                  max(id) as ID
                from
                  SGD_DRAFT";

      $res2 = $this->db->conn->query($sql2);
      $id2  = $res2->fields['ID'];

      if(empty($id2)){
        $id2 = 1;
      }else{
        $id2++;
      }

      $cont = htmlentities($data['content'], ENT_QUOTES, "UTF-8");
      $name = htmlentities($data['name'], ENT_QUOTES, "UTF-8");

      $cont = "'".$cont."'";
      $name = "'".$name."'";

      $record["ID"]                = $id2;
      $record["DRAFT_CREATE_DATE"] = $this->nowDate;
      $record["DRAFT_CONTENT"]     = $cont;
      $record["DRAFT_FOLDER"]      = 1003;
      $record["DRAFT_NAME"]        = $name;
      $record["DRAFT_DEPE_CODE"]   = $this->depe_codi;
      $record["DRAFT_USER_CODE"]   = $this->usua_codi;
      $record["DRAFT_USUA_DOC"]    = $this->usua_doc;
      $record["DRAFT_READ"]        = 0;

      $ok = $this->db->insert($table, $record);

      //log events
      $this->logDraft($id2, 1);

      $this->setSharedDraft($data["apro"],$data["notf"], $id2);

      if($ok){
        return true;
      }else{
        return false;
      }


    }else{
      //save actualy draft
      $this->savePrevDraft($id2);

      $sqlU60 = "
        UPDATE
          SGD_DRAFT
        SET
          DRAFT_READ = 0,
          DRAFT_FOLDER = 1003
        WHERE
         ID = $id2";

      $oks = $this->db->conn->query($sqlU60);

      //log events
      $this->logDraft($id2, 1);

      $this->setSharedDraft($data["apro"],$data["notf"], $id2);

      if($oks){
        return true;
      }else{
        return false;
      }
    }
  }

  private function setSharedDraft($appr, $notf, $idDraft){

    $table2 = 'SGD_DRAFT_ASSIGNMENT';

    foreach ($appr as $lue) {
      $log = explode("-", $lue);
      $login = trim($log[0]);

      $sqlU1 = "SELECT
        USUA_CODI,
        DEPE_CODI
        FROM
        USUARIO
        WHERE
        USUA_LOGIN LIKE '$login'";

      $resU1 = $this->db->conn->query($sqlU1);
      $usua = $resU1->fields['USUA_CODI'];
      $depe = $resU1->fields['DEPE_CODI'];

      $sql4 = " SELECT
        max(id) as ID
        from
        SGD_DRAFT_ASSIGNMENT";

      $res4 = $this->db->conn->query($sql4);
      $id4  = $res4->fields['ID'];

      if(empty($id4)){
        $id4 = 1;
      }else{
        $id4++;
      }

      //DRAFT_ASSIG_STATE
      //0:Activo, 1:Aprobado, 2: Memorando Generado, 3: Descartado
      $record1["ID"]                    = $id4;
      $record1["DRAFT_ASSIG_USER_CODE"] = $usua;
      $record1["DRAFT_ASSIG_DEPE_CODE"] = $depe;
      $record1["DRAFT_ASSIG_STATE"]     = 0;
      $record1["DRAFT_ID"]              = $idDraft;
      $record1["DRAFT_ASSIG_APPROVALL"] = 1;
      $record1["DRAFT_ASSIG_DATE"]      = $this->nowDate;
      $record1["DRAFT_ASSIG_READ"]      = 0;
      $record1["DRAFT_ASSIG_FOLDER"]    = 1000;

      //log events
      $this->logDraft($idDraft, 8, $login);

      $ok1 = $this->db->insert($table2, $record1);

    }


    foreach ($notf as $val) {

      $log = explode("-", $val);
      $login = trim($log[0]);

      $sqlU2 = "SELECT
        USUA_CODI,
        DEPE_CODI
        FROM
        USUARIO
        WHERE
        USUA_LOGIN LIKE '$login'";

      $resU2 = $this->db->conn->query($sqlU2);
      $usua = $resU2->fields['USUA_CODI'];
      $depe = $resU2->fields['DEPE_CODI'];

      $sql3 = " SELECT
        max(id) as ID
        from
        SGD_DRAFT_ASSIGNMENT";

      $res3 = $this->db->conn->query($sql3);
      $id3  = $res3->fields['ID'];

      if(empty($id3)){
        $id3 = 1;
      }else{
        $id3++;
      }

      //DRAFT_ASSIG_STATE
      //0:Activo, 1:Aprobado, 2: Memorando Generado, 3: Descartado
      $record2["ID"]                    = $id3;
      $record2["DRAFT_ASSIG_USER_CODE"] = $usua;
      $record2["DRAFT_ASSIG_DEPE_CODE"] = $depe;
      $record2["DRAFT_ASSIG_STATE"]     = 0;
      $record2["DRAFT_ID"]              = $idDraft;
      $record2["DRAFT_ASSIG_APPROVALL"] = 0;
      $record2["DRAFT_ASSIG_DATE"]      = $this->nowDate;
      $record2["DRAFT_ASSIG_READ"]      = 0;
      $record2["DRAFT_ASSIG_FOLDER"]    = 1001;

      //log events
      $this->logDraft($idDraft, 9, $login);

      $ok2 = $this->db->insert($table2, $record2);
    }
  }


  private function savePrevDraft($idDraft){
    $table = 'SGD_DRAFT_VERSION';

    $sql = " SELECT
      max(id) as ID
      from
      SGD_DRAFT_VERSION";

    $res71 = $this->db->conn->query($sql);
    $id71  = $res71->fields['ID'];

    if(empty($id71)){
      $id71 = 1;
    }else{
      $id71++;
    }

    $sql39  = "
      SELECT
        SD.DRAFT_CONTENT,
        SD.DRAFT_NAME
      FROM
        SGD_DRAFT SD
      WHERE
        SD.ID = $idDraft";

    $res39 = $this->db->conn->query($sql39);

    if(!$res39->EOF){
      $data = $res39->fields['DRAFT_CONTENT'];
    }

    $data = "'".$data."'";

    $record26["ID"]                  = $id71;
    $record26["DRAFT_ID"]            = $idDraft;
    $record26["DRAFT_VER_USER_CODE"] = $this->usua_codi;
    $record26["DRAFT_VER_DEPE_CODE"] = $this->depe_codi;
    $record26["DRAFT_VER_CONTENT"]   = $data;
    $record26["DRAFT_VER_DATE"]      = $this->nowDate;

    //log events
    $this->logDraft($idDraft, 10);

    $this->db->insert($table, $record26);
  }


  private function logDraft($idDraft, $noTx, $obser){
    $table = 'SGD_DRAFT_LOG';
    //salvamos la transaccion sobre el borrador seleccionado
    $sql71 = " SELECT
      max(id) as ID
      from
      SGD_DRAFT_LOG";

    $res71 = $this->db->conn->query($sql71);
    $id71  = $res71->fields['ID'];

    if(empty($id71)){
      $id71 = 1;
    }else{
      $id71++;
    }

    $record20["ID"] = $id71;
    $record20["DRAFT_ID"] = $idDraft;
    $record20["DRAFT_LOG_USUA_CODE"] = $this->usua_codi;
    $record20["DRAFT_LOG_DEPE_CODE"] = $this->depe_codi;
    $record20["DRAFT_LOG_TX"] = $noTx;
    $record20["DRAFT_LOG_DATE"] = $this->nowDate;
    $record20["DRAFT_LOG_OBSER"] = "'".$obser."'";

    $this->db->insert($table, $record20);
  }
}
?>
