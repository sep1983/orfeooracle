<?php
session_start();
$ruta_raiz = "..";

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$host  = $_SERVER['HTTP_HOST'];
$data  = array();
if(NULL === $_SESSION['dependencia']){
  die('La sesion no exite o expiro');
}

include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");

//get the HTTP method, path and body of the request
$method   = $_SERVER['REQUEST_METHOD'];
$depeCodi = $_SESSION["dependencia"];
$usuaCodi = $_SESSION["codusuario"];

if(empty($_SERVER['PATH_INFO'])){
  die('No existen datos a prosesar en la consulta');
}
$salida  = explode('/', trim($_SERVER['PATH_INFO']));

//remove first element space
array_shift($salida);

$nameField = $salida[0];
$id = $salida[1];

$id = preg_replace('/[^a-zA-Z0-9]/', '', $id);
$nameField = preg_replace('/[^a-zA-Z]/', '', $nameField);

if(empty($nameField)){
  die('No exite o no digito parametro de trabajo');
}

?>
