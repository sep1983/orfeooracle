<?php
session_start();

if($_SESSION["krd"]){
  $krd = $_SESSION["krd"];
}

$ruta_raiz = "..";
if (!$_SESSION['dependencia']){
    include "$ruta_raiz/rec_session.php";
}
?>

<!DOCTYPE html>
<html lang="es">

<head>

  <!-- Meta -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Draft Document</title>

  <!-- Styles -->
  <link rel="stylesheet" href="../estilos/elusive-icons.css">
  <link rel="stylesheet" href="../estilos/bulma.css">
  <link rel="stylesheet" href="../estilos/vue-multiselect.min.css">
  <link rel="stylesheet" href="../estilos/borradores.css">

  <!-- Scripts -->
  <script src="../ckeditor/ckeditor.js"></script>
  <script src="../js/vue.js"></script>
  <script src="../js/vue-multiselect2.0.0.js"></script>
  <script src="../js/vue-resource1.3.4.js"></script>
</head>

<body>
  <div id="draf-load" class="columns is-gapless">
    <div class="column is-narrow history">
      <div id="actions" v-on:click="addHistory">
        <div class="columns is-gapless">
          <div class="column">
            <a
              class="button is-white"
              v-on:click="valNewDraft">
              <i class="el el-record"></i> Nuevo
            </a>
            <a
              v-if=save
              class="button is-white"
              v-on:click="valSaveDraft">
              <i class="el el-record"></i> Guardar
            </a>
            <a class="button is-white"
              v-if=showSend
              v-on:click="sendDraft">
              <i class="el el-envelope"></i> Enviar
            </a>
          </div>
          <div class="column">
            <div class="field has-addons-centered">
              <span v-if=idDraft class="tag is-dark">No: {{idDraft}}</span>

              <a class="button"
                v-if=discarded
                v-on:click="discardDraft">
                <i class="el el-trash"></i> Descartar
              </a>
               <a class="button"
                v-if=forApproved
                v-on:click="approve">
                <i class="el el-ok"></i> Aprobar
              </a>
              <a class="button is-success"
                v-if=approved
                v-on:click="createMemo">
                <i class="el el-ok"></i> Crear Memorando
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id="dataTosend">
        <div class="columns">
          <div class="column">
            <div class="field">
              <label>Para Aprobaci&oacute;n</label>
              <p class="control has-icons-left has-icons-right">
                <multiselect
                  v-model="selectedUsersAprob"
                  :options="usersAprob"
                  :searchable="true"
                  :close-on-select="true"
                  :show-labels="true"
                  :multiple="true"
                  @search-change="asyncFind"
                  placeholder="Seleccione el usuario">
                  <span slot="noResult">Digite un nombre para iniciar la b&uacute;squeda.</span>
                </multiselect>
              </p>
              <p v-if="errorappob" class="help is-danger">* Seleccione un usuario</p>
            </div>
          </div>
          <div class="column">
            <div class="field">
              <label>Para Notificaci&oacute;n</label>
              <p class="control has-icons-left has-icons-right">
                <multiselect
                  v-model="selectedUsersNotf"
                  :options="usersNotf"
                  :searchable="true"
                  :close-on-select="true"
                  :show-labels="true"
                  :multiple="true"
                  @search-change="asyncFindNotf"
                  placeholder="Seleccione el usuario">
                  <span slot="noResult">Digite un nombre para iniciar la b&uacute;squeda.</span>
                </multiselect>
              </p>
            </div>
          </div>
        </div>
        <div class="columns">
          <div class="column">
            <div class="field">
              <label>Nombre del borrador</label>
              <div class="control">
                <input
                  class="input"
                  v-model.trim="nameDraft"
                  type="text"
                  placeholder="Nombre para identificar el borrador">
              </div>
              <p v-if="errorname" class="help is-danger">* Minimo 20 caracteres</p>
            </div>
          </div>
          <div class="column">
            <div class="field has-addons-centered">
              &nbsp;
              <p class="control">
                <a
                   v-on:click="showTab(1)"
                   class="button is-white">
                  <span class="icon">
                    <i v-if="!show_1" class="el el-chevron-up"></i>
                    <i v-if="show_1" class="el el-chevron-down"></i>
                  </span>
                  <span>Comentarios</span>
                </a>
                <a
                   v-on:click="showTab(2)"
                   class="button is-white">
                  <span class="icon">
                    <i v-if="!show_2" class="el el-chevron-up"></i>
                    <i v-if="show_2" class="el el-chevron-down"></i>
                  </span>
                  <span>Historico</span>
                </a>
                <a
                   v-on:click="showTab(3)"
                   class="button is-white">
                  <span class="icon">
                    <i v-if="!show_3" class="el el-chevron-up"></i>
                    <i v-if="show_3" class="el el-chevron-down"></i>
                  </span>
                  <span>Log</span>
                </a>
              </p>
            </div>
          </div>
        </div>


        <div v-if="show_1" class="box boxMessage">
          <h6 class="title is-5">Comentarios</h6>
          <ul>
            <li v-for="item in comments" :key="item.id">
              <div class="titleDraft"><span class="date">{{item.date}}</span>
               <span class="tag is-info"> {{item.user}}</span> {{item.comment}}
              </div>
            </li>
          </ul>
        </div>

        <div v-if="show_2" class="box boxMessage">
          <h5 class="title is-5">Historico</h5>
          <ul>
            <li v-for="item in hist"
                :key="item.id"
                v-on:click="getHistory(item.id)">
              <div class="titleDraft">
                <span class="date">
                  <i class="el el-file"></i>
                  {{item.id}} - {{item.date}}
                </span>
                <span class="tag is-info"> {{item.user}}</span>
              </div>
            </li>
          </ul>
        </div>

        <div v-if="show_3" class="box boxMessage">
          <h5 class="title is-5">Log de Eventos</h5>
          <ul>
            <li v-for="item in logs" :key="item.id">
              <div class="titleDraft"><span class="date">{{item.id}} - {{item.date}}</span>
               <span class="tag is-info">{{item.user}}</span> : {{item.tx}} {{item.obser}}

              </div>
            </li>
          </ul>
        </div>


        <p v-if="errorcont" class="help is-danger">* El documento esta sin datos</p>
        <div class="container">
          <textarea id='textrich'>
            <p>(Escribir Ciudad) </p>
            <p><b>PARA:</p>
            <p><b>DE:</p>
            <p><b>ASUNTO:</p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>

            <p>Cordialmente, </p>
            <p>NOMBRE DEL FUNCIONARIO AUTORIZADO</p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
            <p>Anexos:
            <br>Copias:
            <br>Anexos:
            <br>Proyect&oacute;:
            <br>Elabor&oacute;:
            <br>Revis&oacute;:
            <br>Fecha de elaboraci&oacute;n:
            <br>Tipo de respuesta Total (  ) Parcial(  )
            </p>
          </textarea>
        </div>
        <div v-if=idDraft class="box">
          <div class="field is-grouped">
            <p class="control is-expanded">
              <input v-model='inputComment'
                    class="input"
                    type="text"
                    placeholder="Dejar un comentario">
            </p>
            <p
              v-on:click="addComent"
              class="control">
              <a class="button">
                <span class="icon">
                  <i class="el el-envelope"></i>
                </span>
                <span>Enviar</span>
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="column is-narrow folder">
      <div id="search-by-name">
        <span class="textprev">
          <input
                  placeholder="Nombre"
                  v-model="searchbyname"
                  id="elem528"
                  type="text">
          <i class="el el-search el-lg"></i>
        </span>
      </div>


      <div>
        <div class="title is-5">
          Cooperativos
        </div>
        <ul>
          <li v-for="item in folderFilterSystem" v-on:click="selectFolder(item.id)" :key="item.id" v-bind:class="{ folderSelect: item.state  }">
            <p>
            <i class="el el-envelope el-fw"></i> {{item.name}}
            <b v-if=item.nodrf>{{item.nodrf}}</b>
            </p>
          </li>
        </ul>
        <!--
        <div class="title is-5">
          Personales
          <span v-on:click="showCreateFolder()" class="iconfolder">
          <i class="el el-plus"></i>
          </span>
        </div>
        <ul>
          <li v-for="item in folderFilterPerson" v-on:click="selectFolder(item.id)" :key="item.id" v-bind:class="{ folderSelect: item.state  }">
            <p>
              <i class="el el-envelope el-fw"></i> {{item.name}}<span v-if="item.nodrf" class="tag is-warning">{{item.nodrf}}</span>
              <span v-on:click="deleteFolder(item.id)" class="iconfolder">
                <i class="el el-minus"></i>
              </span>
              <b v-if=item.nodrf>{{item.nodrf}}</b>
            </p>
          </li>

          <li class="inputNewFolder" v-bind:class="{ newFolder: stateNewFolder }">
            <input type='text' placeholder="Nombre" v-model="nameNewFolder">
            <span v-on:click="hideCreateFolder()">
              <i class="el el-trash"></i>
            </span>
            <span v-on:click="createFolder()">
              <i class="el el-ok"></i>
            </span>
          </li>
        </ul>
        -->
      </div>
    </div>

    <div class="column is-gapless is-fullwidth draftlist ">
      <div class="generalMenu">
        <span class=""> Actulizaci&oacute;n: {{time}} </span>
        <p v-if=process>
          <i class="el el-cog el-spin el-fw el-1x"></i>
          <span class=""> Procesando ....  </span>
        </p>
      </div>
      <div class="filterdraft">
        <div class="dropdown">
          <div class="filterbutton">
            <a class="button is-small is-white">
              <span class="icon">
                <i class="el el-chevron-down"></i>
              </span>
              <span>Marcar</span>
            </a>
          </div>
          <div class="dropdown-content">
            <div class='titleFilter'>Marcar</div>
            <a v-on:click="markDraft('Sin leer')">Sin leer</a>
            <a v-on:click="markDraft('Leidos')">Leidos</a>
          </div>
        </div>
        <div class="dropdown">
          <div class="filterbutton">
            <a class="button is-small is-white">
              <span class="icon">
                <i class="el el-chevron-down"></i>
              </span>
              <span>Filtrar</span>
            </a>
          </div>
          <div class="dropdown-content">
            <div class='titleFilter'>Filtrar </div>
            <a v-on:click="filterKey = 'Todos'">Todos</a>
            <a v-on:click="filterKey = 'Sin Leer'">Sin leer</a>
            <a v-on:click="filterKey = 'Leidos'">Leidos</a>
          </div>
        </div>
        <!--
        <div class="dropdown">
          <div class="filterbutton">
            <a class="button is-small is-white">
              <span class="icon">
                <i class="el el-chevron-down"></i>
              </span>
              <span>Mover</span>
            </a>
          </div>

          <div class="dropdown-content">
            <div class='titleFilter'>Mover </div>
            <a v-for="folder in systemFolder" :key="folder.id" v-on:click="moveDraft(folder.value)">{{folder.name}}</a>
          </div>

        </div>
        -->
      </div>

      <div class="stateData">
        <input class="allselect" v-bind:class="{ inputMark: inputMark }" v-model="markAllInput" v-on:change="markInputAll" type="checkbox" />
        <span>
            <i class="el el-folder-open"></i>
            {{nameNewFolder}}
          </span>
        <br>
        <span>
          <i class="el el-filter"></i>
            {{filterKey}}
        </span>
        <span v-if=searchbyname>
            <i class="el el-filter"></i>
            {{searchbyname}}
        </span>
      </div>

      <ul class="draft">
        <li v-for="item in draftFilter" :key="item.id" v-bind:class="{ draftselect: item.isSelect, draftselectRead: item.read }">
          <div class='highlight'></div>
          <input type="checkbox" class='inputHidden' v-model='item.isChecked' v-on:change="showAllinput(item.isChecked)" v-bind:class="{ inputMark: inputMark ,draftselect: item.isSelect }" />
          <p v-on:click="selectDraft(item.id)">
            <span class='titleDraft'>
              <span class="tag is-black">{{ item.id }}</span>
              <span class="tag is-white">{{ item.user }}</span>
            </span>
            <span class='segline'>
              {{ item.name }}
             </span>
             <span class='date'>
                {{ item.date }}
             </span>
            <span class='thiline'>
              {{ item.depe }}
            </span>
          </p>
        </li>
      </ul>

      <!-- Alert accions -->
      <div v-if=showAlert  class="notification alertPlace" v-bind:class=[alertType]>
        <button  v-on:click="closeMess" class="delete"></button>
        {{alertText}}
      </div>
      <!-- Alert accions -->

    </div>
  </div>

  <!-- Scripts -->
  <script src="../js/ckeditor-conf.js"></script>
  <script src="./js/borradores.js"></script>
</body>

</html>
