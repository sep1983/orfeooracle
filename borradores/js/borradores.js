Vue.component('Multiselect', VueMultiselect.default)
var vm = new Vue({

 // Folder code
 // name  => 'Por Aprobación',
 // id    => 1000,
 // name  => 'Notificados',
 // id    => 1001,
 // name  => 'Enviados',
 // id    => 1003,
 // name  => 'Guardados',
 // id    => 1004,
 // name  => 'Aprobados',
 // id    => 1005,
 // name  => 'Descartados',
 // id    => 1006,
 // name  => 'Memorandos',
 // id    => 1007,

  el: "#draf-load",

  data: {

    idDraft: '',
    idFolder: '',

    filterKey: "Todos",
    inputAll: false,
    inputMark: false,
    markAllInput: false,
    stateNewFolder: false,
    nameNewFolder: 'Aprobaci\u00F3n',
    nameDraft:'',
    searchbyname: '',
    value: '',
    systemFolder: [],
    draft: [],
    hist: [],
    comments: [],
    logs: [],
    inputComment:'',

    save: true,
    showSend: true,
    discarded: false,
    forApproved: false,
    approved: false,

    show_1: false,
    show_2: false,
    show_3: false,

    usersAprob: [],
    isLoading: false,
    selectedUsersAprob: [],

    usersNotf: [],
    isLoadingN: false,
    selectedUsersNotf: [],


    errorappob: false,
    errorname: false,
    errorcont: false,

    process: false,
    showAlert: false,
    alertType: '',
    alertText: '',

    timer: '',
    time: ''

  },

  created() {
    this.loadDraft();
    this.timer = setInterval(this.loadDraft, 300000)
  },

  computed: {

    folderFilterSystem: function (){
      return this.systemFolder.filter(function(element){
        return element.type === 'system';
      });
    },

    folderFilterPerson: function (){
      return this.systemFolder.filter(function(element){
        return element.type === 'person';
      });
    },

    draftFilter: function() {

      var folder = 1000; // Folder was defined in class_draff

      this.systemFolder.forEach(function(element, index, array) {
        if (element.state) {
          folder = element.id;
        }
      });

      switch (this.filterKey) {
        case "Leidos":
          return this.draft.filter(function(tempDraft){
            return tempDraft.folder === folder;
          }).filter(function(tempDraft) {
            return tempDraft.read === true;
          }).filter(function(tempDraft){
             return this.filterByName(tempDraft);
          },this);
          break;
        case "Sin Leer":
          return this.draft.filter(function(tempDraft){
            return tempDraft.folder === folder;
          }).filter(function(tempDraft) {
            return tempDraft.read === false;
          }).filter(function(tempDraft){
             return this.filterByName(tempDraft);
          },this);
          break;
        default:
          return this.draft.filter(function(tempDraft){
            return tempDraft.folder === folder;
          }).filter(function(tempDraft){
             return this.filterByName(tempDraft);
          },this);
      }
    }
  },

  // define methods under the `methods` object
  methods: {

    loadDraft: function (){
      var urlDraft  = './apiDraft.php/draft';
      var urlFolder = './apiFolder.php/folder';
      var messok    = 'Datos recargados';
      var idFol     = this.idFolder;
      var currentTime = new Date(),
      day = currentTime.getDate(),
      month = currentTime.getMonth() + 1,
      year = currentTime.getFullYear();
      hours = currentTime.getHours(),
      minutes = currentTime.getMinutes();

      if (minutes < 10) {
        minutes = "0" + minutes;
      }

      this.time = day + "/" + month + "/" + year + " " + hours + ":" + minutes;

      //get data names, folders.
      this.$http.get(urlFolder).then(response => {
        // get body data
        this.systemFolder = response.body;
        //get draft from user, .
        this.$http.get(urlDraft).then(response => {
          // get body data
            var valData = response.body;
            var draftInFolder = {};

            if(valData != null){
              valData.forEach(function(element, index, array) {
                array[index].isSelect = false;
                array[index].isChecked = false;
                var lor = draftInFolder[element['folder']];
                draftInFolder[element['folder']] = lor == undefined? 1 : lor + 1;
              });

              this.draft = valData;
            }else{
              this.draft = [];
            }

            this.systemFolder.forEach(function(element, index, array) {
              var dat = draftInFolder[element['id']];
              if(dat != undefined){
                element['nodrf'] = dat;
              }
            });

            if(idFol){
              this.selectFolder(idFol);
            }

        }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlDraft);
        });
      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', urlFolder);
      });

      this.alTt(2,messok);
    },

    asyncFind: function (query) {

      var urlUserSearch = './apiUserSearch.php/user/' + query;
      this.isLoading = true;

      if(query.match(/[A-Za-z0-9]/)){
        this.$http.get(urlUserSearch).then(response => {
          // get body data
            var valData = response.body;
            this.usersAprob = valData;
            this.isLoading = false
        }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlUserSearch);
          this.isLoading = false
        });
      }

    },

    asyncFindNotf: function (query) {

      var urlUserSearch = './apiUserSearch.php/user/' + query;
      this.isLoading = true;

      if(query.match(/[A-Za-z0-9]/)){
        this.$http.get(urlUserSearch).then(response => {
          // get body data
            var valData = response.body;
            this.usersNotf = valData;
            this.isLoadingN = false
        }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlUserSearch);
          this.isLoading = false
        });
      }

    },

    closeMess: function(){
      this.showAlert = false;
      this.alertType = '';
      this.alertText = '';
    },

    alTt: function(noTypeAlert, text){

      var typeAlert = [
        'is-primary',
        'is-link',
        'is-info',
        'is-success',
        'is-warning',
        'is-danger'
      ];

      this.alertType = typeAlert[noTypeAlert];
      this.alertText = text;
      this.showAlert = true;
      this.process = false;

      window.setTimeout(this.closeMess, 2000);
    },

    createMemo: function(){
      //url send draft
      var urlSendMemo = './apiDraft.php/memo/';
      //Id Draft if it was select
      let arrayData = {'idDraft': this.idDraft}

      this.$http.post(urlSendMemo, arrayData).then(response => {
        // get body data
        if(response.status == 200 &&
            response.bodyText == 'true'){
          window.location = "../memorapido";
        };
      }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlSendMemo);
          this.process = false;
      });

      //Reload draft
      this.loadDraft();
    },

    discardDraft: function(){
      //url send draft
      var urlDiscDraft = './apiDraft.php/disc/';
      var messbadU = 'No se pudo descartar el borrador';
      let arrayData = {'idDraft': this.idDraft}
      this.$http.post(urlDiscDraft, arrayData).then(response => {
        var messok = 'Se envi\u00F3 el borrador para ser descartado';
        this.alTt(3,messok);
      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', urlDiscDraft);
        this.alTt(5,messbadU);
      });

      //Reload draft
      this.loadDraft();
    },

    sendDraft: function(){
      //url send draft
      var urlSendDraft = './apiDraft.php/send/';
      //Users who approve
      var usariosAprov = this.selectedUsersAprob;
      //Notified users
      var usariosNotif = this.selectedUsersNotf;
      //Name Draft
      var nameDraft = this.nameDraft;
      //Id Draft if it was select
      var idDraft = this.idDraft;
      //Content text
      var content = this.value;
      var error = false;
      var messbadU = 'No se pudo enviar el documento';

      if( usariosAprov == undefined || usariosAprov.length == 0){
        this.errorappob = true;
        error = true;
      }else{
        this.errorappob = false;
      }

      if(nameDraft == undefined || nameDraft.length < 10){
        this.errorname = true;
        error = true;
      }else{
        this.errorname = false;
      }

      if(content == undefined || content.length < 10){
        this.errorcont = true;
        error = true;
      }else{
        this.errorcont = false;
      }

      if(error){
        return
      }

      this.process = true;
      let arrayData = {'content': content,
                        'name': nameDraft,
                        'apro': usariosAprov,
                        'notf': usariosNotif,
                        'idDraft': idDraft}

      this.$http.post(urlSendDraft, arrayData).then(response => {
        var messok = 'Se envi\u00F3 el borrador para se revisado y aprobado';
        // get body data
        if(response.status == 200 &&
            response.body == true){

            this.errorappob = false;
            this.errorname = false;
            this.errorcont = false;
            this.showSend = false;

            this.alTt(3,messok);
            this.valNewDraft();
        }else{
          this.alTt(5,messbadU);
        };
      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', urlSendDraft);
        this.alTt(5,messbadU);
      });

      //Reload draft
      this.loadDraft();
    },


    valSaveDraft: function(){
      //url send new draft
      var urlNewDraft = './apiDraft.php/save/';
      //Name Draft
      var nameDraft = this.nameDraft;
      //Content text
      var content = this.value;
      var error = false;
      //Id Draft if it was select
      var idDraft = this.idDraft;
      var messbadU = 'No se pudo guardar el documento';
      var messbadC = 'Se genero un error al procesar la url:';

      if(nameDraft.length < 10){
        this.errorname = true;
        error = true;
      }else{
        this.errorname = false;
      }

      if(content.length < 10){
        this.errorcont = true;
        error = true;
      }else{
        this.errorcont = false;
      }

      if(error){
        return
      }

      let arrayData = {'content': content,
                       'name': nameDraft,
                       'idDraft': idDraft}

      this.process = true;
      this.$http.post(urlNewDraft, arrayData).then(response => {
        var data = JSON.parse(response.bodyText);
        var messok = 'Se guardo el borrador';
        // get body data
        if(response.status == 200){
          this.idDraft = data['idDraft'];
          this.discarded = true;
          this.alTt(3,messok);
        }
      }, response => {
        this.alTt(5,messbadU);
        console.error(messbadC, urlNewDraft);
      });
      //Reload draft
      this.loadDraft();
    },

    valNewDraft: function(){
      this.idDraft = '';
      this.showSend = true;
      this.save = true;
      this.forApproved = false;
      this.nameDraft = '';
      this.selectedUsersAprob = [];
      this.selectedUsersNotf = [];
      this.draft.forEach(function(element, index, array) {
        Vue.set(element, "isSelect", false);
      });

      var plant = "<p>(Escribir Ciudad) </p>\
            <p><b>PARA:</p>\
            <p><b>DE:</p>\
            <p><b>ASUNTO:</p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p>Cordialmente, </p>\
            <p>NOMBRE DEL FUNCIONARIO AUTORIZADO</p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p></p>\
            <p>Anexos:\
            <br>Copias:\
            <br>Anexos:\
            <br>Proyect&oacute;:\
            <br>Elabor&oacute;:\
            <br>Revis&oacute;:\
            <br>Fecha de elaboraci&oacute;n:\
            <br>Tipo de respuesta  Total (  ) Parcial(  )\
            </p>";

      CKEDITOR.instances['textrich'].setData(plant);
    },

    showTab: function(noTab) {
      switch (noTab) {
        case 1:
          this.show_1 = !this.show_1;
          this.show_2 = false;
          this.show_3 = false;
          break;
        case 2:
          this.show_1 = false;
          this.show_2 = !this.show_2;
          this.show_3 = false;
          break;
        case 3:
          this.show_1 = false;
          this.show_2 = false;
          this.show_3 = !this.show_3;
      }
    },

    approve: function(){
      const urlapprove = './apiDraft.php/approve/';
      var arrayData = {'idDraft': this.idDraft}
      var messbadU = 'No se pudo aprobar el borrador';

      this.process = true;
      this.$http.post(urlapprove,  arrayData).then(response => {
        var messok = 'Se aprob\u00F3  el borrador';
        // get body data
        if(response.status == 200 &&
          response.bodyText == 'true'){
          this.alTt(3,messok);
        }else{
          this.alTt(5,messbadU);
        };
      }, response => {
        // error callback
        this.alTt(5,messbadU);
        console.error('Se genero un error al procesar la url:', urlapprove);
      });
      //Reload draft
      this.loadDraft();
    },

    addComent: function () {
      const incom = this.inputComment;
      const d = Date.now();
      const da = new Date(d).toLocaleString().slice(0,9);
      //url send new draft
      const urlNewComment = './apiComment.php/new/';
      var messbadU = 'No se pudo agregar el comentario';

      var tempObject = {};
      tempObject.id = d;
      tempObject.user = 'Usuario temporal';
      tempObject.date = da;
      tempObject.comment = this.inputComment;

      if(incom.length > 10 && incom.length <= 301 ){
        this.comments.unshift(tempObject);
      }

      var arrayData = {'content': this.inputComment,
                       'idDraft': this.idDraft}

      this.process = true;
      this.$http.post(urlNewComment, arrayData).then(response => {
        var messok = 'Se agrego el comentario';
        // get body data
        if(response.status == 200 &&
          response.bodyText == 'true'){
          this.alTt(3,messok);
        }else{
          this.alTt(5,messbadU);
        };
      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', urlNewDraft);
        this.alTt(5,messbadU);
      });

      this.showComm = true;
      this.inputComment = '';
    },


    filterByName: function(tempDraft){
      var lowname = tempDraft.name.toLowerCase();
      var lowsear = this.searchbyname.toLowerCase();
      var patt = new RegExp(lowsear);

      if(lowsear.length === 0){
        return true;
      }else{
        return patt.test(lowname);
      }
    },

    showCreateFolder: function() {
      this.stateNewFolder = true;
    },

    hideCreateFolder: function() {
      this.stateNewFolder = false;
      this.nameNewFolder = '';
    },

    createFolder: function(){
      var urlFolderNew = './apiFolder.php/create/';
      var d = new Date();
      var name = this.nameNewFolder.replace(/[^a-zA-Z ]/g, "")
      var id = d.getTime();
      var messbadU = 'No se pudo crear la carpeta';
      var arrayData = { name: this.nameNewFolder,
                   state:false,
                   type: 'person',
                   id: id,
                   value:name};

      this.process = true;
      this.$http.post(urlFolderNew,  arrayData).then(response => {
        var messok = 'Se creo la carpeta correctamente';
        // get body data
        if(response.status == 200 &&
            response.bodyText == 'true'){
            this.systemFolder.push(arrayData);
            this.stateNewFolder = false;
            this.nameNewFolder = '';
            this.alTt(3,messok);
        }else{
          this.alTt(5,messbadU);
        };
      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', nameNewFolder);
        this.alTt(5,messbadU);
      });
    },

    deleteFolder: function(id) {
      var urlFolderDelete = './apiFolder.php/delete/';
      var arrayData = {}
      var messbadU = 'No se pudo borrar la carpeta, No debe tener borradores';
      this.systemFolder.forEach(function(element, index, array) {
        if (element.id === id) {
          arrayData.id = id;
          this.$http.post(urlFolderDelete,  arrayData).then(response => {
            var messok = 'Se borro la carpeta';
            // get body data
            if(response.status == 200 &&
                response.bodyText == 'true'){
              array.splice(index, 1);
              this.alTt(3,messok);
            }else{
              this.alTt(5,messbadU);
            };
          }, response => {
            // error callback
            console.error('Se genero un error al procesar la url:', urlFolderDelete);
            this.alTt(5,messbadU);
          });
        }
      },this);
    },

    moveDraft: function(folder){
      var arrayIdDraft = [];
      var idfolder = '';
      const urlSetFolderDraft = './apiDraft.php/folder/';
      var messbadU = 'No se pudo cambiar de carpeta el borradores';

      this.systemFolder.forEach(function(element, index, array) {
        if (element.name == folder) {
          idfolder = element.id;
        }
      });

      this.draft.forEach(function(element, index, array) {
        if (element.isChecked) {
          arrayIdDraft.push(element.id);
          Vue.set(array[index], "folder", idfolder);
        }
      });

      if(  arrayIdDraft.length > 0 ){
        var arrayData = {'idsDraft': arrayIdDraft,
                          'folder': folder}

        this.process = true;
        this.$http.post(urlSetFolderDraft, arrayData).then(response => {
          var messok = 'Se cambio el borrador de carpeta';
          // get body data
          if(response.status == 200 &&
              response.bodyText == 'true'){
            this.alTt(3,messok);
          }else{
            this.alTt(5,messbadU);
          };
        }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlSetFolderDraft);
          this.alTt(5,messbadU);
        });
      }

    },

    markInputAll: function() {
      const allInput = this.markAllInput;
      this.inputMark = allInput;

      this.draftFilter.forEach(function(element, index, array) {
        element.isChecked = allInput;
      });
    },

    showAllinput: function(e) {
      var tmpState = false;
      this.draft.forEach(function(element) {
        if (element.isChecked) {
          tmpState = true;
        }
      });
      this.inputMark = tmpState;
    },

    markDraft: function(mark) {
      var arrayIdDraft = [];
      var arrayData = {};
      var urlStateRead = './apiDraft.php/stateRead/';
      switch (mark) {
        case "Leidos":
          this.draft.forEach(function(element, index, array) {
            if (element.isChecked) {
              arrayIdDraft.push(element.id);
              Vue.set(array[index], "read", true);
            }
            arrayData.read = true;
          });
          break;
        case "Sin leer":
          this.draft.forEach(function(element, index, array) {
            if (element.isChecked) {
              arrayIdDraft.push(element.id);
              Vue.set(array[index], "read", false);
            }
            arrayData.read = false;
          });
          break;
      }

      if(arrayIdDraft.length > 0){

        arrayData.idDraft = arrayIdDraft;

        this.$http.post(urlStateRead, arrayData).then(response => {
          // get body data
          if(response.status == 200 &&
              response.bodyText == 'true'){
          };
        }, response => {
          // error callback
          console.error('Se genero un error al procesar la url:', urlStateRead );
        });
      }

    },

    selectFolder: function(id) {
      var folderSelected = '';
      this.systemFolder.forEach(function(element, index, array) {
        if (element.id === id) {
          Vue.set(array[index], "state", true);
          folderSelected = element.name;
        } else {
          Vue.set(array[index], "state", false);
        }
      });
      this.nameNewFolder = folderSelected;
      this.idFolder = id;
    },

    selectDraft: function(id) {
      var urlStateReadDraft = './apiDraft.php/stateRead/';
      var urlgetDraftByID = './apiDraft.php/idDraft/' + id;
      var folder= '';

      this.draft.forEach(function(element, index, array) {
        if (element.id === id) {
          let arrayData = {'idDraft': id}
          folder = element.folder;
          this.idDraft = id;
          Vue.set(array[index], "isSelect", true);
          Vue.set(array[index], "read", true);

          this.$http.post(urlStateReadDraft, arrayData).then(response => {
            // get body data
            if(response.status == 200 &&
                response.bodyText == 'true'){
              this.idDraft = id;
            };
          }, response => {
            // error callback
            console.error('Se genero un error al procesar la url:', urlStateReadDraft);
          });
        } else {
          array[index].isSelect = false;
        }
      },this);


      //get data comment from specific draft.
      this.$http.get(urlgetDraftByID).then(response => {
        // get body data
        this.approved  = response.body.genMemo;
        this.nameDraft = response.body.name;
        this.value     = response.body.content;
        this.comments  = response.body.comment;
        this.hist      = response.body.history;
        this.logs      = response.body.log;
        this.selectedUsersAprob = response.body.aprob;
        this.selectedUsersNotf = response.body.notf;
        CKEDITOR.instances['textrich'].setData(this.value);

        //if if for approve and is in folder approve
        switch(folder){
          case 1000:
            this.save = true;
            this.showSend = false;
            this.discarded = true;
            this.forApproved = true;
          break;

          case 1001:
            this.save = true;
            this.showSend = false;
            this.discarded = false;
            this.forApproved = false;
          break;

          case 1003:
            this.save = true;
            this.showSend = false;
            this.discarded = true;
            this.forApproved = false;
          break;

          case 1004:
            this.save = true;
            this.showSend = true;
            this.discarded = true;
            this.forApproved = false;
          break;

          case 1005:
          case 1006:
          case 1007:
            this.save = false;
            this.showSend = false;
            this.discarded = false;
            this.forApproved = false;
          break;
        }

      }, response => {
        // error callback
        console.error('Se genero un error al procesar la url:', urlgetDraftByID);
      });

    },

    getHistory: function(id){
      var newVal = '';
      this.hist.forEach(function(element) {
        if (element.id === id) {
          newVal = element.content;
        }
      });
      CKEDITOR.instances['textrich'].setData(newVal);
    },

    addHistory: function() {
      const incom = this.value;
      const d = Date.now();
      const da = new Date(d).toLocaleString().slice(0, 9);
      var tempObject = {};
      tempObject.id = d;
      tempObject.user = "Usuario temporal";
      tempObject.date = da;
      tempObject.content = incom;
      if(incom.length > 10){
       this.hist.unshift(tempObject);
      }
    },
  }
});

CKEDITOR.replace('textrich', ckConfig);
CKEDITOR.instances['textrich'].on('change', function() {
  vm.value = this.getData()
});
