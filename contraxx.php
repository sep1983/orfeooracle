<?php
session_start();

$ruta_raiz = ".";

$krdOld      = $krd;
$carpetaOld  = $carpeta;
$tipoCarpOld = $tipo_carp;

if(!$krd){
  $krd=$krdOsld;
}

if(!$_SESSION['dependencia']) include "$ruta_raiz/rec_session.php";

$verrad = "";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$numeroa=0;$numero=0;$numeros=0;$numerot=0;$numerop=0;$numeroh=0;

$isql = "select a.*,
           b.depe_nomb
         from
           usuario a,
           dependencia b
         where
           USUA_LOGIN ='$krd' and a.depe_codi=b.depe_codi";

$rs=$db->query($isql);

$contraxx=$rs->fields["USUA_PASW"];

if(trim($rs->fields["USUA_LOGIN"])==trim($krd)){
   $dependencia=$rs->fields["DEPE_CODI"];
   $dependencianomb=$rs->fields["DEPE_NOMB"];
   $codusuario =$rs->fields["USUA_CODI"];
   $contraxx=$rs->fields["USUA_PASW"];
   $nivel=$rs->fields["CODI_NIVEL"];
   $iusuario = " and us_usuario='$krd'";
   $perrad = $rs->fields["PERM_RADI"];
}

?>
<html>
<head>
<script language="JavaScript" type="text/JavaScript">
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<args.length-2; i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requerido.\n'; }
  } if (errors) alert('Asegurese de entrar el password Correcto, \N No puede ser Vacio:\n');
  document.MM_returnValue = (errors == '');
}
function validar(){
    if(document.getElementById('contradrd').value!=document.getElementById('contraver').value){
        alert('!Las contrase\xF1as no coinciden. Verifiquelas!');
        return false;
    }
    return true;
}
</script>
<title>Cambio de Contrase&ntilde;as</title>
<link rel="stylesheet" href="./estilos/orfeo.css">
<link rel="stylesheet" href="./estilos/bulma.css">
<link rel="stylesheet" href="./estilos/elusive-icons.css">
<link rel="stylesheet" href="./estilos/login.css">
</head><?php
?>
  <body>
    <div class="container">
      <div class="centercontent">
        <div id="titleLogin">
          Cambio de la contrase&ntilde;a
        </div>
      <?php
      if(trim($rs->fields["USUA_LOGIN"])==trim($krd)){
      ?>
        <div class="login-box">
          <br>
          <div class="login-form">
            <form action='usuarionuevo.php?<?=session_name()."=".session_id()?>&krd=<?=$krd?>' method=post onSubmit="MM_validateForm('contradrd','','R','contraver','','R');return document.MM_returnValue">
            <input type=hidden value=<?=$depsel?> name=depsel>
            <input type=hidden value=<?=$krd?>    name='usuarionew' ></td>

                <div class="field">
                  <p class="control">
                    Usuario <?=$krd?>
                  </p>
                </div>

                <div class="field">
                  <p class="control">
                    <input type=password placeholder="Contrase&ntilde;a" name=contradrd id=contradrd vale='' class="input">
                  </p>
                </div>

                <div class="field">
                  <p class="control">
                    <input type='password' placeholder="Reescriba la contrase&ntilde;a" name='contraver' id='contraver' vale='' class="input">
                  </p>
                </div>

                <div class="field is-grouped">
                  <p class="control">
                    <input
                    type=submit
                    value='Aceptar'
                    onclick="return validar();"
                    class='button is-white'>
                  </p>
                </div>
          </form>
      <?
      } else {
          echo "<b>No esta Autorizado para entrar </b>";
      }
?>
      </div>
    </div>
  </body>
</html>
