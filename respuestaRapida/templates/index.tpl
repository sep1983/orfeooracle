<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Respuesta Rapida</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../estilos/orfeo.css"           type="text/css"  rel="stylesheet" />
        <link href="../estilos/jquery.treeview.css" type="text/css"  rel="stylesheet" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src='../js/jquery.form.js'            type="text/javascript" language="javascript"></script>
        <script src='../js/jquery.MetaData.js'        type="text/javascript" language="javascript"></script>
        <script src='../js/jquery.MultiFile.pack.js'  type="text/javascript" language="javascript"></script>
        <script src='//malsup.github.io/jquery.blockUI.js' type="text/javascript" language="javascript"></script>
        <script src='../js/jquery.treeview.js'        type="text/javascript" language="javascript"></script>
        <script src='../ckeditor/ckeditor.js'  type="text/javascript" language="javascript"></script>
        <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
        <script language="javascript">

            $(document).ready(function() {
                CKEDITOR.replace( 'texrich', {language: 'es', height: 517} );
                $('#delPlant').on('click', function() {
                    var lengthSelect = $("[name^='planaborrar']:checkbox:checked").map(function() {
                        return $(this).val();
                    }).length;

                    if (lengthSelect > 0) {
                        var input = $("<input>").attr("type", "hidden").attr("name", "delPlanVal").val("True");
                        $('#form1').append($(input));
                        $('#form1').submit();
                    }
                });

                $('#button').on('click', function(event) {
                    if (valFo()) {
                        $(this).prop('disabled', true);
                        return;
                    }
                    event.preventDefault();
                });

                $('span[ref]').on('click', function() {
                    var idString = $(this).attr('ref');
                    var textnew = $("#" + idString).html();
                    CKEDITOR.instances.texrich.setData(textnew);
                });

                $('#T7').MultiFile({
                    STRING: {
                        remove: '<img src="./js/bin.gif" height="16" width="16" alt="x"/>'
                    },
                    list: '#T7-list'
                });

                $("#browser").treeview();

                $('#plantillas').click(function(e) {

                    var seg1 = true;
                    var texcont = CKEDITOR.instances['texrich'].getData();
                    if ($('#nivel').val() === '') {
                        alert('Selecciona una carpeta');
                        seg1 = false;
                    }
                    ;

                    if ($('#nombre').val() === '') {
                        alert('Escribe un nombre');
                        seg1 = false;
                    }
                    ;

                    if ($('#nuevaplan').val() === '') {
                        $('#nuevaplan').val(1);
                    };

                    if (!seg1) {
                        e.preventDefault();
                        e.stopPropagation();
                    } else {
                        $('<input />').attr('type', 'hidden')
                                .attr('name', 'contplant')
                                .attr('value', texcont)
                                .appendTo('#form2');

                        document.form1.submit();
                    }
                });

                $('#form3').submit(function(e) {
                    var seg2 = false;
                    $("input[name='planaborrar[]']:checked").each(function() {
                        seg2 = true;
                    });
                    if (!seg2) {
                        alert('Selecciona una plantilla');
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    ;
                });


                var tmrReady = setInterval(isPageFullyLoaded, 100);

                function isPageFullyLoaded() {
                  if (document.readyState == "loaded" || document.readyState == "complete") {
                     subclassForms();
                     clearInterval(tmrReady);
                  }
                }

                function submitDisabled(_form, currSubmit) {
                  return function () {
                    var mustSubmit = true;
                    if (currSubmit != null)
                      mustSubmit = currSubmit();

                    var els = _form.elements;
                    for (var i = 0; i < els.length; i++) {
                      if (els[i].type == "submit")
                        if (mustSubmit)
                          [i].disabled = true;
                    }
                    return mustSubmit;
                  }

                }
                
                function subclassForms() {
                  for (var f = 0; f < document.forms.length; f++) {
                    var frm = document.forms[f];
                    frm.onsubmit = submitDisabled(frm, frm.onsubmit);
                  }
                }

            });

            function valFo(el) {
                var result = true;
                var destin = $('#destinatario').val();
                var salida = destin.split(";");

                if (destin == "") {
                    alert('El campo destinatario es requerido');
                    $('#destinatario').focus();
                    result = false;
                } else {
                    for (i = 0; i < salida.length; i++) {
                        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(salida[i])) {
                            result = true;
                        } else {
                            alert('El destinatario es incorrecto:  ' + salida[i]);
                            el.destinatario.focus();
                            result = false;
                            break;
                        }
                    }
                }

                return result;
            }
        </script>

        <style type="text/css">

            HTML, BODY{
                font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
                margin: 0px;
                height: 100%;
            }

            #load{
                position:absolute;
                z-index:1;
                border:3px double #999;
                background:#f7f7f7;
                width:300px;
                height:300px;
                margin-top:-150px;
                margin-left:-150px;
                top:50%;
                left:50%;
                text-align:center;
                line-height:300px;
                font-family: verdana, arial,tahoma;
                font-size: 14pt;
            }

            img {
                border: 0 none;
            }

            .MultiFile-label{
                float: left;
                margin: 3px 15px 3px 3px;
            }

            .linkCargar{
                background: url(../estilos/images/flechaAzul.gif) no-repeat;
                cursor: pointer;
                padding-bottom: 17px;
                padding-left: 17px;
            }
        </style>

    </head>
    <body>
      <div class="columns">
        <div class="column">
        <!--{foreach key=idCarpeta item=carpeta from=$carpetas}-->
        <!--{foreach key=id item=archivo from=$carpeta}-->
        <span id='<!--{$archivo.id}-->' style="display:none;">
            <!--{$archivo.ruta}-->
        </span>
        <!--{/foreach}-->
        <!--{/foreach}-->

        <div id="load" style="display:none;">Enviando.....</div>
        <form id="form1" name="form1" method="post" enctype="multipart/form-data" action='../respuestaRapida/procRespuesta.php?<!--{$session}-->' ">
            <input type=hidden name="usuanomb"  value='<!--{$usuanomb}-->' />
            <input type=hidden name="usualog"   value='<!--{$usualog}-->' />
            <input type=hidden name="radPadre"  value='<!--{$radPadre}-->' />
            <input type=hidden name="usuacodi"  value='<!--{$usuacodi}-->' />
            <input type=hidden name="depecodi"  value='<!--{$depecodi}-->' />
            <input type=hidden name="codigoCiu" value='<!--{$codigoCiu}-->' />
            <input type=hidden name="rutaPadre" value='<!--{$rutaPadre}-->' />

            <table class='table'>
                <tr align="center" class="titulos2">
                    <td height="15" colspan="4" class="titulos4">
                       Correo Electronico
                    </td>
                </tr>
                <!--{if $perm_respuesta == '2'}-->
                <tr>
                    <td colspan="4">
                        <table>
                            <tr>
                                <td colspan="4">
                                    <span>Para enviar a multiples correos Separe con ";"&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        Para escribir una nueva linea utilice las teclas [shift] + [Enter]&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        Para escribir un nuevo parrafo utilice la tecla [Enter]</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="20px">Remite:</td>
                                <td width="50%">
                                  <div class="select">
                                    <select name="usMailSelect" id="usMailSelect">
                                      <!--{foreach key=key item=item from=$emails}-->
                                        <!--{if $usMailSelect == $item }-->
                                        <option value=<!--{$item}-->><!--{$item}--></option>
                                        <!--{else}-->
                                        <option value=<!--{$item}-->><!--{$item}--></option>
                                        <!--{/if}-->
                                      <!--{/foreach}-->
                                    </select>
                                  </div>
                                </td>
                                <td width="20px">Destinatario:</td>
                                <td width="50%">
                                    <input class="input is-small" type=text id="destinatario" name="destinatario" value='<!--{$destinatario}-->' maxlength="120">
                                </td>
                            </tr>
                            <tr>
                                <td width="20px">CC:</td>
                                <td>
                                    <input class="input is-small" type=text name="concopia" value='' maxlength="120">
                                </td>
                                <td width="20px">CCO</td>
                                <td>
                                    <input class="input is-small" type=text name="concopiaOculta" value='' maxlength="120">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!--{/if}-->
                <tr>
                    <td colspan="4">
                        <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tr>
                                <input type="hidden" value="2" name="medioRadicar"></input>
                                <td class="titulos">Adjuntar</td>
                                <td colspan=2>
                                    <input class="select_resp" name="archs[]" type="file" id="T7" accept="<!--{$extn}-->"/>
                                    <div id="T7-list" class="select_resp" ></div>
                                </td>
                                <td>
                                  <input type="submit" id="button" name="Button" value="ENVIAR" class="button is-info is-small">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <textarea id="texrich" name="respuesta" value=''><!--{$asunto}--></textarea>
                    </td>
                </tr>
                <tr align="center" class="titulos2">
                    <td height="15" colspan="4" class="titulos4">
                      Administraci&oacute;n de plantillas
                    </td>
                </tr>
                <tr>
                  <td  style="vertical-align: top;">
                    <div class='columns'>
                      <div class='column'>
                        <ul id="browser" class="filetree" style="font-size: 10px;">
                            <label class="label is-small">De click en la plantilla que desea cargar:</label>
                            <div id="form3" name="form3">
                                <!--{foreach key=idCarpeta item=carpeta from=$carpetas}-->
                                <li><span class="folder"><!--{$idCarpeta}--></span>
                                    <ul>
                                        <!--{foreach key=id item=archivo from=$carpeta}-->
                                        <li>
                                            <span class="file">
                                                <!--{if $archivo.show == true}-->
                                                <input type="checkbox" name="planaborrar[]" value="<!--{$archivo.id}-->">
                                                <!--{/if}-->
                                                <span ref="<!--{$archivo.id}-->"><a href='#'><!--{$archivo.nombre}--></a></span>
                                            </span>
                                        </li>
                                        <!--{/foreach}-->
                                    </ul>
                                </li>
                                <!--{/foreach}-->
                                <input type="submit" id="delPlant" name="delPlant" value="Borrar" class="button is-info is-small">
                            </div>
                        </ul>
                      </div>
                      <div id="form2" name="form2" class='column'>
                        <div class="field">
                          <label class="label is-small">Guardar plantilla</label>
                            <div class="control">
                              <input placeholder='Nombre plantilla' class='input is-small' type="text" name="nombre" id="nombre"/><br/>
                            </div>
                        </div>
                        <div class='select'>
                          <select name="nivel" id="nivel">
                              <option value="">Selecciona una Carpeta</option>
                              <!--{section name=nr loop=$perm_carps}-->
                              <option value="<!--{$perm_carps[nr].codigo}-->"><!--{$perm_carps[nr].nombre}--></option>
                              <!--{/section}-->
                          </select>
                        </div>
                        <input type="hidden" id="nuevaplan" name="nuevaplan" value="">
                        <input type="button" id="plantillas" name="plantillas" value="Enviar" class="button is-info is-small">
                      </div>
                    </div>
                  </td>
                </tr>
            </table>
          </form>
        </div>
        <div class="column  ">
          <!--{if !empty($rutaPadre)}-->
          <embed src="<!--{$rutaPadre}-->" type="application/pdf" width="100%" height="100%" />
          <!--{else}-->
          <p class="title is-3"><span class='has-text-danger'>Imagen del radicado no encontrada :(</span></p>
          <p class="subtitle is-5"><!--{$radPadre}--></p>
          <img src="http://apt-web.es/wp-content/uploads/2015/01/PDF_convert.png" alt="HTML5 Doctor Logo" />
          <!--{/if}-->
        </div>
    </body>
</html>
