<?php
$ruta_raiz = "..";
//ini_set('display_errors','On');
//error_reporting(-1);
/**
* Inclusion de archivos para utilizar la libreria ADODB
*
*/
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
if (!isset($db))
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
//$db->conn->debug=true;
require_once("$ruta_raiz/class_control/Dependencia.php");
$objDep = new Dependencia($db);
$msgTitle = "Seleccione el(los) informado(s):";
$arbol = $objDep->getTreeDep('c');
//var_dump($arbol);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Respuesta R&aacute;pida</title>
        <meta   http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link   href="../estilos/orfeo.css" type="text/css"  rel="stylesheet" />
        <script src="../auxLib/PHPMailer/docs/phpdoc/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="../auxLib/PHPMailer/docs/phpdoc/js/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
	<script src="../auxLib/PHPMailer/docs/phpdoc/js/jquery.cookie.js" type="text/javascript"></script>
	<link href="./js/skin/ui.dynatree.css" rel="stylesheet" type="text/css" id="skinSheet">
	<script src="./js/jquery.dynatree.js" type="text/javascript"></script>


        <script lan$("#tree").dynatree("getSelectedNodes")g="javscript">
        function pasarValor(){
		var sn = $("#tree").dynatree("getSelectedNodes"); 
		if(jQuery.isEmptyObject(sn)) {
			opener.document.form1.txtHiddenCC='';
			opener.document.form1.copias='';
			alert('No hubo selecci\xf3n.');
		} else {
			opener.document.form1.txtHiddenCC.value = '' ;
			opener.document.form1.copias.value = '';
			$.each(sn, function(indice, elemento) {
				opener.document.form1.txtHiddenCC.value += elemento.data.key+';' ;
				var re = /D[0-9]{3}/;
				if (re.test(elemento)) {
					//alert(elemento);
				} else {
					opener.document.form1.copias.value += elemento.data.title+'; ';
				}
   			 });
		}
        	window.close();
        }
	
	$(function(){
		$("#tree").dynatree({
			// using default options
			checkbox: true,
      			selectMode: 3,
			strings: {
			        loading: "Loading…",
		        	loadError: "Load error!"
   			},
			onPostInit: function(isReloading, isError) {
				// 'this' is the current tree
			        // isReloading is true, if status was read from existing cookies
			        // isError is only used in Ajax mode
			        // Fire an onActivate() event for the currently active node
				reactivatex();
		        },
			children : <?php echo $arbol;?>
		});
	});

	function reactivatex(){
		var usrSel = "";
		usrSel = opener.document.form1.txtHiddenCC.value;
		$.each(usrSel.split(";"), function(indice, elemento){
			$("#tree").dynatree("getTree").selectKey(elemento);
		});
	}
        </script>
    </head>
    <body>
        <?php echo $msgTitle ?><br/>
        <div id="tree"></div>
        <center><input value='cerrar' onclick="pasarValor()" type='button' class='botones' ></center>
    </body>
</html>
