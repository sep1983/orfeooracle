<?php

/**
* @author <aurigadl@gmail.com>
* @license  GNU AFFERO GENERAL PUBLIC LICENSE
* @copyright

OrfeoGPL Models are the data definition of OrfeoGPL Information System
Copyright (C) 2013 Infometrika Ltda.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



* Parametros de configuracón de la base de datos oracle
* Se debe tener configurado el conector oci8 para
* los sistemas operativos linux
*
La librería adodb tiene varios métodos de conexión

  Multiple modes of connection are supported:

 a. Local Database
   $conn->Connect(false,'scott','tiger');

 b. From tnsnames.ora
   $conn->Connect(false,'scott','tiger',$tnsname);
   $conn->Connect($tnsname,'scott','tiger');

 c. Server + service name
   $conn->Connect($serveraddress,'scott,'tiger',$service_name);

 d. Server + SID
   $conn->connectSID = true;
   $conn->Connect($serveraddress,'scott,'tiger',$SID);

   Se debe tener en cuenta la restricción de seguridad de SELinux
   para algunos sistemas operativos Linux.

   Si ya tiene configurado el oci8 puede ejecutar una de las pruebas
   para validar la conexión.
*/

$usuario= "xxx";
$clave  = "xxx";
$ip     = "xxx";
$puerto = 1521;
$sid    = "xxx";
$uri    = $ip.":".$puerto;
$uris   = $ip.":".$puerto."/".$sid;

// Conexion utilizando la configuracion del tnsnames.ora
$MYDB = "(DESCRIPTION =
          (ADDRESS = (PROTOCOL = TCP)(HOST = $ip)(PORT = $puerto)
          (CONNECT_DATA =
            (SERVER=DEDICATED)
            (SERVICE_NAME = $sid)
          )
        )";

$conn = ocilogon($usuario, $clave, $uris);
#$conn = ocilogon($usuario, $clave, $uri);
#$conn = ocilogon($usuario, $clave, $MYDB);
#$conn = ocilogon($usuario, $clave, $uris, "AL32UTF8", 0);

if (!$conn) {
  $m = oci_error();
  echo $m['message'], "\n";
  exit;
} else {
  print "Conectado a Oracle! conn <br>";
  $stid = oci_parse($conn, 'select table_name from user_tables  where rownum <=  5');
  oci_execute($stid);

  echo "<table>\n";
  while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
    echo "<tr>\n";
    foreach ($row as $item) {
      echo "  <td>".($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;")."</td>\n";

    }
    echo "</tr>\n";

  }
  echo "</table>\n";
}


#Configuracion del oci8
#Muestra todos lo modulos existentes
#Debe encontrae el oci8 en el listado
phpinfo(INFO_MODULES);
?>
