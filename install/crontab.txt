#ELIMINA ACHIVOS TEMPORALES 'magic' GENERADOS POR ORFEO
*/2 * * * *     /bin/find /tmp -mmin +1 -user apache -exec rm {} \;

#ORFEO ANULACION DE RADICADOS VENCIDOS
00 6 * * * php -f /var/www/html/orfeo/anularVencidos.php > /var/log/VencidosOrfeo.log
47 16 * * * php -f /var/www/html/orfeo/tmp/anularVencidos.php > /var/log/VencidosOrfeotmp.logi

#ORFEO ELIMINA ARCHIVOS TEMPORALES EN BODEGA
*/5 0 * * *     /bin/rm -f /mnt/bodega2/tmp/*

#ORFEO ELIMINA ARCHIVOS RESIDUO DE MASIVA
*/10 * * * *    /bin/find /mnt/bodega2/masiva/ -mmin +3 -user apache -exec rm {} \;

#RECUDE TAMAÑO DE ARCHIVO LOG
#*/10 * * * *    /bin/echo > /var/log/httpd/ssl_error_log


#Reinicia servidor 00:01
1 0 * * *       /sbin/reboot

#Reinicia servidor 00:30
30 0 * * *      /sbin/reboot
