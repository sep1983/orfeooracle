<?php
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	             */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS         */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com                   */
/* ================================================================================= */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			                     */
/*                                                                                   */
/* Colocar desde esta linea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/
session_start();
$ruta_raiz = "../..";
if (!isset($_SESSION['dependencia']))   include "../../rec_session.php";
?>
<html>
<head>
<title>Asignacion de tramites Orfeo...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../estilos/orfeo.css">
</head>
<body bgcolor="#FFFFFF" topmargin="0">
<div id="spiffycalendar" class="text"></div>
<link rel="stylesheet" type="text/css" href="../../js/spiffyCal/spiffyCal_v2_1.css">
<?
 $ruta_raiz = "../..";
 include_once "$ruta_raiz/include/db/ConnectionHandler.php";
 $db = new ConnectionHandler("$ruta_raiz");

 if (!$dep_sel) $dep_sel = $_SESSION['dependencia'];

 if ($orden_cambio==1)  {
 	if (!$orderTipo)  {
	   $orderTipo="desc";
	}else  {
	   $orderTipo="";
	}
 }
 $encabezado = "".session_name()."=".session_id()."&krd=$krd&dependencia=$dependencia&dep_sel=$dep_sel&selecdoc=$selecdoc&valRadio=$valRadio&orderTipo=$orderTipo&orderNo=";
 $linkPagina = "$PHP_SELF?$encabezado&radSelec=$radSelec&accion_sal=$accion_sal&nomcarpeta=$nomcarpeta&orderTipo=$orderTipo&orderNo=$orderNo";
error_reporting(0);

if ($guardar AND $codTramite AND $codNov ) {
		$sqlCons = "SELECT *
						FROM SGD_FUNOV_FUNCNOVEDAD
						WHERE FUNOV_CODTRAMITE = $codTramite
						AND FUNOV_CODNOVEDAD = $codNov
						AND FUNOV_DEPE_CODI = $dep_sel
						AND FUNOV_ESTADO = 1
						";
		$rsCons = $db->query($sqlCons);
		if($rsCons->EOF) {
    		$sqlIns = "INSERT INTO SGD_FUNOV_FUNCNOVEDAD (FUNOV_CODTRAMITE,FUNOV_CODNOVEDAD,FUNOV_USUA_DOC,FUNOV_DEPE_CODI,FUNOV_ESTADO,FUNOV_FEC_INI)
				  VALUES($codTramite,$codNov,'$valRadio',$dep_sel,1,sysdate)";
  			$rsIns = $db->query($sqlIns);
		}else{
		    $codFuncionario = $rsCons->fields["FUNOV_USUA_DOC"];
			if($codFuncionario != $valRadio) {
	    		$sqlIns = "UPDATE SGD_FUNOV_FUNCNOVEDAD SET FUNOV_FEC_FIN=sysdate, FUNOV_ESTADO = 0
					  WHERE FUNOV_CODTRAMITE = $codTramite
						AND FUNOV_CODNOVEDAD = $codNov
						AND FUNOV_DEPE_CODI = $dep_sel
						AND FUNOV_ESTADO = 1
						AND FUNOV_USUA_DOC = $codFuncionario";
	  			$rsIns = $db->query($sqlIns);
	    		$sqlIns = "INSERT INTO SGD_FUNOV_FUNCNOVEDAD (FUNOV_CODTRAMITE,FUNOV_CODNOVEDAD,FUNOV_USUA_DOC,FUNOV_DEPE_CODI,FUNOV_ESTADO,FUNOV_FEC_INI)
				  VALUES($codTramite,$codNov,'$valRadio',$dep_sel,1,sysdate)";
  				$rsIns = $db->query($sqlIns);
			}
		}
	}

?>
  <form name='forma' action='cuerpoEdicion.php?<?=$encabezado?>' method='post'>
 <?
    if ($orderNo==98 or $orderNo==99) {
       $order=1;
	   if ($orderNo==98)   $orderTipo="desc";

       if ($orderNo==99)   $orderTipo="";
	}
    else  {
	   if (!$orderNo)  {
  		  $orderNo=0;
	   }
	   $order = $orderNo + 1;
    }
?>
 <table class=borde_tab width='100%' cellspacing="5"><tr><td class=titulos2><center>ASIGNACION DE TRAMITES <?=$varTramRad?></center></td></tr></table>
	<center>
	<TABLE width="682" class="borde_tab" cellspacing="2">

  <TR> 	<td width="22%" class="titulos5" height="20" align="right">CLASE DE TRAMITE</td>
      <td width="78%" colspan="1"  class="listado5">
  <?php
    if(!$codTramite) $codTramite = 0;

	$query1 = "select sgd_mtra_descripcion, sgd_mtra_codigo
	         from sgd_mtra_tramite, SGD_TRADEP_TRAMITEDEP td
			 where td.SGD_TRADEP_DEPECODI = $dependencia
			 	AND td.SGD_TRADEP_TRACODI = sgd_mtra_codigo
			 order by sgd_mtra_descripcion";

	$rs1=$db->conn->query($query1);
	print $rs1->GetMenu2("codTramite", $codTramite, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
?>  </td> </tr>
   <TR> 	<td width="22%" class="titulos5" height="20" align="right">NOVEDAD</td>
        <td colspan="1"  class="listado5">
<?
   	    $query2 = "select distinct(NOV.sgd_mnovt_descripcion) as descripcion, NOV.sgd_mnovt_codigo
	         from sgd_mnovt_novedadtram NOV, SGD_RELT_RELACIONTRAM REL
			 where REL.SGD_RELT_CODTRAMITE = '$codTramite'
			 	AND REL.SGD_RELT_CODNOVEDAD = nov.SGD_mNOVT_CODIGO
 			 order by descripcion";
	    $rs2=$db->conn->query($query2);
	    print $rs2->GetMenu2("codNov", $codNov, "0:-- Seleccione --", false,"","onChange='submit()' class='select'" );
?>   </td>
</tr>

  </table>
  </center>

<?php
if($codTramite and $codNov) {
	  $sqlCons = "SELECT USUA_NOMB
						FROM USUARIO WHERE USUA_DOC IN (SELECT FUNOV_USUA_DOC FROM SGD_FUNOV_FUNCNOVEDAD
						WHERE FUNOV_CODTRAMITE = $codTramite
						AND FUNOV_CODNOVEDAD = $codNov
						AND FUNOV_DEPE_CODI = $dep_sel
						AND FUNOV_ESTADO=1)";
		$usuaNomb = $db->conn->GetOne($sqlCons);
		if($usuaNomb != '') {
?>	<center>
	<TABLE width="682" class="borde_tab" cellspacing="2">
	  <TR> 	<td width="22%" class="titulos5" height="20" align="right">USUARIO ASIGNADO</td>
      <td width="78%" colspan="1"  class="select"> <? echo "$usuaNomb"; ?> </td>
    </table>
  </center>
<?		}

	$isql = "select u.usua_nomb AS NOMBRE,
					u.usua_login AS USUARIO,
					d.depe_nomb AS DEPENDENCIA,
					USUA_DOC AS CHR_USUA_DOC
				from usuario u, dependencia d
				where u.depe_codi = $dep_sel AND u.depe_codi = d.depe_codi AND u.usua_esta=1 " .
				" order by " . $order . " " . $orderTipo;

  $rs=$db->conn->Execute($isql);
	$nregis = $rs->fields["USUA_NOMB"];
	if ($nregis)  {
		echo "<hr><center><b>NO se encontro nada con el criterio de busqueda</center></b></hr>";}
	else  {
		$pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo);
		$pager->toRefLinks = $linkPagina;
		$pager->toRefVars = $encabezado;
		$pager->Render($rows_per_page=50,$linkPagina,$checkbox=chkEnviar);
	}
	if ($valRadio)
	{	$usuSelec = $valRadio;
		$usuario_mat = split("-",$usuSelec,2);
		$usuDocSel = $usuario_mat[0];
		$usuLoginSel = $usuario_mat[1];
  }
?>
	<table border=1 width=100% class=t_bordeGris>
	  <tr><td height="26" colspan="4" valign="top" class='titulos2'>
		  <center>
          <input type="submit" name='guardar' value='Guardar' class='botones_funcion'>
        <input name="cancelar" type="button"  class="botones_funcion"  onClick="window.close();" value="Cerrar">
	   </td>
	  </tr>
	</table>
	<?
}  ?>
</form>
</body>
</html>
