<?php
if($_SESSION['dependencia'] == null ){
  include "$ruta_raiz/cerrar_session.php";
}

$nomcarpeta = '';

$krd = $_SESSION["krd"];
if(!empty($_GET['nomcarpeta'])){
  $nomcarpeta = $_GET['nomcarpeta'];
};
$carpetaOld  = $carpeta;
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
$db = new ConnectionHandler($ruta_raiz);
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);


if (!isset($_GET['carpeta'])){
  $carpeta = "0";
  $nomcarpeta = "Entrada";
}
?>
<table class='table is-narrow is-fullwidth'>
  <thead>
    <tr>
      <th>LISTADO DE:</th>
      <th>USUARIO</th>
      <th>DEPENDENCIA</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?=$nomcarpeta?></td>
      <td><?=$_SESSION['usua_nomb']?></td>
      <td><?=$_SESSION['depe_nomb']?></td>
    </tr>
  </tbody>
</table>

<? if ($swBusqDep){
  include_once "$ruta_raiz/include/query/envios/queryPaencabeza.php";
  $sqlConcat = $db->conn->Concat("dep_sigla", "'-'",$db->conn->substr."(depe_nomb,1,30) ");
  if($_SESSION["usua_perm_impresion"]==2){
    $DepFilt=" and depe_codi in (select depe_codi from impusrrel where usua_doc='".$_SESSION["usua_doc"]."') ";
    $DepFiltPager=" and dep.depe_codi in (select depe_codi from impusrrel where usua_doc='".$_SESSION["usua_doc"]."') ";
  }
  $sql = "select $sqlConcat ,depe_codi from dependencia where depe_estado=1 $DepFilt
    order by 1";
  $rsDep = $db->conn->Execute($sql);
  if(!$depeBuscada) $depeBuscada=$dependencia;
  $result = $rsDep->GetMenu2("dep_sel","$dep_sel","9999:TODAS LAS DEPENDENCIAS", false, 0," onChange='submit();'");
?>

  <form name=formboton action='<?=$pagina_actual?>?<?=session_name()."=".session_id()."&krd=$krd" ?>&estado_sal=<?=$estado_sal?>&estado_sal_max=<?=$estado_sal_max?>&pagina_sig=<?=$pagina_sig?>&dep_sel=<?=$dep_sel?>&nomcarpeta=<?=$nomcarpeta?>' method=post>
    <div class="field is-horizontal  has-addons has-addons-right">
      <div class="field-label is-small">
        <label class="label">Dependencia</label>
      </div>
      <div class="field-body">
        <div class="field is-narrow">
          <div class="control">
            <div class="select is-fullwidth">
              <?=$result?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

<?}?>
