<?php
/**
* @module index_frame
*
* @author Jairo Losada   <jlosada@gmail.com>
* @author Cesar Gonzalez <aurigadl@gmail.com>
* @license  GNU AFFERO GENERAL PUBLIC LICENSE
* @copyright

SIIM2 Models are the data definition of SIIM2 Information System
Copyright (C) 2013 Infometrika Ltda.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

  $drd = false;
  $krd = false;
  if (isset($_POST["krd"])){
      $krd = $_POST["krd"];
  }
  if (isset($_POST["drd"])){
      $drd = $_POST["drd"];
  }

  if (isset($_POST["autenticaPorLDAP"]))
      $autenticaPorLDAP = $_POST["autenticaPorLDAP"];

  $fechah        = date("dmy")."_".time("hms");
  $ruta_raiz     = ".";
  $usua_nuevo    = 3;
  $ValidacionKrd  = "";

  if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $agent = $_SERVER['HTTP_USER_AGENT'];
  }

  include ("config.php");
  $serv = str_replace(".", ".", $_SERVER['REMOTE_ADDR']);

  if ($krd) {
      include "$ruta_raiz/session_orfeo.php";

      if ($usua_nuevo == 0 &&  !$autenticaPorLDAP) {
          include ($ruta_raiz."/contraxx.php");
          $ValidacionKrd = "NOOOO";
          if ($j = 1)
              die("<center> -- </center>");
      }
  }

  $krd = strtoupper($krd);
  $datosEnvio = "&fechah=$fechah&swLog=1&orno=1";

  if ($ValidacionKrd == "Si") {
      header ("Location: $ruta_raiz/index_frames.php?$datosEnvio");
      exit();
  }

  if(file_exists("./estilos/$entidad.login.css")){
    $estilos = $entidad.".login.css";
  }else{
    $estilos = "./estilos/login.css";
  }

  if(file_exists("./img/$entidad.favicon.png")){
    $favicon = "./img/$entidad.favicon.png";
  }else{
    $favicon = "./img/favicon.png";
  }


  if(file_exists("./img/$entidad.login_background.jpeg")){
    $fondo = "./img/$entidad.login_background.jpeg";
  }else{
    $fondo = "./img/login_background.jpeg";
  }

	$err_response = array(
	    '400' => 'La petición realizada es inválida.',
	    '401' => 'Acceso a recurso no atuorizado.',
	    '403' => 'No tiene permisos para acceder a este recurso.',
	    '404' => 'La página solicitada no fué encontrada.',
	    '500' => 'Lo sentimos, ha ocurrido un error inesperado.'
	);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"><!--<![endif]-->
  <head>
    <title>..:: <?=$entidad?> Orfeo  ::..</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <meta name="description" content="Orfeo-SGD">
    <link rel="shortcut icon" href="<?=$favicon?>" onClick="this.reload();">
    <link rel="stylesheet" href="./estilos/bulma.css">
    <link rel="stylesheet" href="./estilos/elusive-icons.css">
    <link href="<?=$estilos?>" rel="stylesheet">
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="icon" href="./icon.ico" type="image/x-icon" />
    <?php
    if($environment == 'dev'){// dev or prod
      echo "<style type=\"text/css\"> .login-box { background-color: red !important; } </style>";
    }
    ?>


  </head>
    <body>
     <?php
     if (strlen(strstr($agent, 'Firefox')) > 0) {
     ?>
      <div class="container">
        <div class="centercontent">
          <div id="titleLogin">
            <?=$entidad?>
          </div>
          <div class="login-box">
            <br>
            <br>
            <br>
            <div class="login-form">

              <form action="./login.php" method="post">

                <div class="field">
                  <p class="control has-icons-left has-icons-right">
                    <input class="input" name="krd" placeholder="Usuario" required type="text">
                    <span class="icon is-small is-left">
                      <i class="el el-user"></i>
                    </span>
                    <span class="icon is-small is-right">
                      <i class="fa fa-check"></i>
                    </span>
                  </p>
                </div>

                <div class="field">
                  <p class="control has-icons-left">
                    <input class="input" name="drd" placeholder="Contrase&ntilde;a" required type="password">
                    <span class="icon is-small is-left">
                      <i class="el el-unlock"></i>
                    </span>
                  </p>

                  <?php if(!empty($message) or !empty($err_response[$_GET['code']])){ ?>
                  <p class="help">
                    <?=$message?>
                    <?=$err_response[$_GET['code']]?>
                  </p>
                  <?php }?>

                </div>

                <div class="field is-grouped">
                  <p class="control">
                    <button type="submit" class="button is-white">Entrar</button>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <?}else{?>
      <section class="hero is-medium is-info is-bold">
        <div class="hero-body">
          <div class="container">
            <h1 class="title">
              Navegador no compatible
            </h1>
            <h2 class="subtitle">
              Orfeo esta desarrollado solo para navegadores libres, usa Mozilla Firefox
            </h2>
          </div>
        </div>
      </section>
      <?}?>
</body>
</html>
